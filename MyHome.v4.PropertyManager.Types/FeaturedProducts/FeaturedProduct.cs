﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.FeaturedProducts
{
    /// <summary>
    /// The data object schema object for FeaturedSpots
    /// </summary>
    [DataContract]
    [Serializable]
    public class FeaturedProduct
    {
        [DataMember]
        public int FeaturedSpotId { get; set; }

        [DataMember]
        public int FeaturedSpotTypeId { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public int OrderId { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public int? PropertyId { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public int? LocalityId { get; set; }

        [DataMember]
        public int? PropertyClassId { get; set; }

        [DataMember]
        public int CreatedByUserId { get; set; }

        [DataMember]
        public DateTime CreatedOn { get; set; }

        [DataMember]
        public DateTime ModifiedOn { get; set; }

        [DataMember]
        public DateTime? EventStartsOn { get; set; }

        [DataMember]
        public DateTime? EventEndsOn { get; set; }

        [DataMember]
        public string CustomJson { get; set; }
    }
}
