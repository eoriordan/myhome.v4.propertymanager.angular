﻿using System;
using MyHome.v4.PropertyManager.Schema.Document;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Types.Document
{
    public class Document
    {
        public int DocumentId { get; set; }

        public int GroupId { get; set; }

        public User CreatedBy { get; set; }

        public int? PropertyId { get; set; }

        public int? ContactId { get; set; }

        public int? WorkflowInstanceId { get; set; }

        public string Name { get; set; }

        public string GoogleDocId { get; set; }

        public string GoogleDocsUsername { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsFile { get; set; }

        public int? FileSizeInBytes { get; set; }

        public Guid? Guid { get; set; }

        public string DocumentPreview { get; set; }

        public bool? UseCustomConverter { get; set; }

        public string Extra { get; set; }

        public DateTime ModifiedOn { get; set; }

        public DocumentTypeEnum DocumentType { get; set; }

        public DocumentContentType DocumentContentType { get; set; }

        public bool IsVirusChecked { get; set; }
    }
}
