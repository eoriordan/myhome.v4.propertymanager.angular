﻿namespace MyHome.v4.PropertyManager.Types
{
    public enum RoleEnum
    {
        ChannelAdministrator = 13,
        CrmAccountsAdmin = 9,
        CrmAccountsManager = 5,
        CrmAccountsUser = 8,
        CrmBranchAdmin = 4,
        CrmBranchManager = 3,
        CrmBranchUser = 6,
        CrmGroupAdmin = 12,
        CrmGroupManager = 11,
        CrmGroupUser = 10,
        DataExportAdmin = 14,
        MyHomeAccountManager = 2,
        MyHomeAdministrator = 1,
        MyHomeSubscriptionsEditor = 7
    }

    public enum RowStatus
    {
        Pending = 1,
        Normal = 2,
        Deleted = 3,
        Banned = 4,
        Live = 5,
        Completed = 6,
        Expired = 7
    }

    public enum OrderStatusEnum
    {
        Pending = 1,
        Completed = 2,
        Rejected = 3,
        Cancelled = 4
    }
}
