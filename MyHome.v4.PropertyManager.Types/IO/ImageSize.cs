﻿namespace MyHome.v4.PropertyManager.Types.IO
{
    public enum ImageSize
    {
        Original,
        Small, //36 x 36
        Thumbnail, //120 x 80
        HomepageThumbnail, //176x132
        TopSpot, //168 x 112
        Medium, //372 x 279
        Carousel, //318 x 200
        Gallery, //400 x 300
        Large, //668 x 501
        Galleria, //940 x 626
        OriginalWithSuffix
    }
}
