﻿namespace MyHome.v4.PropertyManager.Types.IO
{
    public enum AnchorPosition
    {
        Top,
        Bottom,
        Left,
        Right,
        Center
    }
}
