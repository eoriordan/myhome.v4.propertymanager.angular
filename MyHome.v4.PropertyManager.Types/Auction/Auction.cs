﻿using System;

namespace MyHome.v4.PropertyManager.Types.Auction
{
    public class Auction
    {
        public int AuctionId { get; set; }

        public int? GroupId { get; set; }

        public int ChannelId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Announcements { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int? RegionId { get; set; }

        public int? LocalityId { get; set; }

        public string Address { get; set; }

        public string VideoUrl { get; set; }

        public string DocumentsUrl { get; set; }

        public bool IsActive { get; set; }
    }
}
