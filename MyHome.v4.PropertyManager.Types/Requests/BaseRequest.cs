﻿namespace MyHome.v4.PropertyManager.Types.Requests
{
    public class BaseRequest
    {
        public BaseRequest()
        {
            IsErrorResponse = false;
        }

        public bool IsErrorResponse { get; set; }
        public string ErrorString { get; set; }
    }
}
