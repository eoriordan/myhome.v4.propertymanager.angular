﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Requests
{
    /// <summary>
    /// Get the list of groups stored by there unique ids
    /// </summary>
    [DataContract]
    public class GetGroups
    {
        /// <summary>
        /// The requested group ids
        /// </summary>
        [DataMember(Order = 1)]
        public List<int> GroupIds { get; set; }
    }      

    /// <summary>
    /// Get a single group
    /// </summary>
    [DataContract]
    public class GetGroup
    {
        /// <summary>
        /// The unique of the group to load
        /// </summary>
        [DataMember(Order = 1)]
        public int GroupId { get; set; }
    }
}
