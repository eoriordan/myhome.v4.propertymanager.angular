﻿using System;
using System.Collections.Generic;

namespace MyHome.v4.PropertyManager.Types.Requests
{
    public class CreateArticle : BaseRequest
    {
        public CreateArticle()
        {
            CategoryIds = new List<int>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string ListingTitle { get; set; }
        public string ImageUrl { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
        public string SeoUrl { get; set; }
        public bool IsPublished { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsGuestPost { get; set; }
        public bool CommentsEnabled { get; set; }
        public DateTime PublishedDate { get; set; }
        public List<int> CategoryIds { get; set; }
		public bool IsSponsored { get; set; }
		public string SponsorLink { get; set; }
		public string SponsorImage { get; set; }
	}
}
