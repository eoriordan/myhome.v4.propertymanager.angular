﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Types.Groups;
using MyHome.v4.PropertyManager.Types.Property;
using MyHome.v4.PropertyManager.Web.Helpers;
using SortDirection = MyHome.v4.PropertyManager.Schema.SortDirection;

namespace MyHome.v4.PropertyManager.Types.Requests
{
    /// <summary>
    /// Get the list of properties stored by there unique ids
    /// </summary>
    [DataContract]
    public class GetProperties
    {
        public GetProperties()
        {
            PropertyIds = new List<int>();
        }
        /// <summary>
        /// The list of ids for the properties to load
        /// </summary>
        [DataMember]
        public List<int> PropertyIds { get; set; }
    }


    /// <summary>
    /// Get a single property
    /// </summary>
    [DataContract]
    public class GetProperty
    {
        /// <summary>
        /// Ctor
        /// </summary>
        public GetProperty()
        {
            IncludeAnalyticsImpression = false;
        }

        /// <summary>
        /// The unique if of the property to load
        /// </summary>
        [DataMember]
        public int PropertyId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include analytics impression].
        /// </summary>
        /// <value>
        /// <c>true</c> if [include analytics impression]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IncludeAnalyticsImpression { get; set; }
    }


    public class BaseSearchProperties
    {
        public BaseSearchProperties()
        {
            SearchRequest = new GetPropertiesBySearch();
            SortColumn = SortColumn.RefreshedDate;
            SortDirection = SortDirection.Desc;
            Page = 1;
            PageSize = 20;
        }

        /// <summary>
        /// The search request to return properties
        /// </summary>
        [DataMember]
        public GetPropertiesBySearch SearchRequest { get; set; }

        [DataMember]
        public SortColumn SortColumn { get; set; }

        [DataMember]
        public SortDirection SortDirection { get; set; }

        [DataMember]
        public int PageSize{ get; set; }

        [DataMember]
        public int Page { get; set; }

        public void PopulateDefaults(int groupId, List<Group> childGroups, List<int> propertyTypeIds)
        {
            //remove an 0's from the lists
            SearchRequest.LocalityIds.RemoveAll(i => i == 0);
            SearchRequest.PropertyTypeIds.RemoveAll(i => i == 0);
            SearchRequest.GroupIds.RemoveAll(i => i == 0);
            SearchRequest.PropertyClassIds.RemoveAll(i => i == 0);
            SearchRequest.PropertyStatusIds.RemoveAll(i => i == 0);

            //add group ids from baseview model
            if (!SearchRequest.GroupIds.Any())
            {
                SearchRequest.GroupIds.Add(groupId);
                if (childGroups != null && childGroups.Any())
                {
                    SearchRequest.GroupIds.AddRange(childGroups.Select(x => x.GroupId));
                }
            }

            if (SearchRequest.PropertyTypeIds.Any())
            {
                SearchRequest.PropertyTypeIds = propertyTypeIds;
            }
            if (!SearchRequest.RegionId.HasValue || SearchRequest.RegionId == 0)
            {
                SearchRequest.RegionId = 2168;
            }
            if (SearchRequest.PropertyClassIds.Contains(11))
            {
                SearchRequest.PropertyClassIds = new List<int> { (int)PropertyClassEnum.ResidentialForSale };
                SearchRequest.SaleTypeIds.AddRange(new List<int> { 1, 15 });
            }
            if (SearchRequest.PropertyClassIds.Contains(12))
            {
                SearchRequest.PropertyClassIds = new List<int> { (int)PropertyClassEnum.Commercial };
                SearchRequest.PropertyStatusIds = new List<int> { (int)PropertyStatusEnum.ToLet, (int)PropertyStatusEnum.ForSaleOrToLet };
            }
            if (SearchRequest.PropertyClassIds.Contains(6))
            {
                SearchRequest.PropertyClassIds = new List<int> { (int)PropertyClassEnum.Commercial };
                SearchRequest.PropertyStatusIds = new List<int> { (int)PropertyStatusEnum.ForSale, (int)PropertyStatusEnum.ToLet, (int)PropertyStatusEnum.ForSaleOrToLet };
            }
        }
    }

    /// <summary>
    /// Searches for properties
    /// </summary>
    [DataContract]
    public class SearchPropertyIds : BaseSearchProperties
    {
    }

    /// <summary>
    /// Request to get the dictionary containing the localities and the coutn of properties matching search criteria there in.
    /// </summary>
    [DataContract]
    public class GetPropertyCountByLocalityRequest
    {
        public GetPropertyCountByLocalityRequest()
        {
            SearchRequest = new BaseSearchProperties();
        }

        /// <summary>
        /// The search request to return properties to be matched on
        /// </summary>
        [DataMember]
        public BaseSearchProperties SearchRequest { get; set; }
    }

    /// <summary>
    /// Response that gets count of properties fro each locality matching request
    /// </summary>
    [DataContract]
    public class GetPropertyCountByLocalityResponse
    {
        /// <summary>
        /// First member is the locality id the second is count of properties in that locality
        /// </summary>
        [DataMember]
        public Dictionary<int, int> PropertiesByLocality { get; set; }
    }


    /// <summary>
    /// Request to get the dictionary containing the localities and the count of properties matching groupId and property Classes.
    /// </summary>
    [DataContract]
    public class GetGroupLocalitiesWithProperty
    {
        public GetGroupLocalitiesWithProperty()
        {
            PropertyClassIds = new List<int>();
            PropertyTypeIds = new List<int>();
        }
        /// <summary>
        /// The search request to return properties to be matched on
        /// </summary>
        [DataMember]
        public int GroupId { get; set; }
        public List<int> PropertyClassIds { get; set; }
        public List<int> PropertyTypeIds { get; set; }
    }

    /// <summary>
    /// Response that gets list of localities the group has properties in 
    /// </summary>
    [DataContract]
    public class GetGroupLocalitiesWithPropertyResponse
    {
        /// <summary>
        /// First member is the locality id the second is locality Name
        /// </summary>
        [DataMember]
        public Dictionary<int, int> Localities { get; set; }
    }

}
