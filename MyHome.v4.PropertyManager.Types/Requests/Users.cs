﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Types.Requests
{
    /// <summary>
    /// Get a single User
    /// </summary>
    [DataContract]
    public class GetUserByUsername
    {
        /// <summary>
        /// The unique if of the User to load
        /// </summary>
        [DataMember(Order = 1)]
        public string Username { get; set; }
    }

    [DataContract]
    public class GetUsersByGroup
    {
        /// <summary>
        /// The unique if of the User to load
        /// </summary>
        [DataMember(Order = 1)]
        public int GroupId { get; set; }
    }
    

    [DataContract]
    public class GetUsersByGroupIds
    {
        /// <summary>
        /// The list of unique ids of the groups to get Users for
        /// </summary>
        [DataMember(Order = 1)]
        public List<int> GroupIds { get; set; }
    }

    /// <summary>
    /// The response object for the loading of a single User
    /// </summary>
    [DataContract]
    public class GetUserResponse
    {
        /// <summary>
        /// The requested locality
        /// </summary>
        [DataMember(Order = 1)]
        public User User { get; set; }
    }

    /// <summary>
    /// The response object for the editing of a user
    /// </summary>
    [DataContract]
    public class EditUserRequest
    {
        /// <summary>
        /// The requested locality
        /// </summary>
        [DataMember(Order = 1)]
        public User User { get; set; }
    }
}
