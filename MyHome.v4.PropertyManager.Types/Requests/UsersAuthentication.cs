﻿using System;
using System.Runtime.Serialization;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Types.Requests
{

    /// <summary>
    /// The base member request which ingerits from base request which supplies
    /// </summary>
    [DataContract]
    public class BaseMemberRequest
    {
        /// <summary>
        /// Gets or sets the ip address that was used to signup for the account
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        [DataMember]
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the user agent that was used to signup for the account
        /// </summary>
        /// <value>
        /// The user agent.
        /// </value>
        [DataMember]
        public string UserAgent { get; set; }
    }

    /// <summary>
    /// The request to authenticate a user in the system based on there email address and password
    /// </summary>
    [DataContract]
    public class AuthenticateUser : BaseMemberRequest
    {
        /// <summary>
        /// Gets or sets the email address to use in the authentication
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        [DataMember]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the password to use in the authentication
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [DataMember]
        public string Password { get; set; }

    }


    /// <summary>
    /// The request method to request a autheticated user session based on the session id
    /// </summary>
    [DataContract]
    public class AuthenticateUserSession : BaseMemberRequest
    {
        /// <summary>
        /// Gets or sets the session identifier.
        /// </summary>
        /// <value>
        /// The session identifier.
        /// </value>
        [DataMember]
        public Guid SessionId { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class EditUserPasswordResponse : BaseMemberRequest
    {
        [DataMember]
        public UserSession Session { get; set; }
    }

}
