﻿namespace MyHome.v4.PropertyManager.Types.Advice
{
    public class ArticleCategoryMapping
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public int CategoryId { get; set; }
    }
}
