﻿namespace MyHome.v4.PropertyManager.Types.Advice
{
    public class Category
    {
        public int Id { get; set; }
        public string Name{ get; set; }
        public int? ParentCategoryId { get; set; }
        public string UrlSlug { get; set; }
        public string Description { get; set; }
    }
}
