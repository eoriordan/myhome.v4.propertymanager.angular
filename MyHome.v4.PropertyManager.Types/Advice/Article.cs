﻿using System;

namespace MyHome.v4.PropertyManager.Types.Advice
{
    public class Article
    {
        public int Id { get; set; }
        public string Title{ get; set; }
        public string SubTitle { get; set; }
        public string ListingTitle { get; set; }
        public string ImageUrl { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
        public string SeoUrl { get; set; }
        public bool IsPublished { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsGuestPost { get; set; }
        public bool CommentsEnabled { get; set; }
        public DateTime PublishedDate{ get; set; }
        public int CategoryId { get; set; }
		public bool IsSponsored { get; set; }
		public string SponsorLink { get; set; }
		public string SponsorImage { get; set; }
	}
}
