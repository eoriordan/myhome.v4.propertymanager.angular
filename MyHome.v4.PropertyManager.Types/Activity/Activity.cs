﻿using System;
using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema.Activity;
using MyHome.v4.PropertyManager.Schema.Calendar;
using MyHome.v4.PropertyManager.Schema.Document;
using MyHome.v4.PropertyManager.Types.Contacts;
using MyHome.v4.PropertyManager.Types.Property;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Types.Activity
{
    public class Activity
    {
        public int ActivityId { get; set; }

        public ActivityTypeEnum ActivityType { get; set; }

        public PropertySnippet Property { get; set; }

        public User User { get; set; }

        public Contact Contact { get; set; }

        public string Title { get; set; }

        public string SubTitle { get; set; }

        public string Summary { get; set; }

        public IEnumerable<PropertySnippet> Properties { get; set; }

        public IEnumerable<Contact> Contacts { get; set; }

        public IEnumerable<CalendarEventSnippet> Events { get; set; }

        public IEnumerable<DocumentSnippet> Documents { get; set; }

        public DateTime? CreatedOn { get; set; }
    }
}
