﻿using System;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;

namespace MyHome.v4.PropertyManager.Types.Users
{
    [DataContract]
    [Serializable]
    public class UserConfiguration
    {
        public UserConfiguration()
        {
            NotificationSettings = new UserNotificationSettings();
        }

        [DataMember]
        public string GoogleAuthSubToken { get; set; }
        
        [DataMember]
        public string GoogleAuthEmail { get; set; }

        [DataMember]
        public string GoogleAuthAccessToken { get; set; }

        [DataMember]
        public string GoogleAuthTokenType { get; set; }

        [DataMember]
        public long? GoogleAuthExpiresInSeconds { get; set; }

        [DataMember]
        public string GoogleAuthRefreshToken { get; set; }

        [DataMember]
        public string GoogleAuthScope { get; set; }

        [DataMember]
        public DateTime GoogleAuthIssued { get; set; }
        
        [DataMember]
        public bool IsSubscribedToNewsletter { get; set; }
        
        [DataMember]
        public string EulaVersion { get; set; }
        
        [DataMember]
        public DateTime? EulaAcceptedOn { get; set; }

        [DataMember]
        public UserNotificationSettings NotificationSettings { get; set; }

        public bool HasGoogleAuthSubToken
        {
            get
            {
                return !String.IsNullOrEmpty(GoogleAuthSubToken);
            }
        }

        public bool HasGoogleAuthRefreshToken
        {
            get
            {
                return !String.IsNullOrEmpty(GoogleAuthRefreshToken);
            }
        }

        public bool HasGoogleAuthEmail
        {
            get { return !String.IsNullOrEmpty(GoogleAuthEmail); }
        }

        public bool HasAcceptedEula(string eulaVersion)
        {
            return eulaVersion == EulaVersion;
        }

        public string EulaStatus
        {
            get
            {
                if (EulaAcceptedOn.HasValue)
                {
                    return String.Format("EULA {0} accepted on {1}", EulaVersion, EulaAcceptedOn.Value.DateMonthYear());
                }
                return "EULA has not been accepted";
            }
        }
    }
}
