﻿using System;
using System.Runtime.Serialization;
using MyHome.v4.PropertyManager.Types.Groups;

namespace MyHome.v4.PropertyManager.Types.Users
{
    /// <summary>
    /// The user session, that contains the current session that the user is logged in as based on there api and session id
    /// </summary>
    [DataContract]
    public class UserSession
    {
        /// <summary>
        /// Gets or sets the user session identifier.
        /// </summary>
        /// <value>
        /// The user session identifier.
        /// </value>
        [DataMember]
        public int UserSessionId { get; set; }

        /// <summary>
        /// Gets or sets the session identifier.
        /// </summary>
        /// <value>
        /// The session identifier.
        /// </value>
        [DataMember]
        public Guid SessionId { get; set; }

        /// <summary>
        /// Gets or sets the channel identifier.
        /// </summary>
        /// <value>
        /// The channel identifier.
        /// </value>
        [DataMember]
        public int ChannelId { get; set; }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        [DataMember]
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets or sets the user agent.
        /// </summary>
        /// <value>
        /// The user agent.
        /// </value>
        [DataMember]
        public string UserAgent { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>
        /// The created on.
        /// </value>
        [DataMember]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the last activity on.
        /// </summary>
        /// <value>
        /// The last activity on.
        /// </value>
        [DataMember]
        public DateTime LastActivityOn { get; set; }

        /// <summary>
        /// Gets or sets the user associated with the session
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        [DataMember]
        public User User { get; set; }

        /// <summary>
        /// Gets or sets the Group associated with the session
        /// </summary>
        /// <value>
        /// The user.
        /// </value>
        [DataMember]
        public Group Group { get; set; }

    }
}
