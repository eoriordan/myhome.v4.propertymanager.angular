﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Users
{
    /// <summary>
    /// The user preferences json
    /// </summary>
    [DataContract]
    [Serializable]
    public class UserPreference
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UserPreference"/> class.
        /// </summary>
        public UserPreference()
        {
            PropertyClassIDs = new List<int>();
            LocalityIDs = new List<int>();
        }

        /// <summary>
        /// The list of property class ids that the user is intrested in
        /// </summary>
        [DataMember]
        public IEnumerable<int> PropertyClassIDs { get; set; }

        /// <summary>
        /// The list of locality ids that the user is intrested in
        /// </summary>
        [DataMember]
        public IEnumerable<int> LocalityIDs { get; set; }

        /// <summary>
        /// The date the preferences where last updated
        /// </summary>
        [DataMember]
        public DateTime UpdatedOn { get; set; }

    }
}
