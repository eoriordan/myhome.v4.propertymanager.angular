﻿namespace MyHome.v4.PropertyManager.Types.Users
{
    /// <summary>
    /// The role member ship of users in the application
    /// </summary>
    public class RoleMembership
    {
        public int RoleMembershipId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
