﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.Schema.SavedSearch;

namespace MyHome.v4.PropertyManager.Types.Users
{
    [DataContract]
    [Serializable]
    public class User 
    {
        public const string EulaVersion = "v1.1";

        public class WellKnownIDs
        {
            public const int AutoUploadJob = 1;
            public const int Billing = 2;
            public const int SaleAgreed = 3;
            public const int DataImport = 5;
        }

        public User()
        {
            Roles = new List<RoleEnum>();
            UserConfiguration = new UserConfiguration();
            LegacyMappingsJson = new LegacyMappings();
            FavouritePropertyIds = new List<int>();
            ContactDetails = new ContactDetails();
            UserPreference = new UserPreference();
        }

        [DataMember(Name = "UserID")]
        public int UserId { get; set; }
        
        [DataMember]
        public string Username { get; set; }

        public string HashedPassword { get; set; }

        public string PasswordSalt { get; set; }

        [DataMember]
        public string RoleString { get; set; }

        [DataMember(Name = "GroupID")]
        public int GroupId { get; set; }

        [DataMember(Name = "ChannelID")]
        public int ChannelId { get; set; }
        
        [DataMember]
        public ContactDetails ContactDetails { get; set; }
        
        [DataMember]
        public bool EmailConfirmed { get; set; }
        
        [DataMember]
        public string EmailConfirmationKey { get; set; }
        
        [DataMember]
        public DateTime? ConfirmationKeyExpiresOn { get; set; }
        
        [DataMember]
        public bool MobileConfirmed { get; set; }
        
        [DataMember]
        public string Source { get; set; }
        
        [DataMember]
        public DateTime CreatedOn { get; set; }
        
        [DataMember]
        public DateTime ModifiedOn { get; set; }
        
        [DataMember]
        public DateTime LastLoggedOn { get; set; }
        
        [DataMember]
        public RowStatusEnum RowStatusId { get; set; }
        
        [DataMember]
        public LegacyMappings LegacyMappingsJson { get; set; }
        
        [DataMember]
        public int LegacyUserId { get; set; }
        
        [DataMember]
        public UserConfiguration UserConfiguration { get; set; }
        
        [DataMember]
        public bool IsPerson { get; set; }
        
        [DataMember]
        public IEnumerable<RoleEnum> Roles { get; set; }

        [DataMember]
        public IEnumerable<int> FavouritePropertyIds { get; set; }

        /// <summary>
        /// The user preferences for what they are looking for an how they use the site
        /// </summary>
        [DataMember]
        public UserPreference UserPreference { get; set; }

        public bool HasFavouriteProperties
        {
            get
            {
                return FavouritePropertyIds != null && FavouritePropertyIds.Any();
            }
        }

        public bool IsFavourite(int propertyId)
        {
            return FavouritePropertyIds != null && FavouritePropertyIds.Contains(propertyId);
        }

        public bool HasContactDetails
        {
            get
            {
                return ContactDetails != null;
            }
        }

        public bool CanAccessAdminSection
        {
            get
            {
                return Roles.Contains(RoleEnum.MyHomeAccountManager) || Roles.Contains(RoleEnum.MyHomeAdministrator);
            }
        }

        public bool IsBranchManagerOrGroupUser
        {
            get
            {
                return HasRole(RoleEnum.CrmBranchManager, RoleEnum.CrmGroupUser);
            }
        }

        public bool HasGoogleAccount
        {
            get
            {
                return UserConfiguration != null && UserConfiguration.HasGoogleAuthSubToken;
            }
        }

        public bool HasAcceptedEula
        {
            get
            {
                return UserConfiguration != null && UserConfiguration.HasAcceptedEula(EulaVersion);
            }
        }

        public bool HasUserPreferences
        {
            get
            {
                return UserPreference != null;
            }
        }

        public bool RequiredToAcceptEula
        {
            get
            {
                return !HasAcceptedEula && CreatedOn < DateTime.Now.AddHours(-1);
            }
        }

        public bool IsAdministrator
        {
            get
            {
                return HasRole(RoleEnum.MyHomeAdministrator);
            }
        }

        public string DisplayName
        {
            get
            {
                if (ContactDetails != null && ContactDetails.HasName)
                {
                    return ContactDetails.FullName;
                }
                if (IsTwitterUser)
                {
                    return Username.Split("|").First();
                }
                return IsFacebookUser ? "" : Username; 
            }
        }

        public string Email
        {
            get
            {
                return ContactDetails == null ? null : ContactDetails.Email;
            }
        }

        public bool HasConfirmationKeyExpired
        {
            get
            {
                return !ConfirmationKeyExpiresOn.HasValue ||
                ConfirmationKeyExpiresOn.Value < DateTime.Now ||
                string.IsNullOrEmpty(EmailConfirmationKey);
            }
        }

        public bool HasEmail
        {
            get
            {
                return ContactDetails != null && ContactDetails.HasEmail;
            }
        }

        public bool IsFacebookUser
        {
            get
            {
                return Username != null && Username.StartsWith("facebook-");
            }
        }

        public bool IsTwitterUser
        {
            get
            {
                return Username != null && Username.Contains("|Twitter|");
            }
        }

        public bool HasRole(RoleEnum role)
        {
            return Roles != null && Roles.Contains(role);
        }

        public bool HasRole(string role)
        {
            if (string.IsNullOrEmpty(role))
            {
                return true;
            }
            return HasRole((RoleEnum)Enum.Parse(typeof(RoleEnum), role));
        }

        public string RolesAsString
        {
            get
            {
                return Roles != null ? Roles.Join(", ") : "";
            }
        }


        public bool HasRole(List<string> roles)
        {
            if (roles.Count == 0)
            {
                return true;
            }
            return roles.Any(HasRole);
        }

        public bool HasRole(List<RoleEnum> roles)
        {
            return HasRole(roles.Select(r => r.ToString()).ToList());
        }

        public bool HasRole(params RoleEnum[] roles)
        {
            if (!roles.Any())
            {
                return true;
            }
            return roles.Any(HasRole);
        }

        public void DemandRole(string role)
        {
            if (!HasRole(role)) throw new SecurityException("The user does not have the required role : " + role);
        }


        public override string ToString()
        {
            return DisplayName;
        }

        public string ToString(bool includeSource)
        {
            return ToString() + (includeSource ? " (" + Source + ")" : "");
        }

        //public string ToStringWithExternalSource()
        //{
        //    AuthenticationProviderEnum provider;
        //    return ToString(Enum.TryParse<AuthenticationProviderEnum>(Source, out provider));
        //}

        public string NameAndRoles
        {
            get { return String.Format("{0} [u{1}][g{2}] [{3}]", DisplayName, UserId, GroupId, RolesAsString); }
        }

        public static User GetMissingUser(int userId)
        {
            var username = String.Format("User {0}", userId);
            return new User { Username = username };
        }
    }

   
}
