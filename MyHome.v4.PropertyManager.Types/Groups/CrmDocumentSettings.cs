﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Groups
{
    [DataContract]
    [Serializable]
    public class CrmDocumentSettings
    {

        public CrmDocumentSettings()
        {
            HasCustomResidentialMarketingProposal = false;
        }

        [DataMember]
        public bool HasCustomResidentialMarketingProposal { get; set; }
    }
}