﻿namespace MyHome.v4.PropertyManager.Types.Groups
{
    public enum RoleEnum
    {
        ChannelAdministrator = 13,
        CrmAccountsAdmin = 9,
        CrmAccountsManager = 5,
        CrmAccountsUser = 8,
        CrmBranchAdmin = 4,
        CrmBranchManager = 3,
        CrmBranchUser = 6,
        CrmGroupAdmin = 12,
        CrmGroupManager = 11,
        CrmGroupUser = 10,
        DataExportAdmin = 14,
        MyHomeAccountManager = 2,
        MyHomeAdministrator = 1,
        MyHomeSubscriptionsEditor = 7
    }
}
