﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.Types.Product;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Types.Groups
{
    /// <summary>
    /// The representation of a group in the system
    /// </summary>
    [DataContract]
    [Serializable]
    public class Group
    {
        public Group()
        {
            ContactDetails = new ContactDetails();
            Media = new Media();
            Map = new Map();
            Content = new Content();
            GroupConfiguration = new GroupConfiguration();
            GroupPrivateConfiguration = new GroupPrivateConfiguration();
            Users = new List<User>();
            ChildGroups = new List<Group>();
        }

        /// <summary>
        /// Gets or sets the group identifier.
        /// </summary>
        /// <value>
        /// The group identifier.
        /// </value>
        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ShortName { get; set; }

        [DataMember]
        public int GroupTypeId { get; set; }

        [DataMember]
        public string GroupType { get; set; }

        [DataMember]
        public int? ParentGroupId { get; set; }

        [DataMember]
        public int? GroupCategoryOneId { get; set; }

        [DataMember]
        public int? GroupCategoryTwoId { get; set; }

        [DataMember]
        public ContactDetails ContactDetails { get; set; }

        [DataMember]
        public int PackageCredits { get; set; }

        [DataMember]
        public int PurchasedCredits { get; set; }

        [DataMember]
        public Media Media { get; set; }

        [DataMember]
        public int? LocalityId { get; set; }

        [DataMember]
        public Map Map { get; set; }

        [DataMember]
        public Content Content { get; set; }

        [DataMember]
        public string AccountCode { get; set; }

        [DataMember]
        public int? AccountManagerId { get; set; }

        [DataMember]
        public IEnumerable<ProductSubscription> ProductSubscriptions { get; set; }

        //ReportSubscription
        //LegacyMappings

        [DataMember]
        public GroupConfiguration GroupConfiguration { get; set; }

        [DataMember]
        public GroupPrivateConfiguration GroupPrivateConfiguration { get; set; }

        [DataMember]
        public int? LegacyUserId { get; set; }

        [DataMember]
        public DateTime? LastMonthlyBillingRun { get; set; }

        [DataMember]
        public int RefreshCredits { get; set; }

        [DataMember]
        public bool BillInAdvance { get; set; }

        [DataMember]
        public bool DisplayAsBrandbooster { get; set; }

        [DataMember]
        public short RandomisedOrder { get; set; }

        [DataMember]
        public string GroupIdentifier { get; set; }

        [DataMember]
        public int? LocalityTwoId { get; set; }

        [DataMember]
        public int? LocalityThreeId { get; set; }

        [DataMember]
        public int? GroupCategoryThreeId { get; set; }

        [DataMember]
        public int ChannelId { get; set; }

        [DataMember]
        public DateTime CreatedOn { get; set; }

        [DataMember]
        public decimal CurrencyCredit { get; set; }

        [DataMember]
        public string CustomData { get; set; }

        [DataMember]
        public string UrlSlugIdentifier { get; set; }

        [DataMember]
        // ReSharper disable once InconsistentNaming
        public int? GroupOAuthID { get; set; }

        [DataMember]
        public string SapAccountCode { get; set; }

        /// <summary>
        /// Gets or sets the parent group.
        /// </summary>
        /// <value>
        /// The parent group.
        /// </value>
        [DataMember]
        public Group ParentGroup { get; set; }

        /// <summary>
        /// Gets or sets the child groups.
        /// </summary>
        /// <value>
        /// The child groups.
        /// </value>
        [DataMember]
        public List<Group> ChildGroups { get; set; }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        /// <value>
        /// The users.
        /// </value>
        [DataMember]
        public IEnumerable<User> Users { get; set; }

        /// <summary>
        /// Group ids including children
        /// </summary>
        [DataMember]
        public IEnumerable<int> AllGroupIds
        {
            get
            {
                if (ChildGroups.Any())
                {
                    var childGroupIds = ChildGroups.Select(x => x.GroupId).ToList();
                    childGroupIds.Add(GroupId);
                    return childGroupIds;
                }
                return new List<int> { GroupId };
            }
        }
    }
}
