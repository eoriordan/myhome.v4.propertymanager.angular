﻿using System;
using System.Runtime.Serialization;
using MyHome.v4.Common.Schema.Enumerations;

namespace MyHome.v4.PropertyManager.Types.Groups
{
    [DataContract]
    [Serializable]
    public class GroupFeaturedProperty
    {

        public GroupFeaturedProperty() { }

        public GroupFeaturedProperty(int propertyId, int propertyClassId)
        {
            PropertyId = propertyId;
            PropertyClassId = propertyClassId;
        }

        public GroupFeaturedProperty(int propertyId, PropertyClassEnum propertyClass) : this(propertyId, (int)propertyClass) { }

        [DataMember]
        public int PropertyId { get; set; }

        [DataMember]
        public int PropertyClassId { get; set; }

        public PropertyClassEnum PropertyClass
        {
            get { return (PropertyClassEnum)Enum.ToObject(typeof(PropertyClassEnum), PropertyClassId); }
            set { PropertyClassId = (int)value; }
        }

    }
}
