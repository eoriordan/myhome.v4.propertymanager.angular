﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema.Forms;
using MyHome.v4.PropertyManager.Schema.Groups;

namespace MyHome.v4.PropertyManager.Types.Groups
{
    [DataContract]
    [Serializable]
    public class GroupPrivateConfiguration
    {
        public GroupPrivateConfiguration()
        {
            GoogleDocsUsers = new List<GoogleDocsUser>();
            PropertyForm = new Form();
            ContactForm = new Form();
            ContactTags = new List<Tag>();
            Tags = new List<Tag>();
        }

        [DataMember]
        public string GoogleConversionCode { get; set; }
        [DataMember]
        public string GoogleAppsDomain { get; set; }
        [DataMember]
        public string EmailApiUrl { get; set; }
        [DataMember]
        public string EmailApiKey { get; set; }


        [DataMember]
        public string EmailNotificationsSender { get; set; }
        [DataMember]
        public string EmailNotificationsReplyTo { get; set; }
        [DataMember]
        public string WeeklyReportEmail { get; set; }
        [DataMember]
        public IEnumerable<GoogleDocsUser> GoogleDocsUsers { get; set; }
        [DataMember]
        public string GoogleDocsFolderId { get; set; }
        [DataMember]
        public IEnumerable<Tag> ContactTags { get; set; }
        [DataMember]
        public IEnumerable<Tag> Tags { get; set; }
        [DataMember]
        public TagGroups PublicTagGroups { get; set; }
        [DataMember]
        public TagGroups PrivateTagGroups { get; set; }
        [DataMember]
        public bool PriceHistoryOptOut { get; set; }
        [DataMember]
        public bool EnablePropertyAlerts { get; set; }
        [DataMember]
        public bool IsThisYourPropertyOptOut { get; set; }

        [DataMember]
        public string PdfListingsUrl { get; set; }

        [DataMember]
        public FloorplannerSettings Floorplanner { get; set; }
        public bool HasFloorplannerSettings { get { return Floorplanner != null; } }

        [DataMember]
        public Form PropertyForm { get; set; }
        [DataMember]
        public Form ContactForm { get; set; }

        [DataMember]
        public bool? HasReceivedLandlordFreeCurrencyCredit { get; set; } //NOTE: GJ: this can be removed once the private landlord offer is over

        public TagGroup ContactTagGroup { get { return new TagGroup { Name = "Tags", Tags = ContactTagsWithDefaults }; } }
        public IEnumerable<Tag> ContactTagsWithDefaults
        {
            get
            {
                if (ContactTags.Any())
                {
                    return ContactTags;
                }
                return new List<Tag> {
                    new Tag("Buyer"),
                    new Tag("Prospect"),
                    new Tag("Solicitor"),
                    new Tag("Landlord"),
                    new Tag("Tenant"),
                    new Tag("Vendor"),
                    new Tag("Misc")
                };
            }
        }

        public bool HasWeeklyReportEmail { get { return !string.IsNullOrEmpty(WeeklyReportEmail); } }
        public bool HasGoogleAppsDomain { get { return !String.IsNullOrEmpty(GoogleAppsDomain); } }
        public bool HasGoogleConversionCode { get { return !String.IsNullOrEmpty(GoogleConversionCode); } }
        public bool HasCustomForms { get { return HasPropertyForm || HasContactForm; } }
        public bool HasPropertyForm { get { return PropertyForm != null && PropertyForm.Fields.Any(); } }
        public bool HasContactForm { get { return PropertyForm != null && ContactForm.Fields.Any(); } }
        public bool HasEmailNotificationsReplyTo { get { return !String.IsNullOrEmpty(EmailNotificationsReplyTo); } }
        public bool HasBrandedEmailConfiguration
        {
            get
            {
                return !string.IsNullOrEmpty(EmailApiKey) &&
                    !string.IsNullOrEmpty(EmailApiUrl) &&
                    !string.IsNullOrEmpty(EmailNotificationsSender);
            }
        }

        public GoogleDocsUser RandomGoogleDocsUser
        {
            get
            {
                var activeUsers = GoogleDocsUsers.Where(u => u.IsActive).ToList();
                return activeUsers.RandomOrDefault();
            }
        }
    }

    [DataContract]
    [Serializable]
    public class GoogleDocsUser
    {
        public GoogleDocsUser()
        {
            IsActive = true;
        }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
    }


    [DataContract]
    [Serializable]
    public class TagGroup
    {
        public TagGroup()
        {
            Tags = new List<Tag>();
        }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public IEnumerable<Tag> Tags { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Tag
    {
        public Tag() { }
        public Tag(string name) { Name = name; }

        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    [Serializable]
    public class FloorplannerSettings
    {
        [DataMember]
        public int UserId { get; set; }
    }
}