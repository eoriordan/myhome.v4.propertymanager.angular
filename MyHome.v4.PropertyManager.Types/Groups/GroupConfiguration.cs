﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.Common.Schema;
using StringDictionary = MyHome.v4.Common.Schema.StringDictionary;

namespace MyHome.v4.PropertyManager.Types.Groups
{
    [DataContract]
    [Serializable]
    public class GroupConfiguration
    {
        public GroupConfiguration()
        {
            Data = new StringDictionary();
            BrochureSettings = BrochureSettings.Default;
            RentalCrmSettings = new RentalCrmSettings();
            BankAccounts = new List<BankAccount>();
            CompanySettings = new CompanySettings();
            FeaturedProperties = new List<GroupFeaturedProperty>();
            CrmDocumentSettings = new CrmDocumentSettings();             
        }

        [DataMember]
        public string BrandBoosterColour { get; set; }

        [DataMember]
        public string BrochurePrinterEmail { get; set; } //where we send brochure print requests to

        [DataMember]
        public string PropertyManagerCssClass { get; set; }
        [DataMember]
        public string PropertyManagerCustomCss { get; set; }

        [DataMember]
        public BrochureSettings BrochureSettings { get; set; }
        [DataMember]
        public RentalCrmSettings RentalCrmSettings { get; set; }
        [DataMember]
        public StringDictionary Data { get; set; }
        [DataMember]
        public CompanySettings CompanySettings { get; set; }

        [DataMember]
        public List<BankAccount> BankAccounts { get; set; }

        [DataMember]
        public List<GroupFeaturedProperty> FeaturedProperties { get; set; }

        [DataMember]
        public CrmDocumentSettings CrmDocumentSettings { get; set; }

        public bool HasBrandBoosterColour { get { return !String.IsNullOrEmpty(BrandBoosterColour); } }
        public bool HasBrochurePrinterEmail { get { return !String.IsNullOrEmpty(BrochurePrinterEmail); } }
        public bool HasPropertyManagerCssClass { get { return !String.IsNullOrEmpty(PropertyManagerCssClass); } }

        public string PropertyManagerTheme
        {
            get { return HasPropertyManagerCssClass ? PropertyManagerCssClass : ""; }
        }
    }
}
