﻿using System;
using MyHome.v4.PropertyManager.Types.Contacts;
using MyHome.v4.PropertyManager.Types.Groups;
using MyHome.v4.PropertyManager.Types.Property;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Types.Calendar
{
    public class TaskItem
    {
        public int TaskId { get; set; }
        public string Title { get; set; }
        public int? TaskTypeId { get; set; }
        public Group Group { get; set; }
        public User AssignedToUser { get; set; }
        public User CreatedByUser { get; set; }
        public User CompletedByUser { get; set; }
        public DateTime? DueOn { get; set; }
        public DateTime? CompletedOn { get; set; }
        public PropertySnippet Property { get; set; }
        public Contact Contact { get; set; }
        public int? MessageId { get; set; }
        public int? PropertyLeaseId { get; set; }
        public int? WorkflowId { get; set; }
        public DateTime? ActivatedOn { get; set; }
    }
}
