﻿using System;
using MyHome.v4.PropertyManager.Types.Contacts;
using MyHome.v4.PropertyManager.Types.Property;

namespace MyHome.v4.PropertyManager.Types.Calendar
{
    public class CalendarItem
    {
        public string ExternalEventId { get; set; }

        public int? UserId { get; set; }
        public string UserName { get; set; }

        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public bool IsRecuring { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public int? Reminder { get; set; }

        public Contact Contact { get; set; }

        public PropertySnippet Property { get; set; }

        public int? TaskType { get; set; }

        public string Location { get; set; }
    }
}
