﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using MyHome.v4.PropertyManager.Types.Contacts;

namespace MyHome.v4.PropertyManager.Types.OpenViewing
{
    [DataContract]
    [Serializable]
    public class OpenViewing
    {
        public OpenViewing()
        {
            CreatedOn = DateTime.Now;
            ModifiedOn = DateTime.Now;
            Notes = new List<OpenViewingNote>();
        }

        [DataMember]
        public int OpenViewingId { get; set; }

        [DataMember]
        public int PropertyId { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public int CreatedByUserId { get; set; }

        [DataMember]
        public int ModifiedByUserId { get; set; }

        [DataMember]
        [DisplayName("Start Time")]
        public DateTime StartDate { get; set; }

        [DataMember]
        [DisplayName("End Time")]
        public DateTime EndDate { get; set; }

        public bool IsPublic { get; set; }

        [DataMember]
        public DateTime CreatedOn { get; set; }

        [DataMember]
        public DateTime ModifiedOn { get; set; }

        public ICollection<OpenViewingNote> Notes { get; set; }

        public ICollection<Contact> Contacts { get; set; }

        public bool IsUpcoming()
        {
            return this.StartDate.Date >= DateTime.Now.Date;
        }
    }
}
