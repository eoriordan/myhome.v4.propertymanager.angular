﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.OpenViewing
{
    public class OpenViewingNote
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int OpenViewingId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string NoteText { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }
    }
}
