﻿using System;
using MyHome.v4.PropertyManager.Types.Contacts;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Types.Property
{
    public class PropertyOffer
    {
        public int PropertyOfferId { get; set; }

        public PropertySnippet Property { get; set; }

        public int GroupId { get; set; }

        public Contact Contact { get; set; }

        public int StatusId { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreatedOnDate { get; set; }

        public User CreatedByUser { get; set; }

        public DateTime? AcceptedOnDate { get; set; }

        public string Notes { get; set; }

        public string CustomData { get; set; }

        public int? Method { get; set; }

        public DateTime? NotificationDate { get; set; }

        public int? NotifiedByUserId { get; set; }

        public int? NotificationMethod { get; set; }

        public string NotificationNotes { get; set; }
    }
}
