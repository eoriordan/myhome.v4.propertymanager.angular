﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema;
using MyHome.v4.Common.Schema.Enumerations;
using StringDictionary = MyHome.v4.Common.Schema.StringDictionary;

namespace MyHome.v4.PropertyManager.Types.Property
{
    /// <summary>
    /// The property summary for a property
    /// </summary>
    [DataContract]
    [Serializable]
    public class Property : PropertySnippet
    {

        public Property()
        {
            Map = new Map();
            Media = new Media();
            Price = new Price();
            PublicTags = new Tags();
            PrivateTags = new Tags();
            ComputedTags = new Tags();
            CustomFormData = new StringDictionary();
            LegacyMappings = new LegacyMappings();
            Rooms = new Rooms();
            Floors = new Floors();
            FeeStructure = new FeeStructure();
            CustomData = new CustomData();
            Content = new List<PropertyContent>();
            PropertyPeople= new PropertyPeople();
            OpenViewings = new List<OpenViewing.OpenViewing>();
        }

        public const int CommercialForAuctionSaleTypeId = 1;
        public const int CommercialByTenderSaleTypeId = 2;
        public const int ResiForAuctionSaleTypeId = 15;
        public const int ResiByTenderSaleTypeId = 18;

        [DataMember]
        public string OrderedDisplayAddress
        {
            get
            {
                return CleanCrap(DisplayAddress);
            }
        }

        private string CleanCrap(string crap)
        {
            crap = crap.Replace("\n", " ");
            crap = crap.Replace("\r", " ");
            crap = crap.Replace("\t", " ");
            var items = crap.ToCharArray();
            var output = new StringBuilder();
            foreach (var item in items)
            {
                if (char.IsLetterOrDigit(item) || char.IsWhiteSpace(item))
                {
                    output.Append(item);
                }
            }
            return output.ToString().Trim().ToLower();
        }

        /// <summary>
        /// The property reference to display to the user
        /// </summary>
        public string DisplayReference
        {
            get
            {
                if (string.IsNullOrEmpty(PropertyReference))
                {
                    return PropertyId.ToString(CultureInfo.InvariantCulture);
                }
                return PropertyReference;
            }
        }

        /// <summary>
        /// The account reference number for the property
        /// </summary>
        public string AccountReferenceNumber
        {
            get { return GetCustomFormData("AccountReferenceNumber"); }
            set { CustomFormData["AccountReferenceNumber"] = value; }
        }

        /// <summary>
        /// The file reference number for the property
        /// </summary>
        public string FileReferenceNumber
        {
            get { return GetCustomFormData("FileReferenceNumber"); }
            set { CustomFormData["FileReferenceNumber"] = value; }
        }


        /// <summary>
        /// The property status enum for the allowed property status types
        /// </summary>
        public static readonly int[] PublicPropertyStatuses =
        {
            2,
            3,
            4,
            11,
            7,
            8,
            12
        };

        /// <summary>
        /// Determines whether the property has historicaly significant change
        /// </summary>
        /// <param name="otherProperty">The other property.</param>
        /// <returns>Returns true if there is a significant change</returns>
        public bool HasHistoricalySignificantChange(Property otherProperty)
        {
            if (otherProperty == null)
            {
                return true;
            }
            if (PropertyStatusId != otherProperty.PropertyStatusId)
            {
                return true;
            }
            if (otherProperty.Price.Period != null && (Price.Period != null && Price.Period.Value != otherProperty.Price.Period.Value))
            {
                return true;
            }
            if (Price.MinPrice != otherProperty.Price.MinPrice || Price.MaxPrice != otherProperty.Price.MaxPrice ||
                Price.MinRentalPrice != otherProperty.Price.MinRentalPrice ||
                Price.MaxRentalPrice != otherProperty.Price.MaxRentalPrice)
            {
                return true;
            }
            if (Beds.MinBeds != otherProperty.Beds.MinBeds || Beds.MaxBeds != otherProperty.Beds.MaxBeds)
            {
                return true;
            }
            return false;
        }

        [DataMember]
        public int? MinBeds { get; set; }

        [DataMember]
        public int? MaxBeds { get; set; }

        [DataMember]
        public string Eircode { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        [DataMember]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the channel.
        /// </summary>
        /// <value>
        /// The channel.
        /// </value>
        [DataMember]
        public int ChannelId { get; set; }

        /// <summary>
        /// Gets or sets the row status.
        /// </summary>
        /// <value>
        /// The row status.
        /// </value>
        [DataMember]
        public int RowStatusId { get; set; }

        /// <summary>
        /// Gets or sets the vendor unique identifier.
        /// </summary>
        /// <value>
        /// The vendor unique identifier.
        /// </value>
        [DataMember]
        public int? VendorId
        {
            get { return ContactId; }
            set { ContactId = value; }
        }


        /// <summary>
        /// Gets or sets the buyer unique identifier.
        /// </summary>
        /// <value>
        /// The buyer unique identifier.
        /// </value>
        [DataMember]
        public int? BuyerId { get; set; }

        /// <summary>
        /// Gets or sets the vendor solicitor unique identifier.
        /// </summary>
        /// <value>
        /// The vendor solicitor unique identifier.
        /// </value>
        [DataMember]
        public int? VendorSolicitorId { get; set; }

        /// <summary>
        /// Gets or sets the buyer solicitor unique identifier.
        /// </summary>
        /// <value>
        /// The buyer solicitor unique identifier.
        /// </value>
        [DataMember]
        public int? BuyerSolicitorId { get; set; }

        /// <summary>
        /// Gets or sets the developer unique identifier.
        /// </summary>
        /// <value>
        /// The developer unique identifier.
        /// </value>
        [DataMember]
        public int? DeveloperId { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        /// <value>
        /// The notes.
        /// </value>
        [DataMember]
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the sold price.
        /// </summary>
        /// <value>
        /// The sold price.
        /// </value>
        [DataMember]
        public decimal? SoldPrice { get; set; }

        /// <summary>
        /// Gets or sets the fee structure.
        /// </summary>
        /// <value>
        /// The fee structure.
        /// </value>
        [DataMember]
        public FeeStructure FeeStructure { get; set; }


        /// <summary>
        /// Gets or sets the legacy mappings.
        /// </summary>
        /// <value>
        /// The legacy mappings.
        /// </value>
        [DataMember]
        public LegacyMappings LegacyMappings { get; set; }

        /// <summary>
        /// Gets or sets the tax section unique identifier.
        /// </summary>
        /// <value>
        /// The tax section unique identifier.
        /// </value>
        [DataMember]
        public int? TaxSectionId { get; set; }

        /// <summary>
        /// Gets or sets the service level.
        /// </summary>
        /// <value>
        /// The service level.
        /// </value>
        [DataMember]
        public int? ServiceLevel { get; set; }

        /// <summary>
        /// Gets or sets the property reference.
        /// </summary>
        /// <value>
        /// The property reference.
        /// </value>
        [DataMember]
        public string PropertyReference { get; set; }

        /// <summary>
        /// Gets or sets the sale type unique identifier.
        /// </summary>
        /// <value>
        /// The sale type unique identifier.
        /// </value>
        [DataMember]
        public int? SaleTypeId { get; set; }

        /// <summary>
        /// Gets or sets the index of the search results.
        /// </summary>
        /// <value>
        /// The index of the search results.
        /// </value>
        [DataMember]
        public int SearchResultsIndex { get; set; }

        /// <summary>
        /// Gets or sets the index of the search results.
        /// </summary>
        /// <value>
        /// The index of the search results.
        /// </value>
        [DataMember]
        public int? LeaseTermInDays { get; set; }

        /// <summary>
        /// Gets or sets the bathrooms.
        /// </summary>
        /// <value>
        /// The bathrooms.
        /// </value>
        [DataMember]
        public int? Bathrooms { get; set; }

        /// <summary>
        /// Gets or sets the minimum size.
        /// </summary>
        /// <value>
        /// The minimum size.
        /// </value>
        [DataMember]
        public double? MinSize { get; set; }

        /// <summary>
        /// Gets or sets the maximum size.
        /// </summary>
        /// <value>
        /// The maximum size.
        /// </value>
        [DataMember]
        public double? MaxSize { get; set; }

        [DataMember]
        public double? Latitude { get; set; }

        [DataMember]
        public double? Longitude { get; set; }

        [DataMember]
        public int? MinSortPrice { get; set; }

        [DataMember]
        public int? MaxSortPrice { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is trending.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is trending; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsTrending { get; set; }

        public List<string> GetContentTypes()
        {
            return
                Content.Where(m => m.PropertyContentSourceType == PropertyContentSourceTypeEnum.MyHome)
                    .Select(m => m.ContentType)
                    .ToList();
        }

        public Content GetContent()
        {
            return new Content(Content.Where(m => m.PropertyContentSourceType == PropertyContentSourceTypeEnum.MyHome).ToDictionary(m => m.ContentType, m => m.Content));
        }

        public string GetContent(string contentType)
        {
            return GetContent(contentType, PropertyContentSourceTypeEnum.MyHome);
        }

        public string GetContent(string contentType, PropertyContentSourceTypeEnum propertyContentSourceType)
        {
            var item = Content.FirstOrDefault(m => m.ContentType == contentType && m.PropertyContentSourceType == propertyContentSourceType && m.SortOrder == 1);
            return item == null ? string.Empty : item.Content;
        }

        public string GetContent(string contentType, bool formatForBrochure)
        {
            var item = GetContent(contentType);
            return formatForBrochure ? item.FormatForBrochure() : item;
        }

        /// <summary>
        /// Set the property content with the required content for the application
        /// </summary>
        /// <param name="contentType">The content type to set</param>
        /// <param name="content">The content to set</param>
        public void SetContent(string contentType, string content)
        {
            SetContent(contentType, content, PropertyContentSourceTypeEnum.MyHome);
        }

        /// <summary>
        /// Set the property content with the required content for the application
        /// </summary>
        /// <param name="contentType"></param>
        /// <param name="content"></param>
        /// <param name="propertyContentSourceType"></param>
        public void SetContent(string contentType, string content, PropertyContentSourceTypeEnum propertyContentSourceType)
        {
            SetContent(contentType, content, propertyContentSourceType, 1);
        }

        /// <summary>
        /// Set the property content with the required content for the application
        /// </summary>
        /// <param name="contentType">The content type to set</param>
        /// <param name="content">The content to set</param>
        /// <param name="sourceType">The source type to set</param>
        /// <param name="sortOrder">The sort order to set</param>
        public void SetContent(string contentType, string content, PropertyContentSourceTypeEnum sourceType, int sortOrder)
        {
            var item = Content.FirstOrDefault(m => m.ContentType == contentType && m.PropertyContentSourceType == sourceType && m.SortOrder == sortOrder);
            if (item == null)
            {
                Content.Add(new PropertyContent { Content = content, ContentType = contentType, PropertyContentSourceType = sourceType, SortOrder = sortOrder });
            }
            else
            {
                item.Content = content;
            }
        }

        public bool ContainsContent(string contentType)
        {
            return ContainsContent(contentType, PropertyContentSourceTypeEnum.MyHome);
        }

        public bool ContainsContent(string contentType, PropertyContentSourceTypeEnum propertyContentSourceType)
        {
            return Content.Any(m => m.ContentType == contentType && m.PropertyContentSourceType == propertyContentSourceType);
        }

        public void RemoveAllHtmlContent()
        {
            foreach (var item in Content.Where(m => m.PropertyContentSourceType == PropertyContentSourceTypeEnum.MyHome))
            {
                item.Content = item.Content.RemoveAllHtml(true).RemoveAllHtml(true);
                // it's a bit crude to do it twice, but there are some examples of content
                // which contains HTML encoded markup, e.g. &gt;br/&lt
            }
        }

        public void RemoveContent(string contentType)
        {
            RemoveContent(contentType, PropertyContentSourceTypeEnum.MyHome);
        }

        public void RemoveContent(string contentType, PropertyContentSourceTypeEnum propertyContentSourceType)
        {
            var propertyContent = new List<PropertyContent>();
            foreach (var item in Content)
            {
                if (item.PropertyContentSourceType != propertyContentSourceType || (item.ContentType != contentType && item.PropertyContentSourceType == propertyContentSourceType))
                {
                    propertyContent.Add(item);
                }
            }
            Content = propertyContent;
        }

        /// <summary>
        /// The content contains all the content elements for different content sources
        /// </summary>
        [DataMember]
        public ICollection<PropertyContent> Content { get; set; }

        /// <summary>
        /// Gets or sets the peoples associated with aproperty
        /// </summary>
        [DataMember]
        public PropertyPeople PropertyPeople { get; set; }

        /// <summary>
        /// Gets or sets the public tags.
        /// </summary>
        /// <value>
        /// The public tags.
        /// </value>
        [DataMember]
        public Tags PublicTags { get; set; }

        /// <summary>
        /// Gets or sets the private tags.
        /// </summary>
        /// <value>
        /// The private tags.
        /// </value>
        [DataMember]
        public Tags PrivateTags { get; set; }

        /// <summary>
        /// Gets or sets the computed tags.
        /// </summary>
        /// <value>
        /// The computed tags.
        /// </value>
        [DataMember]
        public Tags ComputedTags { get; set; }

        /// <summary>
        /// Gets or sets the title deed.
        /// </summary>
        /// <value>
        /// The title deed.
        /// </value>
        [DataMember]
        public int? TitleDeed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [preference sixty three].
        /// </summary>
        /// <value>
        /// <c>true</c> if [preference sixty three]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool? PreSixtyThree { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [is new build].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is new build]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool? IsNewBuild { get; set; }


        /// <summary>
        /// Gets or sets the custom form data.
        /// </summary>
        /// <value>
        /// The custom form data.
        /// </value>
        [DataMember]
        public StringDictionary CustomFormData { get; set; }

        /// <summary>
        /// Gets or sets the floors.
        /// </summary>
        /// <value>
        /// The floors.
        /// </value>
        [DataMember]
        public Floors Floors { get; set; }

        /// <summary>
        /// Gets or sets the rooms.
        /// </summary>
        /// <value>
        /// The rooms.
        /// </value>
        [DataMember]
        public Rooms Rooms { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        [DataMember]
        public Size Size { get; set; }



        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public int? NegotiatorId { get; set; }


        /// <summary>
        /// Gets or sets the minimum energy rating.
        /// </summary>
        /// <value>
        /// The minimum energy rating.
        /// </value>
        [DataMember]
        public int? MinEnergyRating { get; set; }

        /// <summary>
        /// Gets or sets the maximum energy rating.
        /// </summary>
        /// <value>
        /// The maximum energy rating.
        /// </value>
        [DataMember]
        public int? MaxEnergyRating { get; set; }


        /// <summary>
        /// Gets or sets the created configuration date.
        /// </summary>
        /// <value>
        /// The created configuration date.
        /// </value>
        [DataMember]
        public DateTime CreatedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the created by user unique identifier.
        /// </summary>
        /// <value>
        /// The created by user unique identifier.
        /// </value>
        [DataMember]
        public int? CreatedByUserId { get; set; }

        /// <summary>
        /// Gets or sets the modified configuration date.
        /// </summary>
        /// <value>
        /// The modified configuration date.
        /// </value>
        [DataMember]
        public DateTime ModifiedOnDate { get; set; }

        /// <summary>
        /// Gets or sets the modified by user unique identifier.
        /// </summary>
        /// <value>
        /// The modified by user unique identifier.
        /// </value>
        [DataMember]
        public int? ModifiedByUserId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [is active].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is active]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsActive { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether [is premium ad].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is premium ad]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsPremiumAd { get; set; }


        /// <summary>
        /// Gets or sets the instructed configuration.
        /// </summary>
        /// <value>
        /// The instructed configuration.
        /// </value>
        [DataMember]
        public DateTime? InstructedOn { get; set; }

        /// <summary>
        /// Gets or sets the activated configuration.
        /// </summary>
        /// <value>
        /// The activated configuration.
        /// </value>
        [DataMember]
        public DateTime? ActivatedOn { get; set; }

        /// <summary>
        /// Gets or sets the refreshed configuration.
        /// </summary>
        /// <value>
        /// The refreshed configuration.
        /// </value>
        [DataMember]
        public DateTime? RefreshedOn { get; set; }

        /// <summary>
        /// Gets or sets the agreed configuration.
        /// </summary>
        /// <value>
        /// The agreed configuration.
        /// </value>
        [DataMember]
        public DateTime? AgreedOn { get; set; }

        /// <summary>
        /// Gets or sets the deleted configuration.
        /// </summary>
        /// <value>
        /// The deleted configuration.
        /// </value>
        [DataMember]
        public DateTime? DeletedOn { get; set; }

        /// <summary>
        /// Gets or sets the expires configuration.
        /// </summary>
        /// <value>
        /// The expires configuration.
        /// </value>
        [DataMember]
        public DateTime? ExpiresOn { get; set; }

        /// <summary>
        /// Gets or sets the estimated closed configuration.
        /// </summary>
        /// <value>
        /// The estimated closed configuration.
        /// </value>
        [DataMember]
        public DateTime? EstimatedClosedOn { get; set; }

        /// <summary>
        /// Gets or sets the closed configuration.
        /// </summary>
        /// <value>
        /// The closed configuration.
        /// </value>
        [DataMember]
        public DateTime? ClosedOn { get; set; }

        /// <summary>
        /// Gets or sets the account finalised configuration.
        /// </summary>
        /// <value>
        /// The account finalised configuration.
        /// </value>
        [DataMember]
        public DateTime? AccountFinalisedOn { get; set; }

        /// <summary>
        /// Gets or sets the valued configuration.
        /// </summary>
        /// <value>
        /// The valued configuration.
        /// </value>
        [DataMember]
        public DateTime? ValuedOn { get; set; }

        /// <summary>
        /// Gets or sets the cancelled configuration.
        /// </summary>
        /// <value>
        /// The cancelled configuration.
        /// </value>
        [DataMember]
        public DateTime? CancelledOn { get; set; }

        /// <summary>
        /// Gets or sets the sold configuration.
        /// </summary>
        /// <value>
        /// The sold configuration.
        /// </value>
        [DataMember]
        public DateTime? SoldOn { get; set; }

        /// <summary>
        /// Gets or sets the property lease unique identifier.
        /// </summary>
        /// <value>
        /// The property lease unique identifier.
        /// </value>
        [DataMember]
        public int? PropertyLeaseId { get; set; }

        /// <summary>
        /// Gets or sets the deleted by user unique identifier.
        /// </summary>
        /// <value>
        /// The deleted by user unique identifier.
        /// </value>
        [DataMember]
        public int? DeletedByUserId { get; set; }

        /// <summary>
        /// Gets or sets the legacy identifier.
        /// </summary>
        /// <value>
        /// The legacy identifier.
        /// </value>
        [DataMember]
        public string LegacyIdentifier { get; set; } //NOTE: GJ: remove once we've launched v3

        /// <summary>
        /// Gets or sets a value indicating whether [is top spot].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is top spot]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsTopSpot { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [is private landlord].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is private landlord]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsPrivateLandlord { get; set; }

        /// <summary>
        /// Gets or sets the contact details.
        /// </summary>
        /// <value>
        /// The contact details.
        /// </value>
        [DataMember]
        public ContactDetails ContactDetails { get; set; }

        /// <summary>
        /// Gets or sets the premium ad expires configuration.
        /// </summary>
        /// <value>
        /// The premium ad expires configuration.
        /// </value>
        [DataMember]
        public DateTime? PremiumAdExpiresOn { get; set; }

        /// <summary>
        /// Gets or sets the featured ad expires configuration.
        /// </summary>
        /// <value>
        /// The featured ad expires configuration.
        /// </value>
        [DataMember]
        public DateTime? FeaturedAdExpiresOn { get; set; }

        /// <summary>
        /// Gets or sets the collection of open viewings for this property
        /// </summary>
        /// <value>
        /// The open viewings.
        /// </value>
        [DataMember]
        public IEnumerable<OpenViewing.OpenViewing> OpenViewings { get; set; }

        /// <summary>
        /// Gets or sets the related properties.
        /// </summary>
        /// <value>
        /// The related properties.
        /// </value>
        [DataMember]
        public string RelatedProperties
        {
            get
            {
                if (CustomData != null)
                {
                    if (CustomData.RelatedPropertyIDs.Any())
                    {
                        return string.Join(",", CustomData.RelatedPropertyIDs);
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Gets or sets the contact unique identifier.
        /// </summary>
        /// <value>
        /// The contact unique identifier.
        /// </value>
        public int? ContactId { get; set; } //TODO: GJ: Depreciate: rename to VendorID

        /// <summary>
        /// The unique id of the property to which the property has been transfered to
        /// </summary>
        public int? TransferedToPropertyId { get; set; }

        /// <summary>
        /// The unique id of the property of the property from which the property has been transfered from
        /// </summary>
        public int? TransferedFromPropertyId { get; set; }

        /// <summary>
        /// The id of the user that transfered the property
        /// </summary>
        public int? TransferedByUserId { get; set; }

        /// <summary>
        /// TEMP: just while floorplanner is in trial
        /// 1746057, 1764550, 1764162, 1802117, 1780747, 1874052, 1927458 - floorplanner trial properties
        /// </summary>
        /// <value>
        /// <c>true</c> if [show floorplanner trial]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowFloorplannerTrial
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether [is auction].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is auction]; otherwise, <c>false</c>.
        /// </value>
        public bool IsAuction
        {
            get
            {
                return SaleTypeId == ResiForAuctionSaleTypeId || SaleTypeId == CommercialForAuctionSaleTypeId;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [is tender].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is tender]; otherwise, <c>false</c>.
        /// </value>
        public bool IsTender
        {
            get { return SaleTypeId == ResiByTenderSaleTypeId || SaleTypeId == CommercialByTenderSaleTypeId; }
        }


        /// <summary>
        /// Gets a value indicating whether [has property reference].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has property reference]; otherwise, <c>false</c>.
        /// </value>
        public bool HasPropertyReference
        {
            get { return !String.IsNullOrEmpty(PropertyReference); }
        }

        /// <summary>
        /// Gets the pipeline price.
        /// </summary>
        /// <value>
        /// The pipeline price.
        /// </value>
        public decimal PipelinePrice
        {
            //price used to calculate pipeline fees
            get
            {
                var pipelinePrice = 0.0m;
                if (Price.HasSalePrice && Price.MinPrice.HasValue && Price.Value.HasValue)
                {
                    pipelinePrice = Price.HasPriceRange ? Price.MinPrice.Value : Price.Value.Value;
                }
                if (SoldPrice.HasValue)
                {
                    pipelinePrice = SoldPrice.Value;
                }
                return pipelinePrice;
            }
        }

        /// <summary>
        /// Gets the pipeline fee.
        /// </summary>
        /// <value>
        /// The pipeline fee.
        /// </value>
        public decimal PipelineFee
        {
            get
            {
                var pipelinePrice = PipelinePrice;

                if (pipelinePrice < 250000)
                    return pipelinePrice * 0.01m;
                if (pipelinePrice >= 250000 && pipelinePrice < 35000)
                    return pipelinePrice * 0.0085m;
                return pipelinePrice * 0.0075m;
            }
        }

        /// <summary>
        /// Gets the energy rating for display.
        /// </summary>
        /// <value>
        /// The energy rating for display.
        /// </value>
        public string EnergyRatingForDisplay
        {
            get
            {
                if (MinEnergyRating != MaxEnergyRating)
                {
                    if (MinEnergyRating != null)
                    {
                        var minValue = (EnergyRatingEnum)MinEnergyRating;
                        if (MaxEnergyRating != null)
                        {
                            var maxValue = (EnergyRatingEnum)MaxEnergyRating;
                            return minValue + "-" + maxValue;
                        }
                    }
                }
                if (MaxEnergyRating.HasValue)
                {
                    var maxValue = (EnergyRatingEnum)MaxEnergyRating;
                    return maxValue.ToString();
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the pipeline fee.
        /// </summary>
        /// <value>
        /// The pipeline fee.
        /// </value>
        public PropertySection PropertySection
        {
            get { return GetPropertySection(PropertyClassId); }
        }

        public static PropertySection GetPropertySection(int? classId)
        {
            switch (classId)
            {
                case 1:
                case 2:
                    return PropertySection.Residential;
                case 3:
                case 4:
                case 5:
                    return PropertySection.Lettings;
                case 6:
                    return PropertySection.Commercial;
                case 9:
                case 10:
                    return PropertySection.Overseas;
                default:
                    throw new ArgumentException("Property Class not supported");
            }
        }

        /// <summary>
        /// Gets a value indicating whether [is under offer].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is under offer]; otherwise, <c>false</c>.
        /// </value>
        public bool IsUnderOffer
        {
            get
            {
                return
                    PropertyStatusId == 2 &&
                    SoldPrice.HasValue &&
                    SoldPrice > 0 &&
                    BuyerId.HasValue;
            }
        }



        /// <summary>
        /// Gets the private landlord property status.
        /// </summary>
        /// <value>
        /// The private landlord property status.
        /// </value>
        /// <exception cref="System.ApplicationException">Invalid property state for PrivateLandlordPropertyStatus</exception>
        public PrivateLandlordPropertyStatus PrivateLandlordPropertyStatus
        {
            get
            {
                if (RowStatusId == 3)
                {
                    return PrivateLandlordPropertyStatus.Deleted;
                }
                if (!IsActive && ActivatedOn == null)
                {
                    return PrivateLandlordPropertyStatus.PayNow;
                }
                if (IsActive && !IsExpired)
                {
                    return PrivateLandlordPropertyStatus.Live;
                }
                if (!IsActive && !IsExpired)
                {
                    return PrivateLandlordPropertyStatus.NotActive;
                }
                return PrivateLandlordPropertyStatus.Expired;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [is expired].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is expired]; otherwise, <c>false</c>.
        /// </value>
        public bool IsExpired
        {
            get { return ExpiresOn.HasValue && ExpiresOn.Value <= DateTime.Now; }
        }


        /// <summary>
        /// Gets a value indicating whether [is configuration view bookable].
        /// </summary>
        /// <value>
        /// <c>true</c> if [is configuration view bookable]; otherwise, <c>false</c>.
        /// </value>
        public bool IsOnViewBookable
        {
            get
            {
                return ((PropertyClassId == 1 || PropertyClassId == 2) &&
                       PropertyStatusId == 2 && IsActive);
            }
        }


        /// <summary>
        /// Gets the index of the tag.
        /// </summary>
        /// <value>
        /// The index of the tag.
        /// </value>
        public string TagIndex
        {
            get
            {
                var indexTags = new Tags();
                if (PublicTags != null)
                    indexTags.AddRange(PublicTags);
                if (ComputedTags != null)
                    indexTags.AddRange(ComputedTags);
                return indexTags.Any() ? indexTags.Join("|") : null;
            }
        }

        /// <summary>
        /// Gets the index of the private tag.
        /// </summary>
        /// <value>
        /// The index of the private tag.
        /// </value>
        public string PrivateTagIndex
        {
            get
            {
                return PrivateTags != null
                    ? PrivateTags.Join("|")
                    : null;
            }
        }


        /// <summary>
        /// Gets or sets the promotional snippet.
        /// </summary>
        /// <value>
        /// The promotional snippet.
        /// </value>
        public string PromotionalSnippet
        {
            get { return CustomData != null ? CustomData.PromotionalSnippet : string.Empty; }
            set { if(CustomData != null) CustomData.PromotionalSnippet = value; }
        }

        /// <summary>
        /// Gets of sets and associated article url for the irish times articles
        /// </summary>
        public string AssociatedArticleUrl
        {
            get { return CustomData != null ? CustomData.AssociatedArticleUrl : string.Empty; }
            set { if (CustomData != null) CustomData.AssociatedArticleUrl = value; }
        }


        /// <summary>
        /// Gets the closed configuration original estimated closed configuration.
        /// </summary>
        /// <value>
        /// The closed configuration original estimated closed configuration.
        /// </value>
        public DateTime? ClosedOnOrEstimatedClosedOn
        {
            get
            {
                if (ClosedOn.HasValue) return ClosedOn;
                if (EstimatedClosedOn.HasValue) return EstimatedClosedOn;
                return null;
            }
        }

        /// <summary>
        /// Gets the time since activated.
        /// </summary>
        /// <value>
        /// The time since activated.
        /// </value>
        public String TimeSinceActivated
        {
            get { return ActivatedOn != null ? DateTime.Parse(ActivatedOn.ToString()).SimpleWhen() : "Not Activated"; }
        }

        /// <summary>
        /// Gets the time since refreshed.
        /// </summary>
        /// <value>
        /// The time since refreshed.
        /// </value>
        public String TimeSinceRefreshed
        {
            get { return ActivatedOn != null ? DateTime.Parse(RefreshedOn.ToString()).SimpleWhen() : ""; }
        }

        /// <summary>
        /// Gets the time since edited.
        /// </summary>
        /// <value>
        /// The time since edited.
        /// </value>
        public String TimeSinceEdited
        {
            get { return ModifiedOnDate.SimpleWhen(); }
        }

        /// <summary>
        /// Gets the display time since.
        /// </summary>
        /// <value>
        /// The display time since.
        /// </value>
        public String DisplayTimeSince
        {
            get
            {
                if (ModifiedOnDate > RefreshedOn)
                {
                    return "Edited " + TimeSinceEdited;
                }
                return "Refreshed " + TimeSinceRefreshed;
            }
        }

        /// <summary>
        /// Wills the expire information days.
        /// </summary>
        /// <param name="days">The days.</param>
        /// <returns></returns>
        public bool WillExpireInDays(int days)
        {
            return ExpiresOn.HasValue && ExpiresOn.Value <= DateTime.Now.AddDays(days);
        }

        /// <summary>
        /// Gets the property status changed date.
        /// </summary>
        /// <value>
        /// The property status changed date.
        /// </value>
        public DateTime? PropertyStatusChangedDate
        {
            get
            {
                switch (PropertyStatusId)
                {
                    case 16:
                        return InstructedOn;
                    case 15:
                        return ValuedOn;
                    case 2:
                        return ActivatedOn;
                    case 3:
                        return AgreedOn;
                    case 13:
                        return CancelledOn;
                    case 5:
                        return ClosedOn;
                    case 17:
                        return CreatedOnDate;
                }

                return InstructedOn;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [has been instructed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has been instructed]; otherwise, <c>false</c>.
        /// </value>
        public bool HasBeenInstructed
        {
            get
            {
                return PropertyClassId == 6 ? IsStatus(16, 2, 12, 3, 7, 4, 8, 9, 13) : IsStatus(16, 2, 3, 4, 9, 13);
            }
        }

        /// <summary>
        /// Determines whether the specified statuses is status.
        /// </summary>
        /// <param name="statuses">The statuses.</param>
        /// <returns></returns>
        public bool IsStatus(params int[] statuses)
        {
            return statuses.Contains(PropertyStatusId);
        }


        /// <summary>
        /// Gets the fee.
        /// </summary>
        /// <value>
        /// The fee.
        /// </value>
        public string Fee
        {
            get
            {
                if (FeeStructure != null)
                {
                    return FeeStructure.GetFee(Price.Value).ToCurrency();
                }
                return "-";
            }
        }

        /// <summary>
        /// Gets the fee plus vat.
        /// </summary>
        /// <value>
        /// The fee plus vat.
        /// </value>
        public string FeePlusVat
        {
            get
            {
                if (FeeStructure != null)
                {
                    var fee = FeeStructure.GetFee(Price.Value);
                    fee = fee + fee.Vat();
                    return fee.ToCurrency();
                }
                return "-";
            }
        }


        /// <summary>
        /// Extends the premium expiry.
        /// </summary>
        /// <param name="days">The days.</param>
        public void ExtendPremiumExpiry(int? days)
        {
            if (days.HasValue)
            {
                if (!PremiumAdExpiresOn.HasValue)
                    PremiumAdExpiresOn = DateTime.Now;
                PremiumAdExpiresOn = PremiumAdExpiresOn.Value.AddDays(days.Value).EndOfDay();
            }
        }

        /// <summary>
        /// Gets the sold price asynchronous string.
        /// </summary>
        /// <value>
        /// The sold price asynchronous string.
        /// </value>
        public string SoldPriceAsString
        {
            get
            {
                if (SoldPrice.HasValue)
                {
                    return string.Format("€{0:N0}", SoldPrice.Value);
                }
                return "";
            }
        }


        /// <summary>
        /// Gets a value indicating whether [has contact details].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has contact details]; otherwise, <c>false</c>.
        /// </value>
        public bool HasContactDetails
        {
            get { return ContactDetails != null; }
        }

        /// <summary>
        /// Gets a value indicating whether [is this your property upgrade available].
        /// </summary>
        /// <value>
        /// <c>true</c> if [is this your property upgrade available]; otherwise, <c>false</c>.
        /// </value>
        public bool IsThisYourPropertyUpgradeAvailable
        {
            get
            {
                return PropertyClassId == 1 && !IsPremium && !HasRelatedProperties &&
                       HasLocality;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [hide ads].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hide ads]; otherwise, <c>false</c>.
        /// </value>
        public bool HideAds
        {
            get { return IsPremium; }
        }

        /// <summary>
        /// Gets a value indicating whether [show ads].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show ads]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowAds
        {
            get { return !HideAds; }
        }


        /// <summary>
        /// Gets a value indicating whether [has related properties].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has related properties]; otherwise, <c>false</c>.
        /// </value>
        public bool HasRelatedProperties
        {
            get { return CustomData != null && CustomData.RelatedPropertyIDs != null && CustomData.RelatedPropertyIDs.Any(); }
        }

        /// <summary>
        /// Gets the related property attribute ids.
        /// </summary>
        /// <value>
        /// The related property attribute ds.
        /// </value>
        public IEnumerable<int> RelatedPropertyIDs
        {
            get { return CustomData != null ? CustomData.RelatedPropertyIDs : Enumerable.Empty<int>().ToList(); }
        }

        /// <summary>
        /// Gets a value indicating whether [is parent property].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is parent property]; otherwise, <c>false</c>.
        /// </value>
        public bool IsParentProperty
        {
            get
            {
                return (HasRelatedProperties &&
                        (RelatedPropertyIDs.Count() > 1 ||
                         RelatedPropertyIDs.Count() == 1 && PropertyClassId == 2));
            }
        }

        /// <summary>
        /// Gets a value indicating whether [can be automatic refreshed].
        /// </summary>
        /// <value>
        /// <c>true</c> if [can be automatic refreshed]; otherwise, <c>false</c>.
        /// </value>
        public bool CanBeAutoRefreshed
        {
            get
            {
                return (PropertyStatusId == 2 ||
                        PropertyStatusId == 12 ||
                        PropertyStatusId == 3) && HasLocality;
            }
        }

        /// <summary>
        /// Gets the minimum automatic refresh time window.
        /// </summary>
        /// <value>
        /// The minimum automatic refresh time window.
        /// </value>
        public TimeSpan MinAutoRefreshTimeWindow
        {
            get
            {
                if (PropertySection == PropertySection.Lettings)
                {
                    return 4.Hours();
                }
                return 4.Days();
            }
        }

        /// <summary>
        /// Gets the maximum automatic refresh time window.
        /// </summary>
        /// <value>
        /// The maximum automatic refresh time window.
        /// </value>
        public TimeSpan MaxAutoRefreshTimeWindow
        {
            get
            {
                if (PropertySection == PropertySection.Lettings)
                {
                    return 5.Hours();
                }
                return 6.Days();
            }
        }

        /// <summary>
        /// Determines whether [is valid CRM status change] [the specified property status].
        /// </summary>
        /// <param name="propertyStatus">The property status.</param>
        /// <returns></returns>
        public bool IsValidCrmStatusChange(PropertyStatusEnum propertyStatus)
        {
            if (PropertySection == PropertySection.Residential)
            {
                switch ((PropertyStatusEnum)PropertyStatusId)
                {
                    case PropertyStatusEnum.Appraisal:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.Valued, PropertyStatusEnum.Lost }.Contains(
                                propertyStatus);
                    case PropertyStatusEnum.Valued:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.Instructed, PropertyStatusEnum.Lost }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.Instructed:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.ForSale, PropertyStatusEnum.Cancelled }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.ForSale:
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.SaleAgreed,
                                PropertyStatusEnum.Cancelled,
                                PropertyStatusEnum.Withdrawn
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.SaleAgreed:
                        if ((!SoldPrice.HasValue || !FeeStructure.ActualFee.HasValue || !BuyerSolicitorId.HasValue)
                            && PropertyStatusId == (int)PropertyStatusEnum.SaleAgreed &&
                            propertyStatus == PropertyStatusEnum.SaleAgreed)
                            //NOTE: for existing properties which were not created with workflow
                            return true;
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.Sold,
                                PropertyStatusEnum.ForSale,
                                PropertyStatusEnum.Cancelled,
                                PropertyStatusEnum.Withdrawn
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.Sold:
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.ForSale,
                                PropertyStatusEnum.Cancelled,
                                PropertyStatusEnum.Closed,
                                PropertyStatusEnum.Withdrawn
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.Withdrawn:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.ForSale, PropertyStatusEnum.Cancelled }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.Closed:
                        return false;
                    default:
                        return false;
                }
            }
            if (PropertySection == PropertySection.Lettings)
            {
                switch ((PropertyStatusEnum)PropertyStatusId)
                {
                    case PropertyStatusEnum.Appraisal:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.Instructed, PropertyStatusEnum.Lost }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.Cancelled:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.Instructed }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.Instructed:
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.ToLet,
                                PropertyStatusEnum.Cancelled,
                                PropertyStatusEnum.Lost
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.ToLet:
                        return
                            new List<PropertyStatusEnum>
                                {
                                    PropertyStatusEnum.LetAgreed,
                                    PropertyStatusEnum.Cancelled,
                                    PropertyStatusEnum.Withdrawn
                                }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.LetAgreed:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.Let, PropertyStatusEnum.ToLet }.Contains(
                                propertyStatus);
                    case PropertyStatusEnum.Let:
                        return new List<PropertyStatusEnum> { PropertyStatusEnum.ToLet, PropertyStatusEnum.Withdrawn }.Contains(propertyStatus);
                    case PropertyStatusEnum.Withdrawn:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.ToLet, PropertyStatusEnum.Cancelled }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.Closed:
                        return false;
                    default:
                        return false;
                }
            }
            if (PropertySection == PropertySection.Commercial)
            {
                switch ((PropertyStatusEnum)PropertyStatusId)
                {
                    case PropertyStatusEnum.Appraisal:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.Valued, PropertyStatusEnum.Lost }.Contains(
                                propertyStatus);
                    case PropertyStatusEnum.Valued:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.Instructed, PropertyStatusEnum.Lost }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.Instructed:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.ForSale, PropertyStatusEnum.Cancelled }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.ForSale:
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.SaleAgreed,
                                PropertyStatusEnum.Cancelled,
                                PropertyStatusEnum.Withdrawn
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.ToLet:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.LetAgreed, PropertyStatusEnum.Cancelled }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.LetAgreed:
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.Let,
                                PropertyStatusEnum.ToLet,
                                PropertyStatusEnum.ForSale,
                                PropertyStatusEnum.Cancelled,
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.Let:
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.ToLet,
                                PropertyStatusEnum.Withdrawn,
                                PropertyStatusEnum.Cancelled,
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.ForSaleOrToLet:
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.LetAgreed,
                                PropertyStatusEnum.SaleAgreed,
                                PropertyStatusEnum.Cancelled,
                                PropertyStatusEnum.Withdrawn
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.SaleAgreed:
                        if ((!SoldPrice.HasValue || !FeeStructure.ActualFee.HasValue || !BuyerSolicitorId.HasValue)
                            && PropertyStatusId == (int)PropertyStatusEnum.SaleAgreed &&
                            propertyStatus == PropertyStatusEnum.SaleAgreed)
                            //NOTE: for existing properties which were not created with workflow
                            return true;
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.Sold,
                                PropertyStatusEnum.ForSale,
                                PropertyStatusEnum.Cancelled,
                                PropertyStatusEnum.Withdrawn
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.Sold:
                        return
                            new List<PropertyStatusEnum>
                            {
                                PropertyStatusEnum.ForSale,
                                PropertyStatusEnum.Cancelled,
                                PropertyStatusEnum.Closed,
                                PropertyStatusEnum.Withdrawn
                            }.Contains(propertyStatus);
                    case PropertyStatusEnum.Withdrawn:
                        return
                            new List<PropertyStatusEnum> { PropertyStatusEnum.ForSale, PropertyStatusEnum.Cancelled }
                                .Contains(propertyStatus);
                    case PropertyStatusEnum.Closed:
                        return false;
                    default:
                        return false;
                }
            }
            return false;
        }


        /// <summary>
        /// Sets the auction date time.
        /// </summary>
        /// <value>
        /// The auction date time.
        /// </value>
        public string AuctionDateTime
        {
            set { SetContent("Auction", string.Format("Auction in on {0}.", value)); }
        }

        /// <summary>
        /// Gets the price asynchronous string.
        /// </summary>
        /// <value>
        /// The price asynchronous string.
        /// </value>
        public override string PriceAsString
        {
            get
            {
                if (IsAuction)
                {
                    return "AMV: " + base.PriceAsString;
                }
                return base.PriceAsString;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [has negotiator].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has negotiator]; otherwise, <c>false</c>.
        /// </value>
        public bool HasNegotiator
        {
            get { return NegotiatorId != null; }
        }


        /// <summary>
        /// Gets a value indicating whether [has locality].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has locality]; otherwise, <c>false</c>.
        /// </value>
        public bool HasLocality
        {
            get { return LocalityId.HasValue; }
        }

        /// <summary>
        /// Gets a value indicating whether [has region].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has region]; otherwise, <c>false</c>.
        /// </value>
        public bool HasRegion
        {
            get { return RegionId.HasValue; }
        }

        /// <summary>
        /// Gets a value indicating whether [is overseas].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is overseas]; otherwise, <c>false</c>.
        /// </value>
        public bool IsOverseas
        {
            get
            {
                return PropertyClassId == 9 ||
                       PropertyClassId == 10;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [is lettings].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is lettings]; otherwise, <c>false</c>.
        /// </value>
        public bool IsLettings
        {
            get
            {
                return PropertyClassId == 3 ||
                       PropertyClassId == 4 ||
                       PropertyClassId == 5;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [has videos].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has videos]; otherwise, <c>false</c>.
        /// </value>
        public bool HasVideos
        {
            get { return Media.ByType(MediaItemType.Video).Count > 0; }
        }

        /// <summary>
        /// Gets a value indicating whether [has virtual tours].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has virtual tours]; otherwise, <c>false</c>.
        /// </value>
        public bool HasVirtualTours
        {
            get { return Media.ByType(MediaItemType.VirtualTour).Count > 0; }
        }

        /// <summary>
        /// Gets a value indicating whether [has floor plans].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has floor plans]; otherwise, <c>false</c>.
        /// </value>
        public bool HasFloorPlans
        {
            get { return Media.ByType(MediaItemType.FloorPlan).Count > 0; }
        }


        /// <summary>
        /// Gets a value indicating whether [has rooms].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has rooms]; otherwise, <c>false</c>.
        /// </value>
        public bool HasRooms
        {
            get { return Rooms != null && Rooms.Any(); }
        }

        /// <summary>
        /// Gets a value indicating whether [has public tags].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has public tags]; otherwise, <c>false</c>.
        /// </value>
        public bool HasPublicTags
        {
            get { return PublicTags != null && PublicTags.Any(); }
        }

        /// <summary>
        /// Gets a value indicating whether [has bathrooms].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has bathrooms]; otherwise, <c>false</c>.
        /// </value>
        public bool HasBathrooms
        {
            get { return Bathrooms != null; }
        }

        /// <summary>
        /// Gets a value indicating whether [has size].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has size]; otherwise, <c>false</c>.
        /// </value>
        public bool HasSize
        {
            get { return Size != null && Size.HasSize; }
        }

        /// <summary>
        /// Gets the bathrooms description.
        /// </summary>
        /// <value>
        /// The bathrooms description.
        /// </value>
        public string BathroomsDescription
        {
            get { return HasBathrooms ? Bathrooms.ToString() : ""; }
        }


        /// <summary>
        /// Determines whether this instance [can agent manage] the specified group unique identifier.
        /// </summary>
        /// <param name="groupId">The group unique identifier.</param>
        /// <returns></returns>
        public bool CanAgentManage(int groupId)
        {
            return GroupId == groupId && !IsPrivateLandlord;
        }

        /// <summary>
        /// Determines whether this instance [can private landlord edit] the specified group unique identifier.
        /// </summary>
        /// <param name="groupId">The group unique identifier.</param>
        /// <returns></returns>
        public bool CanPrivateLandlordEdit(int groupId)
        {
            return GroupId == groupId && IsPrivateLandlord;
        }


        /// <summary>
        /// Gets a value indicating whether [is public].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is public]; otherwise, <c>false</c>.
        /// </value>
        public bool IsPublic
        {
            get
            {
                if (RowStatusId != 2)
                    return false;

                if (!IsActive)
                    return false;

                return (PropertyStatusId.In(PublicPropertyStatuses));
            }
        }


        /// <summary>
        /// Gets a value indicating whether [is for sale original for rent].
        /// </summary>
        /// <value>
        /// <c>true</c> if [is for sale original for rent]; otherwise, <c>false</c>.
        /// </value>
        public bool IsForSaleOrForRent
        {
            get { return PropertyStatusId == 2 || PropertyStatusId == 11; }
        }

        /// <summary>
        /// Gets a value indicating whether [can property status be activated].
        /// </summary>
        /// <value>
        /// <c>true</c> if [can property status be activated]; otherwise, <c>false</c>.
        /// </value>
        public bool CanPropertyStatusBeActivated
        {
            get
            {
                return PropertyStatusId == 2 || PropertyStatusId == 11 ||
                       PropertyStatusId == 3 ||
                       PropertyStatusId == 12 ||
                       PropertyStatusId == 7;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [has been activated].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has been activated]; otherwise, <c>false</c>.
        /// </value>
        public bool HasBeenActivated
        {
            get { return ActivatedOn.HasValue; }
        }

        /// <summary>
        /// Gets a value indicating whether [has not been activated].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has not been activated]; otherwise, <c>false</c>.
        /// </value>
        public bool HasNotBeenActivated
        {
            get { return !HasBeenActivated; }
        }

        /// <summary>
        /// Gets a value indicating whether [can be activated].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [can be activated]; otherwise, <c>false</c>.
        /// </value>
        public bool CanBeActivated
        {
            get { return !IsActive && CanPropertyStatusBeActivated; }
        }

        /// <summary>
        /// Gets a value indicating whether [can be deleted].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [can be deleted]; otherwise, <c>false</c>.
        /// </value>
        public bool CanBeDeleted
        {
            get { return !IsActive; }
        }

        /// <summary>
        /// Gets a value indicating whether [can be refreshed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [can be refreshed]; otherwise, <c>false</c>.
        /// </value>
        public bool CanBeRefreshed
        {
            get
            {
                return IsPublic && PropertyClassId != 2 &&
                       (RefreshedOn == null || CanBeRefreshedOn <= DateTime.Now);
            }
        }

        /// <summary>
        /// Gets a value indicating whether [can book top spot].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [can book top spot]; otherwise, <c>false</c>.
        /// </value>
        public bool CanBookTopSpot
        {
            get { return IsPublic && IsForSaleOrForRent; }
        }

        /// <summary>
        /// Gets a value indicating whether [is premium].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is premium]; otherwise, <c>false</c>.
        /// </value>
        public bool IsPremium
        {
            get { return PremiumAdExpiresOn.HasValue && PremiumAdExpiresOn.Value > DateTime.Now; }
        }

        /// <summary>
        /// Gets a value indicating whether [is premium new home].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is premium new home]; otherwise, <c>false</c>.
        /// </value>
        public bool IsPremiumNewHome
        {
            get { return IsPremium && PropertyClassId == 2; }
        }

        /// <summary>
        /// Gets a value indicating whether [is featured].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is featured]; otherwise, <c>false</c>.
        /// </value>
        public bool IsFeatured
        {
            get { return FeaturedAdExpiresOn.HasValue && FeaturedAdExpiresOn.Value > DateTime.Now; }
        }

        /// <summary>
        /// Gets the can be refreshed configuration.
        /// </summary>
        /// <value>
        /// The can be refreshed configuration.
        /// </value>
        public DateTime CanBeRefreshedOn
        {
            get
            {
                if (PropertySection == PropertySection.Lettings || PropertySection == PropertySection.Overseas)
                    if (RefreshedOn == null)
                        return DateTime.Now;
                    else
                    {
                        if (IsPrivateLandlord)
                            return RefreshedOn.Value.AddHours(8);
                        return RefreshedOn.Value.AddDays(1).StartOfDay();
                    }
                return DateTime.Now;
            }
        }

        /// <summary>
        /// Gets the can be refreshed configuration text.
        /// </summary>
        /// <value>
        /// The can be refreshed configuration text.
        /// </value>
        public string CanBeRefreshedOnText
        {
            get
            {
                var when = CanBeRefreshedOn.When();
                if (when == "12:00am tomorrow")
                    return "tomorrow";
                return when;
            }
        }

        /// <summary>
        /// Gets the custom form data.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public string GetCustomFormData(string key, string defaultValue = "")
        {
            return CustomFormData == null ? "" : CustomFormData.Get(key, defaultValue);
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public string GetDescription(int length = 260)
        {
            return GetContent("Description").RemoveAllHtml().Take(length, "...");
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description
        {
            get { return GetContent("Description", false).RemoveAllHtml(); }
            set { SetContent("Description", value); }
        }

        /// <summary>
        /// Gets the features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public string Features
        {
            get { return GetContent("Features", false); }
        }

        /// <summary>
        /// Gets a value indicating whether [has description].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has description]; otherwise, <c>false</c>.
        /// </value>
        public bool HasDescription
        {
            get { return !string.IsNullOrWhiteSpace(GetContent("Description")); }
        }

        /// <summary>
        /// Gets the accommodation.
        /// </summary>
        /// <value>
        /// The accommodation.
        /// </value>
        public string Accommodation
        {
            get { return GetContent("Accommodation", false); }
        }

        /// <summary>
        /// Gets a value indicating whether [has accommodation].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has accommodation]; otherwise, <c>false</c>.
        /// </value>
        public bool HasAccommodation
        {
            get { return !string.IsNullOrWhiteSpace(GetContent("Accommodation")); }
        }

        /// <summary>
        /// Gets the ber details.
        /// </summary>
        /// <value>
        /// The ber details.
        /// </value>
        public string BerDetails
        {
            get { return GetContent("BER Details", false); }
        }

        /// <summary>
        /// Gets a value indicating whether [has ber details].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has ber details]; otherwise, <c>false</c>.
        /// </value>
        public bool HasBerDetails
        {
            get { return !string.IsNullOrWhiteSpace(GetContent("BER Details")); }
        }


        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return DisplayNameAndAddress;
        }
    }
}
