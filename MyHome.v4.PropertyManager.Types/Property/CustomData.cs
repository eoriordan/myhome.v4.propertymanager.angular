﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Property
{
    [DataContract]
    [Serializable]
    public class CustomData
    {
        public CustomData()
        {
            RelatedPropertyIDs = Enumerable.Empty<int>();
        }

        [DataMember]
        public IEnumerable<int> RelatedPropertyIDs { get; set; }

        [DataMember]
        public string PromotionalSnippet { get; set; }

        [DataMember]
        public int? JointAgentGroupId { get; set; }

        [DataMember]
        public string AssociatedArticleUrl { get; set; }

    }
}

