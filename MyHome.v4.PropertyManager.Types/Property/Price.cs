﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema.Enumerations;

namespace MyHome.v4.PropertyManager.Types.Property
{
    [DataContract]
    [Serializable]
    public class Price
    {
        public Price()
        {
            ShowPrice = true;
        }

        [DataMember]
        public decimal? MinPrice { get; set; }
        [DataMember]
        public decimal? MaxPrice { get; set; }
        [DataMember]
        public decimal? MinRentalPrice { get; set; }
        [DataMember]
        public decimal? MaxRentalPrice { get; set; }
        [DataMember]
        public decimal? MinValuation { get; set; }
        [DataMember]
        public decimal? MaxValuation { get; set; }

        /// <summary>
        /// The price peroid used for the 
        /// </summary>
        [DataMember]
        public PricePeriod? Period { get; set; }

        /// <summary>
        /// The lease term in days:
        /// 1 Days
        /// 2 Days
        /// 3 Days
        /// 4 Days
        /// 5 Days
        /// 6 Days
        /// 1 Weeks = 7 Days
        /// 2 Weeks = 14 Days
        /// 3 Weeks = 21 Days
        /// 4 Weeks = 28 Days
        /// 1 Month = 31 Days
        /// 2 Months = 62 Days
        /// 3 Months = 93 Days
        /// 4 Months = 124 Days
        /// 5 Months = 155 Days
        /// 6 Months = 186 Days
        /// 7 Months = 217 Days
        /// 8 Months = 248 Days
        /// 9 Months = 279 Days
        /// 10 Months = 310 Days
        /// 11 Months = 341 Days
        /// 1 Year = 365 Days
        /// 2 Years = 730 Days
        /// 3 Years = 1095 Days
        /// 4 Years = 1460 Days
        /// 5 Years = 1825 Days
        /// 6 Years = 2190 Days
        /// 7 Years = 2555 Days
        /// 8 Years = 2920 Days
        /// 9 Years = 3285 Days
        /// 10 Years = 3650 Days


        /// </summary>
        [DataMember]
        public int? LeaseTermInDays { get; set; }

        [DataMember]
        public decimal? Deposit { get; set; }
        [DataMember]
        public decimal? RentalYieldValue { get; set; }
        [DataMember]
        public string RentalYieldPriceAsString { get; set; }
        [DataMember]
        public decimal? AnnualServiceCharge { get; set; }
        [DataMember]
        public bool ShowPrice { get; set; }
        [DataMember]
        public DateTime? AvailableOn { get; set; }
        [DataMember]
        public LeaseTerm? LeaseTerm { get; set; }
        [DataMember]
        public RentalSizeUnit? RentalSizeUnit { get; set; }
        [DataMember]
        public PriceDescription? PriceDescription { get; set; }

        public decimal? Value
        {
            get
            {
                if (HasPriceRange) throw new ArgumentException("The price already has a range");
                return MinPrice;
            }
            set
            {
                MinPrice = value;
                MaxPrice = value;
            }
        }

        public decimal? RentalValue
        {
            get
            {
                if (HasRentalPriceRange) throw new ArgumentException("The rental price already has a range");
                return MinRentalPrice;
            }
            set
            {
                MinRentalPrice = value;
                MaxRentalPrice = value;
            }
        }

        private int GetYearlyMultiplier()
        {
            switch (Period)
            {
                case PricePeriod.Weekly:
                    return 52;
                case PricePeriod.Monthly:
                    return 12;
                case PricePeriod.Yearly:
                    return 1;
                case PricePeriod.Quarterly:
                    return 4;
                default:
                    throw new InvalidOperationException();
            }
        }

        public decimal TotalRentForYear
        {
            get
            {
                return GetYearlyMultiplier() * RentalValue.Value;
            }
        }

        public decimal? MinMonthlyRentalPrice
        {
            get
            {
                if (HasRentalPrice)
                    return MonthlyAdjustedPrice(MinRentalPrice, Period);
                else
                    return null;
            }
        }

        public decimal? MaxMonthlyRentalPrice
        {
            get
            {
                if (HasRentalPrice)
                {
                    return MonthlyAdjustedPrice(MaxRentalPrice, Period);
                }
                return null;
            }
        }

        private static decimal? MonthlyAdjustedPrice(decimal? price, PricePeriod? period)
        {
            if (!price.HasValue) { return null; }
            if (!period.HasValue) { return null; }

            switch (period.Value)
            {
                case PricePeriod.Weekly:
                    return price * 4;
                case PricePeriod.Monthly:
                    return price;
                case PricePeriod.Yearly:
                    return price / 12;
                case PricePeriod.Daily:
                    return price;
                case PricePeriod.Quarterly:
                    return price / 3;
                default:
                    throw new NotSupportedException("Invalid Period");
            }
        }

        public bool HasPrice { get { return HasSalePrice || HasRentalPrice; } }
        public bool HasSalePrice { get { return (MinPrice.HasValue && MinPrice.Value > 0) || (MaxPrice.HasValue && MaxPrice.Value > 0); } }
        public bool HasRentalPrice { get { return (MinRentalPrice.HasValue && MinRentalPrice.Value > 0) || (MaxRentalPrice.HasValue && MaxRentalPrice.Value > 0); } }
        public bool HasPublicPrice { get { return HasPublicSalePrice || HasPublicRentalPrice; } }
        public bool HasPublicSalePrice { get { return HasSalePrice && ShowPrice; } }
        public bool HasPublicRentalPrice { get { return HasRentalPrice && ShowPrice; } }

        public bool HasMinAndMaxPrice { get { return MinPrice.HasValue && MaxPrice.HasValue; } }
        public bool HasMinAndMaxRentalPrice { get { return MinRentalPrice.HasValue && MaxRentalPrice.HasValue; } }
        public bool HasPriceRange { get { return HasPrice && (MinPrice != MaxPrice); } }
        public bool HasRentalPriceRange { get { return HasPrice && (MinRentalPrice != MaxRentalPrice); } }

        public override string ToString()
        {
            if (!ShowPrice)
            {
                return "POA";
            }
            return PrivatePriceString;
        }

        public string PrivatePriceString
        {
            get
            {
                if (!HasPrice)
                {
                    return "POA";
                }

                var prices = new List<string>();
                if (HasPrice)
                    prices.Add(SalePriceString);
                if (HasRentalPrice)
                    prices.Add(RentalPriceString);

                prices.RemoveAll(p => p == "");

                return prices.Join(" or ");
            }
        }

        private string SalePriceString
        {
            get
            {
                if (!HasSalePrice)
                {
                    return "";
                }

                string value = "";
                if (!HasPriceRange)
                    value = MinPrice.ToShortCurrency();
                else if (!MinPrice.HasValue)
                    value = String.Format("to {0}", MaxPrice.ToCurrency(0));
                else if (!MaxPrice.HasValue)
                    value = String.Format("from {0}", MinPrice.ToCurrency(0));
                else
                    value = String.Format("{0} to {1}", MinPrice.ToCurrency(0), MaxPrice.ToCurrency(0));

                return value;
            }
        }

        private string RentalPriceString
        {
            get
            {
                if (!HasRentalPrice)
                {
                    return "";
                }

                string value = "";
                if (!HasRentalPriceRange)
                    value = MinRentalPrice.ToShortCurrency();
                else if (!MinRentalPrice.HasValue)
                    value = String.Format("to {0}", MaxRentalPrice.ToShortCurrency());
                else if (!MaxRentalPrice.HasValue)
                    value = String.Format("from {0}", MinRentalPrice.ToShortCurrency());
                else
                    value = String.Format("{0} to {1}", MinRentalPrice.ToShortCurrency(), MaxRentalPrice.ToShortCurrency());

                if (Period.HasValue)
                {
                    switch (Period.Value)
                    {
                        case PricePeriod.Weekly:
                            value += " / week";
                            break;
                        case PricePeriod.Monthly:
                            value += " / month";
                            break;
                        case PricePeriod.Yearly:
                            value += " / year";
                            break;
                        case PricePeriod.Daily:
                            value += " / day";
                            break;
                        case PricePeriod.Quarterly:
                            value += " / quarterly";
                            break;
                    }
                }

                if (RentalSizeUnit.HasValue)
                {
                    value += (RentalSizeUnit.Value == Types.Property.RentalSizeUnit.Meters ? " / m²" : " / ft²");
                }

                return value;
            }
        }


        public decimal? MinSaleOrRentalPrice(PropertyClassEnum propertyClass)
        {
            if (PropertyClass.IsRentalClass(propertyClass))
            {
                return MinRentalPrice;
            }
            return MinPrice;
        }

        public decimal? MaxSaleOrRentalPrice(PropertyClassEnum propertyClass)
        {
            if (PropertyClass.IsRentalClass(propertyClass))
            {
                return MaxRentalPrice;
            }
            return MaxPrice;
        }

        public int LeaseTermToDays(LeaseTerm term)
        {
            switch (term)
            {
                case Types.Property.LeaseTerm.LessThanSixMonths:
                    return (31 * 6) - 1;
                case Types.Property.LeaseTerm.SixMonths:
                    return (31 * 6);
                case Types.Property.LeaseTerm.NineMonths:
                    return (31 * 9);
                case Types.Property.LeaseTerm.TwelveMonths:
                    return 365;
            }
            return 365;
        }

        public LeaseTerm DaysToLeaseTerm(int days)
        {
            if (days < (31 * 6))
            {
                return Types.Property.LeaseTerm.LessThanSixMonths;
            }
            if (days == (31 * 6))
            {
                return Types.Property.LeaseTerm.SixMonths;
            }
            if (days > (31 * 6) && days < (31 * 9))
            {
                return Types.Property.LeaseTerm.NineMonths;
            }
            return Types.Property.LeaseTerm.TwelveMonths;
        }

        public string LeaseTermInDaysString
        {
            get
            {
                if (!LeaseTermInDays.HasValue || LeaseTermInDays.Value == 0)
                {
                    return string.Empty;
                }
                if (LeaseTermInDays.Value == 1)
                {
                    return "1 day";
                }
                if (LeaseTermInDays.Value > 1 && LeaseTermInDays.Value < 7)
                {
                    return string.Format("{0} days", LeaseTermInDays.Value);
                }
                if (LeaseTermInDays.Value < 31)
                {
                    var weeks = Convert.ToInt32(Math.Floor((double)LeaseTermInDays.Value / 7));
                    return string.Format("{0} week", weeks);
                }
                if (LeaseTermInDays.Value < 342)
                {
                    var months = Convert.ToInt32(Math.Floor((double)LeaseTermInDays.Value / 31));
                    return string.Format("{0} month", months);
                }
                var years = Convert.ToInt32(Math.Floor((double)LeaseTermInDays.Value / 365));
                return string.Format("{0} year", years);
            }
        }
    }

    [DataContract]
    public enum PricePeriod
    {
        [EnumMember]
        Weekly = 1,
        [EnumMember]
        Monthly = 2,
        [EnumMember]
        Yearly = 3,
        [EnumMember]
        Daily = 4,
        [EnumMember]
        Quarterly = 5
    }
    [DataContract]
    public enum RentalSizeUnit
    {
        [EnumMember]
        Meters = 1,
        [EnumMember]
        Feet = 2
    }
    [DataContract]
    public enum LeaseTerm
    {
        [EnumMember]
        LessThanSixMonths = 1,
        [EnumMember]
        SixMonths = 2,
        [EnumMember]
        NineMonths = 3,
        [EnumMember]
        TwelveMonths = 4
    }
    [DataContract]
    public enum PriceDescription
    {
        [EnumMember]
        AskingPrice = 1,
        [EnumMember]
        AdvicedMinimumValue = 2
    }
}
