﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Extensions.Attributes;
using MyHome.v4.Common.Schema;

namespace MyHome.v4.PropertyManager.Types.Property
{
    /// <summary>
    /// Poco used to search properties
    /// </summary>
    [DataContract]
    [Serializable]
    public class GetPropertiesBySearch
    {
        /// <summary>
        /// Init
        /// </summary>
        public GetPropertiesBySearch()
        {
            IsActive = null;
            IsBackendSearch = false;
            IsAuction = false;
            PropertyIds = new List<int>();
            GroupIds = new List<int>();
            ChannelIds = new List<int> { 1 };
            PropertyTypeIds = new List<int>();
            PropertyClassIds = new List<int>();
            PropertyStatusIds = new List<int>();
            LocalityIds = new List<int>();
            SaleTypeIds = new List<int>();
            NegotiatorIds = new List<int>();
            SolicitorIds = new List<int>();
            BuyerSolicitorIds = new List<int>();
            VendorSolicitorIds = new List<int>();
            TransferedByUserIds = new List<int>();
            RowStatusIds = new List<int> { 2 };
            EnergyRatings = new List<string>();
            Tags = new List<string>();
            PrivateTags = new List<string>();
            Polygons = new List<MapPolygon>();
        }

        /// <summary>
        /// Flag denoting whether the search is coming from the back or front end
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool IsBackendSearch { get; set; }

        /// <summary>
        /// Flag denoting whether to skip the elastic search index and search the database directly
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool SkipSearchIndex { get; set; }

        /// <summary>
        /// Flag denoting whether to return in active propertioes fro groups
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool IsGroupPrivateSearch { get; set; }

        /// <summary>
        /// Flag denoting whether to return sale agreed properties
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool IsSaleAgreed { get; set; }

        /// <summary>
        /// Flag denoting whether to return sold properties
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool IsSold { get; set; }

        /// <summary>
        /// Flag denoting whether to return auction properties
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool IsAuction { get; set; }

        /// <summary>
        /// Flag denoting whether to return sold properties
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool IsBoundsSearch { get; set; }

        /// <summary>
        /// Flag denoting whether to use free text for keywords
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool UseFreeTextSearchForKeywords { get; set; }

        /// <summary>
        /// Flaf denoting whether to search content
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool SearchContent { get; set; }

        /// <summary>
        /// Flag denoting whether to request has keywords
        /// </summary>
        [NotDatabaseSearchMember]
        public bool HasKeywords { get { return !String.IsNullOrWhiteSpace(Query); } }

        /// <summary>
        /// Comtains keywords used for search
        /// </summary>
        [DataMember]
        [FieldName("Address")]
        public string Query { get; set; }

        /// <summary>
        /// The type of date to search by
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public string SearchByDate { get; set; }

        /// <summary>
        /// the from for the searchable date
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public DateTime? FromDate { get; set; }

        /// <summary>
        /// The to for the searchable date
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public DateTime? ToDate { get; set; }

        /// <summary>
        /// the from for the searchable date
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        [FieldName("CreatedOn")]
        public DateTime? CreatedOnFrom
        {
            get
            {
                if (!string.IsNullOrEmpty(SearchByDate) && SearchByDate == "Created")
                {
                    if (FromDate.HasValue)
                    {
                        return FromDate;
                    }
                    return null;
                }
                return null;
            }
        }

        /// <summary>
        /// The to for the searchable date
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        [FieldName("CreatedOn")]
        public DateTime? CreatedOnTo
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Created") return null;
                return ToDate.HasValue ? ToDate : null;
            }
        }

        /// <summary>
        /// the from for the searchable date
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        [FieldName("ActivatedOn")]
        public DateTime? ActivatedOnFrom
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Activated") return null;
                return FromDate.HasValue ? FromDate : null;
            }
        }

        /// <summary>
        /// The to for the searchable date
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        [FieldName("ActivatedOn")]
        public DateTime? ActivatedOnTo
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Activated") return null;
                return ToDate.HasValue ? ToDate : null;
            }
        }

        /// <summary>
        /// the from for the searchable date
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        [FieldName("AgreedOn")]
        public DateTime? AgreedFrom
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Agreed") return null;
                return FromDate.HasValue ? FromDate : null;
            }
        }

        /// <summary>
        /// The to for the searchable date
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        [FieldName("AgreedOn")]
        public DateTime? AgreedTo
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Agreed") return null;
                return ToDate.HasValue ? ToDate : null;
            }
        }

        /// <summary>
        /// the from for the searchable date
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        [FieldName("SoldOn")]
        public DateTime? SoldFrom
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Sold") return null;
                return FromDate.HasValue ? FromDate : null;
            }
        }

        /// <summary>
        /// The to for the searchable date
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        [FieldName("SoldOn")]
        public DateTime? SoldTo
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Sold") return null;
                return ToDate.HasValue ? ToDate : null;
            }
        }

        /// <summary>
        /// the from for the searchable date
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        [FieldName("ClosedOn")]
        public DateTime? ClosedFrom
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Closed") return null;
                return FromDate.HasValue ? FromDate : null;
            }
        }

        /// <summary>
        /// The to for the searchable date
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        [FieldName("SoldOn")]
        public DateTime? ClosedTo
        {
            get
            {
                if (string.IsNullOrEmpty(SearchByDate) || SearchByDate != "Closed") return null;
                return ToDate.HasValue ? ToDate : null;
            }
        }

        /// <summary>
        /// A list of Unique property primary keys associated with the properties we wish to return.
        /// </summary>
        [DataMember]
        public List<int> PropertyIds { get; set; }

        /// <summary>
        /// A list of Unique group primary keys associated with the properties we wish to return.
        /// </summary>
        [DataMember]
        public List<int> GroupIds { get; set; }

        /// <summary>
        /// A list of IDs for the channels we wish to return Properties through 
        /// </summary>
        [DataMember]
        public List<int> ChannelIds { get; set; }

        /// <summary>
        /// A list of unique property type Ids for types we wish returned properties to be associated with 
        /// </summary>
        [DataMember]
        public List<int> PropertyTypeIds { get; set; }

        /// <summary>
        /// A list of unique property class Ids for classes we wish returned properties to be associated with 
        /// </summary>
        [DataMember]
        public List<int> PropertyClassIds { get; set; }

        /// <summary>
        /// A list of property status Ids for statuses wish returned properties to be associated with
        /// </summary>
        [DataMember]
        public List<int> PropertyStatusIds { get; set; }

        /// <summary>
        /// A list of sale type Ids for types which returned properties should be associated with
        /// </summary>
        [DataMember]
        public List<int> SaleTypeIds { get; set; }

        /// <summary>
        /// The single region id that the propery can be associated with, this will get expanded to locality ids any way
        /// so it best to use the locality ids collection
        /// </summary>
        [DataMember]
        public int? RegionId { get; set; }

        /// <summary>
        /// A list of locality Ids for localities returned properties should be associated with
        /// </summary>
        [DataMember]
        public List<int> LocalityIds { get; set; }

        /// <summary>
        /// A list of negotiater (User) Ids for negotiators returned properties should be associated with
        /// </summary>
        [DataMember]
        public List<int> NegotiatorIds { get; set; }

        /// <summary>
        /// A list of solicitor (User) Ids for associated with returned properties
        /// </summary>
        [DataMember]
        public List<int> SolicitorIds { get; set; }

        /// <summary>
        /// A list of buyer solictor (User) Ids for associated with returned properties
        /// </summary>
        [DataMember]
        public List<int> BuyerSolicitorIds { get; set; }

        /// <summary>
        /// A list of vendor solictor (User) Ids for associated with returned properties
        /// </summary>
        [DataMember]
        public List<int> VendorSolicitorIds { get; set; }

        /// <summary>
        /// A list of user Ids for user(s) which transferred properties that should be returned
        /// </summary>
        [DataMember]
        public List<int> TransferedByUserIds { get; set; }

        /// <summary>
        /// A list of row status Ids for row statuses we wish returned properties to be associated with
        /// </summary>
        [DataMember]
        public List<int> RowStatusIds { get; set; }

        /// <summary>
        /// The date propertie(s) returned expire from
        /// </summary>
        [DataMember]
        public DateTime? ExpiresOn { get; set; }

        /// <summary>
        /// The date propertie(s) returned expire from
        /// </summary>
        [DataMember]
        public string Address { get; set; }

        /// <summary>
        /// The date propertie(s) are/were activated from
        /// </summary>
        [DataMember]
        public DateTime? ActivatedOn { get; set; }

        /// <summary>
        /// Date returned properties should be agreed from
        /// </summary>
        [DataMember]
        public DateTime? AgreedOn { get; set; }

        /// <summary>
        /// Return properties where the premium ad expiry date is later than this
        /// </summary>
        [DataMember]
        public DateTime? PremiumAdExpired { get; set; }

        /// <summary>
        /// Return properties where the Created On Date date is later than this
        /// </summary>
        [DataMember]
        public DateTime? CreatedOnDate { get; set; }

        /// <summary>
        /// Search by the CreatedByUserId.
        /// </summary>
        [DataMember]
        public int? CreatedByUserId { get; set; }

        /// <summary>
        /// Return properties where the modified date is later than this
        /// </summary>
        [DataMember]
        public DateTime? ModifiedOnDate { get; set; }

        /// <summary>
        /// Return properties where the Estimated Closed date is later than this
        /// </summary>
        [DataMember]
        public DateTime? EstimatedClosed { get; set; }

        /// <summary>
        /// Return properties where the Closed On date is later than this
        /// </summary>
        [DataMember]
        public DateTime? ClosedOn { get; set; }

        /// <summary>
        /// Return properties where the Instructed On date is later than this
        /// </summary>
        [DataMember]
        public DateTime? InstructedOn { get; set; }

        /// <summary>
        /// Return properties where the Cancelled On date is later than this
        /// </summary>
        [DataMember]
        public DateTime? CancelledOn { get; set; }


        /// <summary>
        /// Return properties where the Sold On date is later than this
        /// </summary>
        [DataMember]
        public DateTime? SoldOn { get; set; }

        /// <summary>
        /// Return properties where the Valued On is later than this
        /// </summary>
        [DataMember]
        public DateTime? ValuedOn { get; set; }

        /// <summary>
        /// A list of energy ratings that returned properties should be associated with 
        /// </summary>
        [DataMember]
        public List<string> EnergyRatings { get; set; }

        /// <summary>
        /// The minimum energy rating to retunr properties for
        /// </summary>
        [DataMember]
        public int? MinEnergyRating { get; set; }

        /// <summary>
        /// The minimum energy rating to retunr properties for
        /// </summary>
        [DataMember]
        public int? MaxEnergyRating { get; set; }

        /// <summary>
        /// The minimum energy rating to retunr properties for
        /// </summary>
        [DataMember]
        public int? MinLeaseTermInDays { get; set; }

        /// <summary>
        /// The minimum energy rating to retunr properties for
        /// </summary>
        [DataMember]
        public int? MaxLeaseTermInDays { get; set; }

        /// <summary>
        /// Polygons containing map points denoting an area to search for properties within.
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public List<MapPolygon> Polygons { get; set; }

        /// <summary>
        /// Co-ordinates for bound searching
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public double? NeLat { get; set; }

        [DataMember]
        [NotDatabaseSearchMember]
        public double? NeLng { get; set; }

        [DataMember]
        [NotDatabaseSearchMember]
        public double? SwLat { get; set; }

        [DataMember]
        [NotDatabaseSearchMember]
        public double? SwLng { get; set; }

        /// <summary>
        /// A list of tags to search for properties by
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public List<string> Tags { get; set; }

        /// <summary>
        /// A list of private tags (shown only internally) to search for properties by
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public List<string> PrivateTags { get; set; }

        /// <summary>
        /// Flaf denoting whether to return only pre-sixty properties.
        /// </summary>
        [DataMember]
        public bool? PreSixtyThree { get; set; }

        /// <summary>
        /// Flag denoting whether to retunr only new build properties.
        /// </summary>
        [DataMember]
        public bool? IsNewBuild { get; set; }

        /// <summary>
        /// Flag deonting whether to return properties which have a premium ad
        /// </summary>
        [DataMember]
        public bool? IsPremiumAd { get; set; }

        /// <summary>
        /// Flag denoting whether to return only active properties
        /// true - return active ONLY /  false - ignore in search
        /// </summary>
        [DataMember]
        public bool? IsActive { get; set; }

        /// <summary>
        /// Flag denoting whether to return only Auction Properties.
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool HasPhotos { get; set; }

        /// <summary>
        /// Flag to denote whether to return properties by private landlord
        /// </summary>
        [DataMember]
        public bool? IsPrivateLandlord { get; set; }

        /// <summary>
        /// Flag denoting whether to return properties which have a map
        /// </summary>
        [DataMember]
        [NotDatabaseSearchMember]
        public bool? HasMap { get; set; }

        /// <summary>
        /// The property reference to return a property for
        /// </summary>
        [DataMember]
        public string PropertyReference { get; set; }

        /// <summary>
        /// The minimum price to return properties for
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        [NotDatabaseSearchMember]
        public int? MinPrice { get; set; }

        /// <summary>
        /// The minimum price to return properties for
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        [NotDatabaseSearchMember]
        public int? MaxPrice { get; set; }

        /// <summary>
        /// The maximum baths to return properties for
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        public int? MaxBathromms { get; set; }

        /// <summary>
        /// The minimum baths to return properties for
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        public int? MinBathrooms { get; set; }


        /// <summary>
        /// The minimum price to return properties for
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        public int? MinSortPrice
        {
            get { return MinPrice.HasValue ? MinPrice : null; }
        }

        /// <summary>
        /// The maximum price to return properties for
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        public int? MaxSortPrice
        {
            get { return MaxPrice.HasValue ? MaxPrice : null; }
        }

        /// <summary>
        /// The minimum bed count to return properties for
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        public int? MinBeds { get; set; }

        /// <summary>
        /// The maximum bed count to return properties for
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        public int? MaxBeds { get; set; }

        /// <summary>
        /// Minimum size to return properties for
        /// </summary>
        [DataMember]
        [GreaterThanOrEqualTo]
        public int? MinSize { get; set; }

        /// <summary>
        /// Maximum size to return properties for
        /// </summary>
        [DataMember]
        [LessThanOrEqualTo]
        public int? MaxSize { get; set; }

        /// <summary>
        /// Checks returning whether search has price/size/beds
        /// </summary>
        [NotDatabaseSearchMember]
        public bool HasPrice { get { return MinPrice.HasValue || MaxSortPrice.HasValue; } }

        [NotDatabaseSearchMember]
        public bool HasBeds { get { return MinBeds.HasValue || MaxBeds.HasValue; } }

        [NotDatabaseSearchMember]
        public bool HasSize { get { return MinSize.HasValue || MaxSize.HasValue; } }

        [DataMember]
        public DateTime? AgreedOnFrom { get; set; }

        [DataMember]
        public DateTime? AgreedOnTo { get; set; }


        /// <summary>
        /// Gets the heading for price for the returned results
        /// </summary>
        [NotDatabaseSearchMember]
        public string PriceHeading
        {
            get
            {
                if (!HasPrice)
                    return "All";
                if (MinPrice.HasValue && MaxSortPrice.HasValue)
                    return String.Format("€{0} -> €{1}", MinPrice.Value.ToShortNumber(), MaxSortPrice.Value.ToShortNumber());
                if (MinPrice.HasValue)
                    return String.Format("€{0}+", MinPrice.Value.ToShortNumber());
                return MaxSortPrice != null ? String.Format("€0 -> €{0}", MaxSortPrice.Value.ToShortNumber()) : "";
            }
        }

        /// <summary>
        /// Gets the heading for beds for the returned results
        /// </summary>
        [NotDatabaseSearchMember]
        public string BedsHeading
        {
            get
            {
                if (!HasBeds)
                    return "All";
                if (MinBeds.HasValue && MaxBeds.HasValue)
                    return String.Format("{0} -> {1} beds", MinBeds.Value.ToShortNumber(), MaxBeds.Value.ToShortNumber());
                if (MinBeds.HasValue)
                    return String.Format("{0}+ beds", MinBeds.Value.ToShortNumber());
                return MaxBeds != null ? String.Format("0 -> {0} beds", MaxBeds.Value.ToShortNumber()) : "";
            }
        }

        /// <summary>
        /// Gets the heading for pre 63 and energy rating for returned results
        /// </summary>
        [NotDatabaseSearchMember]
        public string OtherHeading
        {
            get
            {
                var others = new List<string>();
                if (MinEnergyRating.HasValue)
                {
                    others.Add(String.Format("{0}+", MinEnergyRating));
                }
                if (PreSixtyThree != null)
                {
                    if (PreSixtyThree.Value)
                    {
                        others.Add("Pre '63");
                    }
                }
                return others.Join(", ");
            }
        }

        /// <summary>
        /// Gets the size heading for returned results
        /// </summary>
        [NotDatabaseSearchMember]
        public string SizeHeading
        {
            get
            {
                if (!HasSize)
                    return "All";
                if (MinSize.HasValue && MaxSize.HasValue)
                    return String.Format("{0}m² - {1}m²", MinSize.Value.ToShortNumber(), MaxSize.Value.ToShortNumber());
                if (MinSize.HasValue)
                    return String.Format("{0}m²+", MinSize.Value.ToShortNumber());
                return MaxSize != null ? String.Format("0m² - {0}m²", MaxSize.Value.ToShortNumber()) : "";
            }
        }
    }
}
