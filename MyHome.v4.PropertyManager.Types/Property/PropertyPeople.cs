﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using MyHome.v4.PropertyManager.Types.Contacts;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Types.Property
{
    public class PropertyPeople
    {
        /// <summary>
        /// 
        /// </summary>
        public PropertyPeople()
        {
            PropertyContactWithContacts = new List<PropertyContactWithContact>();
        }

        /// <summary>
        /// Gets or sets the property contacts with a contact attached
        /// </summary>
        [DataMember]
        public IEnumerable<PropertyContactWithContact> PropertyContactWithContacts { get; set; }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public User Negotiator { get; set; }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public Contact Vendor { get; set; }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public Contact VendorSolicitor { get; set; }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public Contact Buyer { get; set; }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public Contact BuyerSolicitor { get; set; }


        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public string NegotiatorName
        {
            get
            {
                if (Negotiator != null)
                {
                    return Negotiator.DisplayName;
                }
                return "";
            }
        }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public string BuyerName
        {
            get
            {
                if (Buyer != null && Buyer.ContactDetails != null && Buyer.ContactDetails.HasName)
                {
                    return Buyer.ContactDetails.DisplayName;
                }
                return "";
            }
        }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public string VendorName
        {
            get
            {
                if (Vendor != null && Vendor.ContactDetails != null && Vendor.ContactDetails.HasName)
                {
                    return Vendor.ContactDetails.DisplayName;
                }
                return "";
            }
        }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public string VendorSolicitorName
        {
            get
            {
                if (VendorSolicitor != null && VendorSolicitor.ContactDetails != null && VendorSolicitor.ContactDetails.HasName)
                {
                    return VendorSolicitor.ContactDetails.DisplayName;
                }
                return "";
            }
        }

        /// <summary>
        /// Gets or sets the negotiator unique identifier.
        /// </summary>
        /// <value>
        /// The negotiator unique identifier.
        /// </value>
        [DataMember]
        public string BuyerSolicitorName
        {
            get
            {
                if (BuyerSolicitor != null && BuyerSolicitor.ContactDetails != null && BuyerSolicitor.ContactDetails.HasName)
                {
                    return BuyerSolicitor.ContactDetails.DisplayName;
                }
                return "";
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether venfor is null
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is new build]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool HasVendor
        {
            get { return Vendor != null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether buyer is null.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is new build]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool HasBuyer
        {
            get { return Buyer != null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether buyer is null.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is new build]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool HasVendorSolicitor
        {
            get { return VendorSolicitor != null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether buyer is null.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is new build]; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool HasBuyerSolicitor
        {
            get { return BuyerSolicitor != null; }
        }

        [DataMember]
        public bool HasPropertyContacts {
            get { return PropertyContactWithContacts.Any(); } }
    }
}
