﻿using MyHome.v4.Common.Schema.Enumerations;

namespace MyHome.v4.PropertyManager.Types.Property
{
    public class SaleType
    {
        public int SaleTypeId { get; set; }

        public string Name { get; set; }

        public PropertyClassEnum PropertyClass { get; set; }

        public string UrlSlug { get; set; }

        public string DescriptionLabelOverride { get; set; }
    }
}
