﻿using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Property
{
    public class GeoLocation
    {
        public GeoLocation()
        {

        }

        public GeoLocation(float latitude, float longitude)
        {
            lat = latitude;
            lon = longitude;
        }

        /// <summary>
        /// The latitude of the location
        /// </summary>
        [DataMember]
        // ReSharper disable once InconsistentNaming
        public float lat { get; set; }

        /// <summary>
        /// The longitude of the location
        /// </summary>
        [DataMember]
        // ReSharper disable once InconsistentNaming
        public float lon { get; set; }
    }
}

