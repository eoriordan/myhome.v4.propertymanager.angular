﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;

namespace MyHome.v4.PropertyManager.Types.Property
{
    [DataContract]
    [Serializable]
    public class FeeStructure
    {
        public FeeStructure()
        {
            Rules = Enumerable.Empty<FeeStructureRule>();
        }

        [DataMember]
        public IEnumerable<FeeStructureRule> Rules { get; set; }
        [DataMember]
        public decimal? FeeDiscount { get; set; }
        [DataMember]
        public decimal? ActualFee { get; set; }
        [DataMember]
        public decimal? ActualFeePercentage { get; set; }

        public decimal? CorrectedActualFeePercentage
        {
            get
            {
                if (ActualFeePercentage == null) return null;
                const decimal threshold = 0.02M; // 0.2 of a percent
                return ActualFeePercentage < threshold ?
                    ActualFeePercentage * 100.0M :
                    ActualFeePercentage;
            }
        }

        public override string ToString()
        {
            var text = Rules.Select(r => r.ToString()).Join(" | ");

            if (FeeDiscount.HasValue)
                text += String.Format(" (with {0} discount)", FeeDiscount.Value.ToCurrency());

            return text;
        }

        public decimal GetFee(decimal? propertyAmount)
        {
            return propertyAmount.HasValue ? GetFee(propertyAmount.Value) : 0;
        }

        public decimal GetFee(decimal propertyAmount)
        {
            var rule = Rules.FirstOrDefault(r => r.IsMatch(propertyAmount));
            var fee = rule == null ? 0 : rule.GetFee(propertyAmount);

            if (FeeDiscount.HasValue)
                fee -= FeeDiscount.Value;

            return fee;
        }

        public FeeStructureRule GetFeeRule(decimal propertyAmount)
        {
            return Rules.FirstOrDefault(r => r.IsMatch(propertyAmount));
        }

        public FeeStructureRule GetFeeRule(FeeType type)
        {
            return Rules.SingleOrDefault(rule => rule.FeeType == type);
        }

        public bool IsLetOnly
        {
            get { return GetFeeRule((FeeType)FeeType.ManagementFee) == null; }
        }
    }

    [DataContract]
    [Serializable]
    public class FeeStructureRule
    {
        [DataMember]
        public int? LowPrice { get; set; }
        [DataMember]
        public int? HighPrice { get; set; }

        [DataMember]
        public decimal? PercentageFee { get; set; }
        [DataMember]
        public decimal? FlatFee { get; set; }

        [DataMember]
        public FeeFrequency? Frequency { get; set; }
        [DataMember]
        public FeeType FeeType { get; set; }

        public bool IsMatch(decimal propertyAmount)
        {
            // ReSharper disable once PossibleInvalidOperationException
            var match = !LowPrice.HasValue && propertyAmount < LowPrice.Value;
            if (HighPrice.HasValue && propertyAmount >= HighPrice.Value) match = false;
            return match;
        }

        public decimal GetFee(decimal propertyAmount)
        {
            if (!IsMatch(propertyAmount))
                throw new ArgumentException("This fee rule is not applicable to the property amount");
            if (!PercentageFee.HasValue && !FlatFee.HasValue)
                throw new ArgumentException("This fee rule does not have a PercentageFee or FlatFee");
            return FlatFee.HasValue ? FlatFee.Value : (propertyAmount * PercentageFee.Value) / 100;
        }

        public override string ToString()
        {
            string value = CriteriaAsText;

            if (PercentageFee.HasValue || FlatFee.HasValue)
            {
                if (!String.IsNullOrEmpty(value))
                    value += " => ";

                value += FeeTypeAsText + (String.IsNullOrEmpty(FeeTypeAsText) ? "" : ": ");
                value += FeeAsText;
            }
            return value;
        }

        public string CriteriaAsText
        {
            get
            {
                string value = "";
                if (LowPrice.HasValue && HighPrice.HasValue)
                    value += String.Format("From {0} to {1}", LowPrice.Value.AsCurrency(), HighPrice.Value.AsCurrency());
                else if (LowPrice.HasValue)
                    value += String.Format("From {0}", LowPrice.Value.AsCurrency());
                else if (HighPrice.HasValue)
                    value += String.Format("Up to {0}", HighPrice.Value.AsCurrency());
                return value;
            }
        }

        private string Denominator
        {
            get { return FeeType == FeeType.SalesFee ? "Total Sale Price" : "rent"; }
        }

        private string FeeTypeAsText
        {
            get
            {
                switch (FeeType)
                {
                    case FeeType.SalesFee:
                        return "";
                    case FeeType.LettingFee:
                        return "Letting Fee";
                    case FeeType.ManagementFee:
                        return "Management Fee";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public string FrequencyText
        {
            get
            {
                switch (Frequency)
                {
                    case null:
                    case FeeFrequency.Single:
                        return null;
                    case FeeFrequency.Weekly:
                        return " per week";
                    case FeeFrequency.Monthly:
                        return " per month";
                    case FeeFrequency.Quarterly:
                        return " per quarter";
                    case FeeFrequency.Biannually:
                        return " biannually";
                    case FeeFrequency.Yearly:
                        return " per year";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public string FeeAsText
        {
            get
            {
                if (PercentageFee.HasValue)
                {
                    return String.Format("{0}% of {1}{2}", PercentageFee, Denominator, FrequencyText);
                }
                if (FlatFee.HasValue)
                {
                    if (FeeType == FeeType.SalesFee && Frequency == null || Frequency == FeeFrequency.Single)
                    {
                        return String.Format("{0} Flat Fee", FlatFee.Value.AsCurrency());
                    }
                    return String.Format("{0}{1}", FlatFee.Value.AsCurrency(), FrequencyText);
                }
                return "No fee";
            }
        }

        private int FromYearsRentDivisor
        {
            get
            {
                switch (Frequency)
                {
                    case FeeFrequency.Yearly:
                        return 1;
                    case FeeFrequency.Biannually:
                        return 2;
                    case FeeFrequency.Quarterly:
                        return 4;
                    case FeeFrequency.Monthly:
                        return 12;
                    case FeeFrequency.Weekly:
                        return 52;
                    case FeeFrequency.Single:
                        return 1;
                    case null:
                        return 1;
                    default:
                        throw new InvalidOperationException();
                }
            }
        }

        public decimal GetFee(Price price)
        {
            if (price.RentalValue.HasValue)
            {
                if (PercentageFee.HasValue)
                {
                    return price.TotalRentForYear / FromYearsRentDivisor * PercentageFee.Value / 100M;
                }
                if (FlatFee.HasValue)
                {
                    return FlatFee.Value;
                }
                return 0;
            }
            // ReSharper disable once PossibleInvalidOperationException
            return GetFee(price.Value.Value);
        }
    }

    [DataContract]
    public enum FeeFrequency
    {
        [EnumMember]
        Single = 1,
        [EnumMember]
        Weekly = 2,
        [EnumMember]
        Monthly = 3,
        [EnumMember]
        Quarterly = 4,
        [EnumMember]
        Biannually = 5,
        [EnumMember]
        Yearly = 6
    }
}
