﻿using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Property
{
    [DataContract]
    public enum TopSpotLevel
    {
        [EnumMember]
        NotAvailable,
        [EnumMember]
        Standard,
        [EnumMember]
        Premium
    }

    [DataContract]
    public enum FeeType
    {
        [EnumMember]
        SalesFee = 0,
        [EnumMember]
        LettingFee = 1,
        [EnumMember]
        ManagementFee = 2
    }

    [DataContract]
    public enum PropertyContentSourceTypeEnum
    {
        [EnumMember]
        AgentWebSite = 2,
        [EnumMember]
        Brochure = 3,
        [EnumMember]
        MyHome = 1
    }

    [DataContract]
    public enum PrivateLandlordPropertyStatus
    {
        [EnumMember]
        PayNow = 1,
        [EnumMember]
        Live = 2,
        [EnumMember]
        NotActive = 3,
        [EnumMember]
        Expired = 4,
        [EnumMember]
        Deleted = 5
    }
}
