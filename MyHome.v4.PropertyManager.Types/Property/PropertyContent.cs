﻿using System;

namespace MyHome.v4.PropertyManager.Types.Property
{
    /// <summary>
    /// The property content object stores the description by type for a property in a normalised form
    /// </summary>
    [Serializable]
    public class PropertyContent
    {

        /// <summary>
        /// The unique id for the property content item
        /// </summary>
        public int PropertyContentId { get; set; }

        /// <summary>
        /// The unique is for the property to which the content is associated
        /// </summary>
        public int PropertyId { get; set; }

        /// <summary>
        /// The property content source type for which to assign the content
        /// </summary>
        public PropertyContentSourceTypeEnum PropertyContentSourceType { get; set; }

        /// <summary>
        /// The content type description display to the user eg. Descroption, Features etc
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// The display order of the text in a collection keyed by the content type field
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// The text of the property content to display to the user based on the agent input
        /// </summary>
        public string Content { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

    }
}
