﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.Types.FeaturedProducts;
using MyHome.v4.PropertyManager.Web.Helpers;
using Group = MyHome.v4.PropertyManager.Types.Groups.Group;

namespace MyHome.v4.PropertyManager.Types.Property
{

    /// <summary>
    /// The property snippet is used for the quick display of a property to display in adverts
    /// </summary>
    [DataContract]
    [Serializable]
    public class PropertySnippet
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertySnippet"/> class.
        /// </summary>
        public PropertySnippet()
        {
            Beds = new Beds();
            Map = new Map();
            Media = new Media();
            Price = new Price();
            CustomData = new CustomData();
            Offers = new List<PropertyOffer>();
            FeaturedProducts = new List<FeaturedProduct>();
        }

        /// <summary>
        /// Gets or sets the property unique identifier.
        /// </summary>
        /// <value>
        /// The property unique identifier.
        /// </value>
        [DataMember]
        public int PropertyId { get; set; }

        /// <summary>
        /// Gets or sets the group unique identifier.
        /// </summary>
        /// <value>
        /// The group unique identifier.
        /// </value>
        [DataMember]
        public int GroupId { get; set; }

        /// <summary>
        /// Gets or sets the group.
        /// </summary>
        /// <value>
        /// The group unique identifier.
        /// </value>
        [DataMember]
        public Group Group{ get; set; }

        /// <summary>
        /// Gets or sets the beds.
        /// </summary>
        /// <value>
        /// The beds.
        /// </value>
        [DataMember]
        public Beds Beds { get; set; }

        /// <summary>
        /// Gets or sets the display address.
        /// </summary>
        /// <value>
        /// The display address.
        /// </value>
        [DataMember]
        public string DisplayAddress { get; set; }
        /// <summary>
        /// Gets or sets the display address.
        /// </summary>
        /// <value>
        /// The display address.
        /// </value>
        [DataMember]
        public string DescriptiveTitle { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public string Name { get; set; }


        /// <summary>
        /// The highest current offer
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public IEnumerable<PropertyOffer> Offers { get; set; }

        /// <summary>
        /// The highest current offer
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [DataMember]
        public List<FeaturedProduct> FeaturedProducts { get; set; }

        /// <summary>
        /// Gets or sets the property class.
        /// </summary>
        /// <value>
        /// The property class.
        /// </value>
        [DataMember]
        public int? PropertyClassId { get; set; }

        /// <summary>
        /// Gets or sets the property status.
        /// </summary>
        /// <value>
        /// The property status.
        /// </value>
        [DataMember]
        public int PropertyStatusId { get; set; }

        /// <summary>
        /// Gets or sets the property type unique identifier.
        /// </summary>
        /// <value>
        /// The property type unique identifier.
        /// </value>
        [DataMember]
        public int? PropertyTypeId { get; set; }

        /// <summary>
        /// Gets or sets the region unique identifier.
        /// </summary>
        /// <value>
        /// The region unique identifier.
        /// </value>
        [DataMember]
        public int? RegionId { get; set; }

        /// <summary>
        /// Gets or sets the locality unique identifier.
        /// </summary>
        /// <value>
        /// The locality unique identifier.
        /// </value>
        [DataMember]
        public int? LocalityId { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        [DataMember]
        public Price Price { get; set; }

        /// <summary>
        /// Gets or sets the media.
        /// </summary>
        /// <value>
        /// The media.
        /// </value>
        [DataMember]
        public Media Media { get; set; }

        /// <summary>
        /// Gets or sets the map.
        /// </summary>
        /// <value>
        /// The map.
        /// </value>
        [DataMember]
        public Map Map { get; set; }

        /// <summary>
        /// Gets or sets the custom data.
        /// </summary>
        /// <value>
        /// The custom data.
        /// </value>
        [DataMember]
        public CustomData CustomData { get; set; }

        /// <summary>
        /// Gets or sets the distance from memory.
        /// </summary>
        /// <value>
        /// The distance from memory.
        /// </value>
        [DataMember]
        public decimal? DistanceFromMe { get; set; }

        /// <summary>
        /// Gets the number of Photos associated with the property
        /// </summary>
        /// <value>
        /// The number of photos.
        /// </value>
        [DataMember]
        public int PhotoCount { get; set; }

        /// <summary>
        /// Gets the display name and address.
        /// </summary>
        /// <value>
        /// The display name and address.
        /// </value>
        public string DisplayNameAndAddress
        {
            get
            {
                if (!String.IsNullOrEmpty(Name))
                {
                    return String.Format("{0} - {1}", Name, DisplayAddress);
                }
                return DisplayAddress;
            }
        }

        public string OfferString
        {
            get
            {
                if (!string.IsNullOrEmpty(AcceptedOffer))
                    return "Accepted: " + AcceptedOffer;
                if (!string.IsNullOrEmpty(HighestOffer))
                    return "Highest: " + HighestOffer;
                return "";
            }
        }

        /// <summary>
        /// The highest offer as a string
        /// </summary>
        public string HighestOffer
        {
            get
            {
                if (Offers.Any())
                {
                    var offers = Offers.Where(x => x.StatusId == 1);
                    if (offers.Any())
                    {
                        return offers.Select(x => x.Amount).Max().AsCurrency();
                    }
                }
                return "";
            }
        }

        /// <summary>
        /// The highest offer as a string
        /// </summary>
        public string AcceptedOffer
        {
            get
            {
                if (Offers.Any())
                {
                    var offers = Offers.Where(x => x.StatusId == 1);
                    if (offers.Any())
                    {
                        var acceptedOffers = offers.Where(x => x.StatusId == 3);
                        if (acceptedOffers.Any())
                        {
                            return acceptedOffers.Select(x => x.Amount).Max().AsCurrency();
                        }
                    }
                }
                return "";
            }
        }


        /// <summary>
        /// Gets the public display price.
        /// </summary>
        /// <value>
        /// The public display price.
        /// </value>
        public string PublicDisplayPrice
        {
            get
            {
                if (IsStatusPublic)
                {
                    return PriceAsString;
                }
                return ((PropertyStatusEnum)Enum.ToObject(typeof(PropertyStatusEnum), PropertyStatusId)).ToString().ToSentenceCase();
            }
        }

        /// <summary>
        /// Gets a value indicating whether [is status public].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is status public]; otherwise, <c>false</c>.
        /// </value>
        public bool IsStatusPublic { get { return PropertyStatusId == 2 || PropertyStatusId == 11 || PropertyStatusId == 12; } }

        /// <summary>
        /// Gets the customer class for display.
        /// </summary>
        /// <value>
        /// The customer class for display.
        /// </value>
        public string CustomerClassForDisplay
        {
            get
            {
                switch (PropertyClassId)
                {
                    case 5:
                    case 3:
                    case 4:
                        return "Tenant";
                    default:
                        return "Buyer";
                }
            }
        }

        /// <summary>
        /// Gets the price asynchronous string.
        /// </summary>
        /// <value>
        /// The price asynchronous string.
        /// </value>
        public virtual string PriceAsString { get { return Price.ToString(); } }

        /// <summary>
        /// Gets the private price asynchronous string.
        /// </summary>
        /// <value>
        /// The private price asynchronous string.
        /// </value>
        public virtual string PrivatePriceAsString { get { return Price.PrivatePriceString; } }

        /// <summary>
        /// Gets the price information words.
        /// </summary>
        /// <value>
        /// The price information words.
        /// </value>
        public virtual string PriceInWords { get { return Price.ToString(); } }

        /// <summary>
        /// Main Photo for the property
        /// </summary>
        public string MainPhoto
        {
            get
            {
                if (Media != null && Media.HasMainPhoto)
                {
                    return DtoHelpers.GetImagePath(Media.MainPhoto.Filename, "_l", PropertyId);
                }

                return "https://www.myhome.ie/app/assets/images/heart.png";
            }
        }

        /// <summary>
        /// Gets the related properties label.
        /// </summary>
        /// <value>
        /// The related properties label.
        /// </value>
        public string RelatedPropertiesLabel
        {
            get { return PropertyClassId == 2 ? "Property Types" : "Development"; }
        }

        /// <summary>
        /// Gets a value indicating whether [has photos].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has photos]; otherwise, <c>false</c>.
        /// </value>
        public bool HasPhotos { get { return Media != null && Media.HasPhotos; } }

        /// <summary>
        /// Gets a value indicating whether [has pdfs].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has pdfs]; otherwise, <c>false</c>.
        /// </value>
        public bool HasPdfs { get { return Media != null && Media.HasPdfs; } }

        /// <summary>
        /// Gets a value indicating whether [has beds].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has beds]; otherwise, <c>false</c>.
        /// </value>
        public bool HasBeds { get { return Beds != null && Beds.HasBeds; } }

        /// <summary>
        /// Gets a value indicating whether [has map].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has map]; otherwise, <c>false</c>.
        /// </value>
        public bool HasMap { get { return Map != null; } }

        /// <summary>
        /// Gets a value indicating whether [has map coordinates].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has map coordinates]; otherwise, <c>false</c>.
        /// </value>
        public bool HasMapCoordinates { get { return HasMap && Map.Longitude.HasValue; } }

        /// <summary>
        /// Gets a value indicating whether [has price].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has price]; otherwise, <c>false</c>.
        /// </value>
        public bool HasPrice { get { return Price != null && Price.HasPrice; } }

        /// <summary>
        /// Gets a value indicating whether [has public price].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has public price]; otherwise, <c>false</c>.
        /// </value>
        public bool HasPublicPrice { get { return Price != null && Price.HasPublicPrice; } }

        /// <summary>
        /// Gets a value indicating whether [has rental price].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has rental price]; otherwise, <c>false</c>.
        /// </value>
        public bool HasRentalPrice { get { return Price != null && Price.HasRentalPrice; } }

        /// <summary>
        /// Gets a value indicating whether [has public rental price].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has public rental price]; otherwise, <c>false</c>.
        /// </value>
        public bool HasPublicRentalPrice { get { return Price != null && Price.HasPublicRentalPrice; } }

        /// <summary>
        /// Gets a value indicating whether [has public sale price].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has public sale price]; otherwise, <c>false</c>.
        /// </value>
        public bool HasPublicSalePrice { get { return Price != null && Price.HasPublicSalePrice; } }

        /// <summary>
        /// Gets a value indicating whether [has minimum and maximum price].
        /// </summary>
        /// <value>
        /// <c>true</c> if [has minimum and maximum price]; otherwise, <c>false</c>.
        /// </value>
        public bool HasMinAndMaxPrice { get { return Price != null && Price.HasMinAndMaxPrice; } }

        /// <summary>
        /// Gets a value indicating whether [has main photo].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has main photo]; otherwise, <c>false</c>.
        /// </value>
        public bool HasMainPhoto { get { return Media != null && Media.HasMainPhoto; } }

        /// <summary>
        /// Gets a value indicating whether [is new property].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is new property]; otherwise, <c>false</c>.
        /// </value>
        public bool IsNewProperty { get { return PropertyId == 0; } }

        /// <summary>
        /// Gets a value indicating whether [is sale agreed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is sale agreed]; otherwise, <c>false</c>.
        /// </value>
        public bool IsSaleAgreed { get { return PropertyStatusId == 3; } }

        /// <summary>
        /// Gets a value indicating whether [is sold].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is sold]; otherwise, <c>false</c>.
        /// </value>
        public bool IsSold { get { return PropertyStatusId == 4; } }

        /// <summary>
        /// Gets a value indicating whether [is locked].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is locked]; otherwise, <c>false</c>.
        /// </value>
        public bool IsLocked { get { return PropertyStatusId == 18; } }

        /// <summary>
        /// Gets the photos.
        /// </summary>
        /// <value>
        /// The photos.
        /// </value>
        public List<MediaItem> Photos { get { return Media != null ? Media.ByType(MediaItemType.Photo) : new List<MediaItem>(); } }

        /// <summary>
        /// Gets the pdfs.
        /// </summary>
        /// <value>
        /// The pdfs.
        /// </value>
        public List<MediaItem> Pdfs { get { return Media != null ? Media.ByType(MediaItemType.PDF) : new List<MediaItem>(); } }

        /// <summary>
        /// Gets the floorplans.
        /// </summary>
        /// <value>
        /// The floorplans.
        /// </value>
        public List<MediaItem> Floorplans { get { return Media != null ? Media.ByType(MediaItemType.FloorPlan) : new List<MediaItem>(); } }

        /// <summary>
        /// Gets the beds description.
        /// </summary>
        /// <value>
        /// The beds description.
        /// </value>
        public string BedsDescription { get { return HasBeds ? Beds.ToString() : ""; } }

        /// <summary>
        /// propertyStatusId to string of EnumValue & seperated on capital letter, e.g. PropertyStatusId==2, Enum=="ForSale", return "For Sale"
        /// </summary>
        public string PropertyStatus
        {
            get
            {
                var propertyStatus = ((PropertyStatusEnum)PropertyStatusId).ToString();
                var regex = new Regex(@"(\p{Lu}\p{Ll}+)");

                var displayPropertyStatus = (from Match match in regex.Matches(propertyStatus) select match.Value).ToList().Join(" ");
                return displayPropertyStatus;
            }
        }

    }
}
