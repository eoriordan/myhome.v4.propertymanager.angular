﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Property
{
    /// <summary>
    /// Poco object to handle floors json from property entity. Only used in Property so not included in common.
    /// </summary>
    [DataContract]
    [Serializable]
    public class Floors : List<Floor> { }

    public class Floor
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int? LengthInMeters { get; set; }
        [DataMember]
        public int? WidthInMeters { get; set; }
        [DataMember]
        public decimal? Size { get; set; }
    }
}
