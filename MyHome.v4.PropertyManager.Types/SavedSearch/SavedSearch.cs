﻿using System;
using MyHome.v4.PropertyManager.Schema.SavedSearch;

namespace MyHome.v4.PropertyManager.Types.SavedSearch
{
    public class SavedSearch
    {
        public int SavedSearchId { get; set; }

        public int? UserId { get; set; }

        public int? GroupId { get; set; }

        public int? ContactId { get; set; }

        public string Title { get; set; }

        public SearchPropertiesRequest SearchPropertiesRequest { get; set; }

        public string IndexData { get; set; }

        public NotificationSetting NotificationSettings { get; set; }

        public int CreatedByUserId { get; set; }

        public int CreatedByGroupId { get; set; }

        public int? BrandedAsGroupId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? LastNotificationCheck { get; set; }

        public DateTime? LastNotificationSent { get; set; }

        public DateTime? NextNotificationCheck { get; set; }

        public string EditKey { get; set; }

        public int NotificationsSent { get; set; }

        public bool IsError { get; set; }
    }
}
