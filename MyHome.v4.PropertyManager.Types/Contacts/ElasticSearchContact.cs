﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Nest;

namespace MyHome.v4.PropertyManager.Types.Contacts
{
    /// <summary>
    /// The contact index to be persisted into the elastic search index
    /// </summary>
    [Serializable]
    [DataContract]
    [ElasticType(IdProperty = "ContactID")]
    public class ElasticSearchContact
    {
        public ElasticSearchContact()
        {
            Tags = new List<string>();
        }

        /// <summary>
        /// The PK value for the Contact
        /// </summary>
        [DataMember]
        public int ContactId { get; set; }

        /// <summary>
        /// The date the contact was created
        /// </summary>
        [DataMember]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// the PK of the group to which the Contact is associated
        /// </summary>
        [DataMember]
        public int? GroupId { get; set; }

        /// <summary>
        /// A list of tags denoting the role of this contact, Solicitor, Vendor, Prospect etc.
        /// </summary>
        [DataMember]
        public IEnumerable<string> Tags { get; set; }

        /// <summary>
        /// Denotes whether this contact has been deleted or not.
        /// </summary>
        [DataMember]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Flags as to whether this contact is public or not
        /// </summary>
        [DataMember]
        public bool IsPublic { get; set; }

        [DataMember]
        public string QueryString { get; set; }

        [DataMember]
        public ElasticSearchContactDetails ContactDetails { get; set; }

        public string TagList
        {
            get { return Tags == null ? null : String.Join(", ", Tags); }
        }
    }
}
