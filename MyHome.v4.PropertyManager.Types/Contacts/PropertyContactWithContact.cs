﻿using System;
using MyHome.v4.Common.Schema;

namespace MyHome.v4.PropertyManager.Types.Contacts
{
    public class PropertyContactWithContact
    {
        public int PropertyContactId { get; set; }
        public int PropertyId { get; set; }
        public int ContactId { get; set; }
        public short PropertyContactTypeId { get; set; }
        public string Notes { get; set; }
        public int? GroupId { get; set; }
        public int? UserId { get; set; }
        public bool IsPublic { get; set; }
        public string IndexData { get; set; }
        public Tags TagList { get; set; }
        public ContactDetails ContactDetails { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? CreatedByUserId { get; set; }
        public ContactConfiguration Configuration { get; set; }
        public string LegacyMappings { get; set; }
        public int? LegacyUserId { get; set; }
        public StringDictionary CustomFormData { get; set; }
        public bool IsDeleted { get; set; }
        public int? RepresentsGroupId { get; set; }
        public int? RepresentsUserId { get; set; }
        public int? ChannelId { get; set; }
        public int? RegionId { get; set; }
        public int? LocalityId { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }
}
