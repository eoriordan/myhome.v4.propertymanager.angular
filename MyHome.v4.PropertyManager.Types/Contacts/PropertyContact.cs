﻿namespace MyHome.v4.PropertyManager.Types.Contacts
{
    public class PropertyContact
    {
        public int PropertyContactId { get; set; }
        public int PropertyId { get; set; }
        public int ContactId { get; set; }
        public short PropertyContactTypeId { get; set; }
        public string Notes { get; set; }
    }
}
