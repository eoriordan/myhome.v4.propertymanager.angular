﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Contacts
{
    [Serializable]
    [DataContract]
    public class ElasticSearchContactDetails
    {
        [DataMember]
        public bool IsPrivate { get; set; }

        [DataMember]
        public bool MobileIsVerified { get; set; }

        [DataMember]
        public bool PhoneIsVerified { get; set; }

        [DataMember]
        public string Mobile { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string CompanyName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Email { get; set; }
    }
}
