﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyHome.v4.Common.Schema;

namespace MyHome.v4.PropertyManager.Types.Contacts
{
    public class Contact
    {
        public Contact()
        {
            CreatedOn = DateTime.Now;
        }

        public int ContactId { get; set; }
        public int? GroupId { get; set; }
        public int? UserId { get; set; }
        public bool IsPublic { get; set; }
        public string IndexData { get; set; }
        public string TagList { get; set; }
        public ContactDetails ContactDetails { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? CreatedByUserId { get; set; }
        public ContactConfiguration Configuration { get; set; }
        public string LegacyMappings { get; set; }
        public int? LegacyUserId { get; set; }
        public StringDictionary CustomFormData { get; set; }
        public bool IsDeleted { get; set; }
        public int? RepresentsGroupId { get; set; }
        public int? RepresentsUserId { get; set; }
        public int? ChannelId { get; set; }
        public int? RegionId { get; set; }
        public int? LocalityId { get; set; }
        public DateTime? DateOfBirth { get; set; }

        public IEnumerable<string> Tags
        {
            get
            {
                if (string.IsNullOrEmpty(TagList)) return null;

                if (TagList.Contains('>') || TagList.Contains('<'))
                {
                    var tags = TagList.Substring(1, TagList.Length - 2);
                    return tags.Split(new[] { '>', '<' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                }

                return TagList.Split(',').Select(x => x.Trim());
            }
        }
    }
}
