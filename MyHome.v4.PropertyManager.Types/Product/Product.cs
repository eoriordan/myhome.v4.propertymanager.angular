﻿using MyHome.v4.Common.Schema.Enumerations;

namespace MyHome.v4.PropertyManager.Types.Product
{
    public class Product
    {
        public int ProductId { get; set; }

        public int ProductRateCardId { get; set; }

        public ProductType ProductType { get; set; }

        public int BillingPeriodId { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Features { get; set; }

        public int? ProductClassId { get; set; }

        public PropertySection? PropertySection { get; set; }

        public int? PropertyClassId { get; set; }

        public decimal Price { get; set; }

        public decimal? VendorPrice { get; set; }

        public bool IsFreeWithPackage { get; set; }

        public int? CreditCost { get; set; }

        public int? CreditAllocation { get; set; }

        public int? RefreshCost { get; set; }

        public int? RefreshAllocation { get; set; }

        public bool IsUserProduct { get; set; }

        public int? DurationInDays { get; set; }
        public int? UpgradeFromProductClassId { get; set; }

        public string CssClass { get; set; }

        public bool IsHidden { get; set; }

        public string SapMaterialCode { get; set; }

        public string AppleProductId { get; set; }

        public string AppleDisplayPrice { get; set; }

        public string CustomData { get; set; }
    }
}
