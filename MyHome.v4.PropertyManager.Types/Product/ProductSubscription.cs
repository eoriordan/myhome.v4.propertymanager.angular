﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Types.Product
{
    /// <summary>
    /// The representation of a Product subscription record in the system
    /// </summary>
    [DataContract]
    [Serializable]
    public class ProductSubscription
    {

        [DataMember]
        public int ProductSubscriptionId { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public Types.Product.Product Product { get; set; }

        [DataMember]
        public int CreatedByUserId { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public decimal? Discount { get; set; }

        [DataMember]
        public int? CreditCost { get; set; }

        [DataMember]
        public int? CreditAllocation { get; set; }

        [DataMember]
        public int? RefreshCost { get; set; }

        [DataMember]
        public int? RefreshAllocation { get; set; }

        [DataMember]
        public DateTime? NextBillingDate { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public DateTime? ReviewDate { get; set; }

        [DataMember]
        public DateTime? CreatedOn { get; set; }

        [DataMember]
        public DateTime? ReviewNotificationSentOn { get; set; }

        public decimal DiscountedPrice
        {
            get
            {
                if (Discount.HasValue)
                {
                    return Price - Discount.Value;
                }

                return Price;
            }
        }
    }
}
