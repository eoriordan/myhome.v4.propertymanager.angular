﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Groups;

namespace MyHome.v4.PropertyManager.IService.Groups
{
    /// <summary>
    /// The interface for the buisness functionality of the group service
    /// </summary>
    public interface IGroupService
    {
        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The request.</param>
        /// <returns>
        /// The group or null if the group was not found
        /// </returns>
        Group GetById(int id);

        /// <summary>
        /// Gets the by ids of the groups
        /// </summary>
        /// <param name="ids">The request.</param>
        /// <returns>
        /// The list of groups
        /// </returns>
        IEnumerable<Group> GetByIds(List<int> ids);
    }
}
