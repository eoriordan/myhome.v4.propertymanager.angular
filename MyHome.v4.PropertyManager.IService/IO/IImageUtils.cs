using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using MyHome.v4.PropertyManager.Types.IO;

namespace MyHome.v4.PropertyManager.IService.IO
{
    public interface IImageUtils
    {
        void Resize(Stream ms, string targetPath, int width, int height, EncoderParameters encoderParameters, ImageFormat imageFormat);
        void Resize(Stream ms, string targetPath, int width, int height);
        void Resize(Stream ms, string targetPath, int width, int height, bool upsizeIfOriginalIsSmaller);
        void Resize(Stream ms, string targetPath, int width, int height, bool upsizeIfOriginalIsSmaller, string watermark);
        void Resize(Stream ms, string targetPath, int width, int height, bool upsizeIfOriginalIsSmaller, string watermark, EncoderParameters encoderParameters, ImageFormat imageFormat = null);
        MemoryStream OverlayImage(MemoryStream ms);
        void ResizeWithPadding(Stream ms, string targetPath, int imageWidth, int imageHeight, int totalWidth, int totalHeight, string watermark, Color paddingColour);
        void ResizeToFit(Stream ms, string targetPath, int maxWidth, int maxHeight);
        void ResizeToFit(Stream ms, string targetPath, int maxWidth, int maxHeight, bool padImage);
        void ResizeToFit(Stream ms, string targetPath, int maxWidth, int maxHeight, bool padImage, string watermark);
        void ResizeToFit(Stream ms, string targetPath, int maxWidth, int maxHeight, bool padImage, string watermark, Color paddingColour);
        void Crop(Stream ms, string targetPath, int width, int height);
        void Crop(Stream ms, string targetPath, int width, int height, AnchorPosition anchor);
        EncoderParameters GetEncoderParameters(long quality = 80L);

        string GetPartitionPath(int number, string pathSeperator = @"\");
    }
}