﻿using System.Collections.Generic;
using System.IO;

namespace MyHome.v4.PropertyManager.IService.IO
{
    /// <summary>
    /// Interface containing all methods a file system provider must implement.
    /// </summary>
    public interface IFileSystemProvider
    {
        /// <summary>
        /// Save a file to the implemented provider
        /// </summary>
        /// <param name="stream">The file stream</param>
        /// <param name="fileName">The name of the file</param>
        /// <param name="location">The location the file will be saved.</param>
        /// <param name="skipChecks">if set to <c>true</c> [skip checks].</param>
        /// <returns>
        /// The location on the file.
        /// </returns>
        string SaveFile(Stream stream, string fileName, string location, bool skipChecks = false);


        /// <summary>
        /// Save a file to the implemented provider
        /// </summary>
        /// <param name="streamsAndFileNames">A dictionary of streams and their corresponding filenames</param>
        /// <param name="location">The location where the file will be saved</param>
        /// <returns>The location on the file.</returns>
        IEnumerable<string> SaveFiles(Dictionary<string, Stream> streamsAndFileNames, string location);


        /// <summary>
        /// Delete a file from the implemented provider
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <param name="location">The location where the file will be saved.</param>
        /// <returns>True or false success bool</returns>
        bool DeleteFile(string fileName, string location);


        /// <summary>
        /// Delete a number of files from the implemented provider
        /// </summary>
        /// <param name="pathsList">The dictionary of fileNames and their corresponding locations.</param>
        /// <returns>True or false success bool</returns>
        bool DeleteFiles(Dictionary<string, string> pathsList);


        /// <summary>
        /// Get the full path to the specified file
        /// </summary>
        /// <param name="fileName">The file name</param>
        /// <param name="location">The file's location</param>
        /// <returns>The full path to the file.</returns>
        string GetFilePath(string fileName, string location);


        /// <summary>
        /// Download a file from the specified provider
        /// </summary>
        /// <param name="fileName">The file name</param>
        /// <param name="location">The location, in a properties case the property ID.</param>
        /// <returns>A file stream of the requested file.</returns>
        Stream DownloadFile(string fileName, string location);


        /// <summary>
        /// Download a number of files from the specified provider
        /// </summary>
        /// <param name="paths">A dictionary of wherein the fileName is the key and the location is the value.</param>
        /// <returns>A list of Streams containing the file Streams for each file.</returns>
        IEnumerable<Stream> DownloadFiles(Dictionary<string, string> paths);


        /// <summary>
        /// Copy a file to another destination within the same provider
        /// </summary>
        /// <param name="file">The file stream to be copied</param>
        /// <param name="fileName">The file name</param>
        /// <param name="destination">The destination path for the file.</param>
        /// <returns>True or false success bool</returns>
        string CopyTo(Stream file, string fileName, string destination);


        /// <summary>
        /// Copy a file to another destination in another provider
        /// </summary>
        /// <param name="file">The file to be copied</param>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="destination">The destination path for the file.</param>
        /// <param name="provider">The provider to which you widh to send the file.</param>
        /// <returns>True or false success bool</returns>
        bool CopyTo(Stream file, string fileName, string destination, IFileSystemProvider provider);


        /// <summary>
        /// Create a directory in the provider implemented
        /// </summary>
        /// <param name="path">The path to the directory within which we wish to create a directory</param>
        /// <returns>The new path to the directory</returns>
        string CreateDirectory(string path);


        /// <summary>
        /// Remoive a directory fro the implemented provider
        /// </summary>
        /// <param name="path">The path for the directory you wish to delete. 
        /// The last part of the path will be deleted</param>
        /// <returns>True or false success bool</returns>
        bool DeleteDirectory(string path);


        /// <summary>
        /// Check whether a directory exists
        /// </summary>
        /// <param name="path">The string path for the directory. The
        /// Last part of the path will be checked.</param>
        /// <returns>True or false exists bool</returns>
        bool DirectoryExists(string path);


        /// <summary>
        /// Check whether a file exists at a location
        /// </summary>
        /// <param name="fileName">The file name</param>
        /// <param name="location">The location, in a properties case the property ID.</param>
        /// <returns>True or false exists bool</returns>
        /// This isn't async as it wouldn't make sense. There would be no guarantee that
        /// the file wouldn't be deleted after the check. It makes more sense to open the file (DownloadFile)
        /// then work with it. http://stackoverflow.com/a/19077180
        bool FileExists(string fileName, string location);

        /// <summary>
        /// Check is a file exists at the specified location
        /// </summary>
        /// <param name="filePath">The file path to check for the existence of a file</param>
        /// <returns>Returns true if the file is found</returns>
        bool FileExists(string filePath);

        /// <summary>
        /// Gets a list of the paths to a directory's contents
        /// </summary>
        /// <param name="location">The location, in a properties case the property ID.</param>
        /// <returns>True or false exists bool</returns>
        IEnumerable<string> ListDirectoryContents(string location);

        /// <summary>
        /// Get the size of a file based on the file path
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="location">The file location.</param>
        /// <returns></returns>
        long FileSize(string fileName, string location);

        /// <summary>
        /// Gets the name of the implemented provder
        /// </summary>
        /// <returns>A string, the provider's name.</returns>
        string GetProviderName();
    }
}
