﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Contacts;
using MyHome.v4.PropertyManager.Types.Property;

namespace MyHome.v4.PropertyManager.IService.Activity
{
    /// <summary>
    /// Service contract encapsulating all the business logic for activities.
    /// </summary>
    public interface IActivityService : IBaseService<Schema.Activity.Activity, Types.Activity.Activity>
    {
        /// <summary>
        /// Returns a collection of fully hydrated activities relating to a <see cref="Property"/>
        /// </summary>
        /// <param name="propertyId">Property id to search for</param>
        /// <returns>fully hydrated collection</returns>
        IEnumerable<Types.Activity.Activity> GetPropertyActivity(int propertyId);

        /// <summary>
        /// Returns a collection of fully hydrated activities relating to a <see cref="Contact"/>
        /// </summary>
        /// <param name="contactId">Contact id to search for</param>
        /// <returns>fully hydrated collection</returns>
        IEnumerable<Types.Activity.Activity> GetContactActivity(int contactId);

        /// <summary>
        /// Logs a new activity item
        /// </summary>
        /// <param name="activity">Activity to log</param>
        void LogActivity(Types.Activity.Activity activity);
    }
}
