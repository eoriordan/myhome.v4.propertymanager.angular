﻿using System;
using System.Collections.Generic;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.Types.Calendar;
using MyHome.v4.PropertyManager.Types.Groups;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.IService.Calendar
{
    public interface ICalendarService
    {
        CalendarItem GetCalendarItem(string externalEventId, Group group, User user);

        ICollection<CalendarItem> GetCalendarItems(Group group, ICollection<User> users, DateTime fromTime, DateTime toTime);

        void DeleteEvent(Group group, User user, string externalEventId);

        void SaveEvent(Group group, User user, CalendarItem item);

        PagedList<TaskItem> GetTasks(Group group, DateTime? minDuedate, DateTime? maxDueDate, int page, int pageSize);
        PagedList<TaskItem> GetTasks(Group group, User user, DateTime? minDuedate, DateTime? maxDueDate, int page, int pageSize);
        PagedList<TaskItem> GetCompletedTasks(Group group, int page, int pageSize);
        PagedList<TaskItem> GetCompletedTasks(Group group, User user, int page, int pageSize);

        void UpdateTask(int taskId, int? completedByUserId, DateTime? completedOn);
        void CreateTask(TaskItem task);

    }
}
