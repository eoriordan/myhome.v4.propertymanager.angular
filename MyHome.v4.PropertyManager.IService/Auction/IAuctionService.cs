﻿namespace MyHome.v4.PropertyManager.IService.Auction
{
    public interface IAuctionService : IBaseService<Schema.Auction.Auction, Types.Auction.Auction>
    {
        void SavePropertyAuction(Types.Auction.Auction auction, int propertyId);
        int? GetPropertyAuctionId(int propertyId);
    }
}
