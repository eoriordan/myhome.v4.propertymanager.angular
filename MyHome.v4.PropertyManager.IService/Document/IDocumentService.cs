﻿using System.Collections.Generic;

namespace MyHome.v4.PropertyManager.IService.Document
{
    public interface IDocumentService : IBaseService<Schema.Document.Document, Types.Document.Document>
    {
        IEnumerable<Types.Document.Document> GetDocuments(int contactId);
    }
}
