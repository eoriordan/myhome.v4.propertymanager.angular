﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyHome.v4.PropertyManager.IService
{
    public interface IBaseService<T, TType>  
        where TType : class, new() 
        where T : class, new()
    {
        /// <summary>
        /// Get the poco of type TType from the persistent storage
        /// </summary>
        /// <param name="id">The uniqe id of the property to load from the persistent storage</param>
        /// <returns>Returns a poco of type TType</returns>
        TType GetById(int id);

        /// <summary>
        /// Get the poco of type TType from the persistent storage
        /// </summary>
        /// <param name="id">The uniqe id of the property to load from the persistent storage</param>
        /// <returns>Returns a poco of type TType</returns>
        Task<TType> GetByIdAsync(int id);

        /// <summary>
        /// Get a list of poco's of type TType from the persistent stroage based on there unique ids
        /// </summary>
        /// <param name="ids">The unique ids to load the items from there persistent storage</param>
        /// <returns>Returns a list of poco's of type TType</returns>
        IEnumerable<TType> GetByIds(List<int> ids);

        /// <summary>
        /// Get a list of poco's of type TType from the persistent stroage based on there unique ids
        /// </summary>
        /// <param name="ids">The unique ids to load the items from there persistent storage</param>
        /// <returns>Returns a list of poco's of type TType</returns>
        Task<IEnumerable<TType>> GetByIdsAsync(List<int> ids);

        /// <summary>
        /// Get all poco's of type TType from the persistent storage
        /// </summary>
        /// <returns>Returns all poco's of type TType from the persistent storage</returns>
        IEnumerable<TType> GetAll(int page = 1, int pageCount = 5000);

        /// <summary>
        /// Get a count of all matching expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        long Count(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Select all matching expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        IEnumerable<TType> Select(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Select all matching expression
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        IEnumerable<TType> Select(Expression<Func<T, bool>> expression, int page);

        /// <summary>
        /// Select all matching expression
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="page"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        IEnumerable<TType> Select(Expression<Func<T, bool>> expression, int page, int pageCount);

            /// <summary>
        /// Get all poco's of type TType from the persistent storage
        /// </summary>
        /// <returns>Returns all poco's of type TType from the persistent storage</returns>
        Task<IEnumerable<TType>> GetAllAsync();

        /// <summary>
        /// Persists the specified poco into the persistent storage and return the populated value
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        TType Persist(TType value);

        /// <summary>
        /// Persists of the specified pocos into the persistent storage
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        IEnumerable<TType> PersistMany(List<TType> values);

        /// <summary>
        /// Deletes the object by its unique id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        bool Delete(int id);

        /// <summary>
        /// Deletes many objects by its unique id
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        bool DeleteMany(List<int> ids);
    }
}