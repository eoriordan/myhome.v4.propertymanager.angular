using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.IService.SessionProvider
{
    public interface ISessionProvider
    {
        UserSession GetSession(string id);
        bool CheckSessionExists(string id);
        bool SetSession(string id, UserSession session);
        bool DeleteSession(string id);
    }
}