﻿using System;
using System.Collections.Generic;

namespace MyHome.v4.PropertyManager.IService.OpenViewing
{
    /// <summary>
    /// Service contract encapsulating all the business logic for Open Viewings.
    /// </summary>
    public interface IOpenViewingService : IBaseService<Schema.OpenViewing.OpenViewing, Types.OpenViewing.OpenViewing>
    {
        /// <summary>
        /// Will return a fully hydrated open viewing entity for the specified property Id
        /// </summary>
        /// <param name="propertyId">The property Id</param>
        /// <returns>Collection of open viewings. Empty if there are none.</returns>
        IEnumerable<Types.OpenViewing.OpenViewing> GetOpenViewings(int propertyId);

        /// <summary>
        /// Will return a fully hydrated open viewing entity for the specified open viewing
        /// </summary>
        /// <param name="openViewingId">The open viewing Id</param>
        /// <returns>Collection of open viewings. Empty if there are none.</returns>
        Types.OpenViewing.OpenViewing GetOpenViewing(int openViewingId);

        /// <summary>
        /// Will persist a new openviewing along with notes, contact/user mappings
        /// </summary>
        /// <param name="openViewing">Open viewing to persist</param>
        /// <exception cref="ArgumentException">If the property does not exist</exception>
        void InsertOpenViewing(Types.OpenViewing.OpenViewing openViewing);

        /// <summary>
        /// Will update an new openviewing along with notes, contact/user mappings
        /// </summary>
        /// <param name="openViewing">Open viewing to update</param>
        /// <exception cref="ArgumentException">If the property does not exist</exception>
        /// <exception cref="ArgumentException">If the open viewing does not exist</exception>
        void UpdateOpenViewing(Types.OpenViewing.OpenViewing openViewing);

        /// <summary>
        /// Will delete the open viewing and all related entities.
        /// </summary>
        /// <param name="openViewingId">Open viewing to be deleted</param>
        /// <exception cref="ArgumentException">If the open viewing does not exist</exception>
        void DeleteOpenViewing(int openViewingId);
    }
}
