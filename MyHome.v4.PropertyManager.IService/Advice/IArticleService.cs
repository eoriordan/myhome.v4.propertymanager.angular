﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Advice;

namespace MyHome.v4.PropertyManager.IService.Advice
{
    public interface IArticleService : IBaseService<Schema.Advice.Article, Article>
    {
        IEnumerable<Article> GetArticlesByCategory(int categoryId, int page, int pageSize);

        IEnumerable<Article> SearchArticles(string searchString);

        IEnumerable<Category> PopulateCategories(int articleId);
    }
}
