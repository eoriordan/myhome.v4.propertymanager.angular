﻿using MyHome.v4.PropertyManager.Types.Advice;

namespace MyHome.v4.PropertyManager.IService.Advice
{
    public interface ICategoryService : IBaseService<Schema.Advice.Category, Category>
    {
    }
}
