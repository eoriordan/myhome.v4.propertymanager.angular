﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Advice;

namespace MyHome.v4.PropertyManager.IService.Advice
{
    public interface IArticleCategoryMappingService : IBaseService<Schema.Advice.ArticleCategoryMapping, ArticleCategoryMapping>
    {
        IEnumerable<ArticleCategoryMapping> GetByArticleId(int articleId);
    }
}
