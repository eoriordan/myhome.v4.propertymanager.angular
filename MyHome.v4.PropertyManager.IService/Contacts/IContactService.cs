﻿using MyHome.v4.PropertyManager.Types.Contacts;

namespace MyHome.v4.PropertyManager.IService.Contacts
{
    public interface IContactService : IBaseService<Schema.Contacts.Contact, Contact>
    {
    }
}
