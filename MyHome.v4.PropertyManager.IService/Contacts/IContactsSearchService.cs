﻿using System.Collections.Generic;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.Types.Contacts;

namespace MyHome.v4.PropertyManager.IService.Contacts
{
    public interface IContactsSearchService
    {
        PagedList<Contact> Search(string searchText, ICollection<int> groupIds, ICollection<string> tags, int page, int pageSize);
        PagedList<Contact> Search(string email, string phone, string mobile, ICollection<int> groupIds);

        void Persist(Contact contact);
    }
}
