﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Contacts;

namespace MyHome.v4.PropertyManager.IService.Contacts
{
    public interface IPropertyContactsService : IBaseService<Schema.Contacts.PropertyContact, PropertyContact>
    {
        IEnumerable<PropertyContactWithContact> GetPropertyContactWithContacts(int propertyId);
    }
}
