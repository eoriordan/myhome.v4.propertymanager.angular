﻿using System;
using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Property;

namespace MyHome.v4.PropertyManager.IService.Property
{
    public interface IPropertyOfferService : IBaseService<Schema.Property.PropertyOffer, Types.Property.PropertyOffer>
    {
        /// <summary>
        /// Gets all the offers relating to the contact
        /// </summary>
        /// <param name="contactId">Contact id</param>
        /// <returns>List of offers</returns>
        IEnumerable<PropertyOffer> GetOffers(int contactId);

        /// <summary>
        /// Adds a new property offer
        /// </summary>
        /// <param name="offer">offer to be added</param>
        void AddOffer(PropertyOffer offer);

        void UpdateOffer(int userId, int groupId, int propertyOfferId, string note);
        void UpdateOffer(int userId, int groupId, int propertyOfferId, string note, int propertyOfferStatus);
        void UpdateOffer(int userId, int groupId, int propertyOfferId, string note, int propertyOfferStatus, DateTime acceptedOn);
        void UpdateOffer(int userId, int groupId, int propertyOfferId, string notifyNote, DateTime? timeStamp, int? negotiatorId, int? method);
    }
}
