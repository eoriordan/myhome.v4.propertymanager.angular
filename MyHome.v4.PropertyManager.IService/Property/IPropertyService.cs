﻿using System.Collections.Generic;
using MyHome.v4.Common.Schema;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.Types.Property;
using MyHome.v4.PropertyManager.Types.Requests;

namespace MyHome.v4.PropertyManager.IService.Property
{
    public interface IPropertyService : IBaseService<Schema.Property.Property, Types.Property.Property>
    {
        /// <summary>
        /// Gets the property by identifier.
        /// </summary>
        /// <param name="id">The request.</param>
        /// <returns>
        /// The property or null if the property was not found
        /// </returns>
        Types.Property.Property GetProperty(int id);

        /// <summary>
        /// Gets the property by ids of the groups
        /// </summary>
        /// <param name="ids">The list of unique group identifiers to load</param>
        /// <param name="apiKey">The API key.</param>
        /// <returns>
        /// The list of properties
        /// </returns>
        IEnumerable<Types.Property.Property> GetByIds(List<int> ids, string apiKey);

        /// <summary>
        /// Searches for properties eith in Elastic search or the Database depending on a flag in the request object
        /// </summary>
        /// <param name="request">The search properties request object containing information to search by</param>
        /// <returns>
        /// The list of properties
        /// </returns>
        PagedList<Types.Property.Property> SearchProperties(BaseSearchProperties request);

        void EnqueueProperty(int propertyId);

        /// <summary>
        /// Gets all the possible sales types for the provided property class
        /// </summary>
        /// <param name="propertyClass">Property class</param>
        /// <returns></returns>
        IEnumerable<SaleType> GetSaleTypes(PropertyClassEnum propertyClass);

        /// <summary>
        /// Will persist the property and any related entities to the Db
        /// </summary>
        /// <param name="property">property to save.</param>
        /// <returns>The property Id</returns>
        int SaveProperty(Types.Property.Property property);
    }
}
