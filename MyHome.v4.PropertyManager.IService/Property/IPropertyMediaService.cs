﻿using System.IO;
using MyHome.v4.Common.Schema;

namespace MyHome.v4.PropertyManager.IService.Property
{
    public interface IPropertyMediaService
    {
        /// <summary>
        /// Will update the order of property media and delete any removed media
        /// </summary>
        /// <param name="propertyId">Id of property to update</param>
        /// <param name="updatedMediaCaptions">Ordered list of captions</param>
        /// <param name="type">Media type</param>
        void UpdatePropertyMedia(int propertyId, string[] updatedMediaCaptions, MediaItemType type);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="mediaItem"></param>
        /// <param name="ms"></param>
        void AddMedia(int propertyId, MediaItem mediaItem, MemoryStream ms);
    }
}
