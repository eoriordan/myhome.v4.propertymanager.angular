﻿namespace MyHome.v4.PropertyManager.IService.SavedSearch
{
    public interface ISavedSearchService : IBaseService<Schema.SavedSearch.SavedSearch, Types.SavedSearch.SavedSearch>
    {
    }
}
