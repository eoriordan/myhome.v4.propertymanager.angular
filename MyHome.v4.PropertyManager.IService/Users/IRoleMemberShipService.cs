﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.IService.Users
{
    public interface IRoleMemberShipService : IBaseService<Schema.Users.RoleMembership, RoleMembership>
    {
        IEnumerable<RoleMembership> GetByUserId(int userId);
        IEnumerable<RoleMembership> GetByUserIds(List<int> userIds);
    }
}
