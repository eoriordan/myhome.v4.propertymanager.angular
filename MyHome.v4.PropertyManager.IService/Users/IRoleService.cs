﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.IService.Users
{
    public interface IRoleService : IBaseService<Schema.Users.Role, Role>
    {
        IEnumerable<Role> GetByUserId(int userId);
        IEnumerable<Role> GetByUserIds(List<int> userIds);
        string GetUserRoleString(int userId);
    }
}
