﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Requests;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.IService.Users
{
    public interface IUserService : IBaseService<Schema.Users.User, User>
    {
        /// <summary>
        /// Gets the user by username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        User GetByUsername(string username);

        /// <summary>
        /// Creates the user for the register user request and add them to the group passed
        /// </summary>
        /// <param name="user">The edit user request.</param>
        /// <returns></returns>
        User EditUser(EditUserRequest user);

        /// <summary>
        /// Sets the last login timestamp for the given userid
        /// </summary>
        /// <param name="userId">id of the user to update</param>
        void LoggedIn(int userId);

        /// <summary>
        /// Returns a list of users for a group
        /// </summary>
        /// <param name="groupId">The id of the group for which users are retuirned</param>
        /// <returns></returns>
        IEnumerable<User> GetUsersByGroupId(int groupId);

        /// <summary>
        /// Gets the users for all the groupds in the group ids list.
        /// </summary>
        /// <param name="groupIds">The group ids to retrieve users for.</param>
        /// <returns></returns>
        IEnumerable<User> GetUsersByGroupIds(List<int> groupIds);
    }
}
