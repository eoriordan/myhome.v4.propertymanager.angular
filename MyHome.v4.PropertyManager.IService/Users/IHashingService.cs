﻿namespace MyHome.v4.PropertyManager.IService.Users
{
    public interface IHashingService
    {
        /// <summary>
        /// Randoms the string.
        /// </summary>
        /// <param name="size">The size.</param>
        /// <returns></returns>
        string RandomString(int size);

        /// <summary>
        /// Salts the and set password.
        /// </summary>
        /// <param name="dbUser">The database user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        Schema.Users.User SaltAndSetPassword(Schema.Users.User dbUser, string password);

        /// <summary>
        /// Hashes the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="salt">The salt.</param>
        /// <returns></returns>
        string HashPassword(string password, string salt);
    }
}
