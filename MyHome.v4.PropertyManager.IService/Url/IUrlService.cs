﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Types.Advice;
using MyHome.v4.PropertyManager.Types.Requests;

namespace MyHome.v4.PropertyManager.IService.Url
{
    public interface IUrlService
    {
        string GenerateSearchPath(BaseSearchProperties request);
        string GenerateArticleUrl(Article article, List<int> categoryIds);
    }
}
