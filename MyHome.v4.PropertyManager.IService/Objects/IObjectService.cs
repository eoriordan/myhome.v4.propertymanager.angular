﻿using MyHome.v4.PropertyManager.Types.Requests;

namespace MyHome.v4.PropertyManager.IService.Objects
{
    public interface IObjectService
    {
        BaseSearchProperties GenerateSearchObject(string url);
    }
}
