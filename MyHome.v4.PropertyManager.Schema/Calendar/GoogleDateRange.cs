﻿using System;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;

namespace MyHome.v4.PropertyManager.Schema.Calendar
{
    [DataContract]
    [Serializable]
    public class GoogleDateRange
    {
        [DataMember]
        public bool AllDay { get; set; }

        [DataMember]
        public DateTime From { get; set; }

        [DataMember]
        public DateTime To { get; set; }

        public override string ToString()
        {
            return From.DayMonthYearAndTime();
        }
    }
}
