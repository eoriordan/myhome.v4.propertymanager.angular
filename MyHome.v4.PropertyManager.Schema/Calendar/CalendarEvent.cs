﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Calendar
{
    public class CalendarEvent
    {
        [Alias("CalendarEventID")]
        [AutoIncrement]
        [PrimaryKey]
        public int CalendarEventId { get; set; }

        [Alias("ExternalEventID")]
        public string ExternalEventId { get; set; }

        [Alias("GroupID")]
        public int GroupId { get; set; }

        [Alias("UserID")]
        public int UserId { get; set; }

        public Attachments Attachments { get; set; }
    }
}
