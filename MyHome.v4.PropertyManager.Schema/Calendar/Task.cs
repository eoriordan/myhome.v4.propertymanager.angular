﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Calendar
{
    public class Task
    {
        [Alias("TaskId")]
        [AutoIncrement]
        [PrimaryKey]
        public int TaskId { get; set; }

        public string Title { get; set; }

        [Alias("TaskTypeID")]
        public int? TaskTypeId { get; set; }

        [Alias("GroupID")]
        public int GroupId { get; set; }

        [Alias("AssignedToUserID")]
        public int? AssignedToUserId { get; set; }

        [Alias("CreatedByUserID")]
        public int CreatedByUserId { get; set; }

        [Alias("CompletedByUserID")]
        public int? CompletedByUserId { get; set; }

        public DateTime? DueOn { get; set; }

        public DateTime? CompletedOn { get; set; }

        [Alias("PropertyID")]
        public int? PropertyId { get; set; }

        [Alias("ContactID")]
        public int? ContactId { get; set; }

        [Alias("MessageID")]
        public int? MessageId { get; set; }

        public Attachments Attachments { get; set; }

        public int? PropertyLeaseId { get; set; }

        [Alias("WorkflowID")]
        public int? WorkflowId { get; set; }

        public DateTime? ActivatedOn { get; set; }
    }
}
