﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using MyHome.v4.PropertyManager.Schema.Contacts;
using MyHome.v4.PropertyManager.Schema.Document;
using MyHome.v4.PropertyManager.Schema.Property;

namespace MyHome.v4.PropertyManager.Schema.Calendar
{
    public class Attachments
    {
        public Attachments()
        {
            Documents = new List<DocumentSnippet>();
            Properties = new List<PropertySnippet>();
            Contacts = new List<ContactSnippet>();
            Events = new List<CalendarEventSnippet>();
        }

        [DataMember]
        public List<DocumentSnippet> Documents { get; set; }
        [DataMember]
        public List<PropertySnippet> Properties { get; set; }
        [DataMember]
        public List<ContactSnippet> Contacts { get; set; }
        [DataMember]
        public List<CalendarEventSnippet> Events { get; set; }
    }
}
