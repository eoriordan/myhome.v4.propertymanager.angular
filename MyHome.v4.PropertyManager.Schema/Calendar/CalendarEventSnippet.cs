﻿using System;
using System.Runtime.Serialization;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Calendar
{
    [Serializable]
    [DataContract]
    public class CalendarEventSnippet
    {
        [DataMember]
        [Alias("ID")]
        public string Id { get; set; }

        [DataMember]
        public string What { get; set; }

        [DataMember]
        public GoogleDateRange When { get; set; }

        [DataMember]
        public string Where { get; set; }

        [DataMember]
        [Alias("CalendarOwnerUserID")]
        public int CalendarOwnerUserId { get; set; }
    }
}
