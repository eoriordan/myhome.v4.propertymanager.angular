﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.PropertyManager.Schema.Calendar;
using MyHome.v4.PropertyManager.Schema.Contacts;
using MyHome.v4.PropertyManager.Schema.Document;
using MyHome.v4.PropertyManager.Schema.Property;

namespace MyHome.v4.PropertyManager.Schema.Activity
{
    [Serializable]
    [DataContract]
    public class ActivityAttachments
    {
        public ActivityAttachments()
        {
            Documents = new List<DocumentSnippet>();
            Properties = new List<PropertySnippet>();
            Events = new List<CalendarEventSnippet>();
            Contacts = new List<ContactSnippet>();
        }

        [DataMember]
        public List<DocumentSnippet> Documents { get; set; }

        [DataMember]
        public List<PropertySnippet> Properties { get; set; }

        [DataMember]
        public List<CalendarEventSnippet> Events { get; set; }

        [DataMember]
        public List<ContactSnippet> Contacts { get; set; }

        [DataMember]
        public string Notes { get; set; }

        [DataMember]
        public decimal? Amount { get; set; }

        public int Count { get { return Documents.Count + Properties.Count + Events.Count; } }

    }
}
