﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Activity
{
    public class ActivityContact
    {
        [Alias("ActivityContactID")]
        [AutoIncrement]
        [PrimaryKey]
        public int ActivityContactId { get; set; }

        [Alias("ActivityID")]
        public int ActivityId { get; set; }

        [Alias("ContactID")]
        public int ContactId { get; set; }
    }
}
