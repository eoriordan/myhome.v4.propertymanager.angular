﻿using System;
using MyHome.v4.Common.Schema;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Activity
{
    public class Activity
    {
        [Alias("ActivityId")]
        [AutoIncrement]
        [PrimaryKey]
        public int ActivityId { get; set; }

        [Alias("ActivityTypeID")]
        public int ActivityTypeId { get; set; }

        [Alias("UserID")]
        public int? UserId { get; set; }

        [Alias("GroupID")]
        public int GroupId { get; set; }

        [Alias("PropertyID")]
        public int? PropertyId { get; set; }

        [Alias("ProductID")]
        public int? ProductId { get; set; }

        public ActivityAttachments Attachments { get; set; }

        public string Summary { get; set; }

        public string Title { get; set; }

        public string SubTitle { get; set; }

        [Alias("CreatedOnDate")]
        public DateTime CreatedOn { get; set; }

        public StringDictionary CustomData { get; set; }

        [Alias("PropertyClassID")]
        public int? PropertyClassId { get; set; }

        [Alias("LocalityID")]
        public int? LocalityId { get; set; }

        [Alias("ContactID")]
        public int? ContactId { get; set; }

        [Alias("ChannelID")]
        public int? ChannelId { get; set; }

        public bool IsPrivate { get; set; }
    }
}
