﻿using System;
using MyHome.v4.Common.Schema;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Contacts
{
    public class PropertyContactWithContact
    {
        public int PropertyContactId { get; set; }
        [Alias("PropertyID")]
        public int PropertyId { get; set; }
        [Alias("ContactID")]
        public int ContactId { get; set; }
        [Alias("PropertyContactTypeID")]
        public short PropertyContactTypeId { get; set; }
        public string Notes { get; set; }
        [Alias("GroupID")]
        public int? GroupId { get; set; }
        [Alias("UserID")]
        public int? UserId { get; set; }
        public bool IsPublic { get; set; }
        public string IndexData { get; set; }
        public Tags TagList { get; set; }
        public ContactDetails ContactDetails { get; set; }
        public DateTime CreatedOn { get; set; }
        [Alias("CreatedByUserID")]
        public int? CreatedByUserId { get; set; }
        public ContactConfiguration Configuration { get; set; }
        public string LegacyMappings { get; set; }
        [Alias("LegacyUserID")]
        public int? LegacyUserId { get; set; }
        public StringDictionary CustomFormData { get; set; }
        public bool IsDeleted { get; set; }
        [Alias("RepresentsGroupID")]
        public int? RepresentsGroupId { get; set; }
        [Alias("RepresentsUserID")]
        public int? RepresentsUserId { get; set; }
        [Alias("ChannelID")]
        public int? ChannelId { get; set; }
        [Alias("RegionID")]
        public int? RegionId { get; set; }
        [Alias("LocalityID")]
        public int? LocalityId { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }
}
