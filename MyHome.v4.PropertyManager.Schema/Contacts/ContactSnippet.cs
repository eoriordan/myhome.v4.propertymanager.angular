﻿using System;
using System.Runtime.Serialization;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Contacts
{
    [Serializable]
    [DataContract]
    public class ContactSnippet
    {
        [DataMember]
        [Alias("ContactID")]
        public int ContactId { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
