﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Contacts
{
    public class PropertyContact
    {
        [Alias("PropertyContactID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyContactId { get; set; }
        [Alias("PropertyID")]
        public int PropertyId { get; set; }
        [Alias("ContactID")]
        public int ContactId { get; set; }
        [Alias("PropertyContactTypeID")]
        public short PropertyContactTypeId { get; set; }
        public string Notes { get; set; }
    }

}
