﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.Common.Schema;

namespace MyHome.v4.PropertyManager.Schema.Contacts
{
    public class ContactConfiguration
    {
        public ContactConfiguration()
        {
            BankAccounts = new List<BankAccount>();
        }

        [DataMember]
        public string GoogleContactId { get; set; }
        [DataMember]
        public List<BankAccount> BankAccounts { get; set; }
    }
}
