﻿using MyHome.v4.Common.Schema;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Groups
{

    /// <summary>
    /// The group category table which defines which category a group is a member of 
    /// </summary>
    public class GroupCategory
    {
        public GroupCategory()
        {
            Media = new Media();
        }

        [Alias("GroupCategoryID")]
        public int GroupCategoryId { get; set; }

        [Alias("ParentGroupCategoryIDList")]
        public string ParentGroupCategoryIdList { get; set; }

        [Alias("Name")]
        public string Name { get; set; }

        [Alias("Description")]
        public string Description { get; set; }

        [Alias("Media")]
        public Media Media { get; set; }

        [Alias("UrlSlug")]
        public string UrlSlug { get; set; }

        [Alias("IsServiceCategory")]
        public bool IsServiceCategory { get; set; }

        [Alias("Order")]
        public short Order { get; set; }
    }
}
