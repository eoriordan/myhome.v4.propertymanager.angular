﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.Common.Schema.Forms;

namespace MyHome.v4.PropertyManager.Schema.Groups
{
    [DataContract]
    [Serializable]
    public class GroupPrivateConfiguration
    {
        public GroupPrivateConfiguration()
        {
            GoogleDocsUsers = new List<GoogleDocsUser>();
            PropertyForm = new Form();
            ContactForm = new Form();
            ContactTags = new List<Tag>();
            PublicTagGroups = new TagGroups();
            PrivateTagGroups = new TagGroups();
            Tags = new List<Tag>();
        }

        [DataMember]
        public string GoogleConversionCode { get; set; }
        [DataMember]
        public string GoogleAppsDomain { get; set; }
        [DataMember]
        public string EmailApiUrl { get; set; }
        [DataMember]
        public string EmailApiKey { get; set; }
        [DataMember]
        public string EmailNotificationsSender { get; set; }
        [DataMember]
        public string EmailNotificationsReplyTo { get; set; }
        [DataMember]
        public string WeeklyReportEmail { get; set; }
        [DataMember]
        public List<GoogleDocsUser> GoogleDocsUsers { get; set; }
        [DataMember]
        public string GoogleDocsFolderId { get; set; }
        [DataMember]
        public TagGroups PublicTagGroups { get; set; }
        [DataMember]
        public TagGroups PrivateTagGroups { get; set; }
        [DataMember]
        public List<Tag> ContactTags { get; set; }
        [DataMember]
        public List<Tag> Tags { get; set; }
        [DataMember]
        public bool PriceHistoryOptOut { get; set; }
        [DataMember]
        public bool EnablePropertyAlerts { get; set; }
        [DataMember]
        public bool IsThisYourPropertyOptOut { get; set; }
        [DataMember]
        public string PdfListingsUrl { get; set; }
        [DataMember]
        public FloorplannerSettings Floorplanner { get; set; }
        [DataMember]
        public Form PropertyForm { get; set; }
        [DataMember]
        public Form ContactForm { get; set; }
        [DataMember]
        public bool? HasReceivedLandlordFreeCurrencyCredit { get; set; } //NOTE: GJ: this can be removed once the private landlord offer is over
    }

    [DataContract]
    [Serializable]
    public class GoogleDocsUser
    {
        public GoogleDocsUser()
        {
            IsActive = true;
        }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
    }

    [DataContract]
    [Serializable]
    public class TagGroups : List<TagGroup>
    {

    }

    [DataContract]
    [Serializable]
    public class TagGroup
    {
        public TagGroup()
        {
            Tags = new List<Tag>();
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<Tag> Tags { get; set; }
    }

    [DataContract]
    [Serializable]
    public class Tag
    {
        public Tag()
        {

        }

        public Tag(string name)
        {
            Name = name;
        }

        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    [Serializable]
    public class FloorplannerSettings
    {
        [DataMember(Name = "UserID")]
        public int UserId { get; set; }
    }
}