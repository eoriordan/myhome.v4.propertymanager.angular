﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Groups
{
    /// <summary>
    /// The group type object defines the type of group
    /// </summary>
    public class GroupType
    {
        [Alias("GroupTypeID")]
        public int GroupTypeId { get; set; }

        [Alias("Name")]
        public string Name { get; set; }
    }
}
