﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.Common.Schema;

namespace MyHome.v4.PropertyManager.Schema.Groups
{
    [DataContract]
    [Serializable]
    public class GroupConfiguration
    {
        public GroupConfiguration()
        {
            Data = new StringDictionary();
            BrochureSettings = BrochureSettings.Default;
            RentalCrmSettings = new RentalCrmSettings();
            BankAccounts = new List<BankAccount>();
            CompanySettings = new CompanySettings();
            FeaturedProperties = new List<GroupFeaturedProperty>();
        }

        [DataMember]
        public string BrandBoosterColour { get; set; }

        [DataMember]
        public string BrochurePrinterEmail { get; set; } //where we send brochure print requests to

        [DataMember]
        public string PropertyManagerCssClass { get; set; }

        [DataMember]
        public string PropertyManagerCustomCss { get; set; }

        [DataMember]
        public BrochureSettings BrochureSettings { get; set; }

        [DataMember]
        public RentalCrmSettings RentalCrmSettings { get; set; }

        [DataMember]
        public StringDictionary Data { get; set; }

        [DataMember]
        public CompanySettings CompanySettings { get; set; }

        [DataMember]
        public List<BankAccount> BankAccounts { get; set; }

        [DataMember]
        public List<GroupFeaturedProperty> FeaturedProperties { get; set; }
    }
}
