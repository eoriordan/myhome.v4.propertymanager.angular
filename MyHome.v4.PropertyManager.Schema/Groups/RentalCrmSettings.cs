﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Groups
{
    [DataContract]
    [Serializable]
    public class RentalCrmSettings
    {
        private const int DaysPerWeek = 7;
        private const int DefaultLeaseInspectionReminderFrequencyInDays = 4 * 4 * DaysPerWeek;
        private const int DefaultLeaseExpiryReminderNoticeInDays = 6 * DaysPerWeek;
        private const decimal DefaultDailyPenaltyAmount = 8.00M;
        private const decimal DefaultAdministrativeChargeAmount = 50.00M;

        public RentalCrmSettings()
        {
            LeaseInspectionReminderFrequencyInDays = DefaultLeaseInspectionReminderFrequencyInDays;
            LeaseExpiryReminderNoticeInDays = DefaultLeaseExpiryReminderNoticeInDays;
            DailyPenaltyAmount = DefaultDailyPenaltyAmount;
            AdministrativeChargeAmount = DefaultAdministrativeChargeAmount;
        }

        [DefaultValue(DefaultLeaseInspectionReminderFrequencyInDays)]
        [DataMember]
        public int LeaseInspectionReminderFrequencyInDays { get; set; }
        [DefaultValue(DefaultLeaseExpiryReminderNoticeInDays)]
        [DataMember]
        public int LeaseExpiryReminderNoticeInDays { get; set; }
        [DefaultValue(typeof(decimal), "8.00")]
        [DataMember]
        public decimal DailyPenaltyAmount { get; set; }
        [DefaultValue(typeof(decimal), "50.00")]
        [DataMember]
        public decimal AdministrativeChargeAmount { get; set; }
    }
}
