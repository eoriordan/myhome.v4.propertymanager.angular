﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Groups
{
    [DataContract]
    [Serializable]
    public class CompanySettings
    {
        [DataMember]
        public string VatNumber { get; set; }
        [DataMember]
        public string ApiEmail { get; set; }
        [DataMember]
        public string CroNumber { get; set; }
        [DataMember]
        public string SalesLicenseNumber { get; set; }
        [DataMember]
        public string RentalsLicenseNumber { get; set; }
        [DataMember]
        public string MarketingDisclaimer { get; set; }
        [DataMember]
        public string IndemnityInsurance { get; set; }
        [DataMember]
        public string IndemnityPolicyNo { get; set; }
        [DataMember]
        public string ClientAccountName { get; set; }
        [DataMember]
        public string ClientAccountBank { get; set; }
        [DataMember]
        public string ComplaintContactDetails { get; set; }
    }
}
