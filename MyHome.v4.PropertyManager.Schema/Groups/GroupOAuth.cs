﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Groups
{
    /// <summary>
    /// The group oauth credentials used for the persistence of oauth configuration for a specified group
    /// </summary>
    [DataContract]
    public class GroupOAuth
    {
        [DataMember]
        public int GroupOAuthID { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string AccessToken { get; set; }

        [DataMember]
        public string TokenType { get; set; }

        [DataMember]
        public int ExpiresInSeconds { get; set; }

        [DataMember]
        public string RefreshToken { get; set; }

        [DataMember]
        public string Scope { get; set; }

        [DataMember]
        public DateTime Issued { get; set; }
    }
}
