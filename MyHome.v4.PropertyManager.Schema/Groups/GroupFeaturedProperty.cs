﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Groups
{
    [DataContract]
    [Serializable]
    public class GroupFeaturedProperty
    {

        public GroupFeaturedProperty() { }

        public GroupFeaturedProperty(int propertyId, int propertyClassId)
        {
            PropertyId = propertyId;
            PropertyClassId = propertyClassId;
        }

        [DataMember(Name = "PropertyID")]
        public int PropertyId { get; set; }

        [DataMember(Name = "PropertyClassID")]
        public int PropertyClassId { get; set; }

    }
}
