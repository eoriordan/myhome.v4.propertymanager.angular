﻿using System;
using MyHome.v4.Common.Schema;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Groups
{
    /// <summary>
    /// The group object provide the access to the group details 
    /// </summary>
    public class Group
    {
        [Alias("GroupID")]
        [AutoIncrement]
        [PrimaryKey]
        public int GroupId { get; set; }

        [Alias("Name")]
        public string Name { get; set; }

        [Alias("ShortName")]
        public string ShortName { get; set; }

        [Alias("GroupTypeID")]
        public int GroupTypeId { get; set; }

        [Alias("ParentGroupId")]
        public int? ParentGroupId { get; set; }

        [Alias("GroupCategoryID1")]
        public int? GroupCategoryOneId { get; set; }

        [Alias("GroupCategoryID2")]
        public int? GroupCategoryTwoId { get; set; }

        [Alias("ContactDetails")]
        public ContactDetails ContactDetails { get; set; }

        [Alias("PackageCredits")]
        public int PackageCredits { get; set; }

        [Alias("PurchasedCredits")]
        public int PurchasedCredits { get; set; }

        [Alias("Media")]
        public Media Media { get; set; }

        [Alias("LocalityID")]
        public int? LocalityId { get; set; }

        [Alias("Map")]
        public Map Map { get; set; }

        [Alias("Content")]
        public Content Content { get; set; }

        [Alias("AccountCode")]
        public string AccountCode { get; set; }

        [Alias("AccountManagerID")]
        public int? AccountManagerId { get; set; }

        //ReportSubscription
        //LegacyMappings

        [Alias("GroupConfiguration")]
        public GroupConfiguration GroupConfiguration { get; set; }

        [Alias("GroupPrivateConfiguration")]
        public GroupPrivateConfiguration GroupPrivateConfiguration { get; set; }

        [Alias("LegacyUserId")]
        public int? LegacyUserId { get; set; }

        [Alias("LastMonthlyBillingRun")]
        public DateTime? LastMonthlyBillingRun { get; set; }

        [Alias("RefreshCredits")]
        public int RefreshCredits { get; set; }

        [Alias("BillInAdvance")]
        public bool BillInAdvance { get; set; }

        [Alias("DisplayAsBrandbooster")]
        public int DisplayAsBrandbooster { get; set; }

        [Alias("RandomisedOrder")]
        public short RandomisedOrder { get; set; }

        [Alias("GroupIdentifier")]
        public string GroupIdentifier { get; set; }

        [Alias("LocalityID2")]
        public int? LocalityTwoId { get; set; }

        [Alias("LocalityID3")]
        public int? LocalityThreeId { get; set; }

        [Alias("GroupCategoryID3")]
        public int? GroupCategoryThreeId { get; set; }

        [Alias("ChannelId")]
        public int ChannelId { get; set; }

        [Alias("CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [Alias("CurrencyCredit")]
        public decimal CurrencyCredit { get; set; }

        [Alias("CustomData")]
        public string CustomData { get; set; }

        [Alias("UrlSlugIdentifier")]
        public string UrlSlugIdentifier { get; set; }

        [Alias("SapAccountCode")]
        public string SapAccountCode { get; set; }

        [Alias("GroupOAuthID")]
        public int? GroupOAuthID { get; set; }
    }
}
