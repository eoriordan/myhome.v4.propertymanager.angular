﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Locality
{
    /// <summary>
    /// The database poco class for dealing with the persistence of localities
    /// </summary>
    public class Locality
    {
        [AutoIncrement]
        [Alias("LocalityID")]
        public int LocalityId { get; set; }

        [Alias("Name")]
        public string Name { get; set; }

        [Alias("DisplayNameOverride")]
        public string DisplayNameOverride { get; set; }

        [Alias("UrlSlug")]
        public string UrlSlug { get; set; }

        [Alias("Code")]
        public string Code { get; set; }

        [Alias("Latitude")]
        public double Latitude { get; set; }

        [Alias("Longitude")]
        public double Longitude { get; set; }

        [Alias("Description")]
        public string Description { get; set; }

        [Alias("NoIndex")]
        public bool NoIndex { get; set; }

        [Alias("MapZoom")]
        public int? MapZoom { get; set; }

        [Alias("TopSpotAvailable")]
        public bool TopSpotAvailable { get; set; }

        [Alias("CreatedOn")]
        public DateTime CreatedOn { get; set; }
    }
}
