﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Locality
{
    /// <summary>
    /// The locality mapping poco for dealing with the mapping of localities to each other
    /// </summary>
    public class LocalityMapping
    {
        [AutoIncrement]
        [Alias("LocalityMappingID")]
        public int LocalityMappingId { get; set; }

        [Alias("ParentLocalityID")]
        public int ParentLocalityId { get; set; }

        [Alias("ChildLocalityID")]
        public int ChildLocalityId { get; set; }

        [Alias("ChannelID")]
        public int ChannelId { get; set; }
    }
}
