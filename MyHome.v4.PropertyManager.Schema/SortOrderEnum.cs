﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema
{
    /// <summary>
    /// The sort column for the database fields
    /// </summary>
    [Flags]
    public enum SortColumn
    {
        /// <summary>
        /// Sort by the added date
        /// </summary>
        [EnumMember]
        AddedDate = 1,
        /// <summary>
        /// Sort by the refreshed date
        /// </summary>
        [EnumMember]
        RefreshedDate = 2,
        /// <summary>
        /// Sort by the modified date
        /// </summary>
        [EnumMember]
        ModifiedDate = 3,
        /// <summary>
        /// Sort by the price
        /// </summary>
        [EnumMember]
        Price = 4,
        /// <summary>
        /// Sort by the address
        /// </summary>
        [EnumMember]
        Address = 5,
        /// <summary>
        /// Sort by the address
        /// </summary>
        [EnumMember]
        Score = 5
    }

    /// <summary>
    /// The sort column for property
    /// </summary>
    [Flags]
    public enum SortDirection
    {
        /// <summary>
        /// Sort ascending
        /// </summary>
        [EnumMember]
        Asc = 1,
        /// <summary>
        /// Sort decending
        /// </summary>
        [EnumMember]
        Desc = 2
    }
}
