﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.FeaturedProduct
{
    public class FeaturedSpot
    {
        [Alias("FeaturedSpotID")]
        [AutoIncrement]
        [PrimaryKey]
        public int FeaturedSpotId { get; set; }
        [Alias("FeaturedSpotTypeID")]
        public int FeaturedSpotTypeId { get; set; }
        [Alias("ProductID")]
        public int ProductId { get; set; }
        [Alias("OrderID")]
        public int OrderId { get; set; }
        [Alias("PropertyID")]
        public int? PropertyId { get; set; }
        [Alias("GroupID")]
        public int GroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        [Alias("LocalityID")]
        public int LocalityId { get; set; }
        [Alias("PropertyClassID")]
        public int PropertyClassId { get; set; }
        [Alias("CreatedByUserID")]
        public int CreatedByUserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime? EventStartsOn { get; set; }
        public DateTime? EventEndsOn { get; set; }
        public string CustomJson { get; set; }
    }

}
