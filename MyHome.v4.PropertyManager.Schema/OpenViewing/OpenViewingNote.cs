﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.OpenViewing
{
    public class OpenViewingNote
    {
        public OpenViewingNote()
        {
            DateCreated = DateTime.Now;
        }

        [AutoIncrement]
        [PrimaryKey]
        public int Id { get; set; }

        public int OpenViewingId { get; set; }

        public int? UserId { get; set; }

        public string NoteText { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
