﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.OpenViewing
{
    public class OpenViewingContactMapping
    {
        [Alias("OpenViewingID")]
        [PrimaryKey]
        public int OpenViewingId { get; set; }

        [Alias("ContactID")]
        [PrimaryKey]
        public int ContactId { get; set; }
    }
}
