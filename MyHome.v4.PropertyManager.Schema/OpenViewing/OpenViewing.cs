﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.OpenViewing
{
    public class OpenViewing
    {
        [Alias("OpenViewingID")]
        [AutoIncrement]
        [PrimaryKey]
        public int OpenViewingId { get; set; }

        [Alias("PropertyID")]
        public int PropertyId { get; set; }

        [Alias("GroupID")]
        public int GroupId { get; set; }

        [Alias("CreatedByUserID")]
        public int? CreatedByUserId { get; set; }

        [Alias("ModifiedByUserID")]
        public int? ModifiedByUserId { get; set; }

        public bool IsPublic { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
