﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.OpenViewing
{
    public class OpenViewingUserMapping
    {
        [Alias("OpenViewingID")]
        [PrimaryKey]
        public int OpenViewingId { get; set; }

        [Alias("UserID")]
        [PrimaryKey]
        public int UserId { get; set; }
    }
}
