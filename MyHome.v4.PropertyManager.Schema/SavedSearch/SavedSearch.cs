﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.SavedSearch
{
    public class SavedSearch
    {
        [Alias("SavedSearchID")]
        [AutoIncrement]
        [PrimaryKey]
        public int SavedSearchId { get; set; }

        [Alias("UserID")]
        public int? UserId { get; set; }

        [Alias("GroupID")]
        public int? GroupId { get; set; }

        [Alias("ContactID")]
        public int? ContactId { get; set; }

        public string Title { get; set; }

        public SearchPropertiesRequest SearchPropertiesRequest { get; set; }

        public string IndexData { get; set; }

        public NotificationSetting NotificationSettings { get; set; }

        [Alias("CreatedByUserID")]
        public int CreatedByUserId { get; set; }

        [Alias("CreatedByGroupID")]
        public int CreatedByGroupId { get; set; }

        [Alias("BrandedAsGroupID")]
        public int? BrandedAsGroupId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? LastNotificationCheck { get; set; }

        public DateTime? LastNotificationSent { get; set; }

        public DateTime? NextNotificationCheck { get; set; }

        public string EditKey { get; set; }

        public int NotificationsSent { get; set; }

        public bool IsError { get; set; }
    }
}
