﻿using System.Collections.Generic;
using MyHome.v4.Common.Schema;
using MyHome.v4.Common.Schema.Enumerations;

namespace MyHome.v4.PropertyManager.Schema.SavedSearch
{
    public class SearchPropertiesRequest
    {
        public List<int> GroupIDs { get; set; }
        public TrafficType TrafficType { get; set; }
        public int? RegionID { get; set; }
        public List<int> LocalityIDs { get; set; }
        public List<int> PropertyTypeIDs { get; set; }
        public List<int> PropertyIDs { get; set; }
        public List<int> PortfolioIDs { get; set; }
        public List<int> SaleTypeIDs { get; set; }
        public bool ReturnSeo { get; set; }
        public bool ReturnTopSpot { get; set; }
        public bool ReturnAgentSnippets { get; set; }
        public bool ExpandLocalities { get; set; }
        public bool IncludeContacts { get; set; }
        public bool IncludeBestOffer { get; set; }
        public bool IncludeLease { get; set; }
        public bool IncludeFeaturedSpots { get; set; }
        public bool SearchContent { get; set; }
        public bool UseFreeTextSearchForKeywords { get; set; }
        public bool PreSixtyThreeOnly { get; set; }
        public bool IsNewBuildOnly { get; set; }
        public PropertySection? PropertySection { get; set; }
        public List<PropertyStatusEnum> PropertyStatuses { get; set; }
        public RowStatusEnum? RowStatus { get; set; }
        public List<PropertyClassEnum> PropertyClasses { get; set; }
        public List<string> Tags { get; set; }
        public List<string> PrivateTags { get; set; }
        public int? MinPrice { get; set; }
        public int? MaxPrice { get; set; }
        public int? MinBeds { get; set; }
        public int? MaxBeds { get; set; }
        public int? MinSize { get; set; }
        public int? MaxSize { get; set; }
        public bool SaveLiveSearch { get; set; }
        public bool SkipSearchIndex { get; set; }
        public SortColumn SortColumn { get; set; }
        public SortDirection SortDirection { get; set; }
        public ViewType ViewType { get; set; }
        public bool IsPremiumAd { get; set; }
        public List<MapPolygon> Polygons { get; set; }
        public bool ForceSpatialIndex { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public int? Channel { get; set; }
    }

    public enum TrafficType
    {
        Internal = 0,
        Website = 1,
        Mobile = 2,
    }

    public enum RowStatusEnum
    {
        Banned = 4,
        Completed = 6,
        Deleted = 3,
        Expired = 7,
        Live = 5,
        Normal = 2,
        Pending = 1
    }
}
