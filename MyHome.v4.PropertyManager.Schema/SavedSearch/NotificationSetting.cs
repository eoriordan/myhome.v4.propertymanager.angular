﻿namespace MyHome.v4.PropertyManager.Schema.SavedSearch
{
    public class NotificationSetting
    {
        public string Type { get; set; }
        public string Frequency { get; set; }
    }
}
