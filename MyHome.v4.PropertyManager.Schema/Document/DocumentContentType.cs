﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Document
{
    public class DocumentContentType
    {
        [Alias("DocumentContentTypeID")]
        [AutoIncrement]
        [PrimaryKey]
        public int DocumentContentTypeId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool? IsVirusCheckRequired { get; set; }
    }
}
