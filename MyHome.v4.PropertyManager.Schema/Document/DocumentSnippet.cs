﻿using System;
using System.Runtime.Serialization;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Document
{
    /// <summary>
    /// The short version of a document stored in google cloud storage
    /// </summary>
    [Serializable]
    [DataContract]
    public class DocumentSnippet
    {
        public DocumentSnippet()
        {
            DocumentContentTypeId = 23; //Unkown
        }

        /// <summary>
        /// The unique id of the document
        /// </summary>
        [DataMember]
        [Alias("DocumentID")]
        public int DocumentId { get; set; }

        /// <summary>
        /// The name of the document to display to the user
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// The flag to indicate if this is a physical file or a google document
        /// </summary>
        [DataMember]
        public bool IsFile { get; set; }

        /// <summary>
        /// The document type of the document
        /// </summary>
        [DataMember]
        public int? DocumentType { get; set; }

        /// <summary>
        /// The document content type associated with the document
        /// </summary>
        [DataMember]
        [Alias("DocumentContentTypeID")]
        public int DocumentContentTypeId { get; set; }
    }
}
