﻿namespace MyHome.v4.PropertyManager.Schema.Document
{
    public enum DocumentTypeEnum
    {
        AntiSocialBehaviourLetter = 33,
        AntiSocialBehaviourLetter7Daynotice = 32,
        BuyerSolicitorLetter = 20,
        ClosingStatement = 29,
        DepositReceipt = 21,
        DepositRefund = 22,
        EraDowneyExtendedTwoPageBrochure = 8,
        EraDowneyWindowBrochure = 7,
        HodnettFordeFourPageBrochure = 52,
        HodnettFordeTwoPageBrochure = 51,
        HodnettFordeWindowBrochure = 50,
        InspectionDocument = 10,
        KillianLynchTwoPageBrochure = 16,
        LandlordInstructionLetter = 43,
        LandlordRenewalLetter = 31,
        LateRentWarning = 34,
        MarketingProposal = 11,
        MetroBrochure = 4,
        NoticetoQuit = 35,
        OfficeAdsReceipt = 27,
        OfficeAndAdsFeeBilled = 28,
        Other = 1,
        PadraigSmithBrochure = 48,
        PersistentlyLateRentWarning = 37,
        PersistentlyLateRentWarningWithFee = 36,
        PortraitWindowBrochure = 5,
        PropertyInspectionReport = 45,
        PropertyOfferExport = 49,
        PropertyOfferNotification = 14,
        PropertyServicesAgreement = 9,
        ReaFourPageBrochure = 13,
        ReaTwoPageBrochure = 12,
        RentIncreaseNotice = 38,
        RentalsFeeInvoice = 47,
        RentalsGenericDocument = 30,
        SaleAgreedBuyerLetter = 25,
        SaleAgreedBuyerSolicitor = 26,
        SaleAgreedVendorLetter = 23,
        SaleAgreedVendorSolicitor = 24,
        StandingOrder = 39,
        StatementofAdvisedLettingValue = 42,
        StatementOfAdvisedMarketValue = 17,
        TenantInventoryForm = 44,
        TenantReceipt = 46,
        TenantReference = 40,
        TenantRenewalLetter = 41,
        TwoPageBrochure = 2,
        TwoPageSingleFeaturesBrochure = 6,
        VendorInstruction = 18,
        VendorSolicitorLetter = 19,
        WindowBrochure = 3
    }
}
