﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Document
{
    public class Document
    {
        [Alias("DocumentID")]
        [AutoIncrement]
        [PrimaryKey]
        public int DocumentId { get; set; }

        [Alias("GroupID")]
        public int GroupId { get; set; }

        [Alias("CreatedByUserID")]
        public int CreatedBy { get; set; }

        [Alias("PropertyID")]
        public int? PropertyId { get; set; }

        [Alias("ContactID")]
        public int? ContactId { get; set; }

        [Alias("WorkflowInstanceID")]
        public int? WorkflowInstanceId { get; set; }

        public string Name { get; set; }

        [Alias("GoogleDocumentID")]
        public string GoogleDocId { get; set; }

        public string GoogleDocsUsername { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsFile { get; set; }

        public int? FileSizeInBytes { get; set; }

        public Guid? Guid { get; set; }

        public string DocumentPreview { get; set; }

        public bool? UseCustomConverter { get; set; }

        public string Extra { get; set; }

        public DateTime ModifiedOn { get; set; }

        [Alias("DocumentTypeID")]
        public int DocumentTypeId { get; set; }

        [Alias("DocumentContentTypeID")]
        public int DocumentContentTypeId { get; set; }

        public bool IsVirusChecked { get; set; }
    }
}
