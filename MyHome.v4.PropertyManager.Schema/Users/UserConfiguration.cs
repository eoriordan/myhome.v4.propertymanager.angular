﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Users
{
    [DataContract]
    [Serializable]
    public class UserConfiguration
    {
        public UserConfiguration()
        {
            NotificationSettings = new UserNotificationSettings();
        }

        [DataMember]
        public string GoogleAuthSubToken { get; set; }

        [DataMember]
        public string GoogleAuthEmail { get; set; }

        [DataMember]
        public string GoogleAuthAccessToken { get; set; }

		[DataMember]
        public string GoogleAuthTokenType { get; set; }

		[DataMember]
        public long? GoogleAuthExpiresInSeconds { get; set; }

		[DataMember]
        public string GoogleAuthRefreshToken { get; set; }

        [DataMember]
        public string GoogleAuthScope { get; set; }

        [DataMember]
        public DateTime GoogleAuthIssued { get; set; }

        [DataMember]
        public bool IsSubscribedToNewsletter { get; set; }

        [DataMember]
        public string EulaVersion { get; set; }

        [DataMember]
        public DateTime? EulaAcceptedOn { get; set; }

        [DataMember]
        public UserNotificationSettings NotificationSettings { get; set; }

    }
}
