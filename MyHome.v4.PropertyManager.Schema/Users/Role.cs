﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Users
{
    public class Role
    {
        [Alias("RoleID")]
        public int RoleId { get; set; }

        [Alias("Name")]
        public string Name { get; set; }
    }
}
