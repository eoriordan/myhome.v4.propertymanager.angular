﻿using System;
using System.Collections.Generic;
using MyHome.v4.Common.Schema;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Users
{
    public class User
    {
        public User()
        {
            UserConfiguration = new UserConfiguration();
            LegacyMappingsJson = new LegacyMappings();
            FavouritePropertyIds = new List<int>();
            ContactDetails = new ContactDetails();
            UserPreference = new UserPreference();
        }

        [AutoIncrement]
        [PrimaryKey]
        [Alias("UserID")]
        public int UserId { get; set; }

        [Alias("Username")]
        public string Username { get; set; }

        [Alias("GroupID")]
        public int GroupId { get; set; }

        [Alias("ContactDetails")]
        public ContactDetails ContactDetails { get; set; }

        [Alias("EmailConfirmed")]
        public bool EmailConfirmed { get; set; }

        [Alias("EmailConfirmationKey")]
        public string EmailConfirmationKey { get; set; }

        [Alias("MobileConfirmed")]
        public bool MobileConfirmed { get; set; }

        [Alias("HashedPassword")]
        public string HashedPassword { get; set; }

        [Alias("PasswordSalt")]
        public string PasswordSalt { get; set; }

        [Alias("Source")]
        public string Source { get; set; }

        [Alias("CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [Alias("ModifiedOn")]
        public DateTime ModifiedOn { get; set; }

        [Alias("FavouritePropertyIDs")]
        public List<int> FavouritePropertyIds { get; set; }

        [Alias("LastLoggedOn")]
        public DateTime LastLoggedOn { get; set; }

        [Alias("RowStatusID")]
        public int RowStatusId { get; set; }

        [Alias("LegacyMappingsJson")]
        public LegacyMappings LegacyMappingsJson { get; set; }

        [Alias("LegacyUserID")]
        public int? LegacyUserId { get; set; }

        [Alias("UserConfiguration")]
        public UserConfiguration UserConfiguration { get; set; }

        [Alias("IsPerson")]
        public bool IsPerson { get; set; }

        [Alias("ConfirmationKeyExpiresOn")]
        public DateTime? ConfirmationKeyExpiresOn { get; set; }

        [Alias("Media")]
        public Media Media { get; set; }

        [Alias("ChannelID")]
        public int ChannelId { get; set; }

        [Alias("UserPreferenceJson")]
        public UserPreference UserPreference { get; set; }

    }
}
