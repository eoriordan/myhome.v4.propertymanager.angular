﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Users
{
    [Serializable]
    [DataContract]
    public class UserNotificationSettings
    {
        public UserNotificationSettings()
        {
            ReceiveAdRefreshReminders = true;
            ReceiveAdExpiryReminders = true;
            ReceiveAdEnquiries = true;
            ReceiveAdTrafficReport = true;
        }

        [DataMember]
        public bool ReceiveAdRefreshReminders { get; set; }
        [DataMember]
        public bool ReceiveAdExpiryReminders { get; set; }
        [DataMember]
        public bool ReceiveAdEnquiries { get; set; }
        [DataMember]
        public bool ReceiveAdTrafficReport { get; set; }
    }
}
