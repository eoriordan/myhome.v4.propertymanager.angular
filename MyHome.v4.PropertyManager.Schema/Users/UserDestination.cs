﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Users
{
    public class UserDestination
    {
        [AutoIncrement]
        [PrimaryKey]
        [Alias("Id")]
        public int Id { get; set; }
        [Alias("UserId")]
        public int UserId { get; set; }
        [Alias("Title")]
        public string Title { get; set; }
        [Alias("Latitude")]
        public double Latitude { get; set; }
        [Alias("Longitude")]
        public double Longitude { get; set; }
        [Alias("TravelTimeInMinutes")]
        public double TravelTimeInMinutes { get; set; }
    }
}
