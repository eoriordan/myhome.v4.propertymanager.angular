﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Users
{    
    /// <summary>
    /// The user session in the database used to store current and expired user sessions
    /// </summary>
    public class UserSession 
    {
        [AutoIncrement]
        [PrimaryKey]
        [Alias("UserSessionID")]
        public int UserSessionId { get; set; }
        
        [Alias("SessionID")]
        public Guid SessionId { get; set; }
        
        [Alias("UserID")]
        public int UserId { get; set; }
        
        [Alias("GroupID")]
        public int GroupId { get; set; }
        
        [Alias("ChannelID")]
        public int ChannelId { get; set; }
        
        [Alias("IPAddress")]
        public string IpAddress { get; set; }
        
        [Alias("UserAgent")]
        public string UserAgent { get; set; }
        
        [Alias("Created")]
        public DateTime CreatedOn { get; set; }
        
        [Alias("LastActivity")]
        public DateTime LastActivityOn { get; set; }
    }

}
