﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Users
{
    /// <summary>
    /// The role member ship of users in the application
    /// </summary>
    public class RoleMembership
    {
        [Alias("RoleMembershipID")]
        public int RoleMembershipId { get; set; }

        [Alias("UserID")]
        public int UserId { get; set; }

        [Alias("RoleID")]
        public int RoleId { get; set; }
    }
}
