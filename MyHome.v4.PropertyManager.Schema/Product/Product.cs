﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Product
{
    public class Product
    {
        [Alias("ProductID")]
        [AutoIncrement]
        [PrimaryKey]
        public int ProductId { get; set; }

        [Alias("ProductRateCardID")]
        public int ProductRateCardId { get; set; }

        [Alias("ProductTypeID")]
        public int ProductTypeId { get; set; }

        [Alias("BillingPeriodID")]
        public int BillingPeriodId { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Features { get; set; }

        [Alias("ProductClassID")]
        public int? ProductClassId { get; set; }

        [Alias("PropertySectionID")]
        public int? PropertySectionId { get; set; }

        [Alias("PropertyClassID")]
        public int? PropertyClassId { get; set; }

        public decimal Price { get; set; }

        public decimal? VendorPrice { get; set; }

        public bool IsFreeWithPackage { get; set; }

        public int? CreditCost { get; set; }

        public int? CreditAllocation { get; set; }

        public int? RefreshCost { get; set; }

        public int? RefreshAllocation { get; set; }

        public bool IsUserProduct { get; set; }

        public int? DurationInDays { get; set; }

        [Alias("UpgradeFromProductClassID")]
        public int? UpgradeFromProductClassId { get; set; }

        public string CssClass { get; set; }

        public bool IsHidden { get; set; }

        [Alias("SAPMaterialCode")]
        public string SapMaterialCode { get; set; }

        [Alias("AppleProductID")]
        public string AppleProductId { get; set; }

        public string AppleDisplayPrice { get; set; }

        public string CustomData { get; set; }
    }
}
