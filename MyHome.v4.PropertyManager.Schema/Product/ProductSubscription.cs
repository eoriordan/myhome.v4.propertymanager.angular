﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Product
{
    public class ProductSubscription
    {
        [Alias("ProductSubscriptionID")]
        [AutoIncrement]
        [PrimaryKey]
        public int ProductSubscriptionId { get; set; }

        [Alias("GroupID")]
        public int GroupId { get; set; }

        [Alias("ProductID")]
        public int ProductId { get; set; }

        [Alias("CreatedByUserID")]
        public int CreatedByUserId { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public decimal? Discount { get; set; }

        public int? CreditCost { get; set; }

        public int? CreditAllocation { get; set; }

        public int? RefreshCost { get; set; }

        public int? RefreshAllocation { get; set; }

        public DateTime? NextBillingDate { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime? ReviewDate { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ReviewNotificationSentOn { get; set; }
    }
}
