﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.PropertyAccount
{
    public class PropertyAccountType
    {
        [Alias("PropertyAccountTypeID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyAccountTypeId { get; set; }
        public string Name { get; set; }
    }
}
