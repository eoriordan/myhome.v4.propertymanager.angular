﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.PropertyAccount
{
    public class PropertyAccountTransactionStatus
    {
        [Alias("PropertyAccountTransactionStatusId")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyAccountTransactionStatusId { get; set; }
        public string Name { get; set; }
    }
}
