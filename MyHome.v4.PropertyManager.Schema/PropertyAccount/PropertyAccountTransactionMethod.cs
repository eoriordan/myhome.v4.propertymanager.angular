﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.PropertyAccount
{
    public class PropertyAccountTransactionMethod
    {
        [Alias("PropertyAccountTransactionMethodID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyAccountTransactionMethodId { get; set; }
        public string Name { get; set; }
    }
}
