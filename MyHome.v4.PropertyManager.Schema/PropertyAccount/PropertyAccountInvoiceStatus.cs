﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.PropertyAccount
{
    public class PropertyAccountInvoiceStatus
    {
        [Alias("PropertyAccountInvoiceStatusID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyAccountInvoiceStatusId { get; set; }
        public string Name { get; set; }
    }
}
