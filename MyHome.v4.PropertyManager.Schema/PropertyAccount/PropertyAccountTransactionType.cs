﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.PropertyAccount
{
    public class PropertyAccountTransactionType
    {
        [Alias("PropertyAccountTransactionTypeID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyAccountTransactionTypeId { get; set; }
        public string Name { get; set; }
        [Alias("AccountTypeID")]
        public int? AccountTypeId { get; set; }
    }
}
