﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.PropertyAccount
{
    public class PropertyAccountLodgment
    {
        [Alias("PropertyAccountLodgmentID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyAccountLodgmentId { get; set; }
        [Alias("GroupID")]
        public int GroupId { get; set; }
        public decimal TotalAmount { get; set; }
        public string ReferenceNumber { get; set; }
        public string AccountNumber { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public DateTime LodgedOn { get; set; }
        [Alias("CreatedByUserID")]
        public int CreatedByUserId { get; set; }
         [Alias("PropertyAccountTypeID")]
        public int PropertyAccountTypeId { get; set; }
    }
}
