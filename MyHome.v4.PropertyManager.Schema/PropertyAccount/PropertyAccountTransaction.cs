﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.PropertyAccount
{
    public class PropertyAccountTransaction
    {
        [Alias("PropertyAccountTransactionID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyAccountTransactionId { get; set; }
        [Alias("TransactionTypeID")]
        public int TransactionTypeId { get; set; }
        [Alias("PropertyID")]
        public int PropertyId { get; set; }
        [Alias("GroupID")]
        public int GroupId { get; set; }
        [Alias("CreatedByUserID")]
        public int? CreatedByUserId { get; set; }
        public decimal Amount { get; set; }
        public decimal Vat { get; set; }
        public string Summary { get; set; }
        public string TransactionDetails { get; set; }
        [Alias("ContactID")]
        public int? ContactId { get; set; }
        [Alias("AccountTypeID")]
        public int? AccountTypeId { get; set; }
        [Alias("StatusID")]
        public int? StatusId { get; set; }
        [Alias("MethodID")]
        public int? MethodId { get; set; }
        public DateTime? ActivatedOnDate { get; set; }
        public DateTime CreatedOnDate { get; set; }
        [Alias("LodgmentID")]
        public int? LodgmentId { get; set; }
        [Alias("InvoiceID")]
        public int? InvoiceId { get; set; }
        public bool IsDebit { get; set; }
        public bool ExcludeFromOfficeAccount { get; set; }
        [Alias("PropertyLeaseID")]
        public int? PropertyLeaseId { get; set; }
        [Alias("TaskID")]
        public int? TaskId { get; set; }
    }
}
