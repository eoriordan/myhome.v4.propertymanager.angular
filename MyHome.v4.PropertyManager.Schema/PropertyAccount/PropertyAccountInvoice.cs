﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.PropertyAccount
{
    public class PropertyAccountInvoice
    {
        [Alias("PropertyAccountInvoiceID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyAccountInvoiceId { get; set; }
        [Alias("PropertyID")]
        public int PropertyId { get; set; }
        [Alias("GroupID")]
        public int GroupId { get; set; }
        [Alias("RowStatusID")]
        public int RowStatusId { get; set; }
        [Alias("StatusID")]
        public int StatusId { get; set; }
        public decimal Amount { get; set; }
        public decimal VatRate { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public DateTime? PaidOnDate { get; set; }
        [Alias("DocumentID")]
        public int? DocumentId { get; set; }
        public string InvoiceTransactionInfo { get; set; }
        public string AuthorisationCode { get; set; }
        public string InvoiceDetails { get; set; }
        public bool IsCreditNote { get; set; }
        [Alias("CreditNoteFromInvoiceID")]
        public int? CreditNoteFromInvoiceId { get; set; }
        public decimal? FeesTotal { get; set; }
        public decimal? PromotionalCampaignTotal { get; set; }
    }
}