﻿using System;
using MyHome.v4.Common.Schema.Enumerations;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Common
{
    /// <summary>
    /// The IndexQueue POCO
    /// </summary>
    public class IndexQueue
    {
        [PrimaryKey]
        [AutoIncrement]
        [Alias("IndexQueueID")]
        public int IndexQueueId { get; set; }

        [Alias("IndexTypeID")]
        public int IndexTypeId { get; set; }

        [Alias("EntityID")]
        public int EntityId { get; set; }

        [Alias("CreatedOn")]
        public DateTime? CreatedOn { get; set; }
    }
}
