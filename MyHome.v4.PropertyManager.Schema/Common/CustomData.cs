﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Common
{
    public class CustomData
    {
        public CustomData()
        {
            RelatedPropertyIDs = new List<int>();
        }

        [DataMember]
        public List<int> RelatedPropertyIDs { get; set; }

        [DataMember]
        public string PromotionalSnippet { get; set; }

        [DataMember]
        public int? JointAgentGroupId { get; set; }

        [DataMember]
        public string AssociatedArticleUrl { get; set; }

    }
}

