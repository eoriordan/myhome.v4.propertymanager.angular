﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Advice
{
    public class Comment
    {
        [AutoIncrement]
        [PrimaryKey]
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public int? InReplyToCommentId { get; set; }
        public string Content { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; }
        public bool IsApproved{ get; set; }
        public bool IsDeleted{ get; set; }
    }
}
