﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Advice
{
    public class Category
    {
        [AutoIncrement]
        [PrimaryKey]
        public int Id { get; set; }
        public string Name{ get; set; }
        public string UrlSlug { get; set; }
        public int? ParentCategoryId { get; set; }
        public string Description { get; set; }
    }
}
