﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Advice
{
    public class ArticleCategoryMapping
    {
        [AutoIncrement]
        [PrimaryKey]
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public int CategoryId { get; set; }
    }
}
