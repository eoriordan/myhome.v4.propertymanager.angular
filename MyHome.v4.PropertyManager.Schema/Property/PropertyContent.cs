﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    /// <summary>
    /// The property content 
    /// </summary>
    public class PropertyContent
    {

        [Alias("PropertyContentID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyContentId { get; set; }
        
        [Alias("PropertyID")]
        public int PropertyId { get; set; }

        [Alias("PropertyContentSourceTypeId")]
        public int PropertyContentSourceTypeId { get; set; }

        [Alias("ContentType")]
        public string ContentType { get; set; }

        [Alias("SortOrder")]
        public int SortOrder { get; set; }

        [Alias("Content")]
        public string Content { get; set; }

        [Alias("CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [Alias("UpdatedOn")]
        public DateTime UpdatedOn { get; set; }

    }
}
