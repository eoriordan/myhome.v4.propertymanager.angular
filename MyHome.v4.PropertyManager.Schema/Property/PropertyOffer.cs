﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    public class PropertyOffer
    {
        [Alias("PropertyOfferID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyOfferId { get; set; }
        [Alias("PropertyID")]
        public int PropertyId { get; set; }
        [Alias("GroupID")]
        public int GroupId { get; set; }
        [Alias("ContactID")]
        public int ContactId { get; set; }
        [Alias("StatusID")]
        public int StatusId { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedOnDate { get; set; }
        [Alias("CreatedByUserID")]
        public int CreatedByUserId { get; set; }
        public DateTime? AcceptedOnDate { get; set; }
        public string Notes { get; set; }
        public PropertyOfferCustomData CustomData { get; set; }
        public int? Method { get; set; }
        public DateTime? NotificationDate { get; set; }
        [Alias("NotifiedByUserID")]
        public int? NotifiedByUserId { get; set; }
        public int? NotificationMethod { get; set; }
        public string NotificationNotes { get; set; }
    }

}
