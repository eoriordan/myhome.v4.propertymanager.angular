﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema.Enumerations;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    [DataContract]
    [Serializable]
    public class Price
    {
        public Price()
        {
            ShowPrice = true;
        }

        [DataMember]
        public decimal? MinPrice { get; set; }
        [DataMember]
        public decimal? MaxPrice { get; set; }
        [DataMember]
        public decimal? RentalYieldValue { get; set; }
        [DataMember]
        public decimal? MinRentalPrice { get; set; }
        [DataMember]
        public decimal? MaxRentalPrice { get; set; }
        [DataMember]
        public decimal? MinValuation { get; set; }
        [DataMember]
        public decimal? MaxValuation { get; set; }
        [DataMember]
        public PricePeriod? Period { get; set; }
        [DataMember]
        public decimal? Deposit { get; set; }
        [DataMember]
        public decimal? AnnualServiceCharge { get; set; }
        [DataMember]
        public bool ShowPrice { get; set; }
        [DataMember]
        public DateTime? AvailableOn { get; set; }
        [DataMember]
        public LeaseTerm? LeaseTerm { get; set; }
        [DataMember]
        public RentalSizeUnit? RentalSizeUnit { get; set; }
        [DataMember]
        public PriceDescription? PriceDescription { get; set; }

        public decimal? Value
        {
            get
            {
                if (HasPriceRange) return null;
                return MinPrice;
            }
            set
            {
                MinPrice = value;
                MaxPrice = value;
            }
        }

        public decimal? RentalValue
        {
            get
            {
                if (HasRentalPriceRange) return null;
                return MinRentalPrice;
            }
            set
            {
                MinRentalPrice = value;
                MaxRentalPrice = value;
            }
        }

        private int GetYearlyMultiplier()
        {
            switch (Period)
            {
                case PricePeriod.Weekly:
                    return 52;
                case PricePeriod.Monthly:
                    return 12;
                case PricePeriod.Yearly:
                    return 1;
                default:
                    throw new InvalidOperationException();
            }
        }

        public decimal TotalRentForYear
        {
            get
            {
                if (RentalValue.HasValue)
                {
                    // ReSharper disable once PossibleInvalidOperationException
                    return GetYearlyMultiplier() * RentalValue.Value;   
                }
                return 0;
            }
        }

        public decimal? MinMonthlyRentalPrice
        {
            get
            {
                if (HasRentalPrice)
                    return MonthlyAdjustedPrice(MinRentalPrice, Period);
                return null;
            }
        }

        public decimal? MaxMonthlyRentalPrice
        {
            get
            {
                if (HasRentalPrice)
                    return MonthlyAdjustedPrice(MaxRentalPrice, Period);
                return null;
            }
        }

        private static decimal? MonthlyAdjustedPrice(decimal? price, PricePeriod? period)
        {
            if (!price.HasValue) return null;
            if (!period.HasValue) return null;

            switch (period.Value)
            {
                case PricePeriod.Weekly:
                    return price * 4;
                case PricePeriod.Monthly:
                    return price;
                case PricePeriod.Yearly:
                    return price / 12;
                default:
                    throw new NotSupportedException("Invalid Period");
            }
        }

        public bool HasPrice { get { return HasSalePrice || HasRentalPrice; } }
        public bool HasSalePrice { get { return (MinPrice.HasValue && MinPrice.Value > 0) || (MaxPrice.HasValue && MaxPrice.Value > 0); } }
        public bool HasRentalPrice { get { return (MinRentalPrice.HasValue && MinRentalPrice.Value > 0) || (MaxRentalPrice.HasValue && MaxRentalPrice.Value > 0); } }
        public bool HasPublicPrice { get { return HasPublicSalePrice || HasPublicRentalPrice; } }
        public bool HasPublicSalePrice { get { return HasSalePrice && ShowPrice; } }
        public bool HasPublicRentalPrice { get { return HasRentalPrice && ShowPrice; } }

        public bool HasMinAndMaxPrice { get { return MinPrice.HasValue && MaxPrice.HasValue; } }
        public bool HasMinAndMaxRentalPrice { get { return MinRentalPrice.HasValue && MaxRentalPrice.HasValue; } }
        public bool HasPriceRange { get { return HasPrice && (MinPrice != MaxPrice); } }
        public bool HasRentalPriceRange { get { return HasPrice && (MinRentalPrice != MaxRentalPrice); } }

        public override string ToString()
        {
            if (!ShowPrice)
                return "POA";
            return PrivatePriceString;
        }

        public string PrivatePriceString
        {
            get
            {
                if (!HasPrice)
                {
                    return "POA";
                }
                var prices = new List<string>();
                if (HasPrice)
                    prices.Add(SalePriceString);
                if (HasRentalPrice)
                    prices.Add(RentalPriceString);

                prices.RemoveAll(p => p == "");

                return prices.Join(" or ");
            }
        }

        private string SalePriceString
        {
            get
            {
                if (!HasSalePrice)
                    return "";

                string value;
                if (!HasPriceRange)
                    value = MinPrice.ToShortCurrency();
                else if (!MinPrice.HasValue)
                    value = String.Format("to {0}", MaxPrice.ToCurrency(0));
                else if (!MaxPrice.HasValue)
                    value = String.Format("from {0}", MinPrice.ToCurrency(0));
                else
                    value = String.Format("{0} to {1}", MinPrice.ToCurrency(0), MaxPrice.ToCurrency(0));

                return value;
            }
        }

        private string RentalPriceString
        {
            get
            {
                if (!HasRentalPrice)
                    return "";

                string value;
                if (!HasRentalPriceRange)
                    value = MinRentalPrice.ToShortCurrency();
                else if (!MinRentalPrice.HasValue)
                    value = String.Format("to {0}", MaxRentalPrice.ToShortCurrency());
                else if (!MaxRentalPrice.HasValue)
                    value = String.Format("from {0}", MinRentalPrice.ToShortCurrency());
                else
                    value = String.Format("{0} to {1}", MinRentalPrice.ToShortCurrency(), MaxRentalPrice.ToShortCurrency());

                if (Period.HasValue)
                {
                    switch (Period.Value)
                    {
                        case PricePeriod.Weekly:
                            value += " / week";
                            break;
                        case PricePeriod.Monthly:
                            value += " / month";
                            break;
                        case PricePeriod.Yearly:
                            value += " / year";
                            break;
                    }
                }

                return value;
            }
        }

        public decimal? MinSaleOrRentalPrice(PropertyClassEnum propertyClass)
        {
            if (PropertyClass.IsRentalClass(propertyClass))
                return MinRentalPrice;
            return MinPrice;
        }

        public decimal? MaxSaleOrRentalPrice(PropertyClassEnum propertyClass)
        {
            if (PropertyClass.IsRentalClass(propertyClass))
                return MaxRentalPrice;

            return MaxPrice;
        }
    }

    public enum PricePeriod
    {
        Weekly = 1,
        Monthly = 2,
        Yearly = 3
    }

    public enum RentalSizeUnit
    {
        Meters = 1,
        Feet = 2
    }

    public enum LeaseTerm
    {
        LessThanSixMonths = 1,
        SixMonths = 2,
        NineMonths = 3,
        TwelveMonths = 4
    }

    public enum PriceDescription
    {
        AskingPrice = 1,
        AdvicedMinimumValue = 2
    }
}
