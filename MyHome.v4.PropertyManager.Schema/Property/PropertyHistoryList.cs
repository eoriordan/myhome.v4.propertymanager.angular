﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema.Enumerations;

namespace MyHome.v4.PropertyManager.Schema.Property
{

    [DataContract]
    [Serializable]
    public class PropertyHistory
    {
        [DataMember]
        public int PropertyHistoryID { get; set; }
        [DataMember]
        public int PropertyID { get; set; }
        [DataMember]
        public int GroupID { get; set; }
        [DataMember]
        public PropertyStatusEnum PropertyStatus { get; set; }
        [DataMember]
        public int? PropertyTypeID { get; set; }
        [DataMember]
        public int? LocalityID { get; set; }
        [DataMember]
        public int? ChannelID { get; set; }

        [DataMember]
        public decimal? Price { get; set; }
        [DataMember]
        public PropertyClassEnum PropertyClass { get; set; }
        [DataMember]
        public int? ActivityID { get; set; }
        [DataMember]
        public DateTime CreatedOn { get; set; }

        [DataMember]
        public decimal? PrevPrice { get; set; }
        [DataMember]
        public PropertyStatusEnum? PrevPropertyStatus { get; set; }
        [DataMember]
        public DateTime? PrevCreatedOn { get; set; }
        [DataMember]
        public PropertySnippet PropertySnippet { get; set; }

        public bool PriceHasChanged { get { return Price != PrevPrice && PrevPrice.HasValue; } }

        [DataMember]
        public decimal? PriceChangeDelta { get; set; }
        [DataMember]
        public decimal? PriceChangePercentage { get; set; }
        [DataMember]
        public decimal? AbsolutePriceChangePercentage { get; set; }
        [DataMember]
        public bool HasPriceRange { get; set; }

        public string PriceChangeDisplayText
        {
            get
            { //TODO: GJ: improve
                return String.Format("{0} -> {1}", PrevPrice.ToTwitterPrice(), Price.ToTwitterPrice());
            }
        }

        [DataMember]
        public bool IsPublic { get; set; }
        [DataMember]
        public bool OptOut { get; set; }

        public bool IsChange { get { return PriceChangeDelta.HasValue && PriceChangeDelta != 0; } }
        public bool IsPositiveChange { get { return PriceChangeDelta > 0; } }
        public bool IsNegativeChange { get { return PriceChangeDelta < 0; } }

        public bool IsChangeBetween(decimal minPriceChangePercentage, decimal maxPriceChangePercentage)
        {
            return IsChange && AbsolutePriceChangePercentage.Value >= minPriceChangePercentage && AbsolutePriceChangePercentage <= maxPriceChangePercentage;
        }

        public bool IsPublicTweet
        {
            get
            {
                return PropertyClass == PropertyClassEnum.ResidentialForSale && IsChangeBetween(0.3m, 70) && !OptOut;
            }
        }

        public string PreviousPriceTimeSpanInDays
        {
            get
            {
                if (PrevCreatedOn.HasValue)
                    return (CreatedOn - PrevCreatedOn.Value).Days.ToString(CultureInfo.InvariantCulture);
                return "";
            }
        }

        public string CssClass
        {
            get
            {
                if (PriceChangeDelta.HasValue && PriceChangeDelta.Value > 0)
                    return "up";
                return "down";
            }
        }
    }



    [DataContract]
    [Serializable]
    public class PropertyHistoryList : List<PropertyHistory>
    {
        public PropertyHistoryList() { }
        public PropertyHistoryList(IEnumerable<PropertyHistory> history)
        {
            AddRange(history);
        }

        public PropertyHistoryList PositivePriceChanges
        {
            get { return new PropertyHistoryList(this.Where(ph => ph.IsPositiveChange)); }
        }
        public PropertyHistoryList NegativePriceChanges
        {
            get { return new PropertyHistoryList(this.Where(ph => ph.IsNegativeChange)); }
        }

        public decimal AveragePriceChangePercentage
        {
            get { return this.Average(ph => ph.PriceChangePercentage != null ? ph.PriceChangePercentage.Value : 0); }
        }

        public int PositiveCount { get { return PositivePriceChanges.Count(); } }
        public decimal PositiveAveragePriceChangePercentage
        {
            get { return PositivePriceChanges.Any() ? PositivePriceChanges.Average(ph => ph.PriceChangePercentage.Value) : 0; }
        }

        public int NegativeCount { get { return NegativePriceChanges.Count(); } }
        public decimal NegativeAveragePriceChangePercentage
        {
            get { return NegativePriceChanges.Any() ? NegativePriceChanges.Average(ph => ph.PriceChangePercentage.Value) : 0; }
        }
    }
}
