﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    [DataContract]
    [Serializable]
    public class PropertyOfferCustomData
    {
        [DataMember]
        public Price Price { get; set; }
    }
}
