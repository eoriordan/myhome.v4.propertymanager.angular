﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    public class PropertyNote
    {
        [AutoIncrement]
        [PrimaryKey]
        public int Id { get; set; }
        public int PropertyId{ get; set; }
        public int GroupId { get; set; }
        public string Note{ get; set; }
        public DateTime NoteDate{ get; set; }
        public bool IsPublic{ get; set; }
    }
}
