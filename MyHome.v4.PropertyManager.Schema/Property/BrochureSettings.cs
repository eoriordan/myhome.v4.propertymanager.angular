﻿using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    [DataContract]
    [Serializable]
    public class BrochureSettings
    {

        /// <summary>
        /// The header background colour for the brochure
        /// </summary>
        [DataMember]
        public string HeaderBackgroundColour { get; set; }

        /// <summary>
        /// The text colour of the header
        /// </summary>
        [DataMember]
        public string HeaderTextColour { get; set; }

        /// <summary>
        /// The sub heading background colour, this is not used in the default two pages but maybe used in others
        /// </summary>
        [DataMember]
        public string SubHeaderBackgroundColour { get; set; }

        /// <summary>
        /// The sub heading text colour, this is not used in the default two pages but maybe used in others
        /// </summary>
        [DataMember]
        public string SubHeaderTextColour { get; set; }

        /// <summary>
        /// The body background colour used for the complete color of the brochure background
        /// </summary>
        [DataMember]
        public string BodyBackgroundColour { get; set; }

        /// <summary>
        /// The alternative background colour used in the footer of the brochure
        /// </summary>
        [DataMember]
        public string FooterAlternativeBackgroundColour { get; set; }

        /// <summary>
        /// The background color used the footer of the brochure
        /// </summary>
        [DataMember]
        public string FooterBackgroundColour { get; set; }

        /// <summary>
        /// The text color used in the footer of the brochure
        /// </summary>
        [DataMember]
        public string FooterTextColour { get; set; }

        /// <summary>
        /// The default colour stack used in the creation of the brochure if they have not been overridden
        /// </summary>
        public static BrochureSettings Default
        {
            get
            {
                return new BrochureSettings
                {
                    HeaderBackgroundColour = "#FFFFFF",
                    HeaderTextColour = "#000000",
                    SubHeaderBackgroundColour = "#33333",
                    SubHeaderTextColour = "#EEEEEE",
                    BodyBackgroundColour = "#FFFFFF",
                    FooterBackgroundColour = "#FF0000",
                    FooterTextColour = "#000000",
                    FooterAlternativeBackgroundColour = "#33333",
                };
            }
        }
    }
}
