﻿using System;
using System.Collections.Generic;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.Schema.Common;
using MyHome.v4.PropertyManager.Schema.Groups;
using ServiceStack.DataAnnotations;
using StringDictionary = MyHome.v4.Common.Schema.StringDictionary;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    /// <summary>
    /// the datbase poco class for dealing with the persistence of properties
    /// </summary>
    public class Property
    {
        [Alias("PropertyID")]
        [AutoIncrement]
        [PrimaryKey]
        public int PropertyId { get; set; }

        [Alias("GroupID")]
        public int GroupId { get; set; }

        [Alias("Beds")]
        public Beds Beds { get; set; }

        [Alias("Address")]
        public string Address { get; set; }

        [Alias("DisplayAddress")]
        public string DisplayAddress { get; set; }

        [Alias("Name")]
        public string Name { get; set; }

        [Alias("RowStatusID")]
        public int RowStatusId { get; set; }

        [Alias("PropertyClassID")]
        public int PropertyClassId { get; set; }

        [Alias("PropertyStatusID")]
        public int PropertyStatusId { get; set; }

        [Alias("PropertyTypeID")]
        public int? PropertyTypeId { get; set; }

        [Alias("ContactID")]
        public int? ContactId { get; set; }

        [Alias("PropertyReference")]
        public string PropertyReference { get; set; }

        [Alias("LocalityID")]
        public int? LocalityId { get; set; }

        [Alias("RegionID")]
        public int? RegionId { get; set; }

        [Alias("Latitude")]
        public float? Latitude { get; set; }

        [Alias("Longitude")]
        public float? Longitude { get; set; }

        [Alias("Map")]
        public Map Map { get; set; }

        [Alias("SaleTypeID")]
        public int? SaleTypeId { get; set; }

        [Alias("TaxSectionID")]
        public int? TaxSectionId { get; set; }

        [Alias("MinSortPrice")]
        public int? MinSortPrice { get; set; }

        [Alias("MaxSortPrice")]
        public int? MaxSortPrice { get; set; }

        [Alias("MinBeds")]
        public int? MinBeds { get; set; }

        [Alias("MaxBeds")]
        public int? MaxBeds { get; set; }

        [Alias("Bathrooms")]
        public int? Bathrooms { get; set; }

        [Alias("MinSize")]
        public float? MinSize { get; set; }

        [Alias("MaxSize")]
        public float? MaxSize { get; set; }

        [Alias("Content")]
        public Content Content { get; set; }

        [Alias("PublicTags")]
        public Tags PublicTags { get; set; }

        [Alias("PrivateTags")]
        public Tags PrivateTags { get; set; }

        [Alias("ComputedTags")]
        public Tags ComputedTags { get; set; }

        [Alias("Price")]
        public Price Price { get; set; }

        [Alias("TagIndex")]
        public string TagIndex { get; set; }

        [Alias("CustomFormData")]
        public StringDictionary CustomFormData { get; set; }

        [Alias("ProfessionalTeam")]
        public string ProfessionalTeam { get; set; }

        [Alias("Media")]
        public Media Media { get; set; }

        [Alias("PhotoCount")]
        public int PhotoCount { get; set; }

        [Alias("Floors")]
        public Floors Floors { get; set; }

        [Alias("NegotiatorID")]
        public int? NegotiatorId { get; set; }

        [Alias("EnergyRating")]
        public string EnergyRating { get; set; }

        [Alias("CreatedOnDate")]
        public DateTime? CreatedOnDate { get; set; }

        [Alias("CreatedByUserID")]
        public int? CreatedByUserId { get; set; }

        [Alias("ModifiedOnDate")]
        public DateTime? ModifiedOnDate { get; set; }

        [Alias("ActivatedOn")]
        public DateTime? ActivatedOn { get; set; }

        [Alias("AgreedOn")]
        public DateTime? AgreedOn { get; set; }

        [Alias("RefreshedOn")]
        public DateTime? RefreshedOn { get; set; }

        [Alias("DeletedOn")]
        public DateTime? DeletedOn { get; set; }

        [Alias("IsActive")]
        public bool? IsActive { get; set; }

        [Alias("PublishOnMyHome")]
        public bool PublishOnMyHome { get; set; }

        [Alias("DeletedByUserID")]
        public bool DeletedByUserId { get; set; }

        [Alias("SimilarProperties")]
        public List<PropertySnippet> SimilarProperties { get; set; }

        [Alias("LegacyIdentifier")]
        public string LegacyIdentifier { get; set; }

        [Alias("LegacyMappings")]
        public LegacyMappings LegacyMappings { get; set; }

        [Alias("Rooms")]
        public String Rooms { get; set; }

        [Alias("Size")]
        public Size Size { get; set; }

        [Alias("TitleDeedID")]
        public int? TitleDeedId { get; set; }

        [Alias("MinRentalPrice")]
        public decimal? MinRentalPrice { get; set; }

        [Alias("MaxRentalPrice")]
        public decimal? MaxRentalPrice { get; set; }

        [Alias("IsPrivateLandlord")]
        public bool IsPrivateLandlord { get; set; }

        [Alias("ContactDetails")]
        public ContactDetails ContactDetails { get; set; }

        [Alias("ExpiresOn")]
        public DateTime? ExpiresOn { get; set; }

        [Alias("InstructedOn")]
        public DateTime? InstructedOn { get; set; }

        [Alias("EstimatedClosedOn")]
        public DateTime? EstimatedClosedOn { get; set; }

        [Alias("AccountFinalisedOn")]
        public DateTime? AccountFinalisedOn { get; set; }

        [Alias("SoldPrice")]
        public decimal? SoldPrice { get; set; }

        [Alias("ClosedOn")]
        public DateTime? ClosedOn { get; set; }

        [Alias("ValuedOn")]
        public DateTime? ValuedOn { get; set; }

        [Alias("FeeStructure")]
        public FeeStructure FeeStructure { get; set; }

        [Alias("PremiumAdExpiresOn")]
        public DateTime? PremiumAdExpiresOn { get; set; }

        [Alias("BuyerID")]
        public int? BuyerId { get; set; }

        [Alias("FileReference")]
        public string FileReference { get; set; }

        [Alias("CancelledOn")]
        public DateTime? CancelledOn { get; set; }

        [Alias("SoldOn")]
        public DateTime? SoldOn { get; set; }

        [Alias("ChannelID")]
        public int ChannelId { get; set; }

        [Alias("VendorSolicitorID")]
        public int? VendorSolicitorId { get; set; }

        [Alias("BuyerSolicitorID")]
        public int? BuyerSolicitorId { get; set; }

        [Alias("DeveloperID")]
        public int? DeveloperId { get; set; }

        [Alias("PortfolioID")]
        public int? PortfolioId { get; set; }

        [Alias("ServiceLevelID")]
        public int? ServiceLevelId { get; set; }

        [Alias("PropertyLeaseID")]
        public int? PropertyLeaseId { get; set; }

        [Alias("Notes")]
        public string Notes { get; set; }

        [Alias("History")]
        public PropertyHistoryList History { get; set; }

        [Alias("FeaturedAdExpiresOn")]
        public DateTime? FeaturedAdExpiresOn { get; set; }

        [Alias("MinEnergyRating")]
        public int? MinEnergyRating { get; set; }

        [Alias("MaxEnergyRating")]
        public int? MaxEnergyRating { get; set; }

        [Alias("PreSixtyThree")]
        public bool? PreSixtyThree { get; set; }

        [Alias("IsNewBuild")]
        public bool? IsNewBuild { get; set; }

        [Alias("CustomData")]
        public CustomData CustomData { get; set; }

        [Alias("IsPremiumAd")]
        public bool IsPremiumAd { get; set; }

        [Alias("TransferedToPropertyID")]
        public int? TransferedToPropertyId { get; set; }

        [Alias("TransferedFromPropertyID")]
        public int? TransferedFromPropertyId { get; set; }

        [Alias("TransferedByUserID")]
        public int? TransferedByUserId { get; set; }

        [Alias("IsTrending")]
        public bool IsTrending { get; set; }

        public string Eircode { get; set; }
    }
}
