﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema.Enumerations;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    [DataContract]
    public class PropertyClass
    {
        public PropertyClass()
        {
            SecondaryUrlSlug = "";
        }

        [DataMember]
// ReSharper disable once InconsistentNaming
        public int PropertyClassID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string MainUrlSlug { get; set; }
        [DataMember]
        public string SecondaryUrlSlug { get; set; }

        public PropertyClassEnum ToEnum()
        {
            // ReSharper disable once PossibleInvalidOperationException
            return Name.ToEnum<PropertyClassEnum>().Value;
        }

        public static PropertyClassEnum GetPropertySectionDefaultPropertyClass(PropertySection section)
        {
            switch (section)
            {
                case PropertySection.Residential:
                    return PropertyClassEnum.ResidentialForSale;
                case PropertySection.Lettings:
                    return PropertyClassEnum.LettingsToRent;
                case PropertySection.Commercial:
                    return PropertyClassEnum.Commercial;
                case PropertySection.Overseas:
                    return PropertyClassEnum.OverseasForSale;
                default:
                    throw new ArgumentException("PropertySection is not supported");
            }
        }

        public static List<PropertyClassEnum> GetPropertySectionClasses(PropertySection? section)
        {
            if (section.HasValue)
                return GetPropertySectionClasses(section.Value);
            return new List<PropertyClassEnum>();
        }

        public static List<PropertyClassEnum> GetPropertySectionClasses(PropertySection section)
        {
            switch (section)
            {
                case PropertySection.Residential:
                    return new List<PropertyClassEnum> { PropertyClassEnum.ResidentialForSale, PropertyClassEnum.ResidentialNewHomes };
                case PropertySection.Lettings:
                    return new List<PropertyClassEnum> { PropertyClassEnum.LettingsToRent, PropertyClassEnum.LettingsToShare, PropertyClassEnum.LettingsHolidayHomes };
                case PropertySection.Commercial:
                    return new List<PropertyClassEnum> { PropertyClassEnum.Commercial };
                case PropertySection.Overseas:
                    return new List<PropertyClassEnum> { PropertyClassEnum.OverseasForSale, PropertyClassEnum.OverseasHolidayHomes };
                default:
                    throw new ArgumentException("PropertySection is not supported");
            }
        }


        public static PropertyClassEnum GetFromLegacyClassAndSubClass(string propertyClass, string propertySubClass)
        {
            var identifier = String.Format("{0}-{1}", propertyClass.ToLower(), propertySubClass.ToLower());
            switch (identifier)
            {
                case "residential-main":
                    return PropertyClassEnum.ResidentialForSale;
                case "residential-newhomes":
                    return PropertyClassEnum.ResidentialNewHomes;
                case "lettings-main":
                    return PropertyClassEnum.LettingsToRent;
                case "lettings-share":
                    return PropertyClassEnum.LettingsToShare;
                case "lettings-holidayhomes":
                    return PropertyClassEnum.LettingsHolidayHomes;
                case "commercial-main":
                    return PropertyClassEnum.Commercial;
                case "overseas-main":
                    return PropertyClassEnum.OverseasForSale;
                case "overseas-holidayhomes":
                    return PropertyClassEnum.OverseasHolidayHomes;
                default:
                    return PropertyClassEnum.ResidentialForSale;
            }
        }


        public static List<PropertyClassEnum> GetMainPropertyClasses()
        {
            return new List<PropertyClassEnum>
            {
                PropertyClassEnum.ResidentialForSale,
                PropertyClassEnum.ResidentialNewHomes,
                PropertyClassEnum.LettingsToRent,
                PropertyClassEnum.LettingsToShare,
                PropertyClassEnum.LettingsHolidayHomes,
                PropertyClassEnum.Commercial,
                PropertyClassEnum.OverseasForSale,
                PropertyClassEnum.OverseasHolidayHomes
            };
        }

        public static List<PropertyStatusEnum> GetPropertyStatus(PropertyClassEnum propertyClass)
        {
            return new Dictionary<PropertyClassEnum, List<PropertyStatusEnum>>
            {
                { PropertyClassEnum.ResidentialForSale, new List<PropertyStatusEnum> { PropertyStatusEnum.ForSale } },
                { PropertyClassEnum.ResidentialNewHomes, new List<PropertyStatusEnum> {PropertyStatusEnum.ForSale } },
                { PropertyClassEnum.LettingsToRent, new List<PropertyStatusEnum> {PropertyStatusEnum.ToLet} },
                { PropertyClassEnum.LettingsToShare, new List<PropertyStatusEnum> {PropertyStatusEnum.ToLet } },
                { PropertyClassEnum.LettingsHolidayHomes, new List<PropertyStatusEnum> {PropertyStatusEnum.ToLet } },
                { PropertyClassEnum.Commercial, new List<PropertyStatusEnum> {PropertyStatusEnum.ForSale, PropertyStatusEnum.ToLet, PropertyStatusEnum.ForSaleOrToLet } },
                { PropertyClassEnum.OverseasForSale, new List<PropertyStatusEnum> {PropertyStatusEnum.ForSale } },
                { PropertyClassEnum.OverseasHolidayHomes, new List<PropertyStatusEnum> {PropertyStatusEnum.ToLet } },
            }[propertyClass];
        }

        public static List<PropertyStatusEnum> GetActivePropertyStatus(PropertyClassEnum propertyClass)
        {
            return new Dictionary<PropertyClassEnum, List<PropertyStatusEnum>>
            {
                { PropertyClassEnum.ResidentialForSale, new List<PropertyStatusEnum> { PropertyStatusEnum.ForSale, PropertyStatusEnum.SaleAgreed, PropertyStatusEnum.Sold } },
                { PropertyClassEnum.ResidentialNewHomes, new List<PropertyStatusEnum> {PropertyStatusEnum.ForSale, PropertyStatusEnum.SaleAgreed, PropertyStatusEnum.Sold } },
                { PropertyClassEnum.LettingsToRent, new List<PropertyStatusEnum> {PropertyStatusEnum.ToLet, PropertyStatusEnum.LetAgreed } },
                { PropertyClassEnum.LettingsToShare, new List<PropertyStatusEnum> {PropertyStatusEnum.ToLet, PropertyStatusEnum.LetAgreed } },
                { PropertyClassEnum.LettingsHolidayHomes, new List<PropertyStatusEnum> {PropertyStatusEnum.ToLet, PropertyStatusEnum.LetAgreed } },
                { PropertyClassEnum.Commercial, new List<PropertyStatusEnum> {PropertyStatusEnum.ForSale, PropertyStatusEnum.ToLet, PropertyStatusEnum.ForSaleOrToLet, PropertyStatusEnum.SaleAgreed, PropertyStatusEnum.LetAgreed, PropertyStatusEnum.Reserved, PropertyStatusEnum.Sold } },
                { PropertyClassEnum.OverseasForSale, new List<PropertyStatusEnum> {PropertyStatusEnum.ForSale, PropertyStatusEnum.SaleAgreed, PropertyStatusEnum.Sold } },
                { PropertyClassEnum.OverseasHolidayHomes, new List<PropertyStatusEnum> {PropertyStatusEnum.ToLet, PropertyStatusEnum.LetAgreed } },
            }[propertyClass];
        }

        public static bool IsRentalClass(PropertyClassEnum propertyClass)
        {
            switch (propertyClass)
            {
                case PropertyClassEnum.LettingsToRent:
                case PropertyClassEnum.LettingsToShare:
                case PropertyClassEnum.LettingsHolidayHomes:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsOverseasClass(PropertyClassEnum propertyClass)
        {
            switch (propertyClass)
            {
                case PropertyClassEnum.OverseasForSale:
                case PropertyClassEnum.OverseasHolidayHomes:
                    return true;
                default:
                    return false;
            }
        }

        public static string GetTitle(PropertyClassEnum propertyClass)
        {
            switch (propertyClass)
            {
                case PropertyClassEnum.Commercial:
                    return "Commercial Property";
                case PropertyClassEnum.LettingsHolidayHomes:
                    return "Rental Holiday Home";
                case PropertyClassEnum.LettingsToRent:
                    return "Rental Property";
                case PropertyClassEnum.LettingsToShare:
                    return "Property To Share";
                case PropertyClassEnum.OverseasForSale:
                    return "Overseas Property For Sale";
                case PropertyClassEnum.OverseasHolidayHomes:
                    return "Overseas Holiday Home";
                case PropertyClassEnum.ResidentialForSale:
                    return "Residential Property For Sale";
                case PropertyClassEnum.ResidentialNewHomes:
                    return "Development For Sale";
                default:
                    throw new ArgumentException("PropertyClass not supported");
            }
        }
    }
}
