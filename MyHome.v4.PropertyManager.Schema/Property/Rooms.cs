﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    public class Rooms : List<Room> { }

    [DataContract]
    [Serializable]
    public class Room
    {

        public Room()
        {
            DisplayInMeters = true;
            ImperialFormat = false;
        }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public decimal? SizeInMeters { get; set; }
        [DataMember]
        public decimal? SizeInFeet { get; set; }
        [DataMember]
        public decimal? LengthInMeters { get; set; }
        [DataMember]
        public decimal? WidthInMeters { get; set; }
        [DataMember]
        public decimal? LengthInFeet { get; set; }
        [DataMember]
        public decimal? WidthInFeet { get; set; }
        [DataMember]
        public bool DisplayInMeters { get; set; }
        [DataMember]
        public bool ImperialFormat { get; set; } //Should we convert the fraction to xx"y' ?

        public string PublicSize
        {
            get
            {
                var publicSize = "";
                if (LengthInFeet.HasValue && WidthInFeet.HasValue)
                {
                    publicSize = String.Format("{0}ft x {1}ft", LengthInFeet.Value, WidthInFeet.Value);
                }
                else if (LengthInMeters.HasValue && WidthInMeters.HasValue)
                {
                    publicSize = String.Format("{0}m x {1}m", LengthInMeters.Value, WidthInMeters.Value);
                }
                else if (SizeInMeters.HasValue)
                {
                    publicSize = SizeInMeters.Value + "m2";
                }
                else if (SizeInFeet.HasValue)
                {
                    publicSize = SizeInFeet.Value + "ft2";
                }
                return publicSize;
            }
        }

        public override string ToString()
        {
            var size = PublicSize;
            if (string.IsNullOrEmpty(size))
            {
                if (SizeInMeters.HasValue)
                {
                    size = SizeInMeters.Value + "m2";
                }
                else if (SizeInFeet.HasValue)
                {
                    size = SizeInFeet.Value + "ft2";
                }
                else
                {
                    size = string.Empty;
                }
            }

            var output = Name;
            if (!string.IsNullOrEmpty(size))
            {
                output += " - " + size;
            }
            if (!string.IsNullOrEmpty(Description))
            {
                output += " - " + Description;
            }
            return output;
        }
    }
}
