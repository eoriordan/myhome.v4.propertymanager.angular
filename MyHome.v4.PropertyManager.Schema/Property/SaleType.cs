﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    public class SaleType
    {
        [Alias("SaleTypeID")]
        [AutoIncrement]
        [PrimaryKey]
        public int SaleTypeId { get; set; }

        public string Name { get; set; }

        [Alias("PropertyClassID")]
        public int PropertyClassId { get; set; }

        public string UrlSlug { get; set; }

        public string DescriptionLabelOverride { get; set; }
    }
}
