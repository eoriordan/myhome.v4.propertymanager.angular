﻿using System.Collections.Generic;

namespace MyHome.v4.PropertyManager.Schema.Property
{
    /// <summary>
    /// Poco object to handle floors json from property entity. Only used in Property so not included in common.
    /// </summary>
    public class Floors : List<Floor> { }

    public class Floor
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int? LengthInMeters { get; set; }

        public int? WidthInMeters { get; set; }

        public decimal? Size { get; set; }
    }
}
