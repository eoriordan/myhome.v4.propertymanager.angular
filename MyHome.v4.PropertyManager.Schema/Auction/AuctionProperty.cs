﻿using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Auction
{
    public class AuctionProperty
    {
        [Alias("AuctionPropertyID")]
        [AutoIncrement]
        [PrimaryKey]
        public int AuctionPropertyId { get; set; }

        [Alias("AuctionID")]
        public int AuctionId { get; set; }

        [Alias("PropertyID")]
        public int PropertyId { get; set; }

        public int? RunningOrder { get; set; }

        public string SpecialConditions { get; set; }

        public string ReservePrice { get; set; }
    }
}
