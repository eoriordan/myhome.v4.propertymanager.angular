﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Auction
{
    public class Auction
    {
        [Alias("AuctionID")]
        [AutoIncrement]
        [PrimaryKey]
        public int AuctionId { get; set; }

        [Alias("GroupID")]
        public int? GroupId { get; set; }

        [Alias("ChannelID")]
        public int ChannelId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Announcements { get; set; }

        public DateTime? StartDate { get; set; }

        [Alias("BiddingEndDate")]
        public DateTime EndDate { get; set; }

        public int? RegionId { get; set; }

        public int? LocalityId { get; set; }

        public string Address { get; set; }

        public string VideoUrl { get; set; }

        public string DocumentsUrl { get; set; }

        public bool IsActive { get; set; }
    }
}
