﻿

using System;
using System.Runtime.Serialization;

namespace MyHome.v4.PropertyManager.Schema.Orders
{
    [DataContract]
    [Serializable]
    public class PaymentDetail
    {
        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Identifier { get; set; }

        [DataMember]
        public string ProductIdentifier { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public string Receipt { get; set; }
    }
}
