﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.Orders
{
    public class Order
    {
        [Alias("OrderID")]
        [AutoIncrement]
        [PrimaryKey]
        public int OrderId { get; set; }
        [Alias("GroupID")]
        public int GroupId { get; set; }
        [Alias("UserID")]
        public int UserId { get; set; }
        [Alias("OrderStatusID")]
        public int OrderStatusId { get; set; }
        [Alias("ProductID")]
        public int? ProductId { get; set; }
        [Alias("ProductTypeID")]
        public int? ProductTypeId { get; set; }
        [Alias("PropertyID")]
        public int? PropertyId { get; set; }
        [Alias("PropertyClassID")]
        public int? PropertyClassId { get; set; }
        [Alias("ChannelID")]
        public int? ChannelId { get; set; }
        [Alias("HostID")]
        public int? HostId { get; set; }
        [Alias("ProductRateCardID")]
        public int? ProductRateCardId { get; set; }
        [Alias("RealExTransactionID")]
        public string RealExTransactionId { get; set; }
        public decimal Amount { get; set; }
        public decimal? CurrencyCredit { get; set; }
        public int Credits { get; set; }
        public int RefreshCredits { get; set; }
        [Alias("CreatedOnDate")]
        public DateTime CreatedOn { get; set; }
        [Alias("ExpiresOnDate")]
        public DateTime? ExpiresOn { get; set; }
        public DateTime? ProcessedOn { get; set; }
        public string Descriptor { get; set; }
        [Alias("PropertySectionID")]
        public int? PropertySectionId { get; set; }
        [Alias("ParentOrderID")]
        public int? ParentOrderId { get; set; }
        [Alias("GroupTypeID")]
        public int? GroupTypeId { get; set; }
        [Alias("PaymentTypeID")]
        public int PaymentTypeId { get; set; }
        [Alias("PaymentDetailsJson")]
        public PaymentDetail PaymentDetails { get; set; }
    }
}
