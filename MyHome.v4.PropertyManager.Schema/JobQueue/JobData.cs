﻿using MyHome.v4.Common.Schema;

namespace MyHome.v4.PropertyManager.Schema.JobQueue
{
    public class JobData
    {
        public string ToJson()
        {
            return JsonHelper.SerializeObject(this);
        }
    }
}
