﻿using System;
using ServiceStack.DataAnnotations;

namespace MyHome.v4.PropertyManager.Schema.JobQueue
{
    public class JobQueue
    {
        [Alias("JobQueueID")]
        [AutoIncrement]
        [PrimaryKey]
        public int JobQueueId { get; set; }

        [Alias("JobQueueTypeID")]
        public int JobQueueTypeId { get; set; }

        [Alias("JobQueuePriorityID")]
        public int JobQueuePriorityId { get; set; }

        public string JsonData { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ExecuteOn { get; set; }

        public DateTime? StartedOn { get; set; }

        public DateTime? CompletedOn { get; set; }

        public bool IsComplete { get; set; }

        public bool IsError { get; set; }

        public string Log { get; set; }

        public string WorkerExecutor { get; set; }
    }
}
