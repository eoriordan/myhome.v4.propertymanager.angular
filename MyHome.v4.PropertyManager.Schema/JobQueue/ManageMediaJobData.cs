﻿using System.Collections.Generic;

namespace MyHome.v4.PropertyManager.Schema.JobQueue
{
    /// <summary>
    /// The job data for managing media within the application, allow for the conversion and deletions of
    /// data from the SAN and other locations
    /// </summary>
    public class ManageMediaJobData : JobData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManageMediaJobData"/> class.
        /// </summary>
        public ManageMediaJobData()
        {
            MediaToDelete = new List<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManageMediaJobData"/> class.
        /// </summary>
        /// <param name="mediaToDelete">The media to delete.</param>
        public ManageMediaJobData(IEnumerable<string> mediaToDelete) : this()
        {
            MediaToDelete.AddRange(mediaToDelete);
        }

        /// <summary>
        /// Gets or sets the media to delete.
        /// </summary>
        /// <value>
        /// The media to delete.
        /// </value>
        public List<string> MediaToDelete { get; set; }
    }
}
