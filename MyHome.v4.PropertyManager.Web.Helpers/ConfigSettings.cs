﻿namespace MyHome.v4.PropertyManager.Web.Helpers
{
    public static class ConfigSettings
    {
        public static class AppConfig
        {
            public static string Platform
            {
                get { return System.Configuration.ConfigurationManager.AppSettings["Platform"]; }
            }

            public static string ApiKey
            {
                get { return System.Configuration.ConfigurationManager.AppSettings["ApiKey"]; }
            }

            public static string BaseImageUrl
            {
                get { return System.Configuration.ConfigurationManager.AppSettings["BaseImageUrl"] ?? string.Empty; }
            }

            public static string AzureStorageSan
            {
                get { return System.Configuration.ConfigurationManager.AppSettings["Azure.Storage.San"]; }
            }
        }

        public static class Pagination
        {
            public static int PageSize = 20;
        }

        public static class UserSession
        {
            public static string Cookie = "MyHomePropertyManager";
        }
    }
}
