﻿
using System.Web;
using System.Web.Security;

namespace MyHome.v4.PropertyManager.Web.Helpers
{
    public static class SessionHelper
    {
        public static FormsAuthenticationTicket GetAuthTicket(HttpRequestBase httpRequest)
        {
            if (httpRequest.Cookies[ConfigSettings.UserSession.Cookie] != null)
            {
                var value = httpRequest.Cookies[ConfigSettings.UserSession.Cookie].Value;
                return FormsAuthentication.Decrypt(value);
            }
            return null;
        }
    }
}
