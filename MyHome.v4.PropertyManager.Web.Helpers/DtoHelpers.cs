﻿using System;

namespace MyHome.v4.PropertyManager.Web.Helpers
{
    /// <summary>
    /// Hlper methds for dtos
    /// </summary>
    public static class DtoHelpers
    {
        /// <summary>
        /// Gets the path for an image
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="suffix"></param>
        /// <param name="id">of group or property</param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetImagePath(string fileName, string suffix, int id, string type = "media")
        {
            if (fileName.Contains("//"))  // It is already an expanded image url
            {
                return fileName;
            }
            if (id != 0 && id != 1)
            {
                var idLenth = id.ToString().Length;
                var propertyIdSplit = id.ToString().ToCharArray();
                var zeroPrefix = "";
                if (idLenth == 2)
                {
                    zeroPrefix = "0";
                }
                string path;
                if (!string.IsNullOrEmpty(zeroPrefix))
                {
                    path = ConfigSettings.AppConfig.BaseImageUrl + "/" + type + "/" + propertyIdSplit[idLenth - 1] + "/"
                           + propertyIdSplit[idLenth - 2] + "/" + zeroPrefix + "/" + zeroPrefix + id + "/";
                }
                else
                {
                    path = ConfigSettings.AppConfig.BaseImageUrl + "/" + type + "/" + propertyIdSplit[idLenth - 1] + "/" +
                           propertyIdSplit[idLenth - 2] +
                           "/" + propertyIdSplit[idLenth - 3] + "/" + id + "/";
                }
                if (String.IsNullOrEmpty(fileName))
                {
                    return "";
                }

                string[] sections = fileName.Split('.');

                if (sections.Length == 1)
                {
                    return path.Replace(" ", "%20") + fileName.Replace(" ", "%20") + suffix.Replace(" ", "%20");
                }
                var extension = sections[sections.Length - 1];

                var urlWithoutExtension = path + fileName.Substring(0, fileName.Length - extension.Length - 1);

                if (!string.IsNullOrEmpty(suffix))
                    return string.Format("{0}{1}.{2}", urlWithoutExtension.Replace(" ", "%20"), suffix.Replace(" ", "%20"), extension.Replace(" ", "%20"));

                return String.Format("{0}.{1}", urlWithoutExtension.Replace(" ", "%20"), extension.Replace(" ", "%20"));
            }
            return "";
        }
    }
}
