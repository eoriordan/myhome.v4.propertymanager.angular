﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.ServiceClient.Locality;
using MyHome.v4.ServiceClient.Locality.Schema;

namespace MyHome.v4.PropertyManager.Web.Helpers
{
    public static class SearchFormHelpers
    {
        private static readonly string[] SearchTypes = { "residential", "newhomes", "onview-properties", "rentals", "commercial-sales", "commercial-rentals", "auctions-properties", "rentals-sharing", "rentals-holiday", "overseas-rentals", "overseas-sales" };

        private static readonly Dictionary<int, Dictionary<int, string>> PropertyTypes = new Dictionary<int, Dictionary<int, string>>
        {
            { 1, new Dictionary<int,string> {
                    {36,"Apartment"},
                    {37,"Terraced House"},
                    {38,"Semi-Detached House"},
                    {39,"Detached House"},
                    {40,"Bungalow"},
                    {41,"Holiday Home"},
                    {42,"Country House"},
                    {43,"Farm"},
                    {44,"Site"},
                    {45,"End of Terrace"},
                    {46,"Cottage"},
                    {47,"Duplex"},
                    {48,"Townhouse"},
                    {49,"Penthouse"},
                    {50,"Studio"},
                    {51,"Dormer"},
                    {52,"Live-Work Unit"},
                    {53,"Period House"},
                    {54,"Mews"},
                    {97,"House"},
                    {101,"Investment"}
                }
            },
            { 2, new Dictionary<int,string> {
                    {55,"Apartment"},
                    {56,"Terraced House"},
                    {57,"Semi-Detached House"},
                    {58,"Detached House"},
                    {59,"Bungalow"},
                    {60,"Holiday Home"},
                    {61,"Country House"},
                    {62,"Farm"},
                    {63,"Site"},
                    {64,"End of Terrace"},
                    {65,"Cottage"},
                    {66,"Duplex"},
                    {67,"Townhouse"},
                    {68,"Penthouse"},
                    {69,"Studio"},
                    {70,"Dormer"},
                    {71,"Live-Work Unit"},
                    {72,"Period House"},
                    {73,"Mews"},
                    {98,"House"}
                }
            },
            { 3, new Dictionary<int,string> {
                    {74,"Apartment"},
                    {75,"Terraced House"},
                    {76,"Semi-Detached"},
                    {77,"Detached"},
                    {78,"Bungalow"},
                    {79,"Country House"},
                    {80,"Studio"},
                    {99,"House"}
                }
            },
            { 4, new Dictionary<int,string> {
                    { 81,"Single Room"},
                    {82,"Double Room"},
                    {83,"Twin Room"},
                    {84,"Shared Room"}
                }
            },
            { 5, new Dictionary<int,string> {
                    { 85,"Apartment"},
                    {86,"Terraced House"},
                    {87,"Semi-Detached"},
                    {88,"Detached"},
                    {89,"Bungalow"},
                    {90,"Country House"},
                    {91,"Farm"},
                    {92,"Site"},
                    {93,"Studio"},
                    {94,"Cottage"},
                    {95,"Townhouse"},
                    {96,"Lodge"},
                    {100,"House"}
                }
            },
            { 6, new Dictionary<int,string> {
                    {6,"Office"},
                    {7,"Industrial"},
                    {8,"Business"},
                    {9,"Investment"},
                    {10,"Pubs & Restaurants"},
                    {11,"Hotels & B+Bs"},
                    {12,"Retail"},
                    {13,"Farm Land"},
                    {14,"Development Land"},
                    {15,"Parking Space"},
                    {102,"Live-Work Unit"}
                }
            },
            { 9, new Dictionary<int,string> {
                    {1,"Apartment"},
                    {2,"Villa/House"},
                    {3,"Land/Sites"}
                }
            },
            { 10, new Dictionary<int,string> {
                    {4,"Apartment"},
                    {5,"Villa/House"}
                }
            }
        };

        public static Dictionary<int, string> SavedSearchFrequency = new Dictionary<int, string>
        {
            {0, "None"},
            {1, "Hourly"},
            {2, "Daily"},
            {3, "Twice a day"},
            {4, "Three times a day"},
            {5, "Weekly"},
            {6, "Real Time"}
        };

        public static Dictionary<string, string> Reasons = new Dictionary<string, string>
        {
            {"", ""},
            {"Moving Up", "Trading Up"},
            {"Trading Down", "Trading Down"},
            {"Breakup", "MBU"},
            {"Investors", "Investors"},
            {"Executor", "Executors"},
            {"Receivership", "Receivership"},
            {"Fair Deal", "Fair Deal"}
        };

        public static readonly List<int> SaleStatusIds = new List<int> { 2, 3, 4 };
        public static readonly List<int> RentStatusIds = new List<int> { 7, 8, 11, 12 };

        public static bool IsValidSearchType(string searchType)
        {
            return SearchTypes.Contains(searchType);
        }

        public static bool IsRentalSearchType(string searchType)
        {
            return searchType.Contains("rentals");
        }
        public static bool IsOverseasSearchType(string searchType)
        {
            return searchType.Contains("overseas");
        }

        public static SelectList GetSavedSearchFrequency()
        {
            return new SelectList(SavedSearchFrequency.Select(x => CreateItem(x.Value, x.Key.ToString())).ToList(), "Value", "Text");
        }

        public static SelectList GetReminders()
        {
            var reminders = new Dictionary<int, string>
            {
                {-1, "-- No Reminder --"}
            };

            for (int i = 10; i <= 240; i+=10)
            {
                var ts = TimeSpan.FromMinutes(i);
                if (ts.Hours > 0)
                {
                    if (ts.Minutes == 0)
                    {
                        reminders.Add(i, string.Format("{0} Hour{1}", ts.Hours, (ts.Hours == 1 ? string.Empty : "s")));
                    }
                    else
                    {
                        reminders.Add(i, string.Format("{0} Hour{2} {1} Minutes", ts.Hours, ts.Minutes, (ts.Hours == 1 ? string.Empty : "s")));
                    }
                }
                else
                {
                    reminders.Add(i, string.Format("{0} Minutes", i));
                }
            }

            reminders.Add(1440, "24 Hours");

            return new SelectList(reminders.Select(x => CreateItem(x.Value, x.Key.ToString())).ToList(), "Value", "Text");
        }

        public static SelectList GetDurations()
        {
            var durations = new Dictionary<int, string>();

            for (int i = 15; i <= 300; i += 15)
            {
                var ts = TimeSpan.FromMinutes(i);
                if (ts.Hours > 0)
                {
                    if (ts.Minutes == 0)
                    {
                        durations.Add(i, string.Format("{0} Hour{1}", ts.Hours, (ts.Hours == 1 ? string.Empty : "s")));
                    }
                    else
                    {
                        durations.Add(i, string.Format("{0} Hour{2} {1} Minutes", ts.Hours, ts.Minutes, (ts.Hours == 1 ? string.Empty : "s")));
                    }
                }
                else
                {
                    durations.Add(i, string.Format("{0} Minutes", i));
                }
            }

            durations.Add(1440, "All Day");

            return new SelectList(durations.Select(x => CreateItem(x.Value, x.Key.ToString())).ToList(), "Value", "Text");
        }

        public static SelectList GetReasons()
        {
            return new SelectList(Reasons.Select(x => CreateItem(x.Value, x.Key)).ToList(), "Value", "Text");
        }

        public static SelectList GetBedsList()
        {
            return new SelectList(Enumerable.Range(0, 10)
                .Select(x => new SelectListItem { Text = x.ToString(), Value = x.ToString() })
                .ToList(), "Value", "Text", null);
        }

        public static SelectListItem CreateItem(string text, string value, bool selected = false)
        {
            return new SelectListItem { Selected = selected, Text = text, Value = value };
        }

        private static List<SelectListItem> GetPriceList(int[] prices, string emptyText = "", int? selectedPrice = null)
        {
            var list = new List<SelectListItem> { CreateItem(emptyText, "") };

            list.AddRange(
                prices.Select(
                    price =>
                        CreateItem(((decimal) price).ToCurrency(), price.ToString(CultureInfo.InvariantCulture), price == selectedPrice)));

            return list;
        }
        public static SelectList GetLargePriceRange(string emptyText = "", int? selectedPrice = null)
        {
            int[] prices =
            {
                25000, 50000, 75000, 100000, 125000, 150000, 175000, 200000, 220000, 225000, 250000,
                275000, 300000, 350000, 400000, 450000, 500000, 550000, 600000, 650000, 700000, 800000, 900000, 1000000,
                1250000, 1500000, 1750000, 2000000, 2250000, 2500000, 2750000, 3000000, 4000000, 5000000
            };
            var priceList = GetPriceList(prices, emptyText, selectedPrice);
            priceList.Add(CreateItem("> 5,000,000", "50000000"));

            return new SelectList(priceList, "Value", "Text", selectedPrice.HasValue ? selectedPrice : null);
        }
        public static List<SelectListItem> GetSmallPriceRange(string emptyText = "", int? selectedPrice = null)
        {
            int[] prices =
            {
                400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000, 1100, 1200, 1300, 1400,
                1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3500,
                4000, 4500, 5000
            };
            return GetPriceList(prices, emptyText, selectedPrice);
        }

        public static SelectList GetOverseasLocalities()
        {
            var items = new List<SelectListItem>();
            var regionDict = new Dictionary<int, string>
            {
                {4233, "Africa"},
                {1067, " -- Cape Verde"},
                {1499, " -- Egypt"},
                {2701, " -- Mauritius"},
                {2843, " -- Morocco"},
                {3185, " -- South Africa"},
                {4234, "Asia"},
                {2169, " -- India"},
                {2178, " -- Indonesia"},
                {3362, " -- Sri Lanka"},
                {3556, " -- Thailand"},
                {3716, " -- UAE (Dubai)"},
                {4235, "Australasia"},
                {122, " -- Australia"},
                {2857, " -- New Zealand"},
                {4238, "Europe"},
                {4, " -- Andorra"},
                {88, " -- Austria"},
                {319, " -- Bulgaria"},
                {591, " -- Caribbean"},
                {928, " -- Croatia"},
                {1095, " -- Cyprus"},
                {1108, " -- Czech Republic"},
                {1505, " -- Estonia"},
                {1513, " -- France"},
                {1736, " -- Germany"},
                {1923, " -- Greece"},
                {2125, " -- Hungary"},
                {2183, " -- Italy"},
                {2484, " -- Latvia"},
                {2563, " -- Lithuania"},
                {2694, " -- Malta"},
                {2812, " -- Montenegro"},
                {2924, " -- Poland"},
                {3009, " -- Portugal"},
                {3139, " -- Romania"},
                {3192, " -- Serbia"},
                {3195, " -- Slovakia"},
                {3221, " -- Slovenia"},
                {3255, " -- Spain"},
                {3366, " -- Switzerland"},
                {3627, " -- Turkey"},
                {3723, " -- United Kingdom"},
                {4236, "North America"},
                {480, " -- Canada"},
                {2909, " -- Panama"},
                {3837, " -- USA "},
                {4237, "South America"},
                {63, " -- Argentina"},
                {246, " -- Brazil"},
                {3920, " -- Venezuela"}
            };

            items.Add(CreateItem("Select County/City", "0"));
            items.AddRange(regionDict.Select(locality => CreateItem(locality.Value, locality.Key.ToString(CultureInfo.InvariantCulture))));
            return new SelectList(items, "Value", "Text");
        }

        public static SelectList GetIrelandLocalities(int? selected)
        {
            var items = new List<SelectListItem>();
            var regionDict = new Dictionary<int, string>
            {
                {1080, "Carlow"},
                {887, "Cavan"},
                {618, "Clare"},
                {737, "Cork"},
                {769, " -- Cork City"},
                {4243, " -- Cork West"},
                {1464, "Donegal"},
                {1265, "Dublin"},
                {1441, " -- Dublin 1"},
                {1442, " -- Dublin 2"},
                {1443, " -- Dublin 3"},
                {1444, " -- Dublin 4"},
                {1445, " -- Dublin 5"},
                {1446, " -- Dublin 6"},
                {1447, " -- Dublin 6W"},
                {1448, " -- Dublin 7"},
                {1449, " -- Dublin 8"},
                {1450, " -- Dublin 9"},
                {1451, " -- Dublin 10"},
                {1452, " -- Dublin 11"},
                {1453, " -- Dublin 12"},
                {1454, " -- Dublin 13"},
                {1455, " -- Dublin 14"},
                {1456, " -- Dublin 15"},
                {1457, " -- Dublin 16"},
                {1458, " -- Dublin 17"},
                {1459, " -- Dublin 18"},
                {1460, " -- Dublin 20"},
                {1461, " -- Dublin 22"},
                {1462, " -- Dublin 24"},
                {1365, " -- Dublin North"},
                {1406, " -- Dublin South"},
                {1463, " -- Dublin County"},
                {4610, " -- North County Dublin"},
                {4611, " -- South County Dublin"},
                {1430, " -- Dublin West"},
                {2013, "Galway"},
                {2035, " -- Galway City"},
                {2448, "Kerry"},
                {2362, "Kildare"},
                {2414, "Kilkenny"},
                {2670, "Laois"},
                {2648, "Leitrim"},
                {2594, "Limerick"},
                {4155, " -- Limerick City"},
                {2515, "Longford"},
                {2535, "Louth"},
                {2781, "Mayo"},
                {2712, "Meath"},
                {2771, "Monaghan"},
                {2888, "Offaly"},
                {3091, "Roscommon"},
                {3254, "Sligo"},
                {3587, "Tipperary"},
                {3944, "Waterford"},
                {3969, "Westmeath"},
                {4054, "Wexford"},
                {4011, "Wicklow"}
            };

            items.Add(CreateItem("Select County/City", "0"));
            items.AddRange(regionDict.Select(locality => CreateItem(locality.Value, locality.Key.ToString(CultureInfo.InvariantCulture), selected.HasValue && selected.Value == locality.Key)));
            return new SelectList(items, "Value", "Text", selected);
        }


        public static List<SelectListItem> GetIrelandSlugLocalities()
        {
            var regionDict = new Dictionary<string, string>
            {
                {"ireland", "Select County/City"},
                {"carlow", "Carlow"},
                {"cavan", "Cavan"},
                {"clare-1", "Clare"},
                {"cork", "Cork"},
                {"cork-city", " -- Cork City"},
                {"cork-west", " -- Cork West"},
                {"donegal", "Donegal"},
                {"dublin", "Dublin"},
                {"dublin-1", " -- Dublin 1"},
                {"dublin-2", " -- Dublin 2"},
                {"dublin-3", " -- Dublin 3"},
                {"dublin-4", " -- Dublin 4"},
                {"dublin-5", " -- Dublin 5"},
                {"dublin-6", " -- Dublin 6"},
                {"dublin-6w", " -- Dublin 6W"},
                {"dublin-7", " -- Dublin 7"},
                {"dublin-8", " -- Dublin 8"},
                {"dublin-9", " -- Dublin 9"},
                {"dublin-10", " -- Dublin 10"},
                {"dublin-11", " -- Dublin 11"},
                {"dublin-12", " -- Dublin 12"},
                {"dublin-13", " -- Dublin 13"},
                {"dublin-14", " -- Dublin 14"},
                {"dublin-15", " -- Dublin 15"},
                {"dublin-16", " -- Dublin 16"},
                {"dublin-17", " -- Dublin 17"},
                {"dublin-18", " -- Dublin 18"},
                {"dublin-20", " -- Dublin 20"},
                {"dublin-22", " -- Dublin 22"},
                {"dublin-24", " -- Dublin 24"},
                {"dublin-north", " -- Dublin North"},
                {"dublin-south", " -- Dublin South"},
                {"dublin-county", " -- Dublin County"},
                {"dublin-north-county", " -- North County Dublin"},
                {"dublin-south-county", " -- South County Dublin"},
                {"dublin-west", " -- Dublin West"},
                {"galway", "Galway"},
                {"galway-city", " -- Galway City"},
                {"kerry", "Kerry"},
                {"kildare", "Kildare"},
                {"kilkenny", "Kilkenny"},
                {"laois", "Laois"},
                {"leitrim", "Leitrim"},
                {"limerick", "Limerick"},
                {"limerick-city", " -- Limerick City"},
                {"longford", "Longford"},
                {"louth", "Louth"},
                {"mayo", "Mayo"},
                {"meath", "Meath"},
                {"monaghan", "Monaghan"},
                {"offaly", "Offaly"},
                {"roscommon-1", "Roscommon"},
                {"sligo", "Sligo"},
                {"tipperary", "Tipperary"},
                {"waterford", "Waterford"},
                {"westmeath", "Westmeath"},
                {"wexford", "Wexford"},
                {"wicklow", "Wicklow"}
            };
            return regionDict.Select(locality => CreateItem(locality.Value, locality.Key.ToString(CultureInfo.InvariantCulture))).ToList();
        }
        public static SelectList GetSizeList(string emptyText = "", int? selected = null)
        {
            var list = new List<SelectListItem> { CreateItem(emptyText, "") };

            list.AddRange(
                from size in
                    new List<int>
                    {
                        10,
                        20,
                        30,
                        40,
                        50,
                        60,
                        70,
                        80,
                        90,
                        100,
                        125,
                        150,
                        175,
                        200,
                        250,
                        300,
                        400,
                        500,
                        750,
                        1000,
                        2500,
                        5000,
                        10000
                    }
                let text =
                    String.Format("{0} m² ({1} ft²)", size.NumberWithCommas(), ((int)(size * 10.7639)).NumberWithCommas())
                select CreateItem(text, size.ToString(CultureInfo.InvariantCulture), size == selected));

            return new SelectList(list, "Value", "Text", selected);
        }
        public static SelectList GetEnergyRatingList(string selected)
        {
            var list = new List<SelectListItem>
            {
                CreateItem("-- Select BER --", "", selected == ""),
                CreateItem("A1", "150", selected == "150"),
                CreateItem("A2", "140", selected == "150"),
                CreateItem("A3", "130", selected == "130"),
                CreateItem("B1", "120", selected == "120"),
                CreateItem("B2", "110", selected == "110"),
                CreateItem("B3", "100", selected == "100"),
                CreateItem("C1", "90", selected == "90"),
                CreateItem("C2", "80", selected == "80"),
                CreateItem("C3", "70", selected == "70"),
                CreateItem("D1", "60", selected == "60"),
                CreateItem("D2", "50", selected == "50"),
                CreateItem("E1", "40", selected == "40"),
                CreateItem("E2", "30", selected == "30"),
                CreateItem("F", "20", selected == "20"),
                CreateItem("G", "10", selected == "10"),
            };
            return new SelectList(list, "Value", "Text", selected);
        }

        public static SelectList GetSearchableDates(string selected)
        {
            var list = new List<SelectListItem>
            {
                CreateItem("-- Select Search Date --", "", selected == ""),
                CreateItem("Created", "Created", selected == "Created"),
                CreateItem("Activated", "Activated", selected == "Activated"),
                CreateItem("Agreed", "Agreed", selected == "Agreed"),
                CreateItem("Sold", "Sold", selected == "Sold"),
                CreateItem("Closed", "Closed", selected == "Closed")
            };
            return new SelectList(list, "Value", "Text", selected);
        }

        static List<SelectListItem> GetNumberListList(string emptyText = "", string labelText = "", int low = 1, int high = 10,
           int? selected = null)
        {
            var list = new List<SelectListItem> { CreateItem(emptyText, "") };

            for (int i = low; i <= high; i++)
            {
                list.Add(CreateItem(
                    i.ToString(CultureInfo.InvariantCulture) + labelText,
                    i.ToString(CultureInfo.InvariantCulture),
                    i == selected)
                 );
            }

            return list;
        }

        public static List<SelectListItem> GetMinBedsList(string emptyText = "", int? selected = null)
        {
            return GetNumberListList("Min Beds", " Bed Min", 0);
        }

        public static List<SelectListItem> GetMinBathsList(string emptyText = "", int? selected = null)
        {
            return GetNumberListList("Min Baths", " Bath", 0, 5);
        }

        public static List<SelectListItem> GetMaxBedsList(string emptyText = "", int? selected = null)
        {
            return GetNumberListList("Max Beds", " Bed Max", 0);
        }

        /// <summary>
        /// return property types for class
        /// </summary>
        /// <param name="propertyClassId"></param>
        /// <returns></returns>
        public static SelectList GetPropertyTypeList(int propertyClassId)
        {
            var items = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "-- Select Type --",
                    Value = ""
                }
            };
            if (propertyClassId > 0 && propertyClassId < 11)
            {
                items.AddRange(PropertyTypes[propertyClassId].Select(propertyType => CreateItem(propertyType.Value, propertyType.Key.ToString(CultureInfo.InvariantCulture))));
            }
            return new SelectList(items, "Value", "Text");
        }

        /// <summary>
        /// return property types for class
        /// </summary>
        /// <param name="propertyClassId"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static MultiSelectList GetPropertyTypeList(int propertyClassId, List<int> selected)
        {
            var items = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "-- Select Type --",
                    Value = ""
                }
            };
            if (propertyClassId > 0 && propertyClassId < 11)
            {
                items.AddRange(PropertyTypes[propertyClassId].Select(propertyType => CreateItem(propertyType.Value, propertyType.Key.ToString(CultureInfo.InvariantCulture))));
            }
            return new MultiSelectList(items, "Value", "Text", selected);
        }

        public static SelectList GetPropertyStatusList(int? selected = null)
        {
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem
            {
                Text = "-- Select Status --",
                Value = ""
            });
            var propertyStatuses = Enum.GetValues(typeof(PropertyStatusEnum)).Cast<PropertyStatusEnum>();
            list.AddRange(from propertyStatus in propertyStatuses
                let propertyStatusId = (int) propertyStatus
                select CreateItem(propertyStatus.ToString().Humanize(false), propertyStatusId.ToString(CultureInfo.InvariantCulture), selected == propertyStatusId));

            return new SelectList(list, "Value", "Text", selected.ToString());
        }

        public static List<SelectListItem> SortOrderList(string emptyText = "", int? selected = null)
        {
            var list = new List<SelectListItem>
            {
                CreateItem(emptyText, ""),
                CreateItem("Most Recent", "most recent", selected == 0),
                CreateItem("Lowest Price", "lowest price", selected == 1),
                CreateItem("Highest Price", "highest price", selected == 2)
            };

            return list;
        }
        public static List<SelectListItem> SearchTypeList(string emptyText = "", string selected = null)
        {
            var list = new List<SelectListItem>
            {
                CreateItem("For Sale", "residential", selected ==  "residential"),
                CreateItem("New Homes", "newhomes", selected == "newhomes"),
                CreateItem("To Rent", "rentals", selected == "rentals"),
                CreateItem("Auction Properties", "auctions-properties", selected =="auctions-properties"),
                CreateItem("Open Viewings", "onview-properties", selected =="onview-properties"),
                CreateItem("Sharing", "rentals-sharing", selected == "rentals-sharing"),
                CreateItem("Commercial Sales", "commercial-sales", selected == "commercial-sales"),
                CreateItem("Commercial Rentals", "commercial-rentals", selected == "commercial-rentals"),
                CreateItem("Irish Holiday Homes", "rentals-holiday", selected == "rentals-holiday"),
                CreateItem("Overseas To Rent", "overseas-rentals", selected == "overseas-rentals"),
                CreateItem("Overseas For Sale", "overseas-sales", selected == "overseas-sales")
            };

            return list;
        }

        public static SelectList GetPropertyClassList(string emptyText = "", string selected = null)
        {
            var list = new List<SelectListItem>
            {
                CreateItem("For Sale", "1", selected ==  "1"),
                CreateItem("New Homes", "2", selected == "2"),
                CreateItem("To Rent", "3", selected == "3"),
                CreateItem("Auction Properties", "11", selected =="11"),
                CreateItem("Sharing", "4", selected == "4"),
                CreateItem("Commercial Sales", "6", selected == "6"),
                CreateItem("Commercial Rentals", "12", selected == "12"),
                CreateItem("Irish Holiday Homes", "5", selected == "5"),
                CreateItem("Overseas To Rent", "10", selected == "10"),
                CreateItem("Overseas For Sale", "9", selected == "9")
            };

            return new SelectList(list, "Value", "Text", selected);
        }

        public static string GetSearchType(List<int> propertyClassIds = null, List<int> propertyStatusIds = null)
        {
            //default residential property types list
            if (propertyClassIds == null)
            {
                return "residential";
            }

            if (propertyClassIds.Count == 1)
            {
                if (propertyClassIds.Contains(10))
                {
                    return "overseas-rentals";
                }
                if (propertyClassIds.Contains(9))
                {
                    return "overseas-sales";
                }
                if (propertyClassIds.Contains(6))
                {
                    return propertyStatusIds != null
                        && RentStatusIds.Any(x => propertyStatusIds.Contains(x))
                            ? "commercial-rentals"
                            : "commercial-sales";
                }
                if (propertyClassIds.Contains(5))
                {
                    return "rentals-holiday";
                }
                if (propertyClassIds.Contains(4))
                {
                    return "rentals-sharing";
                }
                if (propertyClassIds.Contains(3))
                {
                    return "rentals";
                }
                if (propertyClassIds.Contains(2))
                {
                    return "newhomes";
                }
                if (propertyClassIds.Contains(1))
                {
                    return "residential";
                }
            }

            if (propertyClassIds.Contains(4) && propertyClassIds.Contains(3))
            {
                return "rentals";
            }

            if (propertyClassIds.Contains(6) && propertyClassIds.Contains(1))
            {
                return "auctions-properties";
            }
            return "residential";
        }

        public static List<int> GetPropertyClassIdsFromSearchType(string searchType)
        {
            switch (searchType)
            {
                case "newhomes": return new List<int> { 2 };
                case "rentals": return new List<int> { 3 };
                case "commercial-sales":
                case "commercial-rentals": return new List<int> { 6 };
                case "auctions-properties": return new List<int> { 1, 6 };
                case "rentals-sharing": return new List<int> { 4 };
                case "rentals-holiday": return new List<int> { 5 };
                case "overseas-rentals": return new List<int> { 10 };
                case "overseas-sales": return new List<int> { 9 };
                default: return new List<int> { 1 };
            }
        }

        public static List<int> GetPropertyStatusIdsFromSearchType(string searchType)
        {
            //ignoring 12: ForSaleOrToLet
            switch (searchType)
            {
                case "rentals":
                case "commercial-rentals":
                case "rentals-sharing":
                case "rentals-holiday":
                case "overseas-rentals":
                    return new List<int> { 11 };

                default: return new List<int> { 2 };
            }
        }

        /// <summary>
        /// return localities for the region
        /// </summary>
        /// <param name="localityService"></param>
        /// <param name="deliveryApiKey"></param>
        /// <param name="id"></param>
        /// <param name="selectedLocality"></param>
        /// <returns></returns>
        public static List<SelectListItem> GetRegionLocalities(ILocalityService localityService, string deliveryApiKey, int? id, int? selectedLocality, bool includeDefault = false)
        {
            if (id == null || id == 0 || id == 2168 || id == 2887) //Ireland or Overseas
            {
                return new List<SelectListItem>();
            }

            var localities = localityService.Get(new GetLocalityRequest
            {
                LocalityId = (int)id,
                ApiKey = deliveryApiKey,
                CorrelationId = Guid.NewGuid().ToString(),
                ChannelId = 1
            });

            var items = new List<SelectListItem>();
            if (includeDefault)
            {
                items.Add(new SelectListItem { Text = "Any Locality", Value = "" });
            }
            items.AddRange(localities.Children.Select(x => CreateItem(x.Name, x.LocalityId.ToString(), x.LocalityId == selectedLocality)).ToList());
            return items;
        }
        /// <summary>
        /// return localities for the region
        /// </summary>
        /// <param name="localityService"></param>
        /// <param name="deliveryApiKey"></param>
        /// <param name="id"></param>
        /// <param name="selectedLocality"></param>
        /// <returns></returns>
        public static List<SelectListItem> GetRegionLocalities(ILocalityService localityService, string deliveryApiKey, int? id, List<int> selectedLocalities, bool includeDefault = false)
        {
            if (id == null || id == 0 || id == 2168 || id == 2887) //Ireland or Overseas
            {
                return new List<SelectListItem>();
            }

            var localities = localityService.Get(new GetLocalityRequest
            {
                LocalityId = (int)id,
                ApiKey = deliveryApiKey,
                CorrelationId = Guid.NewGuid().ToString(),
                ChannelId = 1
            });

            var items = new List<SelectListItem>();
            if (includeDefault)
            {
                items.Add(new SelectListItem { Text = "Any Locality", Value = "" });
            }
            items.AddRange(localities.Children.Select(x => CreateItem(x.Name, x.LocalityId.ToString(), selectedLocalities.Contains(x.LocalityId))).ToList());
            return items;
        }

        public static string ToSlug(this string phrase)
        {
            string str = phrase.RemoveAccent().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public static Dictionary<string, ICollection<string>> PublicPropertyTags(PropertyClassEnum propertyClass)
        {
            var features = new List<string>();
            var others = new List<string>();
            var accessibility = new List<string>();

            if (propertyClass == PropertyClassEnum.LettingsToRent)
            {
                features = new List<string> { "Furnished", "Unfurnished", "Parking", "Central Heating", "Washing Machine", "Dryer", "Dishwasher", "Microwave", "Cable TV", "Internet", "Alarm" };
                others = new List<string> { "Suits Students", "Suits Professionals", "References Required", "Pets Allowed" };
                accessibility = new List<string> { "Step-free Access", "Walk-in Shower", "Wheelchair Access" };
            }
            else if (propertyClass == PropertyClassEnum.LettingsToShare)
            {
                features = new List<string> { "Parking", "Own bathroom", "Washing machine", "Dryer", "Dishwasher", "Microwave", "Internet", "Alarm", "Cable TV" };
                others = new List<string> { "Suits Students", "Suits Professionals", "References Required", "Males Only", "Females Only", "Smoker", "Non Smoker", "Pets Allowed" };
                accessibility = new List<string> { "Step-free Access", "Walk-in Shower", "Wheelchair Access" };
            }
            else if (propertyClass == PropertyClassEnum.LettingsHolidayHomes)
            {
                features = new List<string> { "Parking", "Cooker", "Washing Machine", "Dryer", "Dishwasher", "Microwave", "Close to Beach", "Playground", "Swimming Pool", "Gym", "Bar", "Restaurant", "Golf", "Serviced Apartment", "Kids Club" };
                accessibility = new List<string> { "Step-free Access", "Walk-in Shower", "Wheelchair Access" };
            }
            else if (propertyClass == PropertyClassEnum.OverseasForSale || propertyClass == PropertyClassEnum.OverseasHolidayHomes)
            {
                features = new List<string> { "Beach View", "Cable Television", "Central Heating", "Dishwasher", "Dryer", "Garden", "House Alarm", "Internet", "Microwave", "Parking", "Patio/Balcony", "Pets Allowed", "Serviced Property", "Swimming Pool", "Washing Machine" };
                accessibility = new List<string> { "Step-free Access", "Walk-in Shower", "Wheelchair Access" };
            }

            var dictionary = new Dictionary<string, ICollection<string>>();

            if (features.Any())
            {
                dictionary.Add("Features", features);
            }

            if (others.Any())
            {
                dictionary.Add("Other", others);
            }

            if (accessibility.Any())
            {
                dictionary.Add("Accessibility", accessibility);
            }

            return dictionary;
        }
    }
}
