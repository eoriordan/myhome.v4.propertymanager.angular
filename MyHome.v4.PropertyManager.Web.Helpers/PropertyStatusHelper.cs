﻿using System.Web;
using System.Web.Mvc;

namespace MyHome.v4.PropertyManager.Web.Helpers
{
    public static class PropertyStatusHelper
    {
        public static IHtmlString PropertyStatus(int classId, int statusId, bool isActive)
        {
            var currentType = "";
            var statusType = "";
            var currentSubType = "";

            switch (classId)
            {
                case 1:
                    currentType = "Residential";
                    break;
                case 2:
                    currentType = "Residential";
                    currentSubType = "New Home";
                    break;
                case 3:
                    currentType = "Lettings";
                    break;
                case 4:
                    currentType = "Lettings";
                    currentSubType = "To Share";
                    break;
                case 5:
                    currentType = "Lettings";
                    currentSubType = "Holiday Home";
                    break;
                case 6:
                    currentType = "Commercial";
                    break;
                case 9:
                    currentType = "Overseas";
                    break;
                case 10:
                    currentType = "Overseas";
                    currentSubType = "Holiday Home";
                    break;
            }

            switch (statusId)
            {
                case 1:
                    statusType = "Appraisal";
                    break;
                case 2:
                    statusType = "ForSale";
                    break;
                case 3:
                    statusType = "SaleAgreed";
                    break;
                case 4:
                    statusType = "Sold";
                    break;
                case 5:
                    statusType = "Closed";
                    break;
                case 6:
                    statusType = "Reserved";
                    break;
                case 7:
                    statusType = "LetAgreed";
                    break;
                case 8:
                    statusType = "Let";
                    break;

                case 9:
                    statusType = "Withdrawn";
                    break;
                case 10:
                    statusType = "Archived";
                    break;
                case 11:
                    statusType = "ToLet";
                    break;
                case 12:
                    statusType = "ForSaleOrToLet";
                    break;
                case 13:
                    statusType = "Cancelled";
                    break;
                case 14:
                    statusType = "Lost";
                    break;
                case 15:
                    statusType = "Valued";
                    break;
                case 16:
                    statusType = "Instructed";
                    break;
                case 17:
                    statusType = "Transferred";
                    break;
                case 18:
                    statusType = "Locked";
                    break;
            }

            return new MvcHtmlString(string.Format(
                "<div class = \"property-status {0} active-{1}\"><span class=\"property-class-span-{0}\">{2} {3}</span>" +
                "<span class=\"property-status-span-{4}\">{4}</span><span class=\"property-status-{5}\"> {5}</span></div>", currentType.Replace(" ", "").ToLower(), isActive,
                currentType, currentSubType, statusType, isActive ? "Active" : "InActive"));
        }
    }
}
