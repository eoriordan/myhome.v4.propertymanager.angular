﻿namespace MyHome.v4.PropertyManager.Web.Helpers
{
    public static class PropertyStatus
    {
        public static string GetStatusTitle(int statusId)
        {
            var statusType = "";
            switch (statusId)
            {
                case 1:
                    statusType = "Appraisal";
                    break;
                case 2:
                    statusType = "ForSale";
                    break;
                case 3:
                    statusType = "SaleAgreed";
                    break;
                case 4:
                    statusType = "Sold";
                    break;
                case 5:
                    statusType = "Closed";
                    break;
                case 6:
                    statusType = "Reserved";
                    break;
                case 7:
                    statusType = "LetAgreed";
                    break;
                case 8:
                    statusType = "Let";
                    break;
                case 9:
                    statusType = "Withdrawn";
                    break;
                case 10:
                    statusType = "Archived";
                    break;
                case 11:
                    statusType = "ToLet";
                    break;
                case 12:
                    statusType = "ForSaleOrToLet";
                    break;
                case 13:
                    statusType = "Cancelled";
                    break;
                case 14:
                    statusType = "Lost";
                    break;
                case 15:
                    statusType = "Valued";
                    break;
                case 16:
                    statusType = "Instructed";
                    break;
                case 17:
                    statusType = "Transferred";
                    break;
                case 18:
                    statusType = "Locked";
                    break;
            }

            return statusType;
        }
    }
}
