import * as moment from 'moment';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.less']
})
export class AppShellComponent implements OnInit {
  public currYear: string;

  constructor() { }

  ngOnInit() {
    this.currYear = moment().format('YYYY');
  }

}
