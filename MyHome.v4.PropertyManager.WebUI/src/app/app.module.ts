import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';import { HttpClientModule } from '@angular/common/http';

import { AppShellComponent } from './app-shell/app-shell.component';

@NgModule({
  declarations: [
    AppShellComponent
],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppShellComponent]
})
export class AppModule { }
