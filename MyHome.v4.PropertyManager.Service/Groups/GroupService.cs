﻿using System.Collections.Generic;
using System.Linq;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.IRepository.Groups;
using MyHome.v4.PropertyManager.IRepository.Product;
using MyHome.v4.PropertyManager.IService.Groups;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.Groups;
using MyHome.v4.PropertyManager.Types.Product;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.Groups
{
    /// <summary>
    /// The implementation of the group functionality for the service
    /// </summary>
    public class GroupService : BaseService<Group, Types.Groups.Group>, IGroupService
    {
        private readonly IProductSubscriptionRepository _productSubscriptionRepository;
        private readonly IProductRepository _productRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IUserService _userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GroupService" /> class.
        /// </summary>
        /// <param name="productSubscriptionRepository"></param>
        /// <param name="productRepository"></param>
        /// <param name="groupRepository">The group repository.</param>
        /// <param name="userService"></param>
        public GroupService(
            IProductSubscriptionRepository productSubscriptionRepository,
            IProductRepository productRepository,
            IGroupRepository groupRepository,
            IUserService userService)
            : base(groupRepository)
        {
            _productSubscriptionRepository = productSubscriptionRepository;
            _productRepository = productRepository;
            _groupRepository = groupRepository;
            _userService = userService;
        }

        /// <summary>
        /// Gets a group and populates it with relative data
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public new Types.Groups.Group GetById(int groupId)
        {
            return ConvertToServiceModel(_groupRepository.GetById(groupId));
        }

        /// <summary>
        /// Return a groups children
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private List<Types.Groups.Group> GetChildren(int id)
        {
            var groups = _groupRepository.GetByParentGroupId(id);
            if (groups.Any())
            {
                return groups.Select(ConvertToServiceModel).ToList();
            }
            return new List<Types.Groups.Group>();
        }

        /// <summary>
        /// Return a groups users
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        private IEnumerable<Types.Users.User> GetUsers(int groupId)
        {
            return _userService.GetUsersByGroupId(groupId);
        }

        private Types.Groups.Group ConvertToServiceModel(Group schemaGroup)
        {
            var serviceModelGroup = schemaGroup.ConvertTo<Types.Groups.Group>();

            serviceModelGroup.GroupConfiguration.FeaturedProperties = schemaGroup.GroupConfiguration.FeaturedProperties.Select(x => x.ConvertTo<Types.Groups.GroupFeaturedProperty>()).ToList();
            serviceModelGroup.GroupPrivateConfiguration.ContactTags = schemaGroup.GroupPrivateConfiguration.ContactTags.Select(x => x.ConvertTo<Types.Groups.Tag>()).ToList();
            serviceModelGroup.GroupPrivateConfiguration.GoogleDocsUsers = schemaGroup.GroupPrivateConfiguration.GoogleDocsUsers.Select(x => x.ConvertTo<Types.Groups.GoogleDocsUser>()).ToList();

            serviceModelGroup.GroupTypeId = schemaGroup.GroupTypeId;

            serviceModelGroup.Users = GetUsers(schemaGroup.GroupId);
            serviceModelGroup.ChildGroups = GetChildren(schemaGroup.GroupId);

            var schemaProductSubscriptions = _productSubscriptionRepository.Select(x => x.GroupId == schemaGroup.GroupId).ToList();
            var productIds = schemaProductSubscriptions.Select(x => x.ProductId).Distinct().ToList();
            var schemaProductsById = _productRepository.Select(x => productIds.Contains(x.ProductId)).ToDictionary(x => x.ProductId);

            var productSubscriptions = new List<ProductSubscription>();
            foreach (var schemaProductSubscription in schemaProductSubscriptions)
            {
                var productSubscription = schemaProductSubscription.ConvertTo<ProductSubscription>();
                var schemaProduct = schemaProductsById[schemaProductSubscription.ProductId];
                productSubscription.Product = schemaProduct.ConvertTo<Product>();
                productSubscription.Product.ProductType = (ProductType) schemaProduct.ProductTypeId;

                if (schemaProduct.PropertySectionId.HasValue)
                {
                    productSubscription.Product.PropertySection = (PropertySection) schemaProduct.PropertySectionId.Value;
                }

                productSubscriptions.Add(productSubscription);
            }

            serviceModelGroup.ProductSubscriptions = productSubscriptions;

            return serviceModelGroup;
        }
    }
}