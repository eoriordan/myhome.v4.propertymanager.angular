﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository.Document;
using MyHome.v4.PropertyManager.IService.Document;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.Document;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.Document
{
    public class DocumentService : BaseService<Schema.Document.Document, Types.Document.Document>, IDocumentService
    {
        private readonly IDocumentRepository _documentRepository;
        private readonly IDocumentContentTypeRepository _documentContentTypeRepository;

        private readonly IUserService _userService;

        public DocumentService(
            IDocumentRepository documentRepository,
            IDocumentContentTypeRepository documentContentTypeRepository,
            IUserService userService) : base(documentRepository)
        {
            _documentRepository = documentRepository;
            _documentContentTypeRepository = documentContentTypeRepository;
            _userService = userService;
        }

        public IEnumerable<Types.Document.Document> GetDocuments(int contactId)
        {
            var dbDocuments = _documentRepository.Select(x => x.ContactId == contactId);

            var documents = new Collection<Types.Document.Document>();

            foreach (var dbDocument in dbDocuments)
            {
                var doc = dbDocument.ConvertTo<Types.Document.Document>();
                doc.DocumentType = (DocumentTypeEnum) dbDocument.DocumentTypeId;
                doc.DocumentContentType = _documentContentTypeRepository.Select(x => x.DocumentContentTypeId == dbDocument.DocumentContentTypeId).FirstOrDefault();
                doc.CreatedBy = _userService.Select(x => x.UserId == dbDocument.CreatedBy).FirstOrDefault();

                documents.Add(doc);
            }

            return documents;
        }
    }
}
