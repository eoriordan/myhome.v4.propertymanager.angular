﻿using MyHome.v4.PropertyManager.IRepository.FeaturedProduct;
using MyHome.v4.PropertyManager.IService.FeaturedProduct;
using MyHome.v4.PropertyManager.Schema.FeaturedProduct;

namespace MyHome.v4.PropertyManager.Service.FeaturedProduct
{
    public class FeaturedProductService : BaseService<FeaturedSpot, Types.FeaturedProducts.FeaturedProduct>, IFeaturedProductService
    {
        public FeaturedProductService(IFeaturedProductRepository featuredProductRepository)
            : base(featuredProductRepository)
        {
        }
    }
}
