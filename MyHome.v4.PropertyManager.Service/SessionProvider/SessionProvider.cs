﻿using System;
using System.Net;
using MyHome.v4.Common.Caching;
using MyHome.v4.PropertyManager.IService.SessionProvider;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Service.SessionProvider
{
    public class SessionProvider : ISessionProvider
    {
        private readonly ICacheClient _cacheClient;

        public SessionProvider(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient;
        }
        public UserSession GetSession(string id)
        {
            var key = GetSessionCacheKey(id);
            for (int i = 0; i < 3; i++)
            {
                var session = _cacheClient.Get<UserSession>(key);
                if (session != null)
                    return session;
            }
            return _cacheClient.Get<UserSession>(key);
        }

        public bool CheckSessionExists(string guid)
        {
            //TODO extend the CacheClient to use Exists in the ServiceStack.Redis provider
            return _cacheClient.Get<UserSession>(GetSessionCacheKey(guid)) != null;
        }

        public bool SetSession(string id, UserSession session)
        {
            var saved = false;
            try
            {
                while (!saved)
                {
                    var key = GetSessionCacheKey(id);
                    saved = _cacheClient.Set(key, session, DateTime.Now.AddDays(1));
                }

                return true;
            }
            catch (WebException)
            {
                throw new WebException();
            }
        }

        public bool DeleteSession(string id)
        {
            try
            {
                _cacheClient.Remove(GetSessionCacheKey(id));
                return true;
            }
            catch (WebException)
            {
                throw new WebException();
            }
        }

        private string GetSessionCacheKey(string id)
        {
            return string.Format("Users:Sessions:{0}", id);
        }
    }
}
