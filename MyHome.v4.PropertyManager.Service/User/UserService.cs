﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Types;
using MyHome.v4.PropertyManager.Types.Requests;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.User
{
    /// <summary>
    /// The user service deals with the buisness logic for the persistence of users
    /// </summary>
    public class UserService : BaseService<Schema.Users.User, Types.Users.User>, IUserService
    {
        /// <summary>
        /// Access ot user repository methods
        /// </summary>
        private readonly IUserRepository _userRepository;

        /// <summary>
        /// Access to role membership repository methods
        /// </summary>
        private readonly IRoleMembershipRepository _roleMembershipRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        public UserService(
            IUserRepository userRepository,
            IRoleMembershipRepository roleMembershipRepository)
             : base(userRepository)
        {
            _userRepository = userRepository;
            _roleMembershipRepository = roleMembershipRepository;
        }

        /// <summary>
        /// Gets the user by username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public Types.Users.User GetByUsername(string username)
        {
            var dbUser = _userRepository.GetByUsername(1, username.Trim());
            if (dbUser != null)
            {
                var user = dbUser.ConvertTo<Types.Users.User>();
                user.Roles = GetUserRoles(dbUser.UserId);
                return user;
            }
            return null;
        }


        public void LoggedIn(int userId)
        {
            var dbUser = _userRepository.GetById(userId);
            dbUser.LastLoggedOn = DateTime.Now;
            _userRepository.Persist(dbUser);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Types.Users.User EditUser(EditUserRequest user)
        {
            var userDb = _userRepository.Edit(user.User.ConvertTo<Schema.Users.User>());
            return userDb.ConvertTo<Types.Users.User>();
        }

        /// <summary>
        /// Gets a list of users for a specific group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<Types.Users.User> GetUsersByGroupId(int groupId)
        {
            var dbUsers = _userRepository.GetByGroupId(groupId);
            if (!dbUsers.Any())
            {
                return new List<Types.Users.User>();
            }

            var users = dbUsers.Select(m => m.ConvertTo<Types.Users.User>()).ToList();
            var userRoles = GetUserRoles(dbUsers.Select(x => x.UserId).ToList());
            //The user roles dict is keyed from the user id
            foreach (var user in users)
            {
                user.Roles = userRoles[user.UserId];
            }

            return users;
        }

        /// <summary>
        /// Gets a list of users for a list of groups
        /// </summary>
        /// <param name="groupIds"></param>
        /// <returns></returns>
        public IEnumerable<Types.Users.User> GetUsersByGroupIds(List<int> groupIds)
        {
            var dbUsers = _userRepository.GetByGroupIds(groupIds);
            if (dbUsers.Any())
            {
                var users = dbUsers.Select(m => m.ConvertTo<Types.Users.User>()).ToList();
                var userRoles = GetUserRoles(dbUsers.Select(x => x.UserId).ToList());
                //The user roles dict is keyed from the user id
                foreach (var user in users)
                {
                    user.Roles = userRoles[user.UserId];
                }
                return users;                                     
            }
            return new List<Types.Users.User>();
        }
                                                                 

        /// <summary>
        /// Updates the confirmation key and expiry for a user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UpdateUserConfirmationKeyRequest(int userId)
        {
            var dbUser = _userRepository.GetById(userId);
            dbUser.EmailConfirmationKey = Guid.NewGuid().ToString();
            dbUser.ConfirmationKeyExpiresOn = DateTime.Now.AddMonths(2);
            var updatedUser = _userRepository.Persist(dbUser);
            return updatedUser.EmailConfirmationKey != dbUser.EmailConfirmationKey;
        }


        /// <summary>
        /// Gets the user roles.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        private IEnumerable<RoleEnum> GetUserRoles(int userId)
        {
            var dbRoles = _roleMembershipRepository.GetByUserId(userId);
            return dbRoles.Select(m => (RoleEnum)Enum.ToObject(typeof(RoleEnum), m.RoleId)).ToList();
        }

        /// <summary>
        /// Gets the user roles in a dictionary of user id, user roles which can be assigned in method
        /// </summary>
        /// <param name="userIds">The user ids.</param>
        /// <returns></returns>
        private Dictionary<int, List<RoleEnum>> GetUserRoles(List<int> userIds)
        {
            var dbRoles = _roleMembershipRepository.GetByUserIds(userIds);

            var dict = new Dictionary<int, List<RoleEnum>>();

            foreach (var userId in userIds)
            {
                //Get a list of roles for each user id and add them to the dictionary
                var roles = dbRoles.Where(a => a.UserId == userId).Select(m => (RoleEnum)Enum.ToObject(typeof(RoleEnum), m.RoleId)).ToList();
                dict.Add(userId, roles);
            }
            return dict;
        }
    }
}
