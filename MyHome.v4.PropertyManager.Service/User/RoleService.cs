﻿using System.Collections.Generic;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Service.User
{
    public class RoleService : BaseService<Schema.Users.Role, Role>, IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IRoleMembershipRepository _roleMembershipRepository;

        public RoleService(IRoleRepository repo, IRoleMembershipRepository roleMembershipRepository) : base(repo)
        {
            _roleMembershipRepository = roleMembershipRepository;
            _roleRepository = repo;
        }

        public IEnumerable<Role> GetByUserId(int userId)
        {
            var membershipRecords = _roleMembershipRepository.GetByUserId(userId);
            if (membershipRecords.Any())
            {
                var dbRoles = _roleRepository.GetByIds(membershipRecords.Select(x => x.RoleId).ToList());
                var enumerable = dbRoles as Schema.Users.Role[] ?? dbRoles.ToArray();
                if (enumerable.Any())
                {
                    return ConvertMany(enumerable.ToList());
                }
            }
            return new List<Role>();
        }

        public IEnumerable<Role> GetByUserIds(List<int> userIds)
        {
            var membershipRecords = _roleMembershipRepository.GetByUserIds(userIds);
            if (membershipRecords.Any())
            {
                var dbRoles = _roleRepository.GetByIds(membershipRecords.Select(x => x.RoleId).ToList());
                var enumerable = dbRoles as Schema.Users.Role[] ?? dbRoles.ToArray();
                if (enumerable.Any())
                {
                    return ConvertMany(enumerable.ToList());
                }
            }
            return new List<Role>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string GetUserRoleString(int userId)
        {
            var membershipRecords = _roleMembershipRepository.GetByUserId(userId);
            if (membershipRecords.Any())
            {
                var dbRoles = _roleRepository.GetByIds(membershipRecords.Select(x => x.RoleId).ToList());
                var enumerable = dbRoles as Schema.Users.Role[] ?? dbRoles.ToArray();
                if (enumerable.Any())
                {
                    var roleNames = enumerable.Select(x => x.Name.Trim());
                    return string.Join(",", roleNames).Trim();
                }
            }
            return "";
        }
    }
}
