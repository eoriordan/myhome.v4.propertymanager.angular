﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Types.Users;

namespace MyHome.v4.PropertyManager.Service.User
{
    public class RoleMemberShipService : BaseService<Schema.Users.RoleMembership, RoleMembership>, IRoleMemberShipService
    {
        private readonly IRoleMembershipRepository _roleMembershipRepository;

        public RoleMemberShipService(IRoleMembershipRepository roleMembershipRepository) : base(roleMembershipRepository)
        {
            _roleMembershipRepository = roleMembershipRepository;
        }

        public IEnumerable<RoleMembership> GetByUserId(int userId)
        {
            return ConvertMany(_roleMembershipRepository.GetByUserId(userId));
        }

        public IEnumerable<RoleMembership> GetByUserIds(List<int> userIds)
        {
            return ConvertMany(_roleMembershipRepository.GetByUserIds(userIds));
        }
    }
}
