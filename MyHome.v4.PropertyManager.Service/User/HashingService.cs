﻿using System;
using System.Security.Cryptography;
using System.Text;
using MyHome.v4.PropertyManager.IService.Users;

namespace MyHome.v4.PropertyManager.Service.User
{
    public class HashingService : IHashingService
    {
        /// <summary>
        /// The random number method used in the generation of the random strings for passwords and confimation keys
        /// </summary>
        private static readonly Random Random = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Randoms the string.
        /// </summary>
        /// <param name="size">The size.</param>
        /// <returns></returns>
        public string RandomString(int size)
        {
            var chars = "abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMONQURSTUVWXYZ0123456789".ToCharArray();
            var builder = new StringBuilder();
            for (var i = 0; i < size; i++)
            {
                builder.Append(chars[Random.Next(0, chars.Length - 1)]);
            }
            return builder.ToString();
        }

        /// <summary>
        /// Salts the and set password.
        /// </summary>
        /// <param name="dbUser">The database user.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public Schema.Users.User SaltAndSetPassword(Schema.Users.User dbUser, string password)
        {
            dbUser.PasswordSalt = Guid.NewGuid().ToString();
            dbUser.HashedPassword = HashPassword(password, dbUser.PasswordSalt);
            return dbUser;
        }

        /// <summary>
        /// Hashes the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="salt">The salt.</param>
        /// <returns></returns>
        public string HashPassword(string password, string salt)
        {
            return HashString(string.Format("{0}{1}", password, salt)).Trim();
        }

        /// <summary>
        /// Hashes the string
        /// </summary>
        /// <param name="stringToHash">The string to hash.</param>
        /// <returns></returns>
        private string HashString(string stringToHash)
        {
            var sha = new SHA1Managed();
            var hash = sha.ComputeHash(Encoding.ASCII.GetBytes(stringToHash));

            var stringBuilder = new StringBuilder();
            foreach (var b in hash)
            {
                stringBuilder.AppendFormat("{0:x2}", b);
            }
            return stringBuilder.ToString().ToUpper();
        }
    }
}
