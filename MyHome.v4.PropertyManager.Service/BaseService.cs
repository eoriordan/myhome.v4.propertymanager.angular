﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IService;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service
{
    public class BaseService<T, TType> : IBaseService<T, TType> 
        where T : class, new()
        where TType : class, new()
    {
        protected IBaseRepository<T> Repo;

        public BaseService(IBaseRepository<T> repo)
        {
            Repo = repo;
        }

        /// <summary>
        /// Get the poco of type T from the persistent storage
        /// </summary>
        /// <param name="id">The uniqe id of the property to load from the persistent storage</param>
        /// <returns>Returns a poco of type T</returns>
        public TType GetById(int id)
        {
            var entity = Repo.GetById(id);
            return entity != null ? entity.ConvertTo<TType>() : null;
        }

        /// <summary>
        /// Gets the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception></exception>
        public async Task<TType> GetByIdAsync(int id)
        {
            var entity = await Repo.GetByIdAsync(id);
            if (entity != null)
            {
                return entity.ConvertTo<TType>();
            }
            return null;
        }


        /// <summary>
        /// Get a list of poco's of type T from the persistent storage based on there unique ids
        /// </summary>
        /// <param name="ids">The unique ids to load the items from there persistent storage</param>
        /// <returns>Returns a list of poco's of type T</returns>
        public IEnumerable<TType> GetByIds(List<int> ids)
        {
            var results = Repo.GetByIds(ids);
            var enumerable = results as T[] ?? results.ToArray();
            if (enumerable.Any())
            {
                var converted = enumerable.Select(x => x.ConvertTo<TType>());
                return converted.ToList();
            }
            return new List<TType>();
        }

        /// <summary>
        /// Gets the by ids asynchronous.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        /// <exception></exception>
        public async Task<IEnumerable<TType>> GetByIdsAsync(List<int> ids)
        {
            var entities = await Repo.GetByIdsAsync(ids);
            var enumerable = entities as T[] ?? entities.ToArray();
            if (enumerable.Any())
            {
                return enumerable.Select(x => x.ConvertTo<TType>()).ToList();
            }
            return new List<TType>();
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TType> GetAll(int page = 1, int pageCount = 5000)
        {
            var results = Repo.GetAll(page, pageCount);
            var enumerable = results as T[] ?? results.ToArray();
            if (enumerable.Any())
            {
                return enumerable.Select(x => x.ConvertTo<TType>()).ToList();
            }
            return new List<TType>();
        }

        public long Count(Expression<Func<T, bool>> expression)
        {
            return Repo.Count(expression);
        }

        public IEnumerable<TType> Select(Expression<Func<T, bool>> expression)
        {
            return Select(expression, 1, 5000);
        }

        public IEnumerable<TType> Select(Expression<Func<T, bool>> expression, int page)
        {
            return Select(expression, page, 5000);
        }


        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TType> Select(Expression<Func<T, bool>> expression, int page, int pageCount)
        {
            var results = Repo.Select(expression, page, pageCount);
            var enumerable = results as T[] ?? results.ToArray();
            if (enumerable.Any())
            {
                return enumerable.Select(x => x.ConvertTo<TType>()).ToList();
            }
            return new List<TType>();
        }

        /// <summary>
        /// Gets all asynchronous.
        /// </summary>
        /// <returns></returns>
        /// <exception></exception>
        public async Task<IEnumerable<TType>> GetAllAsync()
        {
            var entities = await Repo.GetAllAsync();
            var enumerable = entities as T[] ?? entities.ToArray();
            if (enumerable.Any())
            {
                return enumerable.Select(x => x.ConvertTo<TType>()).ToList();
            }
            return new List<TType>();
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception></exception>
        public bool Delete(int id)
        {
            return Repo.Delete(id);
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception></exception>
        public bool HardDelete(int id)
        {
            return Repo.HardDelete(id);
        }

        /// <summary>
        /// Deletes the many.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        /// <exception></exception>
        public bool DeleteMany(List<int> ids)
        {
            return Repo.DeleteMany(ids);
        }

        /// <summary>
        /// Persists the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception></exception>
        public TType Persist(TType value)
        {
            return Repo.Persist(Convert(value)).ConvertTo<TType>();
        }

        /// <summary>
        /// Persists the many.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        /// <exception></exception>
        public IEnumerable<TType> PersistMany(List<TType> values)
        {
            var dbValues = values.Select(x => x.ConvertTo<T>()).ToList();
            return Repo.PersistMany(dbValues).Select(x => x.ConvertTo<TType>()).ToList();
        }

        /// <summary>
        /// Converts the specified item S to type T
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public TType Convert(T item)
        {
            //We convert the item and then retrun the first of a default object
            var items = ConvertMany(new List<T> { item });
            return items.FirstOrDefault();
        }

        /// <summary>
        /// Converts the specified items.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public List<TType> ConvertMany(List<T> items)
        {
            //-- We dont wannt to return null items
            // ReSharper disable once CompareNonConstrainedGenericWithNull
            return items.Where(m => m != null).Select(m => m.ConvertTo<TType>()).ToList();
        }

        /// <summary>
        /// Converts the specified item S to type T
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public T Convert(TType item)
        {
            //We convert the item and then retrun the first of a default object
            var items = ConvertMany(new List<TType> { item });
            return items.FirstOrDefault();
        }

        /// <summary>
        /// Converts the specified items.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns></returns>
        public List<T> ConvertMany(List<TType> items)
        {
            //-- We dont wannt to return null items
            // ReSharper disable once CompareNonConstrainedGenericWithNull
            return items.Where(m => m != null).Select(m => m.ConvertTo<T>()).ToList();
        }
    }
}