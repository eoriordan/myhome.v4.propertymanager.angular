﻿using MyHome.v4.PropertyManager.IRepository.Contacts;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.Types.Contacts;

namespace MyHome.v4.PropertyManager.Service.Contacts
{
    public class ContactsService : BaseService<Schema.Contacts.Contact, Contact>, IContactService
    {
        public ContactsService(IContactRepository repo) : base(repo)
        {
        }
    }
}
