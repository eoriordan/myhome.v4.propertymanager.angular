﻿using System;
using System.Collections.Generic;
using System.Linq;
using Elasticsearch.Net.ConnectionPool;
using MyHome.v4.Common.Caching;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.Types.Contacts;
using MyHome.v4.PropertyManager.Web.Helpers;
using Nest;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.Contacts
{
    public class ContactsSearchService : IContactsSearchService
    {
        private IElasticClient _searchClient;
        private readonly IMyHomeConfigurationManager _configurationManager;

        public ContactsSearchService(IMyHomeConfigurationManager configManager)
        {
            _configurationManager = configManager;
        }

        public PagedList<Contact> Search(string searchText, ICollection<int> groupIds, ICollection<string> tags, int page, int pageSize)
        {

            var searchWildCard = string.Format("*{0}*", string.IsNullOrEmpty(searchText) ? string.Empty : searchText.ToLowerInvariant());

            Func<SearchDescriptor<ElasticSearchContact>, SearchDescriptor<ElasticSearchContact>> searchPredicate = a => a.From(page == 1 ? 0 : (page - 1) * pageSize)
                .TrackScores()
                .Size(pageSize)
                .Query(q =>
                    q.Wildcard(wc => wc.OnField(f => f.ContactDetails.LastName).Value(searchWildCard)) ||
                    q.Wildcard(wc => wc.OnField(f => f.ContactDetails.FirstName).Value(searchWildCard)) ||
                    q.Wildcard(wc => wc.OnField(f => f.ContactDetails.CompanyName).Value(searchWildCard)) ||
                    q.Wildcard(wc => wc.OnField(f => f.ContactDetails.Email).Value(searchWildCard)) ||
                    q.Wildcard(wc => wc.OnField(f => f.ContactDetails.Phone).Value(searchWildCard)))
                .Filter(f => new FilterContainer(new AndFilter { Filters = CreateContactsSearchFilter(groupIds, tags) }))
                .SortDescending("_score");

            var searchResult = GetClient().Search(searchPredicate);

            var result = new PagedList<Contact>
            {
                Page = page,
                PageSize = pageSize,
                ResultCount = Convert.ToInt32(searchResult.Total)
            };

            result.AddRange(searchResult.Documents.Select(x => x.ConvertTo<Contact>()));
            return result;
        }

        public PagedList<Contact> Search(string email, string phone, string mobile, ICollection<int> groupIds)
        {
            var mailParts = string.IsNullOrEmpty(email) || !email.Contains('@') ? new [] {string.Empty, string.Empty} : email.Split('@');
            var cleanPhone = string.IsNullOrEmpty(phone) ? string.Empty : new string(phone.Where(char.IsDigit).ToArray());
            var cleanMobile = string.IsNullOrEmpty(mobile) ? string.Empty : new string(mobile.Where(char.IsDigit).ToArray());

            Func<SearchDescriptor<ElasticSearchContact>, SearchDescriptor<ElasticSearchContact>> searchPredicate = a =>
                a.From(0)
                    .TrackScores()
                    .Size(ConfigSettings.Pagination.PageSize)
                    .Query(q =>
                        (q.Wildcard(wc => wc.OnField(f => f.ContactDetails.Email).Value(mailParts[0])) &&
                         q.Wildcard(wc => wc.OnField(f => f.ContactDetails.Email).Value(mailParts[1]))) ||
                        q.Fuzzy(wc => wc.OnField(f => f.ContactDetails.Mobile).Value(cleanPhone)) ||
                        q.Fuzzy(wc => wc.OnField(f => f.ContactDetails.Mobile).Value(cleanMobile)) ||
                        q.Fuzzy(wc => wc.OnField(f => f.ContactDetails.Phone).Value(cleanPhone)) ||
                        q.Fuzzy(wc => wc.OnField(f => f.ContactDetails.Phone).Value(cleanMobile)))
                    .Filter(f => new FilterContainer(new AndFilter { Filters = CreateContactsSearchFilter(groupIds, null) }))
                    .SortDescending("_score");

            var searchResult = GetClient().Search(searchPredicate);

            var result = new PagedList<Contact>
            {
                Page = 1,
                PageSize = ConfigSettings.Pagination.PageSize,
                ResultCount = Convert.ToInt32(searchResult.Total)
            };

            result.AddRange(searchResult.Documents.Select(x => x.ConvertTo<Contact>()));
            return result;
        }

        public void Persist(Contact contact)
        {
            GetClient().Index(contact.ConvertTo<ElasticSearchContact>());
        }

        private static IEnumerable<IFilterContainer> CreateContactsSearchFilter(ICollection<int> groups, ICollection<string> tags)
        {
            var items = new List<IFilterContainer>();

            if (groups != null && groups.Any())
                items.AddRange(groups.Select(c => new FilterDescriptor<ElasticSearchContact>().And(a => a.Term(b => b.GroupId, c))));

            if (tags != null && tags.Any())
                items.Add(new FilterDescriptor<ElasticSearchContact>().Terms(m => m.Tags, tags.Select(tag => tag.ToLowerInvariant()), TermsExecution.Or));

            return items;
        }

        /// <summary>
        /// Get the elastic search client to connect to the elastic search cluster
        /// </summary>
        /// <returns></returns>
        private IElasticClient GetClient()
        {
            if (_searchClient != null) return _searchClient;
            
            //Setup the node list so we can use the cluster to connect to instead of the single machine
            var nodeList = _configurationManager.GetElasticSearchEnpoints();

            //Create a connection pool
            var connectionPool = new SniffingConnectionPool(nodeList);

            //Create the connection settings to use that pool
            var settings = new ConnectionSettings(connectionPool);

            settings.MaximumRetries(3);
            settings.SetConnectTimeout(500);

            //Setup contact 
            settings.MapDefaultTypeIndices(dictionary => dictionary.Add(typeof(ElasticSearchContact), "contact"));
            settings.MapDefaultTypeNames(dictionary => dictionary.Add(typeof(ElasticSearchContact), "contact"));

            _searchClient = new ElasticClient(settings);

            EnsureContactIndexExists();
            return _searchClient;
        }

        /// <summary>
        /// Ensures the contact index exists.
        /// </summary>
        private void EnsureContactIndexExists()
        {
            var contactIndex = _searchClient.IndexExists(m => m.Index(typeof(ElasticSearchContact)));
            if (contactIndex.Exists) return;
            
            _searchClient.CreateIndex("contact");
            _searchClient.Map<ElasticSearchContact>(m => m.Type("contact").Index("contact").IdField(a => a.Path("ContactId")));   
        }
    }
}
