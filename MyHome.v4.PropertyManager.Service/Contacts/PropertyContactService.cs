﻿using System.Collections.Generic;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.Schema.Contacts;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.Contacts
{
    public class PropertyContactService : BaseService<PropertyContact, Types.Contacts.PropertyContact>, IPropertyContactsService
    {
        private readonly IPropertyContactRepository _propertyContactRepository;

        public PropertyContactService(IPropertyContactRepository propertyContactRepository) : base(propertyContactRepository)
        {
            _propertyContactRepository = propertyContactRepository;
        }

        public IEnumerable<Types.Contacts.PropertyContactWithContact> GetPropertyContactWithContacts(int propertyId)
        {
            return _propertyContactRepository.GetPropertyContactWithContacts(propertyId)
                .Select(x => x.ConvertTo<Types.Contacts.PropertyContactWithContact>()).ToList();
        }
    }
}
