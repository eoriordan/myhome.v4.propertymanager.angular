﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.IRepository.Common;
using MyHome.v4.PropertyManager.IRepository.OpenViewing;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.IService.OpenViewing;
using MyHome.v4.PropertyManager.Schema.Common;
using MyHome.v4.PropertyManager.Schema.OpenViewing;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.OpenViewing
{
    public class OpenViewingService : BaseService<Schema.OpenViewing.OpenViewing, Types.OpenViewing.OpenViewing>, IOpenViewingService
    {
        private readonly IPropertyRepository _propertyRepository;

        private readonly IOpenViewingRepository _openViewingRepository;
        private readonly IOpenViewingNoteRepository _openViewingNoteRepository;
        private readonly IOpenViewingContactMappingRepository _openViewingContactMappingRepository;
        private readonly IOpenViewingUserMappingRepository _openViewingUserMappingRepository;
        private readonly IIndexQueueRepository _indexQueueRepository;

        private readonly IContactService _contactService;

        public OpenViewingService(
            IPropertyRepository propertyRepository,
            IOpenViewingRepository openViewingRepository,
            IOpenViewingNoteRepository openViewingNoteRepository,
            IOpenViewingContactMappingRepository openViewingContactMappingRepository,
            IOpenViewingUserMappingRepository openViewingUserMappingRepository,
            IIndexQueueRepository indexQueueRepository,
            IContactService contactService)
            : base(openViewingRepository)
        {
            _propertyRepository = propertyRepository;
            _openViewingRepository = openViewingRepository;
            _openViewingNoteRepository = openViewingNoteRepository;
            _openViewingContactMappingRepository = openViewingContactMappingRepository;
            _openViewingUserMappingRepository = openViewingUserMappingRepository;
            _indexQueueRepository = indexQueueRepository;
            _contactService = contactService;
        }

        public IEnumerable<Types.OpenViewing.OpenViewing> GetOpenViewings(int propertyId)
        {
            var dbOpenViewings = _openViewingRepository.Select(x => x.PropertyId == propertyId).ToDictionary(x => x.OpenViewingId);

            if (!dbOpenViewings.Any())
            {
                return Enumerable.Empty<Types.OpenViewing.OpenViewing>();
            }

            var dbOpenViewingNotes = _openViewingNoteRepository.Select(x => dbOpenViewings.Keys.Contains(x.OpenViewingId)).ToLookup(x => x.OpenViewingId);
            var dbContactMappings = _openViewingContactMappingRepository.Select(x => dbOpenViewings.Keys.Contains(x.OpenViewingId)).ToLookup(x => x.OpenViewingId);
            //var dbUserMappings = _openViewingUserMappingRepository.Select(x => dbOpenViewings.Keys.Contains(x.OpenViewingId)).ToLookup(x => x.OpenViewingId);

            var openViewings = dbOpenViewings.Values.Select(x => x.ConvertTo<Types.OpenViewing.OpenViewing>()).ToList();

            var uniqueContactIds = dbContactMappings.SelectMany(x => x).Select(x => x.ContactId).Distinct().ToList();
            var contactsById = _contactService.Select(x => uniqueContactIds.Contains(x.ContactId)).ToDictionary(x => x.ContactId);

            foreach (var openViewing in openViewings)
            {
                openViewing.Notes = dbOpenViewingNotes[openViewing.OpenViewingId].Select(x => x.ConvertTo<Types.OpenViewing.OpenViewingNote>()).ToList();

                var openViewingContactIds = dbContactMappings[openViewing.OpenViewingId].Select(x => x.ContactId);
                openViewing.Contacts = openViewingContactIds.Select(contactId => contactsById[contactId]).ToList();
            }

            return openViewings;
        }

        public Types.OpenViewing.OpenViewing GetOpenViewing(int openViewingId)
        {
            var dbOpenViewing = _openViewingRepository.Select(x => x.OpenViewingId == openViewingId).FirstOrDefault();

            if (dbOpenViewing == null)
            {
                throw new ArgumentException(string.Format("unable to find open viewing {0}", openViewingId));
            }

            var openViewing = dbOpenViewing.ConvertTo<Types.OpenViewing.OpenViewing>();

            var dbOpenViewingNotes = _openViewingNoteRepository.Select(x => x.OpenViewingId == openViewingId).ToList();
            openViewing.Notes = dbOpenViewingNotes.Select(x => x.ConvertTo<Types.OpenViewing.OpenViewingNote>()).ToList();

            var dbContactMappings = _openViewingContactMappingRepository.Select(x => x.OpenViewingId == openViewingId).ToList();
            var uniqueContactIds = dbContactMappings.Select(x => x.ContactId).Distinct().ToList();
            openViewing.Contacts = _contactService.Select(x => uniqueContactIds.Contains(x.ContactId)).ToList();

            return openViewing;
        }

        public void InsertOpenViewing(Types.OpenViewing.OpenViewing openViewing)
        {
            var dbProperty = _propertyRepository.GetProperty(openViewing.PropertyId);

            if (dbProperty == null)
            {
                throw new ArgumentException(string.Format("Cannot create open viewing for property {0} as it does not exist", openViewing.PropertyId));
            }

            openViewing.GroupId = dbProperty.GroupId;

            Schema.OpenViewing.OpenViewing dbOpenViewing = null;

            try
            {
                dbOpenViewing = _openViewingRepository.Persist(openViewing.ConvertTo<Schema.OpenViewing.OpenViewing>());

                SaveNotes(openViewing, dbOpenViewing.OpenViewingId);
                SaveContacts(openViewing, dbOpenViewing.OpenViewingId);

                EnqueueProperty(openViewing.PropertyId);
            }
            catch (Exception)
            {
                
                if (dbOpenViewing != null)
                {
                    _openViewingContactMappingRepository.Delete(dbOpenViewing.OpenViewingId);
                    _openViewingNoteRepository.Delete(dbOpenViewing.OpenViewingId);
                    _openViewingRepository.Delete(dbOpenViewing.OpenViewingId);
                }
            }
        }

        public void UpdateOpenViewing(Types.OpenViewing.OpenViewing openViewing)
        {
            var dbOpenViewing = _openViewingRepository.Select(x => x.OpenViewingId == openViewing.OpenViewingId).FirstOrDefault();

            if (dbOpenViewing == null)
            {
                throw new ArgumentException(string.Format("Cannot delete open viewing {0} as it does not exist", openViewing.OpenViewingId));
            }

            dbOpenViewing.ModifiedOn = DateTime.Now;
            dbOpenViewing.ModifiedByUserId = openViewing.ModifiedByUserId;
            dbOpenViewing.StartDate = openViewing.StartDate;
            dbOpenViewing.EndDate = openViewing.EndDate;
            dbOpenViewing.IsPublic = openViewing.IsPublic;

            _openViewingRepository.Persist(dbOpenViewing);

            SaveNotes(openViewing, openViewing.OpenViewingId);
            SaveContacts(openViewing, dbOpenViewing.OpenViewingId);

            EnqueueProperty(openViewing.PropertyId);
        }

        public void DeleteOpenViewing(int openViewingId)
        {
            var dbOpenViewing = _openViewingRepository.Select(x => x.OpenViewingId == openViewingId).FirstOrDefault();

            if (dbOpenViewing == null)
            {
                throw new ArgumentException(string.Format("Cannot delete open viewing {0} as it does not exist", openViewingId));
            }

            var propertyId = dbOpenViewing.PropertyId;

            _openViewingContactMappingRepository.Delete(openViewingId);
            _openViewingUserMappingRepository.Delete(openViewingId);

            var dbNoteIds = _openViewingNoteRepository.Select(x => x.OpenViewingId == openViewingId).Select(x => x.Id).ToList();
            _openViewingNoteRepository.DeleteMany(dbNoteIds);

            _openViewingRepository.Delete(openViewingId);

            EnqueueProperty(propertyId);
        }

        private void SaveNotes(Types.OpenViewing.OpenViewing openViewing, int openViewingId)
        {
            if (openViewing.Notes != null && openViewing.Notes.Any())
            {
                _openViewingNoteRepository.PersistMany(openViewing.Notes.Select(x => new OpenViewingNote
                    {
                        Id = x.Id,
                        UserId = openViewing.ModifiedByUserId,
                        OpenViewingId = openViewingId,
                        NoteText = x.NoteText
                    })
                    .ToList());
            }
        }

        private void SaveContacts(Types.OpenViewing.OpenViewing openViewing, int openViewingId)
        {
            _openViewingContactMappingRepository.Delete(openViewingId);
            if (openViewing.Contacts != null && openViewing.Contacts.Any())
            {
                _openViewingContactMappingRepository.PersistMany(openViewing.Contacts.Select(x => new OpenViewingContactMapping
                {
                    OpenViewingId = openViewingId,
                    ContactId = x.ContactId
                }).ToList());
            }
        }

        private void EnqueueProperty(int propertyId)
        {
            _indexQueueRepository.Persist(new IndexQueue
            {
                CreatedOn = DateTime.Now,
                EntityId = propertyId,
                IndexTypeId = (int)IndexQueueTypeEnum.PropertyIndex // need to update OrmLite to >4.0.54 to support enum <-> int read/write
            });
        }
    }
}
