﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using MyHome.v4.Common.Schema;
using Newtonsoft.Json.Linq;

namespace MyHome.v4.PropertyManager.Service.Objects
{
    public class PropertyMapper
    {

        public static T ApplyProperties<T>(T instance, Dictionary<string, object> propertiesToApply)
        {
            if (propertiesToApply == null || propertiesToApply.Count == 0)
            {
                return instance;
            }
            return ApplyProperties(instance, JsonHelper.SerializeObject(propertiesToApply));
        }

        public static T ApplyProperties<T>(T instance, string json)
        {
            var jObject = JObject.Parse(json);
            foreach (var field in jObject)
            {

                var property = instance.GetType().GetProperty(field.Key);

                if (IsAList(field.Value))
                {
                    var list = (IList)Activator.CreateInstance(property.PropertyType);
                    foreach (var value in GetValues(field.Value))
                        list.Add(value);
                    property.SetValue(instance, list, null);
                    continue;
                }

                object typedValue = null;

                if (IsAnObject(field.Value))
                {
                    typedValue = Activator.CreateInstance(property.PropertyType);
                    ApplyProperties(typedValue, field.Value.ToString());
                }

                if (property != null)
                {
                    if (IsANullableType(property))
                    {
                        var baseType = Nullable.GetUnderlyingType(property.PropertyType);
                        var nullableType = typeof(Nullable<>).MakeGenericType(baseType);
                        var value = ChangeType(GetTypedValue(field.Value), baseType);
                        typedValue = Activator.CreateInstance(nullableType, value);
                    }
                    if (typedValue == null)
                    {
                        typedValue = ChangeType(GetTypedValue(field.Value), property.PropertyType);
                    }

                    if (typedValue != null)
                    {
                        property.SetValue(instance, typedValue, null);
                    }
                }
            }
            return instance;
        }

        private static object ChangeType(object value, Type type)
        {
            if (value == null) return null;
            if (type.IsEnum)
            {
                if (value is string)
                {
                    value = Enum.Parse(type, (string)value, true);
                }
                else
                {
                    return Enum.ToObject(type, value);
                }
            }
            try
            {
                return Convert.ChangeType(value, type);
            }
            catch
            {
                return null;
            }

        }

        private static bool IsANullableType(PropertyInfo property)
        {
            return property != null && property.PropertyType.Name.StartsWith("Nullable");
        }


        private static IEnumerable<object> GetValues(IEnumerable<JToken> value)
        {
            var values = new ArrayList();
            foreach (var item in value.Values())
                values.Add(GetTypedValue(item));
            return values.ToArray();
        }

        private static object GetTypedValue(JToken value)
        {
            TypeCode type = GetTypeCode(value.Type);
            object typedValue = Convert.ChangeType(value.ToString(), type);
            if (type == TypeCode.String)
                typedValue = ((string)typedValue).Replace("\"", "").Replace("\'", "");
            return typedValue;
        }

        private static bool IsAnObject(JToken value)
        {
            return types[value.Type.ToString()] == TypeCode.Object;
        }

        private static bool IsAList(JToken value)
        {
            return value.Type == JTokenType.Array;
        }

        private static TypeCode GetTypeCode(JTokenType jTokenType)
        {
            return types[jTokenType.ToString()];
        }

        private static Dictionary<string, TypeCode> types = new Dictionary<string, TypeCode>
        {
            { "Integer", TypeCode.Int32 },
            { "String", TypeCode.String },
            { "Boolean", TypeCode.Boolean },
            { "Float", TypeCode.Double },
            { "Object", TypeCode.Object }
        };

    }
}
