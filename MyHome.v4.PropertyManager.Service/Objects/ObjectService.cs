﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.IService.Objects;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Types.Requests;
using MyHome.v4.PropertyManager.Web.Helpers;
using MyHome.v4.ServiceClient.Locality;
using MyHome.v4.ServiceClient.Locality.Schema;
using MyHome.v4.ServiceClient.PropertyType;
using MyHome.v4.ServiceClient.PropertyType.Schema;
using SortDirection = MyHome.v4.PropertyManager.Schema.SortDirection;
using StringExtensions = MyHome.v4.Common.Extensions.StringExtensions;

namespace MyHome.v4.PropertyManager.Service.Objects
{
    public class ObjectService : IObjectService
    {
        private readonly ILocalityService _localityService;
        private readonly IPropertyTypeService _propertyTypeService;

        public ObjectService(ILocalityService localityService, IPropertyTypeService propertyTypeService)
        {
            _localityService = localityService;
            _propertyTypeService = propertyTypeService;
        }

        public BaseSearchProperties GenerateSearchObject(string searchPath)
        {
            try
            {
                //Clear up the urls
                var url = searchPath.ToLower();
                if (searchPath.Contains("http://") || searchPath.Contains("https://"))
                {
                    url = RemoveHostname(url);
                }

                //Map any query string paraeters passed
                var mappedParameters = new SearchParametersMap(url);

                //We create the return object to map everything to
                var search = new BaseSearchProperties
                {
                    SortColumn = SortColumn.RefreshedDate,
                    SortDirection = SortDirection.Desc
                };

                //If the url contains results of search then we know its not a seo url
                var isSeoUrl = (!url.Contains("/results") && !url.Contains("/search"));

                //Split into sections
                var urlSections = url.Split('?')[0].Split('/').Where(s => !string.IsNullOrEmpty(s)).ToList();

                var section = urlSections[0];

                if (isSeoUrl)
                {
                    var propertyClassIsSpecified = (urlSections.Count == 4);
                    var propertyClassSecondaryUrlSlug = propertyClassIsSpecified ? urlSections[2] : "";
                    var regionSlug = urlSections[1];
                    var propertyTypeAndLocalitySlug = propertyClassIsSpecified ? urlSections[3] : urlSections[2];
                    string localitySlug = null;
                    if (propertyTypeAndLocalitySlug.Contains("-in-"))
                    {
                        localitySlug = propertyTypeAndLocalitySlug.Split("-in-", 2)[1];
                    }
                    var propertyTypeSlug = propertyTypeAndLocalitySlug.Split("-in-")[0];
                    AddPropertyClass(search, section, propertyClassSecondaryUrlSlug);

                    //Commercial searches use property status
                    if (search.SearchRequest.PropertyClassIds.FirstOrDefault() == (int)PropertyClassEnum.Commercial)
                    {
                        if (propertyTypeAndLocalitySlug.Contains("to-rent"))
                        {
                            search.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ToLet);
                        }

                        if (propertyTypeAndLocalitySlug.Contains("for-sale"))
                        {
                            search.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ForSale);
                        }
                        if (propertyTypeAndLocalitySlug.Contains("commercial-property") || (!propertyTypeAndLocalitySlug.Contains("for-sale") && !propertyTypeAndLocalitySlug.Contains("to-rent")))
                        {
                            //either commercial-property (no locality & no propertytype) OR [PropertyType]-in-[Locality]  e.g. office-in-portlaoise
                            search.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ForSale);
                            search.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ToLet);
                        }
                        search.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ForSaleOrToLet);
                    }

                    var region = _localityService.Get(new GetLocalityByUrlSlug { ApiKey = ConfigSettings.AppConfig.ApiKey, /*"21C74E36-3D9C-45AE-BB65-7C66EE17178E"*/ CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E", ChannelId = 1, UrlSlug = regionSlug });
                        search.SearchRequest.RegionId = region.LocalityId;

                    if (!string.IsNullOrEmpty(localitySlug))
                    {
                        var locality = _localityService.Get(new GetLocalityByUrlSlug { ApiKey = ConfigSettings.AppConfig.ApiKey, /*"21C74E36-3D9C-45AE-BB65-7C66EE17178E"*/ CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E", ChannelId = 1, UrlSlug = localitySlug });
                        //We dont throw an error here any more because if we have the hight level region then we can still
                        //display relevant results to the user
                        if (locality != null)
                        {
                            search.SearchRequest.RegionId = locality.IsParent ? locality.LocalityId : locality.Region.LocalityId;
                            search.SearchRequest.LocalityIds.Add(locality.LocalityId);
                        }
                        else
                        {
                            if (!region.IsIreland)
                            {
                                search.SearchRequest.LocalityIds.Add(search.SearchRequest.RegionId.Value);
                            }
                        }
                    }
                    else
                    {
                        if (!region.IsIreland)
                        {
                            search.SearchRequest.LocalityIds.Add(search.SearchRequest.RegionId.Value);
                        }
                    }

                    if (section == "holiday-homes")
                    {
                        if (propertyTypeSlug.EndsWith("for-sale"))
                        {
                            search.SearchRequest.PropertyClassIds.Add(region.IsOverseasLocality ?
                                (int)PropertyClassEnum.OverseasForSale :
                                (int)PropertyClassEnum.ResidentialForSale);
                        }
                        else
                        {
                            search.SearchRequest.PropertyClassIds.Add(region.IsOverseasLocality ?
                                (int)PropertyClassEnum.OverseasHolidayHomes :
                                (int)PropertyClassEnum.LettingsHolidayHomes);
                        }
                    }

                    if (search.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.ResidentialForSale) || search.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.Commercial))
                    {
                        if (propertyTypeSlug.StartsWith("sale-agreed"))
                        {
                            search.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.SaleAgreed);
                        }
                        else if (propertyTypeSlug.StartsWith("sold"))
                        {
                            search.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.Sold);
                        }
                    }

                    var genericPropertyTypeSlugs = new List<string> { "holiday-homes-to-rent", "property-for-sale", "property-to-let", "sale-agreed-property", "sold-property", "shared-accommodation", "commercial-property", "property-to-rent", "for-sale", "to-let", "to-rent", "sale-agreed", "sold" };
                    propertyTypeSlug = propertyTypeSlug.Replace(genericPropertyTypeSlugs).Trim('-');

                    if (!string.IsNullOrEmpty(propertyTypeSlug))
                    {
                        if (propertyTypeSlug == "land")
                        {
                            search.SearchRequest.PropertyTypeIds.Add(44); // -- Site
                            search.SearchRequest.PropertyTypeIds.Add(14); // -- Development Land
                            search.SearchRequest.PropertyTypeIds.Add(43); // -- Farm
                            search.SearchRequest.PropertyTypeIds.Add(13); // -- Farm Land
                        }
                        else
                        {
                            var propertyType = _propertyTypeService.Get(new GetPropertyTypeByUrlSlug { ApiKey = ConfigSettings.AppConfig.ApiKey, /*"21C74E36-3D9C-45AE-BB65-7C66EE17178E"*/ CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E", PropertyClassId = search.SearchRequest.PropertyClassIds.FirstOrDefault(), UrlSlug = propertyTypeSlug });
                            //If we dont have any luck with the first property class then try the second, this is mainly for land as it uses two property classes
                            if (propertyType == null && search.SearchRequest.PropertyClassIds.Count >= 2)
                            {
                                propertyType = _propertyTypeService.Get(new GetPropertyTypeByUrlSlug { ApiKey = ConfigSettings.AppConfig.ApiKey, /*"21C74E36-3D9C-45AE-BB65-7C66EE17178E"*/ CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E", PropertyClassId = search.SearchRequest.PropertyClassIds[1], UrlSlug = propertyTypeSlug });
                                }
                            //If the property type is not null then add it to the service
                            if (propertyType != null)
                            {
                                search.SearchRequest.PropertyTypeIds.Add(propertyType.PropertyTypeId);
                            }
                        }
                    }

                    //If we have land and sites we need to ensure that we add the correct status
                    if (IsLandSearch(search) && search.SearchRequest.PropertyStatusIds.Count == 0)
                    {
                        search.SearchRequest.PropertyStatusIds.AddIfNew(2); // -- For Sale
                        search.SearchRequest.PropertyStatusIds.AddIfNew(12); // -- To Let
                    }
                }
                else
                {
                    var propertyClassIsSpecified = (urlSections.Count == 3);
                    var propertyClassSecondaryUrlSlug = propertyClassIsSpecified ? urlSections[1] : "";
                    AddPropertyClass(search, section, propertyClassSecondaryUrlSlug);
                }

                PropertyMapper.ApplyProperties(search.SearchRequest, mappedParameters);

                var irelandLocality = _localityService.GetIrelandLocality(new GetIrelandLocalityRequest
                {
                    ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    ChannelId = 1
                });

                var overseasLocality = _localityService.GetOverseasLocality(new GetOverseasLocalityRequest
                {
                    ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    ChannelId = 1
                });

                //If after mapping we get that the region id is null we set it to ireland
                if (section != "holiday-homes")
                {
                    if (!search.SearchRequest.RegionId.HasValue || search.SearchRequest.RegionId.Value == 0)
                    {
                        search.SearchRequest.RegionId = irelandLocality.LocalityId;
                    }
                }

                if (section == "holiday-homes" && !search.SearchRequest.RegionId.HasValue)
                {
                    switch (search.SearchRequest.PropertyClassIds.FirstOrDefault())
                    {
                        case (int)PropertyClassEnum.LettingsHolidayHomes:
                            search.SearchRequest.RegionId = irelandLocality.LocalityId;
                            break;
                        case (int)PropertyClassEnum.OverseasHolidayHomes:
                        case (int)PropertyClassEnum.OverseasForSale:
                            search.SearchRequest.RegionId = overseasLocality.LocalityId;
                            break;
                    }
                }

                if (!search.SearchRequest.LocalityIds.Any() && search.SearchRequest.RegionId.HasValue)
                {
                    var region = _localityService.Get(new GetLocalityRequest
                    {
                        ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                        CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                        ChannelId = 1, LocalityId = search.SearchRequest.RegionId.Value
                    });
                    if (!region.IsIreland)
                    {
                        search.SearchRequest.LocalityIds.AddIfNew(search.SearchRequest.RegionId.Value);
                    }
                }

                //if (mappedParameters.ContainsKey("ViewType"))
                //{
                //    var viewType = mappedParameters["ViewType"].ToString().ToEnum<ViewType>();
                //    if (viewType.HasValue)
                //        search.ViewType = viewType.Value;
                //}

                if (mappedParameters.ContainsKey("SortColumn"))
                {
                    var sortColumn = StringExtensions.ToEnum<SortColumn>(mappedParameters["SortColumn"].ToString());
                    if (sortColumn.HasValue)
                    {
                        search.SortColumn = sortColumn.Value;
                    }
                }

                if (mappedParameters.ContainsKey("SortDirection"))
                {
                    var sortDirection = StringExtensions.ToEnum<SortDirection>(mappedParameters["SortDirection"].ToString());
                    if (sortDirection.HasValue)
                    {
                        search.SortDirection = sortDirection.Value;
                    }
                }

                if (mappedParameters.ContainsKey("Page"))
                {
                    int page;
                    if (int.TryParse(mappedParameters["Page"].ToString(), out page))
                    {
                        search.Page = page;
                    }
                }

                if (mappedParameters.ContainsKey("GroupIds"))
                {
                    search.SearchRequest.GroupIds.AddRange((IEnumerable<int>)mappedParameters["GroupIds"]);
                }

                if (mappedParameters.ContainsKey("MaxPrice"))
                {
                    search.SearchRequest.MaxPrice = Convert.ToInt32(mappedParameters["MaxPrice"]);
                }

                if (mappedParameters.ContainsKey("MinPrice"))
                {
                    search.SearchRequest.MinPrice = Convert.ToInt32(mappedParameters["MinPrice"]);
                }

                if (mappedParameters.ContainsKey("MinSize"))
                {
                    search.SearchRequest.MinSize = Convert.ToInt32(mappedParameters["MinSize"]);
                }

                if (mappedParameters.ContainsKey("MaxSize"))
                {
                    search.SearchRequest.MaxSize = Convert.ToInt32(mappedParameters["MaxSize"]);
                }

                if (mappedParameters.ContainsKey("NegotiatorIds"))
                {
                    search.SearchRequest.NegotiatorIds.AddRange((IEnumerable<int>)mappedParameters["NegotiatorIds"]);
                }

                if (mappedParameters.ContainsKey("MinEnergyRating"))
                {
                    search.SearchRequest.MinEnergyRating = Convert.ToInt32(mappedParameters["MinEnergyRating"]);
                }

                if (mappedParameters.ContainsKey("Query"))
                {
                    search.SearchRequest.Query = mappedParameters["Query"].ToString();
                }

                if (mappedParameters.ContainsKey("Tags"))
                {
                    var tags =
                        mappedParameters["tags"].ToString()
                            .Split("|")
                            .Where(x => !string.IsNullOrEmpty(x) && x != " " && x != "|")
                            .ToList();

                    search.SearchRequest.Tags.AddRange(tags);
                }

                if (mappedParameters.ContainsKey("PropertyTypeIds"))
                {
                    search.SearchRequest.PropertyTypeIds.AddRange((IEnumerable<int>)mappedParameters["PropertyTypeIds"]);
                }

                if (search.SearchRequest.RegionId.HasValue && !search.SearchRequest.LocalityIds.Any())
                {
                    search.SearchRequest.LocalityIds.Add(search.SearchRequest.RegionId.Value);
                }

                if (mappedParameters.ContainsKey("PropertyStatusIds"))
                {
                    search.SearchRequest.PropertyStatusIds.AddRange((IEnumerable<int>)mappedParameters["PropertyStatusIds"]);
                }

                if (mappedParameters.ContainsKey("datesearchtype"))
                {
                    search.SearchRequest.SearchByDate = mappedParameters["datesearchtype"].ToString();
                }

                if (mappedParameters.ContainsKey("ToDate"))
                {
                    search.SearchRequest.ToDate = Convert.ToDateTime(mappedParameters["ToDate"]);
                }

                if (mappedParameters.ContainsKey("FromDate"))
                {
                    search.SearchRequest.FromDate = Convert.ToDateTime(mappedParameters["FromDate"]);
                }

                return PrepareForSearch(search);
            }
            catch (Exception ex)
            {
                //TODO: Log exception here
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// The preperation method for searches used to normalise the search so all base searches use the default information
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        protected BaseSearchProperties PrepareForSearch(BaseSearchProperties search)
        {
            var request = search.Clone();

            request.SearchRequest.LocalityIds = request.SearchRequest.LocalityIds ?? new List<int>();
            request.SearchRequest.PropertyTypeIds = request.SearchRequest.PropertyTypeIds ?? new List<int>();
            request.SearchRequest.PropertyClassIds = request.SearchRequest.PropertyClassIds ?? new List<int>();
            request.SearchRequest.PropertyStatusIds = request.SearchRequest.PropertyStatusIds ?? new List<int>();
            request.SearchRequest.GroupIds = request.SearchRequest.GroupIds ?? new List<int>();
            request.SearchRequest.SaleTypeIds = request.SearchRequest.SaleTypeIds ?? new List<int>();

            //PrepareForSearch
            if (request.SearchRequest.PropertyStatusIds.Count == 0)
            {
                foreach (var propertyClass in request.SearchRequest.PropertyClassIds)
                {
                    switch (propertyClass)
                    {
                        case (int)PropertyClassEnum.ResidentialForSale:
                        case (int)PropertyClassEnum.ResidentialNewHomes:
                        case (int)PropertyClassEnum.OverseasForSale:
                            request.SearchRequest.PropertyStatusIds.AddIfNew((int)PropertyStatusEnum.ForSale);
                            break;
                        case (int)PropertyClassEnum.OverseasHolidayHomes:
                        case (int)PropertyClassEnum.LettingsToRent:
                        case (int)PropertyClassEnum.LettingsToShare:
                        case (int)PropertyClassEnum.LettingsHolidayHomes:
                            request.SearchRequest.PropertyStatusIds.AddIfNew((int)PropertyStatusEnum.ToLet);
                            break;
                        case (int)PropertyClassEnum.Commercial:
                            request.SearchRequest.PropertyStatusIds.AddIfNew((int)PropertyStatusEnum.ForSale);
                            request.SearchRequest.PropertyStatusIds.AddIfNew((int)PropertyStatusEnum.ForSaleOrToLet);
                            break;
                    }
                }
            }
           
            //We make sure there are no 0's in the search criteria
            request.SearchRequest.LocalityIds = request.SearchRequest.LocalityIds.Where(m => m > 0).ToList();
            request.SearchRequest.PropertyTypeIds = request.SearchRequest.PropertyTypeIds.Where(m => m > 0).ToList();
            request.SearchRequest.PropertyClassIds = request.SearchRequest.PropertyClassIds.Where(m => m > 0).ToList();
            request.SearchRequest.PropertyStatusIds = request.SearchRequest.PropertyStatusIds.Where(m => m > 0).ToList();
            request.SearchRequest.GroupIds = request.SearchRequest.GroupIds.Where(m => m > 0).ToList();
            request.SearchRequest.SaleTypeIds = request.SearchRequest.SaleTypeIds.Where(m => m > 0).ToList();

            if (!request.SearchRequest.RegionId.HasValue && !request.SearchRequest.LocalityIds.Any())
            {
                request.SearchRequest.RegionId = IsOverseas(request) ? 2887 : 2168; //Overseas Or Ireland
            }

            return request;
        }

        public string ClearUrl(string url)
        {
            return ResetPage(ResetSort(RemoveHostname(url)));
        }

        public string RemoveHostname(string url)
        {
            var hostname = url.Match(@"https?://([A-z0-9\.:\-])+/").Single();
            return url.Remove(hostname);
        }

        public string ResetSort(string url)
        {
            if (url.Contains("sort="))
            {
                return url.Replace(url.Match("sort=[a-z]+").Single(), "direction=asc");
            }
            return url;
        }
        public string ResetPage(string url)
        {
            if (url.Contains("page="))
            {
                url = url.Replace(url.Match("page=[0-9]+").Single(), "page=1");
            }
            return url;
        }

        private void AddPropertyClass(BaseSearchProperties search, string section, string propertyClassSecondaryUrlSlug)
        {
            if (section != "holiday-homes" && section != "land")
            {
                var propertyClassId = GetPropertyClassByUrlSlugs(section, propertyClassSecondaryUrlSlug);
                if (propertyClassId.HasValue)
                {
                    search.SearchRequest.PropertyClassIds.Add(propertyClassId.Value);
                }
            }
            if (section == "land")
            {
                search.SearchRequest.PropertyClassIds.Add(1); //Resi
                search.SearchRequest.PropertyClassIds.Add(6); //Commercial
            }
            else
            {
                //-- Rules from SeoUrlGenerator.GetSearchCanonicalUrlPath
                //If we get LettingsHolidayHomes blank the second part of the url
                //If we get OverseasHolidayHomes we make the second section overseas-for-rent
                //If we get OverseasForSale we make the second section overseas-for-sale
                switch (propertyClassSecondaryUrlSlug)
                {
                    //-- We dont set the blank as it upsets the country use of the second slug
                    //case "":
                    //    search.PropertyClasses.Add(PropertyClassEnum.LettingsHolidayHomes);
                    //    break;
                    case "overseas-for-rent":
                        search.SearchRequest.PropertyClassIds.Add((int)PropertyClassEnum.OverseasHolidayHomes);
                        break;
                    case "overseas-for-sale":
                        search.SearchRequest.PropertyClassIds.Add((int)PropertyClassEnum.OverseasForSale);
                        break;
                }
            }
        }

        public int? GetPropertyClassByUrlSlugs(string mainUrlSlug, string secondaryUrlSlug)
        {
            var commercialPropertyTypes = new List<string>
            {
                "office",
                "industrial",
                "business",
                "investment",
                "pub-and-restaurant",
                "hotel-and-b-and-b",
                "retail",
                "farm-land",
                "development-land",
                "parking-space",
                "live-work-unit"
            };

            if (mainUrlSlug == "residential" && string.IsNullOrEmpty(secondaryUrlSlug))
            {
                return (int)PropertyClassEnum.ResidentialForSale;
            }
            if (secondaryUrlSlug == "new-homes")
            {
                return (int)PropertyClassEnum.ResidentialNewHomes;
            }
            if (mainUrlSlug == "rentals")
            {
                if (secondaryUrlSlug == "share")
                {
                    return (int)PropertyClassEnum.LettingsToShare;
                }
                return (int)PropertyClassEnum.LettingsToRent;
            }
            if (mainUrlSlug == "commercial")
            {
                return (int)PropertyClassEnum.Commercial;
            }
            if (mainUrlSlug == "onview" && secondaryUrlSlug.Contains("for-sale"))
            {
                return (int)PropertyClassEnum.ResidentialForSale;
            }
            if (mainUrlSlug == "onview" && secondaryUrlSlug.Contains("to-rent"))
            {
                return (int)PropertyClassEnum.LettingsToRent;
            }
            if (mainUrlSlug == "onview" && secondaryUrlSlug.Contains("commercial"))
            {
                return (int)PropertyClassEnum.Commercial;
            }
            if (mainUrlSlug == "onview" && commercialPropertyTypes.Contains(secondaryUrlSlug))
            {
                return (int)PropertyClassEnum.Commercial;
            }
            if (mainUrlSlug == "holiday-homes")
            {
                //case (int)PropertyClassEnum.LettingsHolidayHomes:
                //case (int)PropertyClassEnum.OverseasForSale:
                //case (int)PropertyClassEnum.OverseasHolidayHomes:
            }
            return null;
        }

        protected bool IsOverseas(BaseSearchProperties search)
        {
            return search.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.OverseasForSale) ||
                   search.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.OverseasHolidayHomes);
        }

        /// <summary>
        /// Determines whether [is seo URL] [the specified search].
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        protected bool IsSeoUrl(BaseSearchProperties search)
        {
            // 1) Had a region id
            // 2) Has less one locality id
            // 3) Has only one property class id, unless it is land and then it is 2
            //If it is a land search we have two property class ids so we ignore the property class id rule
            if (IsLandSearch(search))
            {
                return (search.SearchRequest.RegionId.HasValue && search.SearchRequest.LocalityIds.Count <= 1);
            }
            //If its not a land seaarch we check the 3 rules
            return (search.SearchRequest.RegionId.HasValue && search.SearchRequest.LocalityIds.Count <= 1) &&
                   search.SearchRequest.PropertyClassIds.Count == 1;
        }

        /// <summary>
        /// Determines whether the search request is a land search
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        protected bool IsLandSearch(BaseSearchProperties search)
        {
            var landPropertyTypeIds = new List<int> { 44, 14, 43, 13 };
            //If there are two property classes (1 & 6) and the property type id is in (44,14,43,13)
            if (search.SearchRequest.PropertyClassIds.Count == 2 && search.SearchRequest.PropertyClassIds.Contains(1) &&
                search.SearchRequest.PropertyClassIds.Contains(6))
            {
                //Does the search request only contain property types for land
                return search.SearchRequest.PropertyTypeIds.All(landPropertyTypeIds.Contains);
            }
            return false;
        }

        /// <summary>
        /// Determines whether [is rental search] [the specified search properties].
        /// </summary>
        /// <param name="searchProperties">The search properties.</param>
        /// <returns></returns>
        protected bool IsRentalSearch(BaseSearchProperties searchProperties)
        {
            if (searchProperties.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.LettingsToRent))
            {
                return true;
            }
            if (searchProperties.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.LettingsToShare))
            {
                return true;
            }
            if (searchProperties.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.LettingsHolidayHomes))
            {
                return true;
            }
            if (searchProperties.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.OverseasHolidayHomes))
            {
                return true;
            }
            if (searchProperties.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.ToLet))
            {
                return true;
            }
            return false;
        }
    }
}
