﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using MyHome.v4.Common.Extensions;

namespace MyHome.v4.PropertyManager.Service.Objects
{
    /// <summary>
    /// The paramater map used to decode the search engine friends urls for the applicatin
    /// </summary>
    public class SearchParametersMap : Dictionary<string, object>
    {

        private Dictionary<string, string> ParameterMap
        {
            get
            {
                return new Dictionary<string, string>
                {
                    { "page", "Page" },
                    { "pagesize", "PageSize" },
                    { "region", "RegionId" },
                    { "regionid", "RegionId" },
                    { "minprice", "MinPrice" },
                    { "maxprice", "MaxPrice" },
                    { "minbeds", "MinBeds" },
                    { "maxbeds", "MaxBeds" },
                    { "minsize", "MinSize" },
                    { "maxsize", "MaxSize" },
                    { "minbathrooms", "MinBathrooms" },
                    { "minleasetermindays", "MinLeaseTermInDays" },
                    { "maxleasetermindays", "MaxLeaseTermInDays" },
                    { "query", "Query" },
                    { "view", "ViewType" },
                    { "sort", "SortColumn" },
                    { "direction", "SortDirection" },
                    { "nelat", "NELat" },
                    { "swlat", "SWLat" },
                    { "nelng", "NELng" },
                    { "swlng", "SWLng" },
                    { "isprivatelandlord", "IsPrivateLandlord" },
                    { "amenity", "AmenityID" },
                    { "addedsince", "AddedSinceDaysAgo" },
                    { "hasphotos", "HasPhotos" },
                    { "minenergyrating", "MinEnergyRating" },
                    { "presixtythreeonly", "PreSixtyThree" },
                    { "todate", "ToDate" },
                    { "fromdate", "FromDate" },
                    { "datesearchtype", "SearchByDate" }
                };
            }
        }
        private Dictionary<string, string> IntListParameterMap
        {
            get
            {
                return new Dictionary<string, string>
                {
                   { "localities", "LocalityIds" },
                   { "localityids", "LocalityIds" },
                   { "agent", "GroupIds" },
                   { "type", "PropertyTypeIds"},
                   { "propertytypeids", "PropertyTypeIds"},
                   { "saletypes", "SaleTypeIds"},
                   { "propertyclasses", "PropertyClassIds" },
                   { "propertyclassids", "PropertyClassIds" },
                   { "status", "PropertyStatusIds" },
                   { "negotiator", "NegotiatorIds" }
               };
            }
        }
        private Dictionary<string, string> StringListParameterMap
        {
            get
            {
                return new Dictionary<string, string>
                {
                   { "tags", "Tags" }
              };
            }
        }


        public SearchParametersMap(string url)
        {
            var query = HttpUtility.ParseQueryString(url.Split("?").Last());
            CreateMappedValuesDictionary(query);
        }

        public SearchParametersMap(NameValueCollection values)
        {
            CreateMappedValuesDictionary(values);
        }

        public SearchParametersMap()
        {

        }

        private void CreateMappedValuesDictionary(NameValueCollection dictionary)
        {
            foreach (var key in ParameterMap.Keys)
            {
                if (!string.IsNullOrEmpty(dictionary[key]))
                {
                    this[ParameterMap[key]] = dictionary[key];
                }
            }

            foreach (var key in IntListParameterMap.Keys)
            {
                if (!string.IsNullOrEmpty(dictionary[key]))
                {
                    string[] splittedValues = dictionary[key].Split('|', ',');
                    this[IntListParameterMap[key]] = splittedValues.Where(v => v.IsNumeric()).Select(int.Parse);
                }
            }

            foreach (var key in StringListParameterMap.Keys)
            {
                if (!string.IsNullOrEmpty(dictionary[key]))
                {
                    this[StringListParameterMap[key]] = dictionary[key].Split('|', ',');
                }
            }
        }

    }
}
