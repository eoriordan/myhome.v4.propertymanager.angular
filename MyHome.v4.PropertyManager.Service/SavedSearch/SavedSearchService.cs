﻿using MyHome.v4.PropertyManager.IRepository.SavedSearch;
using MyHome.v4.PropertyManager.IService.SavedSearch;

namespace MyHome.v4.PropertyManager.Service.SavedSearch
{
    public class SavedSearchService : BaseService<Schema.SavedSearch.SavedSearch, Types.SavedSearch.SavedSearch>, ISavedSearchService
    {
        private readonly ISavedSearchRepository _savedSearchRepository;

        public SavedSearchService(ISavedSearchRepository savedSearchRepository) : base(savedSearchRepository)
        {
            _savedSearchRepository = savedSearchRepository;
        }
    }
}
