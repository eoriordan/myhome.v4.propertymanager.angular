﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MyHome.v4.Common.Extensions;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IService.Activity;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.IService.Property;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.Activity;
using MyHome.v4.PropertyManager.Schema.Property;
using MyHome.v4.PropertyManager.Types.Contacts;
using ServiceStack;
using PropertyOffer = MyHome.v4.PropertyManager.Types.Property.PropertyOffer;
using PropertySnippet = MyHome.v4.PropertyManager.Types.Property.PropertySnippet;

namespace MyHome.v4.PropertyManager.Service.Property
{
    public class PropertyOfferService : BaseService<Schema.Property.PropertyOffer, Types.Property.PropertyOffer>, IPropertyOfferService
    {
        private readonly IPropertyOfferRepository _propertyOfferRepository;
        private readonly IContactService _contactService;
        private readonly IUserService _userService;
        private readonly IPropertyRepository _propertyRepository;
        private readonly IActivityService _activityService;

        public PropertyOfferService(
            IPropertyRepository propertyRepository,
            IPropertyOfferRepository propertyOfferRepository,
            IUserService userService,
            IContactService contactService,
            IActivityService activityService) : base(propertyOfferRepository)
        {
            _propertyRepository = propertyRepository;
            _propertyOfferRepository = propertyOfferRepository;
            _userService = userService;
            _contactService = contactService;
            _activityService = activityService;
        }

        public IEnumerable<PropertyOffer> GetOffers(int contactId)
        {
            var dbOffers = _propertyOfferRepository.Select(x => x.ContactId == contactId).ToList();
            if (dbOffers.Any())
            {
                var propertyIds = dbOffers.Select(x => x.PropertyId).Distinct().ToList();
                var userIds = dbOffers.Select(x => x.CreatedByUserId).Distinct().ToList();

                var contact = _contactService.Select(x => x.ContactId == contactId).FirstOrDefault();

                var propertiesById = _propertyRepository
                    .Select(x => propertyIds.Contains(x.PropertyId))
                    .Select(x => x.ConvertTo<Types.Property.Property>())
                    .ToDictionary(x => x.PropertyId);

                var usersById = _userService
                    .Select(x => userIds.Contains(x.UserId))
                    .ToDictionary(x => x.UserId);

                var offers = new Collection<PropertyOffer>();
                foreach (var propertyOffer in dbOffers)
                {
                    var offer = propertyOffer.ConvertTo<PropertyOffer>();
                    offer.Contact = contact;
                    offer.Property = propertiesById[propertyOffer.PropertyId];
                    offer.CreatedByUser = usersById[propertyOffer.CreatedByUserId];

                    offers.Add(offer);
                }

                return offers;
            }

            return Enumerable.Empty<PropertyOffer>();
        }

        public void AddOffer(PropertyOffer offer)
        {
            var schemeOffer = offer.ConvertTo<Schema.Property.PropertyOffer>();
            schemeOffer.CreatedByUserId = offer.CreatedByUser.UserId;
            schemeOffer.ContactId = offer.Contact.ContactId;
            schemeOffer.PropertyId = offer.Property.PropertyId;

            // required to not break V3 property manager.
            schemeOffer.CustomData = new PropertyOfferCustomData
            {
                Price = new Schema.Property.Price
                {
                    MaxPrice = offer.Amount,
                    MinPrice = offer.Amount,
                    ShowPrice = true
                }
            };

            _propertyOfferRepository.Persist(schemeOffer);
        }

        public void UpdateOffer(int userId, int groupId, int propertyOfferId, string note)
        {
            var offer = _propertyOfferRepository.GetById(propertyOfferId);
            if (offer == null) throw new ArgumentException("Offer not found");
            UpdateOffer(userId, groupId, offer, note, offer.StatusId, offer.AcceptedOnDate);
        }

        public void UpdateOffer(int userId, int groupId, int propertyOfferId, string note, int propertyOfferStatus)
        {
            var offer = _propertyOfferRepository.GetById(propertyOfferId);
            if (offer == null) throw new ArgumentException("Offer not found");
            UpdateOffer(userId, groupId, offer, note, propertyOfferStatus, offer.AcceptedOnDate);
        }

        public void UpdateOffer(int userId, int groupId, int propertyOfferId, string note, int propertyOfferStatus, DateTime acceptedOn)
        {
            var offer = _propertyOfferRepository.GetById(propertyOfferId);
            if (offer == null) throw new ArgumentException("Offer not found");
            UpdateOffer(userId, groupId, offer, note, propertyOfferStatus, acceptedOn);
        }

        public void UpdateOffer(int userId, int groupId, int propertyOfferId, string notifyNote, DateTime? timeStamp, int? negotiatorId, int? method)
        {
            var offer = _propertyOfferRepository.GetById(propertyOfferId);
            if (offer == null) throw new ArgumentException("Offer not found");

            offer.NotificationDate = timeStamp;
            offer.NotifiedByUserId = negotiatorId;
            offer.NotificationMethod = method;
            offer.NotificationNotes = notifyNote;

            _propertyOfferRepository.Persist(offer);

            _activityService.LogActivity(GetNotifiedActivity(userId, groupId, offer));
        }

        private void UpdateOffer(int userId, int groupId, Schema.Property.PropertyOffer offer, string note, int propertyOfferStatus, DateTime? acceptedOn)
        {
            offer.Notes = note;
            offer.StatusId = propertyOfferStatus;
            offer.AcceptedOnDate = acceptedOn;
            _propertyOfferRepository.Persist(offer);

            _activityService.LogActivity(GetActivity(userId, groupId, offer));
        }

        private static Types.Activity.Activity GetActivity(int userId, int groupId, Schema.Property.PropertyOffer offer)
        {
            string status;
            switch (offer.StatusId)
            {
                case 2:
                    status = "Withdrawn";
                    break;
                case 3:
                    status = "Accepted";
                    break;
                case 5:
                    status = "Rejected";
                    break;
                default:
                    status = string.Empty;
                    break;
            }

            var title = string.Format(
                "The offer of {0} made by [Contact] on [PropertyAddress] has been {1}.",
                offer.CustomData.Price.MaxPrice.ToCurrency(0),
                status);

            return GetActivity(userId, groupId, offer, title);
        }

        private static Types.Activity.Activity GetNotifiedActivity(int userId, int groupId, Schema.Property.PropertyOffer offer)
        {
            string method;
            switch (offer.Method)
            {
                case 1:
                    method = "Telephone";
                    break;
                case 2:
                    method = "In Person";
                    break;
                case 3:
                    method = "Email";
                    break;
                case 4:
                    method = "Fax";
                    break;
                case 5:
                default:
                    method = "Other";
                    break;
            }

            var title = string.Format(
                "The vendor has been notified by '{0}' of the offer of {1} made by [Contact] on [PropertyAddress].",
                method,
                offer.CustomData.Price.MaxPrice.ToCurrency(0));

            return GetActivity(userId, groupId, offer, title);
        }

        private static Types.Activity.Activity GetActivity(int userId, int groupId, Schema.Property.PropertyOffer offer, string title)
        {
            var activity = new Types.Activity.Activity
            {
                User = new Types.Users.User { UserId = userId, GroupId = groupId },
                ActivityType = ActivityTypeEnum.PropertyCrm,
                CreatedOn = DateTime.Now,
                Property = new PropertySnippet { PropertyId = offer.PropertyId },
                Title = title
            };

            if (offer.ContactId > 0)
            {
                activity.Contacts = new List<Contact>
                {
                    new Contact
                    {
                        ContactId = offer.ContactId
                    }
                };
            }

            return activity;
        }
    }
}
