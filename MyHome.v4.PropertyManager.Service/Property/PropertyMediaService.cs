﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using MyHome.v4.Common.Schema;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.IRepository.JobQueue;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IService.IO;
using MyHome.v4.PropertyManager.IService.Property;
using MyHome.v4.PropertyManager.Schema.JobQueue;
using MyHome.v4.PropertyManager.Service.Azure;

namespace MyHome.v4.PropertyManager.Service.Property
{
    public class PropertyMediaService : IPropertyMediaService
    {
        private readonly IPropertyRepository _propertyRepository;
        private readonly IFileSystemProvider _fileSystemProvider;
        private readonly IJobQueueRepository _jobQueueRepository;
        private readonly IImageUtils _imageUtils;

        public PropertyMediaService(
            IPropertyRepository propertyRepository,
            IFileSystemProvider fileSystemProvider,
            IJobQueueRepository jobQueueRepository,
            IImageUtils imageUtils)
        {
            _propertyRepository = propertyRepository;
            _fileSystemProvider = fileSystemProvider;
            _jobQueueRepository = jobQueueRepository;
            _imageUtils = imageUtils;
        }

        public void UpdatePropertyMedia(int propertyId, string[] updatedMediaCaptions, MediaItemType type)
        {
            var property = _propertyRepository.GetById(propertyId);
            if (property == null)
            {
                throw new ArgumentException(string.Format("No property found with id: {0}", propertyId));
            }

            var media = new Media { Guid = property.Media.Guid };

            foreach (var caption in updatedMediaCaptions)
            {
                var item = property.Media.Items.FirstOrDefault(x => x.Caption.Equals(caption) && x.Type == type);
                if (item != null)
                {
                    media.Items.Add(item);
                }
            }

            // Add all the media of types we don't care about.
            media.Items.AddRange(property.Media.Items.Where(x => x.Type != type));

            var itemsToDelete = property.Media.Items.Where(x => x.Type == type && !updatedMediaCaptions.Contains(x.Caption)).ToList();
            if (itemsToDelete.Any())
            {
                DeleteMediaFiles(property, itemsToDelete);
            }

            property.Media = media;

            _propertyRepository.Persist(property);
        }

        public void AddMedia(int propertyId, MediaItem mediaItem, MemoryStream ms)
        {
            var property = _propertyRepository.GetById(propertyId);
            if (property == null)
            {
                throw new ArgumentException(string.Format("No property found with id: {0}", propertyId));
            }

            //The file name that is passed does not matter, we take the extension, lower case it and then append it to a guid
            var extension = Path.GetExtension(mediaItem.Filename);
            if (!string.IsNullOrEmpty(extension))
            {
                mediaItem.Filename = Guid.NewGuid() + extension.ToLower();
            }
            mediaItem.Filename = ResizeOrImportMedia(mediaItem, property, ms);

            property.Media.AddIfNew(mediaItem);
            _propertyRepository.Persist(property);
        }

        private string ResizeOrImportMedia(MediaItem media, Schema.Property.Property property, MemoryStream ms)
        {
            if (media.Type == MediaItemType.Photo || media.Type == MediaItemType.FloorPlan)
            {
                return Resize(property, media, ms);
            }
            if (media.Type == MediaItemType.PDF)
            {
                return Import(property, media, ms);
            }
            if (media.Type == MediaItemType.VirtualTour)
            {
                return Import(property, media, ms);
            }
            if (media.Type == MediaItemType.Video)
            {
                return Import(property, media, ms);
            }
            throw new NotSupportedException("We currently don't support importing " + media.Type);
        }

        /// <summary>
        /// Resizes from stream.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="item">The item.</param>
        /// <param name="ms">The ms.</param>
        /// <returns></returns>
        private string Resize(Schema.Property.Property property, MediaItem item, MemoryStream ms)
        {
            var originalImage = Image.FromStream(ms);

            if (originalImage.PropertyIdList.Contains(0x0112))
            {
                int rotationValue = originalImage.GetPropertyItem(0x0112).Value[0];
                switch (rotationValue)
                {
                    case 1: // landscape, do nothing
                        break;
                    case 8: // rotated 90 right
                        // de-rotate:
                        originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate270FlipNone);
                        break;

                    case 3: // bottoms up
                        originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate180FlipNone);
                        break;

                    case 6: // rotated 90 left
                        originalImage.RotateFlip(rotateFlipType: RotateFlipType.Rotate90FlipNone);
                        break;
                }
            }

            using (var m = new MemoryStream())
            {
                originalImage.Save(m, ImageFormat.Jpeg);
                byte[] data = m.ToArray();

                item.Filename = Import(property, item, m);
                if (ms.Length > 0)
                {
                    var watermark = string.Format("www.myhome.ie/{0}", property.PropertyId);

                    _imageUtils.Resize(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.Thumbnail)), 36, 36);
                    _imageUtils.Resize(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.Small)), 36, 36);
                    //small
                    _imageUtils.ResizeToFit(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.Thumbnail)), 120, 80, true, null, Color.White); //thumbnail
                    _imageUtils.ResizeToFit(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.HomepageThumbnail)), 176, 132, true, null, Color.White); //homepage thubnail
                    _imageUtils.Crop(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.TopSpot)), 168, 112);
                    //topspot
                    _imageUtils.ResizeToFit(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.Medium)), 372, 279,
                        true, null, Color.White); //medium
                    _imageUtils.Resize(ms, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.Carousel)), 318, 200, true); //carousel
                    _imageUtils.ResizeToFit(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.Gallery)), 400, 300, true, null, Color.White); //gallery
                    _imageUtils.ResizeToFit(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.Large)), 668, 501, false, watermark); //large
                    if (property.PropertyClassId == (int)PropertyClassEnum.ResidentialNewHomes)
                    {
                        _imageUtils.Resize(m, GetPropertyFileName(property, item.Filename, GetImageSuffix(ImageSize.Galleria)), 940, 626, false); //NewHomes Galleria
                    }
                }
            }
            return item.Filename;
        }

        /// <summary>
        /// Imports from stream.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="item">The item.</param>
        /// <param name="ms">The ms.</param>
        /// <returns></returns>
        private string Import(Schema.Property.Property property, MediaItem item, MemoryStream ms)
        {
            if (item.Type == MediaItemType.Video)
            {
                return item.Filename;
            }
            item.Filename = Path.GetFileName(item.Filename);
            if (ms.Length > 0)
            {
                ms.Seek(0, SeekOrigin.Begin);

                var filePath = GetOriginalFilePath(property, item);
                SaveCloudFile(ms, filePath);
            }
            return item.Filename;
        }

        private void DeleteMediaFiles(Schema.Property.Property property, List<MediaItem> items)
        {
            var imagesToDelete = new List<string>();
            foreach (var mediaItem in items)
            {
                imagesToDelete.AddRange(from ImageSize imageSize in Enum.GetValues(typeof(ImageSize)) select GetMediaFilePath(property, mediaItem, imageSize));
            }

            _jobQueueRepository.Persist(new JobQueue
            {
                JobQueuePriorityId = 3,
                CreatedOn = DateTime.Now,
                ExecuteOn = DateTime.Now,
                JobQueueTypeId = 2,
                WorkerExecutor = "ManageMediaWorker",
                JsonData = new ManageMediaJobData
                {
                    MediaToDelete = imagesToDelete
                }.ToJson()
            });
        }

        private string GetMediaFilePath(Schema.Property.Property property, MediaItem item, ImageSize imageSize)
        {
            return Cloud.Combine(GetPropertyImagesPath(property), imageSize == ImageSize.OriginalWithSuffix ? GetOriginalFileName(property, item) : GetImageFilenameWithThumbnailSuffix(item.Filename, GetImageSuffix(imageSize)));
        }

        private string GetPropertyImagesPath(Schema.Property.Property property)
        {
            return Cloud.Combine("media", _imageUtils.GetPartitionPath(property.PropertyId));
        }

        private string GetPropertyFileName(Schema.Property.Property property, string filename, string suffix)
        {
            return Cloud.Combine(GetPropertyImagesPath(property), GetImageFilenameWithThumbnailSuffix(filename, suffix));
        }

        private string GetOriginalFileName(Schema.Property.Property property, MediaItem item)
        {
            var filename = item.Filename;
            if (item.Type == MediaItemType.Photo || item.Type == MediaItemType.FloorPlan)
            {
                var suffix = String.Format("o-{0}", Truncate(property.Media.Guid, 5));
                filename = GetImageFilenameWithThumbnailSuffix(item.Filename, suffix);
            }
            return filename;
        }

        public string GetOriginalFilePath(Schema.Property.Property property, MediaItem item)
        {
            return Cloud.Combine(GetPropertyImagesPath(property), GetOriginalFileName(property, item));
        }

        private string GetImageFilenameWithThumbnailSuffix(string filename, string suffix)
        { 
            //converts 'someimage.jpg' to 'someimage_th.jpg'
            if (String.IsNullOrEmpty(filename))
            {
                return "";
            }
            string[] sections = filename.Split('.');

            if (sections.Length == 1)
            {
                return filename + "_" + suffix;
            }
            var extension = sections[sections.Length - 1];

            var urlWithoutExtension = filename.Substring(0, filename.Length - extension.Length - 1);
            if (!String.IsNullOrEmpty(suffix))
                return String.Format("{0}_{1}.{2}", urlWithoutExtension, suffix, extension);
            return String.Format("{0}.{1}", urlWithoutExtension, extension);
        }

        private string GetImageSuffix(ImageSize imageSize)
        {
            switch (imageSize)
            {
                case ImageSize.Small:
                    return "s";
                case ImageSize.Thumbnail:
                    return "t";
                case ImageSize.HomepageThumbnail:
                    return "h";
                case ImageSize.TopSpot:
                    return "ts";
                case ImageSize.Medium:
                    return "m";
                case ImageSize.Carousel:
                    return "c";
                case ImageSize.Gallery:
                    return "g";
                case ImageSize.Large:
                    return "l";
                case ImageSize.Galleria:
                    return "n";
                case ImageSize.OriginalWithSuffix:
                    return "o-SUFFIX";
                default:
                    return "";
            }
        }

        private string SaveCloudFile(Stream source, string filePath)
        {
            //We dont pass a location as we let the provider work out where to save the file
            _fileSystemProvider.SaveFile(source, filePath, string.Empty, true);
            return filePath;
        }

        private string Truncate(Guid guid, int maxLength)
        {
            var str = guid.ToString();
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }
    }
}
