﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.IRepository.Common;
using MyHome.v4.PropertyManager.IRepository.Groups;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.IService.FeaturedProduct;
using MyHome.v4.PropertyManager.IService.OpenViewing;
using MyHome.v4.PropertyManager.IService.Property;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.Common;
using MyHome.v4.PropertyManager.Schema.Property;
using MyHome.v4.PropertyManager.Types.Groups;
using MyHome.v4.PropertyManager.Types.Property;
using MyHome.v4.PropertyManager.Types.Requests;
using MyHome.v4.PropertyManager.Web.Helpers;
using MyHome.v4.ServiceClient.Locality;
using MyHome.v4.ServiceClient.Locality.Schema;
using MyHome.v4.ServiceClient.PropertyType;
using MyHome.v4.ServiceClient.PropertyType.Schema;
using ServiceStack;
using PropertyClass = MyHome.v4.PropertyManager.Types.Property.PropertyClass;
using PropertyContent = MyHome.v4.PropertyManager.Types.Property.PropertyContent;
using PropertyOffer = MyHome.v4.PropertyManager.Types.Property.PropertyOffer;
using Rooms = MyHome.v4.PropertyManager.Types.Property.Rooms;
using SaleType = MyHome.v4.PropertyManager.Types.Property.SaleType;

namespace MyHome.v4.PropertyManager.Service.Property
{

    /// <summary>
    /// Implementation of property service for access to property data.
    /// </summary>
    public class PropertyService : BaseService<Schema.Property.Property, Types.Property.Property>, IPropertyService
    {
        private readonly ISaleTypeRepository _saleTypeRepository;
        private readonly IPropertyRepository _propertyRepository;
        private readonly IPropertyContentRepository _propertyContentRepository;
        private readonly IPropertyOfferRepository _propertyOfferRepository;
        private readonly IFeaturedProductService _featuredProductService;
        private readonly IGroupRepository _groupRepository;
        private readonly IContactService _contactService;
        private readonly IPropertyContactsService _propertyContactsService;
        private readonly ILocalityService _localityService;
        private readonly IPropertyTypeService _propertyTypeService;
        private readonly IUserService _userService;
        private readonly IOpenViewingService _openViewingService;
        private readonly IIndexQueueRepository _indexQueueRepository;
        private static readonly string ApiKey = ConfigSettings.AppConfig.ApiKey; //System.Configuration.ConfigurationManager.AppSettings["ApiKey"];

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyService" /> class.
        /// </summary>
        /// <param name="propertyRepository">The property repository.</param>
        /// <param name="propertyContentRepository">The property content repository.</param>
        /// <param name="localityService"></param>
        /// <param name="propertyOfferRepository"></param>
        /// <param name="userService"></param>
        /// <param name="propertyTypeService"></param>
        /// <param name="featuredProductService"></param>
        /// <param name="groupRepository"></param>
        /// <param name="contactService"></param>
        /// <param name="indexQueueRepository"></param>
        /// <param name="saleTypeRepository"></param>
        /// <param name="propertyContactsService"></param>
        /// <param name="openViewingService"></param>
        public PropertyService(
            IPropertyRepository propertyRepository,
            IPropertyContentRepository propertyContentRepository,
            ILocalityService localityService,
            IPropertyOfferRepository propertyOfferRepository,
            IUserService userService,
            IPropertyTypeService propertyTypeService,
            IFeaturedProductService featuredProductService,
            IGroupRepository groupRepository,
            IContactService contactService,
            IIndexQueueRepository indexQueueRepository,
            ISaleTypeRepository saleTypeRepository,
            IPropertyContactsService propertyContactsService,
            IOpenViewingService openViewingService) : base(propertyRepository)
        {
            _propertyRepository = propertyRepository;
            _propertyContentRepository = propertyContentRepository;
            _localityService = localityService;
            _propertyOfferRepository = propertyOfferRepository;
            _userService = userService;
            _propertyTypeService = propertyTypeService;
            _featuredProductService = featuredProductService;
            _groupRepository = groupRepository;
            _contactService = contactService;
            _propertyContactsService = propertyContactsService;
            _openViewingService = openViewingService;
            _indexQueueRepository = indexQueueRepository;
            _saleTypeRepository = saleTypeRepository;
        }


        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The request.</param>
        /// <returns>
        /// The property or null if the group was not found
        /// </returns>
        public Types.Property.Property GetProperty(int id)
        {
            var dbProperty = _propertyRepository.GetById(id);
            if (dbProperty != null)
            {
                return Convert(dbProperty);
            }
            return null;
        }

        /// <summary>
        /// Return
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public string GetSimilarPropertiesCacheKey(Types.Property.Property property)
        {
            return String.Format("SimilarProperties|{0}", property.PropertyId);
        }

        /// <summary>
        /// Get properties by ids
        /// </summary>
        /// <param name="ids">The list of unique property identifiers to load</param>
        /// <param name="apiKey">The API key.</param>
        /// <returns>
        /// The list of properties
        /// </returns>
        public IEnumerable<Types.Property.Property> GetByIds(List<int> ids, string apiKey)
        {
            var dbProperties = _propertyRepository.GetByIds(ids).ToList();
            return ConvertMany(dbProperties);
        }


        public void PrepareRequest(BaseSearchProperties request)
        {
            //Ensure the collections are not null so we get no errors -- Probably good to look at why this happens
            request.SearchRequest.LocalityIds = request.SearchRequest.LocalityIds ?? new List<int>();
            request.SearchRequest.PropertyClassIds = request.SearchRequest.PropertyClassIds ?? new List<int>();
            request.SearchRequest.ChannelIds = request.SearchRequest.ChannelIds ?? new List<int>();

            //If the locality is ireland we remove these values and search across the country by class.
            if (request.SearchRequest.RegionId == 2168 || request.SearchRequest.LocalityIds.Any(i => i == 2168))
            {
                request.SearchRequest.LocalityIds = new List<int>();
                request.SearchRequest.RegionId = null;
            }
        }

        /// <summary>
        /// Get properties by ids
        /// </summary>
        /// <param name="request">The search request contaiing the information to search by</param>
        /// <returns>
        /// The list of properties
        /// </returns>
        public PagedList<Types.Property.Property> SearchProperties(BaseSearchProperties request)
        {
            //populate defaults
            PrepareRequest(request);

            //Populate request with default values
            request = PopulateSearchRequest(request);

            //We validate the search property request to ensure that it is valid and we can be processess
            ValidateAndThrow(request);

            //Populate the paged list of items
            var items = new PagedList<Types.Property.Property>();

            //Populate and clone the request with the required values to be able to make the search request
            var searchRequest = PopulateSearchRequest(request);


            //Fire off the request to the server if elastic search fails
            var dbProperties = _propertyRepository.SearchProperties(searchRequest.SearchRequest.CreatePredicate<Schema.Property.Property>(),
                searchRequest.Page,
                searchRequest.PageSize,
                searchRequest.SortColumn,
                searchRequest.SortDirection);

            //Populate the paged list of items
            items.AddRange(ConvertMany(dbProperties));
            items.Page = dbProperties.Page;
            items.PageSize = dbProperties.PageSize;
            items.ResultCount = dbProperties.ResultCount;
            return items;
        }

        public void EnqueueProperty(int propertyId)
        {
            _indexQueueRepository.Persist(new IndexQueue
            {
                CreatedOn = DateTime.Now,
                EntityId = propertyId,
                IndexTypeId = (int)IndexQueueTypeEnum.PropertyIndex // need to update OrmLite to >4.0.54 to support enum <-> int read/write
            });
        }

        public IEnumerable<SaleType> GetSaleTypes(PropertyClassEnum propertyClass)
        {
            var propertyClassId = (int) propertyClass;
            var dbSaleTypes = _saleTypeRepository.Select(x => x.PropertyClassId == propertyClassId).ToList();

            if (!dbSaleTypes.Any())
            {
                return Enumerable.Empty<SaleType>();
            }

            var saleTypes = new Collection<SaleType>();
            foreach (var dbSaleType in dbSaleTypes)
            {
                var saleType = dbSaleType.ConvertTo<SaleType>();
                saleType.PropertyClass = propertyClass;
                saleTypes.Add(saleType);
            }

            return saleTypes;
        }

        public int SaveProperty(Types.Property.Property property)
        {
            var savedProperty = this.Persist(property);

            var existingContentIds = property.Content == null ? Enumerable.Empty<int>() : property.Content
                .Where(x => x.PropertyContentId > 0)
                .Select(x => x.PropertyContentId)
                .ToList();

            var propertyContent = _propertyContentRepository.GetByPropertyId(savedProperty.PropertyId);
            foreach (var content in propertyContent.Where(x => !existingContentIds.Contains(x.PropertyContentId)))
            {
                _propertyContentRepository.Delete(content.PropertyContentId);
            }

            if (property.Content != null)
            {
                foreach (var content in property.Content)
                {
                    content.PropertyId = savedProperty.PropertyId;
                    var dbEntity = content.ConvertTo<Schema.Property.PropertyContent>();
                    dbEntity.PropertyContentSourceTypeId = (int) content.PropertyContentSourceType;
                    _propertyContentRepository.Persist(dbEntity);
                }
            }

            return savedProperty.PropertyId;
        }

        /// <summary>
        /// Validate the search properties request based on ticket MH-2514 and if there is a problem
        /// we throw and exception and allow the services above resolve the problem
        /// </summary>
        /// <param name="request">The request.</param>
        // ReSharper disable once UnusedParameter.Local
        private void ValidateAndThrow(BaseSearchProperties request)
        {
            //- We cant do anything is the search request is null so we check it
            if (request.SearchRequest == null)
            {
                throw new ArgumentNullException("request", "The search request object cannot be null");
            }
            //- If no property class is supplied, throw an argument exception
            if (!request.SearchRequest.PropertyClassIds.Any())
            {
                throw new ArgumentException("The search request must contain at least one property class id", "request");
            }
            //- Ensure a valid page size is passed, 9, 10, 12, 20, 30, 50, 250, 500 else throw and argument exception
            var pageSizes = new List<int> { 9, 10, 12, 20, 30, 50, 250, 500 };
            if (!pageSizes.Contains(request.PageSize))
            {
                throw new ArgumentException("The page size of the request can only be 9, 10, 12, 20, 30, 50, 250, 500");
            }
            //- Ensure the channel ids passed are valid compared to the api key else throw a argument exception
            if (!request.SearchRequest.ChannelIds.Any())
            {
                throw new ArgumentException("The search request must contain at least one channel id");
            }
        }

        /// <summary>
        /// Populates the search request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private BaseSearchProperties PopulateSearchRequest(BaseSearchProperties request)
        {
            //We clone the serarch as we are going to be changing the original request
            var searchRequest = request.Clone();

            //Ensure none of the collections are null so we do not get errors -- We do this as the object is passed over the wire
            searchRequest.SearchRequest.PropertyClassIds = searchRequest.SearchRequest.PropertyClassIds.Any()
                ? searchRequest.SearchRequest.PropertyClassIds : new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            searchRequest.SearchRequest.PropertyStatusIds = searchRequest.SearchRequest.PropertyStatusIds ?? new List<int>();
            searchRequest.SearchRequest.PropertyTypeIds = searchRequest.SearchRequest.PropertyTypeIds ?? new List<int>();
            searchRequest.SearchRequest.GroupIds = searchRequest.SearchRequest.GroupIds ?? new List<int>();
            searchRequest.SearchRequest.LocalityIds = searchRequest.SearchRequest.LocalityIds ?? new List<int>();

            //- If no property status is set, set the property status based on the property class
            if (searchRequest.SearchRequest.PropertyClassIds.Any() && !searchRequest.SearchRequest.PropertyStatusIds.Any())
            {
                //For each property class ensure we have the correct status type
                foreach (var propertyClassId in request.SearchRequest.PropertyClassIds)
                {
                    var propertyClass = (PropertyClassEnum)Enum.ToObject(typeof(PropertyClassEnum), propertyClassId);
                    switch (propertyClass)
                    {
                        case PropertyClassEnum.ResidentialForSale:
                        case PropertyClassEnum.ResidentialNewHomes:
                        case PropertyClassEnum.OverseasForSale:
                            searchRequest.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ForSale);
                            break;
                        case PropertyClassEnum.OverseasHolidayHomes:
                        case PropertyClassEnum.LettingsToRent:
                        case PropertyClassEnum.LettingsToShare:
                        case PropertyClassEnum.LettingsHolidayHomes:
                            searchRequest.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ToLet);
                            break;
                        case PropertyClassEnum.Commercial:
                            searchRequest.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ForSale);
                            searchRequest.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ToLet);
                            searchRequest.SearchRequest.PropertyStatusIds.Add((int)PropertyStatusEnum.ForSaleOrToLet);
                            break;
                    }
                }
                //We ensure that we only allow people to search on a certain list of property status    
                searchRequest.SearchRequest.PropertyStatusIds = searchRequest.SearchRequest.PropertyStatusIds.Intersect(GetValidStatusIds()).ToList();
            }

            //- Expand localities, ensure we are not searching on more then 2050 localities
            if (searchRequest.SearchRequest.LocalityIds.Any())
            {
                searchRequest.SearchRequest.RegionId = null;
                searchRequest.SearchRequest.LocalityIds = _localityService.Expand(new GetExpandToDescendants
                {
                    ChannelId = 1,
                    ApiKey = ApiKey,
                    CorrelationId = ApiKey,
                    LocalityIds = request.SearchRequest.LocalityIds
                });
            }
            else if (request.SearchRequest.RegionId.HasValue)
            {
                var region =
                    _localityService.Get(new GetLocalityRequest
                    {
                        ChannelId = 1,
                        ApiKey = ApiKey,
                        CorrelationId = ApiKey,
                        LocalityId = request.SearchRequest.RegionId.Value
                    });
                searchRequest.SearchRequest.LocalityIds.AddRange(region.SelfAndDescendantIDs);
            }

            searchRequest.SearchRequest.LocalityIds = searchRequest.SearchRequest.LocalityIds.Take(2050).ToList();

            //We can only process a certain about of localities
            searchRequest.SearchRequest.LocalityIds = searchRequest.SearchRequest.LocalityIds.Take(2050).ToList();

            return searchRequest;
        }

        /// <summary>
        /// Gets the valid status ids.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<int> GetValidStatusIds()
        {
            return new List<int>
                {
                    (int) PropertyStatusEnum.ForSale,
                    (int) PropertyStatusEnum.ToLet,
                    (int) PropertyStatusEnum.ForSaleOrToLet,
                    (int) PropertyStatusEnum.SaleAgreed,
                    (int) PropertyStatusEnum.Sold,
                    (int) PropertyStatusEnum.Let,
                    (int) PropertyStatusEnum.Reserved,
                    (int) PropertyStatusEnum.LetAgreed
                };
        }

        /// <summary>
        /// Converts many db properties into service model properties
        /// </summary>
        /// <param name="dbProperties">The database properties.</param>
        /// <returns></returns>
        public new List<Types.Property.Property> ConvertMany(List<Schema.Property.Property> dbProperties)
        {
            //Convert properties
            var properties = dbProperties.Select(m => m.ConvertTo<Types.Property.Property>()).ToList();
            if (properties.Any())
            {
                //Get the property ids and load property content by it
                var propertyIds = properties.Select(m => m.PropertyId).Distinct().ToList();
                //Get the property content from the database
                var dbPropertyContent = _propertyContentRepository.GetByPropertyIds(propertyIds);
                //Get the fatured spots, open viwings, top spots etc.
                var featuredProducts = _featuredProductService.Select(x => propertyIds.Contains(x.PropertyId)).ToList();
                //get the property offers
                var propertyOffersByPropertyId = GetPropertyOffers(propertyIds, properties.ToArray()).ToLookup(x => x.Property.PropertyId);

                //Add the property content for each 
                foreach (var property in properties)
                {
                    //add group
                    property.Group = _groupRepository.GetById(property.GroupId).ConvertTo<Group>();
                    //add content
                    property.Content = dbPropertyContent.Where(m => m.PropertyId == property.PropertyId).Select(ConvertPropertyContent).ToList();
                    //get the offers
                    property.Offers = propertyOffersByPropertyId[property.PropertyId].ToList();
                    
                    //get negotitor
                    if (property.NegotiatorId.HasValue)
                    {
                        property.PropertyPeople.Negotiator = _userService.GetById(property.NegotiatorId.Value);
                    }
                    //add title
                    property.DescriptiveTitle = GetDescriptiveTitle(property);

                    //add rooms
                    var p = dbProperties.SingleOrDefault(x => x.PropertyId == property.PropertyId);
                    if (p != null) property.Rooms = JsonHelper.DeserializeObject<Rooms>(p.Rooms);

                    //add products
                    var propertyProducts = featuredProducts.Where(x => x.PropertyId == property.PropertyId).Select(x => x.ConvertTo<Types.FeaturedProducts.FeaturedProduct>()).ToList();
                    property.FeaturedProducts = propertyProducts;
                    property.OpenViewings = _openViewingService.GetOpenViewings(property.PropertyId).ToList();
                }
            }
            return properties;
        }

        /// <summary>
        /// Converts db propertiy into service model property
        /// </summary>
        /// <param name="dbProperty">The database properties.</param>
        /// <returns></returns>
        public new Types.Property.Property Convert(Schema.Property.Property dbProperty)
        {
            //Convert properties
            var property = dbProperty.ConvertTo<Types.Property.Property>();
            //add group
            property.Group = _groupRepository.GetById(property.GroupId).ConvertTo<Group>();
            //add content
            property.Content = _propertyContentRepository.Select(m => m.PropertyId == property.PropertyId).Select(ConvertPropertyContent).ToList();
            
            //get the offers
            property.Offers = GetPropertyOffers(new List<int> { property.PropertyId }, property).ToList();
            
            //get negotitor
            if (property.NegotiatorId.HasValue)
            {
                property.PropertyPeople.Negotiator = _userService.GetById(property.NegotiatorId.Value);
            }
            //add title
            property.DescriptiveTitle = GetDescriptiveTitle(property);
            //add rooms
            property.Rooms = JsonHelper.DeserializeObject<Rooms>(dbProperty.Rooms);
            //add property people and contacts
            var contacts = _contactService.GetByIds(new List<int>
            {
                dbProperty.ContactId.GetValueOrDefault(),
                dbProperty.VendorSolicitorId.GetValueOrDefault(),
                dbProperty.BuyerId.GetValueOrDefault(),
                dbProperty.BuyerSolicitorId.GetValueOrDefault()
            }).ToList();

            property.PropertyPeople.Vendor = contacts.SingleOrDefault(x => x.ContactId == (property.VendorId.HasValue ? property.VendorId.Value : 0));
            property.PropertyPeople.Buyer = contacts.SingleOrDefault(x => x.ContactId == (property.BuyerId.HasValue ? property.BuyerId.Value : 0));
            property.PropertyPeople.VendorSolicitor = contacts.SingleOrDefault(x => x.ContactId == (property.VendorSolicitorId.HasValue ? property.VendorSolicitorId.Value : 0));
            property.PropertyPeople.BuyerSolicitor = contacts.SingleOrDefault(x => x.ContactId == (property.BuyerSolicitorId.HasValue ? property.BuyerSolicitorId.Value : 0));

            //add other contacts
            property.PropertyPeople.PropertyContactWithContacts =
                _propertyContactsService.GetPropertyContactWithContacts(property.PropertyId);

            //add open viewings
            property.OpenViewings = _openViewingService.GetOpenViewings(property.PropertyId).ToList();

            return property;
        }

        public string GetDescriptiveTitle(Types.Property.Property property)
        {
            var desString = property.BedsDescription;

            if (property.PropertyTypeId.HasValue)
            {
                desString += ", " + _propertyTypeService.Get(new GetPropertyType
                {
                    PropertyTypeId = property.PropertyTypeId.Value,
                    ApiKey = ConfigSettings.AppConfig.ApiKey, //System.Configuration.ConfigurationManager.AppSettings["ApiKey"],
                    CorrelationId = Guid.NewGuid().ToString()
                }).Name;
            }

            if (property.PropertyClassId != null)
            {
                desString += ", " + PropertyClass.GetTitle((PropertyClassEnum)property.PropertyClassId);
            }

            return desString;
        }

        /// <summary>
        /// Converts database property content to the service model property content
        /// </summary>
        /// <param name="dbPropertyContent">Content of the database property.</param>
        /// <returns></returns>
        private static PropertyContent ConvertPropertyContent(Schema.Property.PropertyContent dbPropertyContent)
        {
            var propertyContent = dbPropertyContent.ConvertTo<PropertyContent>();
            propertyContent.PropertyContentSourceType =
                (PropertyContentSourceTypeEnum)
                Enum.ToObject(typeof(PropertyContentSourceTypeEnum), dbPropertyContent.PropertyContentSourceTypeId);
            return propertyContent;
        }

        private IEnumerable<PropertyOffer> GetPropertyOffers(ICollection<int> propertyIds, params Types.Property.Property[] properties)
        {
            var dbPropertyOffers = _propertyOfferRepository.Select(x => propertyIds.Contains(x.PropertyId)).ToList();

            if (dbPropertyOffers.Any())
            {
                var propertiesByIds = properties.ToDictionary(x => x.PropertyId);

                var contactIds = dbPropertyOffers.Select(x => x.ContactId).Distinct().ToList();
                var contactsById = _contactService.Select(x => contactIds.Contains(x.ContactId)).ToDictionary(x => x.ContactId);

                var userIds = dbPropertyOffers.Select(x => x.CreatedByUserId).Distinct().ToList();
                var usersById = _userService.Select(x => userIds.Contains(x.UserId)).ToDictionary(x => x.UserId);

                var offers = new Collection<PropertyOffer>();

                foreach (var dbPropertyOffer in dbPropertyOffers)
                {
                    var offer = dbPropertyOffer.ConvertTo<PropertyOffer>();
                    offer.Property = propertiesByIds[dbPropertyOffer.PropertyId];
                    offer.CreatedByUser = usersById[dbPropertyOffer.CreatedByUserId];
                    offer.Contact = contactsById[dbPropertyOffer.ContactId];

                    offers.Add(offer);
                }

                return offers;
            }

            //var propertyOffer = dbPropertyOffer.ConvertTo<PropertyOffer>();
            //propertyOffer.Property = property;
            //propertyOffer.Contact = _contactService.Select(x => x.ContactId == dbPropertyOffer.ContactId).FirstOrDefault();
            //propertyOffer.CreatedByUser = _userService.Select(x => x.UserId == dbPropertyOffer.CreatedByUserId).FirstOrDefault();

            return Enumerable.Empty<PropertyOffer>();
        }
    }
}
