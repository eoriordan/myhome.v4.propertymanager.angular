﻿using System.Linq;
using MyHome.v4.PropertyManager.IRepository.Auction;
using MyHome.v4.PropertyManager.IService.Auction;
using MyHome.v4.PropertyManager.Schema.Auction;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.Auction
{
    public class AuctionService : BaseService<Schema.Auction.Auction, Types.Auction.Auction>, IAuctionService
    {
        private readonly IAuctionRepository _auctionRepository;
        private readonly IAuctionPropertyRepository _auctionPropertyRepository;

        public AuctionService(IAuctionRepository auctionRepository, IAuctionPropertyRepository auctionPropertyRepository)
            : base(auctionRepository)
        {
            _auctionRepository = auctionRepository;
            _auctionPropertyRepository = auctionPropertyRepository;
        }

        public void SavePropertyAuction(Types.Auction.Auction auction, int propertyId)
        {
            if (auction.AuctionId > 0)
            {
                var existingAuction = _auctionRepository.GetById(auction.AuctionId);
                if (existingAuction != null)
                {
                    var auctionProperties = _auctionPropertyRepository.Select(x => x.AuctionId == existingAuction.AuctionId).ToList();
                    if (auctionProperties.All(x => x.PropertyId != propertyId))
                    {
                        var maxRunningOrder = auctionProperties.Any() ? auctionProperties.Max(x => x.RunningOrder) : 0;

                        _auctionPropertyRepository.Persist(new AuctionProperty
                        {
                            PropertyId = propertyId,
                            AuctionId = existingAuction.AuctionId,
                            RunningOrder = maxRunningOrder + 1
                        });
                    }
                }
            }
            else
            {
                auction.ChannelId = 1;
                var dbAuction = _auctionRepository.Persist(auction.ConvertTo<Schema.Auction.Auction>());
                var ap = new AuctionProperty
                {
                    PropertyId = propertyId,
                    AuctionId = dbAuction.AuctionId,
                    RunningOrder = 1
                };
                _auctionPropertyRepository.Persist(ap);
            }
        }

        public int? GetPropertyAuctionId(int propertyId)
        {
            var auctionProperties = _auctionPropertyRepository.Select(x => x.PropertyId == propertyId).ToList();
            if (auctionProperties.Any())
            {
                return auctionProperties.Max(x => x.AuctionId);
            }
            return null;
        }
    }
}
