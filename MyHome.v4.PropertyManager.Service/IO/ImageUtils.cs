﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using MyHome.v4.PropertyManager.IService.IO;
using MyHome.v4.PropertyManager.Types.IO;

namespace MyHome.v4.PropertyManager.Service.IO
{
    public class ImageUtils : IImageUtils
    {
        private readonly Color _defaultPaddingColor = Color.FromArgb(51, 51, 51);
        private readonly IFileSystemProvider _provider;

        /// <summary>
        /// The image utils which uses the azure connection
        /// </summary>
        /// <param name="provider">The file system provider</param>
        public ImageUtils(IFileSystemProvider provider)
        {
            _provider = provider;
        }


        public void Resize(Stream ms, string targetPath, int width, int height, EncoderParameters encoderParameters, ImageFormat imageFormat)
        {
            Resize(ms, targetPath, width, height, false, null, encoderParameters, imageFormat);
        }

        public void Resize(Stream ms, string targetPath, int width, int height)
        {
            Resize(ms, targetPath, width, height, false, null);
        }

        public void Resize(Stream ms, string targetPath, int width, int height, bool upsizeIfOriginalIsSmaller)
        {
            Resize(ms, targetPath, width, height, upsizeIfOriginalIsSmaller, null);
        }

        public void Resize(Stream ms, string targetPath, int width, int height, bool upsizeIfOriginalIsSmaller, string watermark)
        {
            Resize(ms, targetPath, width, height, upsizeIfOriginalIsSmaller, watermark, GetEncoderParameters());
        }

        public void Resize(Stream ms, string targetPath, int width, int height, bool upsizeIfOriginalIsSmaller, string watermark, EncoderParameters encoderParameters, ImageFormat imageFormat = null)
        {
            ms.Seek(0, SeekOrigin.Begin);
            using (var originalImage = Image.FromStream(ms))
            {
                if (!upsizeIfOriginalIsSmaller)
                {
                    if ((originalImage.Height < height) && (originalImage.Width < width))
                    {
                        height = originalImage.Height;
                        width = originalImage.Width;
                    }
                }

                using (var bitmap = new Bitmap(width, height))
                {
                    using (var graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphics.CompositingQuality = CompositingQuality.HighQuality;
                        graphics.SmoothingMode = SmoothingMode.HighQuality;
                        graphics.DrawImage(originalImage, new Rectangle(0, 0, width, height));
                        if (!String.IsNullOrEmpty(watermark))
                        {
                            WatermarkImage(graphics, watermark);
                        }
                    }
                    if (imageFormat == null)
                    {
                        imageFormat = ImageFormat.Jpeg;
                    }
                    using (var imageStream = new MemoryStream())
                    {
                        bitmap.Save(imageStream, GetEncoder(imageFormat), encoderParameters);
                        SaveToAzure(targetPath, imageStream);
                    }
                }
            }
        }

        /// <summary>
        /// Saves the memory stream straight to azure
        /// </summary>
        /// <param name="targetPath">The target path.</param>
        /// <param name="ms">The ms.</param>
        private void SaveToAzure(string targetPath, MemoryStream ms)
        {
            ms.Seek(0, SeekOrigin.Begin);
            _provider.SaveFile(ms, targetPath, string.Empty);
        }

        private void WatermarkImage(Graphics graphics, string text)
        {
            var font = new Font("arial", 9, FontStyle.Bold);

            var stringFormat = new StringFormat { Alignment = StringAlignment.Far };

            float x = graphics.VisibleClipBounds.Right - 5;
            float y = graphics.VisibleClipBounds.Bottom - 19;

            var semiTransBrush2 = new SolidBrush(Color.FromArgb(153, 0, 0, 0));
            graphics.DrawString(text, font, semiTransBrush2, new PointF(x + 1, y + 1), stringFormat);
            var semiTransBrush = new SolidBrush(Color.FromArgb(153, 255, 255, 255));
            graphics.DrawString(text, font, semiTransBrush, new PointF(x, y), stringFormat);
        }

        public MemoryStream OverlayImage(MemoryStream ms)
        {
            var request = System.Net.WebRequest.Create("http://photos-a.propertyimages.ie/content/oldsiteimages/Static/images/generic/myhomelogo.png");
            var response = request.GetResponse();
            var responseStream = response.GetResponseStream();
            var outputStream = new MemoryStream();
            if (responseStream != null)
            {
                var overlayImageBitmap = new Bitmap(responseStream);

                using (var bitmap = Image.FromStream(ms))
                {
                    using (var graphics = Graphics.FromImage(bitmap))
                    {
                        using (var overlay = overlayImageBitmap)
                        {
                            overlay.SetResolution(graphics.DpiX, graphics.DpiY);

                            const int margin = 2;
                            const int x = margin;

                            var y = bitmap.Height - overlay.Height - margin;
                            try
                            {
                                graphics.DrawImage(overlay, new Point(x, y));
                                bitmap.Save(outputStream, GetEncoder(ImageFormat.Jpeg), GetEncoderParameters());
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                }
            }
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        public void ResizeWithPadding(Stream ms, string targetPath, int imageWidth, int imageHeight, int totalWidth, int totalHeight, string watermark, Color paddingColour)
        {
            ms.Seek(0, SeekOrigin.Begin);
            using (var originalImage = Image.FromStream(ms))
            {
                using (var bitmap = new Bitmap(totalWidth, totalHeight))
                {
                    using (var graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphics.CompositingQuality = CompositingQuality.HighQuality;
                        graphics.SmoothingMode = SmoothingMode.HighQuality;

                        var brush = new SolidBrush(paddingColour);
                        graphics.FillRectangle(brush, 0, 0, totalWidth, totalHeight);

                        var x = (totalWidth - imageWidth) / 2;
                        var y = (totalHeight - imageHeight) / 2;

                        graphics.DrawImage(originalImage, new Rectangle(x, y, imageWidth, imageHeight));

                        if (!String.IsNullOrEmpty(watermark))
                        {
                            WatermarkImage(graphics, watermark);
                        }
                    }
                    using (var imageStream = new MemoryStream())
                    {
                        bitmap.Save(imageStream, GetEncoder(ImageFormat.Jpeg), GetEncoderParameters());
                        SaveToAzure(targetPath, imageStream);
                    }
                }
            }
        }

        public void ResizeToFit(Stream ms, string targetPath, int maxWidth, int maxHeight)
        {
            ResizeToFit(ms, targetPath, maxWidth, maxHeight, false, null);
        }

        public void ResizeToFit(Stream ms, string targetPath, int maxWidth, int maxHeight, bool padImage)
        {
            ResizeToFit(ms, targetPath, maxWidth, maxHeight, padImage, null);
        }

        public void ResizeToFit(Stream ms, string targetPath, int maxWidth, int maxHeight, bool padImage, string watermark)
        {
            ResizeToFit(ms, targetPath, maxWidth, maxHeight, padImage, watermark, _defaultPaddingColor);
        }

        public void ResizeToFit(Stream ms, string targetPath, int maxWidth, int maxHeight, bool padImage, string watermark, Color paddingColour)
        {
            ms.Seek(0, SeekOrigin.Begin);
            using (var originalImage = Image.FromStream(ms))
            {
                var widthRatio = originalImage.Width / (decimal)maxWidth;
                var heightRatio = originalImage.Height / (decimal)maxHeight;
                var ratio = Math.Max(widthRatio, heightRatio);

                var scaleImage = (ratio > 1m); //only downsclae larger images

                if (ratio < 1m)
                    ratio = 1m; //no upscale

                var newWidth = originalImage.Width / ratio;
                var newHeight = originalImage.Height / ratio;

                if (padImage || !scaleImage)
                {
                    ResizeWithPadding(ms, targetPath, (int)Math.Round(newWidth), (int)Math.Round(newHeight), maxWidth, maxHeight, watermark, paddingColour);
                }
                else
                {
                    Resize(ms, targetPath, (int)Math.Round(newWidth), (int)Math.Round(newHeight), true, watermark);
                }
            }
        }

        public void Crop(Stream ms, string targetPath, int width, int height)
        {
            Crop(ms, targetPath, width, height, AnchorPosition.Center);
        }

        public void Crop(Stream ms, string targetPath, int width, int height, AnchorPosition anchor)
        {
            ms.Seek(0, SeekOrigin.Begin);
            using (var originalImage = Image.FromStream(ms))
            {
                var sourceWidth = originalImage.Width;
                var sourceHeight = originalImage.Height;
                const int sourceX = 0;
                const int sourceY = 0;
                var destX = 0;
                var destY = 0;

                float nPercent;

                var nPercentW = (width / (float)sourceWidth);
                var nPercentH = (height / (float)sourceHeight);

                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentW;
                    switch (anchor)
                    {
                        case AnchorPosition.Top:
                            destY = 0;
                            break;
                        case AnchorPosition.Bottom:
                            destY = (int)(height - (sourceHeight * nPercent));
                            break;
                        default:
                            destY = (int)((height - (sourceHeight * nPercent)) / 2);
                            break;
                    }
                }
                else
                {
                    nPercent = nPercentH;
                    switch (anchor)
                    {
                        case AnchorPosition.Left:
                            destX = 0;
                            break;
                        case AnchorPosition.Right:
                            destX = (int)(width - (sourceWidth * nPercent));
                            break;
                        default:
                            destX = (int)((width - (sourceWidth * nPercent)) / 2);
                            break;
                    }
                }

                var destWidth = (int)(sourceWidth * nPercent);
                var destHeight = (int)(sourceHeight * nPercent);

                using (var bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb))
                {
                    bitmap.SetResolution(originalImage.HorizontalResolution, originalImage.VerticalResolution);

                    using (var graphics = Graphics.FromImage(bitmap))
                    {
                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        graphics.DrawImage(originalImage,
                            new Rectangle(destX - 1, destY - 1, destWidth + 1, destHeight + 1),
                            new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                            GraphicsUnit.Pixel);
                    }

                    using (var imageStream = new MemoryStream())
                    {
                        bitmap.Save(imageStream, GetEncoder(ImageFormat.Jpeg), GetEncoderParameters());
                        SaveToAzure(targetPath, imageStream);
                    }
                }
            }
        }
        public string GetPartitionPath(int number, string pathSeperator = @"\")
        { //returns '3\7\5\341573' for '341573'
            string numberAsString = number.ToString(CultureInfo.InvariantCulture).PadLeft(3, '0');

            return String.Format(@"{1}{0}{2}{0}{3}{0}{4}",
                pathSeperator,
                numberAsString.Substring(numberAsString.Length - 1, 1),
                numberAsString.Substring(numberAsString.Length - 2, 1),
                numberAsString.Substring(numberAsString.Length - 3, 1),
                numberAsString);
        }

        public EncoderParameters GetEncoderParameters(long quality = 80L)
        {
            var encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = new EncoderParameter(Encoder.Quality, quality);
            return encoderParams;
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            return ImageCodecInfo.GetImageDecoders().FirstOrDefault(codec => codec.FormatID == format.Guid);
        }

    }
}
