﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository.Activity;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IService.Activity;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.Activity;
using MyHome.v4.PropertyManager.Types.Property;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.Activity
{
    public class ActivityService : BaseService<Schema.Activity.Activity, Types.Activity.Activity>, IActivityService
    {
        private readonly IActivityRepository _activityRepository;
        private readonly IActivityContactRepository _activityContactRepository;
        private readonly IUserService _userService;
        private readonly IContactService _contactsService;
        private readonly IPropertyRepository _propertyRepository;

        public ActivityService(
            IActivityRepository activityRepository,
            IActivityContactRepository activityContactRepository,
            IUserService userService,
            IContactService contactsService,
            IPropertyRepository propertyRepository) : base(activityRepository)
        {
            _activityRepository = activityRepository;
            _activityContactRepository = activityContactRepository;
            _userService = userService;
            _contactsService = contactsService;
            _propertyRepository = propertyRepository;
        }

        public IEnumerable<Types.Activity.Activity> GetPropertyActivity(int propertyId)
        {
            var dbActivities = _activityRepository.Select(x => x.PropertyId == propertyId).ToList();
            if (!dbActivities.Any())
            {
                return Enumerable.Empty<Types.Activity.Activity>();
            }

            var activityIds = dbActivities.Select(x => x.ActivityId).Distinct().ToList();

            var activityContacts = _activityContactRepository.Select(x => activityIds.Contains(x.ActivityId)).ToList();
            var contactsByActivity = activityContacts.ToLookup(x => x.ActivityId);
            var activityContactIds = activityContacts.Select(x => x.ContactId).ToList();

            var contactIds = dbActivities.Where(x => x.Attachments != null)
                .SelectMany(x => x.Attachments.Contacts).Select(x => x.ContactId)
                .Concat(dbActivities.Select(x => x.ContactId.GetValueOrDefault()))
                .Concat(activityContactIds)
                .Distinct()
                .ToList();

            return PopulateActivities(dbActivities, contactIds, null, contactsByActivity);
        }

        public IEnumerable<Types.Activity.Activity> GetContactActivity(int contactId)
        {
            var contactActivities = _activityContactRepository.Select(x => x.ContactId == contactId).Select(x => x.ActivityId).ToList();
            var dbActivities = _activityRepository.Select(x => x.ContactId == contactId)
                .Concat(_activityRepository.Select(x => contactActivities.Contains(x.ActivityId))).Distinct().ToList();

            if (!dbActivities.Any())
            {
                return Enumerable.Empty<Types.Activity.Activity>();
            }

            var contactIds = dbActivities
                .SelectMany(x => x.Attachments.Contacts).Select(x => x.ContactId)
                .Concat(dbActivities.Select(x => x.ContactId.GetValueOrDefault()))
                .Concat(contactActivities)
                .Distinct()
                .ToList();

            return PopulateActivities(dbActivities, contactIds, contactId, null);
        }

        public void LogActivity(Types.Activity.Activity activity)
        {
            var dbActivity = activity.ConvertTo<Schema.Activity.Activity>();
            dbActivity.GroupId = activity.User.GroupId;
            dbActivity.UserId = activity.User.UserId;
            dbActivity.ActivityTypeId = (int)activity.ActivityType;
            dbActivity.Attachments = new ActivityAttachments();

            if (activity.Property != null)
            {
                dbActivity.PropertyId = activity.Property.PropertyId;
                dbActivity.LocalityId = activity.Property.LocalityId;
                dbActivity.PropertyClassId = activity.Property.PropertyClassId;
                dbActivity.Attachments.Properties.Add(activity.Property.ConvertTo<Schema.Property.PropertySnippet>());
            }

            var savedActivity = this._activityRepository.Persist(dbActivity);

            if (activity.Contacts != null && activity.Contacts.Any())
            {
                var activityContacts = activity.Contacts.Select(x => new ActivityContact
                {
                    ContactId = x.ContactId,
                    ActivityId = savedActivity.ActivityId
                }).ToList();

                this._activityContactRepository.PersistMany(activityContacts);
            }
        }

        private IEnumerable<Types.Activity.Activity> PopulateActivities(List<Schema.Activity.Activity> dbActivities, ICollection<int> contactIds, int? contactId, ILookup<int, ActivityContact> contactsByActivity)
        {
            var userIds = dbActivities.Select(x => x.UserId).Distinct().ToList();
            var usersById = _userService.Select(x => userIds.Contains(x.UserId)).ToDictionary(x => x.UserId);

            var propertyIds = dbActivities.Where(x => x.PropertyId.HasValue).Select(x => x.PropertyId.Value)
                .Distinct()
                .Concat(dbActivities.Where(x => x.Attachments != null).SelectMany(x => x.Attachments.Properties).Select(x => x.PropertyId).Distinct().ToList());

            var propertiesById = _propertyRepository.Select(x => propertyIds.Contains(x.PropertyId)).ToDictionary(x => x.PropertyId);

            var contactsById = _contactsService.Select(x => contactIds.Contains(x.ContactId)).ToDictionary(x => x.ContactId);

            var activities = new Collection<Types.Activity.Activity>();

            foreach (var dbActivity in dbActivities)
            {
                var activity = dbActivity.ConvertTo<Types.Activity.Activity>();
                activity.ActivityType = (ActivityTypeEnum) dbActivity.ActivityTypeId;
                activity.Contact = dbActivity.ContactId.HasValue && contactsById.ContainsKey(dbActivity.ContactId.Value) ? contactsById[dbActivity.ContactId.Value] : null;

                if (dbActivity.UserId.HasValue)
                {
                    activity.User = usersById[dbActivity.UserId.Value];
                }

                if (dbActivity.PropertyId.HasValue)
                {
                    var property = propertiesById[dbActivity.PropertyId.Value];
                    if (property != null)
                    {
                        activity.Property = property.ConvertTo<PropertySnippet>();
                    }
                }

                if (dbActivity.Attachments != null)
                {
                    activity.Documents = dbActivity.Attachments.Documents;
                    activity.Events = dbActivity.Attachments.Events;

                    var activityPropertyIds = dbActivity.Attachments.Properties.Select(x => x.PropertyId).ToList();
                    activity.Properties = propertiesById
                        .Where(x => activityPropertyIds.Contains(x.Key))
                        .Select(y => y.Value.ConvertTo<PropertySnippet>())
                        .ToList();

                    var activityContactIds = dbActivity.Attachments.Contacts
                        .Where(x => x.ContactId != contactId.GetValueOrDefault())
                        .Select(x => x.ContactId)
                        .ToList();

                    if (contactsByActivity != null && contactsByActivity.Contains(activity.ActivityId))
                    {
                        activityContactIds = activityContactIds.Concat(contactsByActivity[activity.ActivityId].Select(x => x.ContactId)).ToList();
                    }

                    activity.Contacts = contactsById.Where(x => activityContactIds.Contains(x.Key))
                        .Select(y => y.Value)
                        .ToList();
                }

                activities.Add(activity);
            }
            return activities;
        }
    }
}
