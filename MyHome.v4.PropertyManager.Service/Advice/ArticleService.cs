﻿using System.Collections.Generic;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository.Advice;
using MyHome.v4.PropertyManager.IService.Advice;
using MyHome.v4.PropertyManager.Types.Advice;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.Advice
{
    public class ArticleService : BaseService<Schema.Advice.Article, Article>, IArticleService
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IArticleCategoryMappingRepository _articleCategoryMappingRepository;
        private readonly ICategoryService _categoryService;

        public ArticleService(
            IArticleRepository repo,
            ICategoryService categoryService,
            IArticleCategoryMappingRepository articleCategoryMappingRepository) : base(repo)
        {
            _articleRepository = repo;
            _categoryService = categoryService;
            _articleCategoryMappingRepository = articleCategoryMappingRepository;
        }

        public IEnumerable<Article> GetArticlesByCategory(int categoryId, int page, int pageSize)
        {
            return ConvertMany(_articleRepository.GetByCategory(categoryId, page, pageSize).ToList());
        }

        public IEnumerable<Article> SearchArticles(string searchString)
        {
            return ConvertMany(_articleRepository.SearchArticles(searchString).ToList());
        }

        public IEnumerable<Category> PopulateCategories(int articleId)
        {
            var ids = _articleCategoryMappingRepository.GetByArticleId(articleId);
            if (ids.Any())
            {
                var categories = _categoryService.GetByIds(ids.Select(x => x.CategoryId).ToList());
                var enumerable = categories as Category[] ?? categories.ToArray();
                if (enumerable.Any())
                {
                    return enumerable.Select(x => x.ConvertTo<Category>()).ToList();
                }
            }
            return new List<Category>();
        }
    }
}
