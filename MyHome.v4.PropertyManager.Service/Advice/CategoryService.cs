﻿using MyHome.v4.PropertyManager.IRepository.Advice;
using MyHome.v4.PropertyManager.IService.Advice;
using MyHome.v4.PropertyManager.Types.Advice;

namespace MyHome.v4.PropertyManager.Service.Advice
{
    public class CategoryService : BaseService<Schema.Advice.Category, Category>, ICategoryService
    {
        public CategoryService(ICategoryRepository repo) : base(repo)
        {
        }
    }
}
