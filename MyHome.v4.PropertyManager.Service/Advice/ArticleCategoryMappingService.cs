﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.IRepository.Advice;
using MyHome.v4.PropertyManager.IService.Advice;
using MyHome.v4.PropertyManager.Types.Advice;

namespace MyHome.v4.PropertyManager.Service.Advice
{
    public class ArticleCategoryMappingService : BaseService<Schema.Advice.ArticleCategoryMapping, ArticleCategoryMapping>, IArticleCategoryMappingService
    {
        private readonly IArticleCategoryMappingRepository _articleCategoryMappingRepository;

        public ArticleCategoryMappingService(IArticleCategoryMappingRepository articleCategoryMappingRepository) : base(articleCategoryMappingRepository)
        {
            _articleCategoryMappingRepository = articleCategoryMappingRepository;
        }

        public IEnumerable<ArticleCategoryMapping> GetByArticleId(int articleId)
        {
            return ConvertMany(_articleCategoryMappingRepository.GetByArticleId(articleId));
        }
    }
}
