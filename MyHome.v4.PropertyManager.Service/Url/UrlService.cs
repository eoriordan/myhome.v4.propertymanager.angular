﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using MyHome.v4.Common.Extensions;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.IService.Advice;
using MyHome.v4.PropertyManager.IService.Url;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Types.Advice;
using MyHome.v4.PropertyManager.Types.Requests;
using MyHome.v4.PropertyManager.Web.Helpers;
using MyHome.v4.ServiceClient.Locality;
using MyHome.v4.ServiceClient.Locality.Schema;
using MyHome.v4.ServiceClient.PropertyType;
using MyHome.v4.ServiceClient.PropertyType.Schema;
using Nest.Resolvers;
using SortDirection = MyHome.v4.PropertyManager.Schema.SortDirection;

namespace MyHome.v4.PropertyManager.Service.Url
{
    public class UrlService : IUrlService
    {
        private readonly ICategoryService _categoryService;
        private readonly ILocalityService _localityService;
        private readonly IPropertyTypeService _propertyTypeService;

        public UrlService(
            ICategoryService categoryService, ILocalityService localityService, IPropertyTypeService propertyTypeService)
        {
            _categoryService = categoryService;
            _localityService = localityService;
            _propertyTypeService = propertyTypeService;
        }

        /// <summary>
        /// Generates the search path. The api key and correlation id are taken from the search request
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        public string GenerateSearchPath(BaseSearchProperties search)
        {
            AssignRegionIdIfNull(search);
            var request = PrepareForSearch(search);

            var urlParameters = GetUrlParameters(request);
            if (string.IsNullOrEmpty(urlParameters))
            {
                return GetSearchCanonicalUrlPath(request).ToLower();
            }
            return GetSearchCanonicalUrlPath(request).ToLower() + "?" + urlParameters;
        }

        public string GetSearchCanonicalUrlPath(BaseSearchProperties search)
        {
            string propertyClassSecondaryUrlSlug;
            LocalityService.Api.ServiceModel.Types.Locality locality = null;

            //The SEO for holiday homes only works for singlar localities, if we have mutiple localities we revert back to the standard results

            if (search.SearchRequest.PropertyClassIds.Any()
                && !IsSeoUrl(search)
                && (!IsHolidayHomes(search.SearchRequest.PropertyClassIds) || search.SearchRequest.LocalityIds.Any()))
            {
                if (search.SearchRequest.PropertyClassIds.Count != 1)
                {
                    return string.Format("/{0}/results", PropertyClassUrlSlug(search.SearchRequest.PropertyClassIds.First()));
                }
                propertyClassSecondaryUrlSlug = PropertyClassSecondaryUrlSlug(search.SearchRequest.PropertyClassIds.First());
                propertyClassSecondaryUrlSlug = OverseasSecondaryUrlSlug(search.SearchRequest.PropertyClassIds, propertyClassSecondaryUrlSlug);

                //Create the url with one or two segments based on the if the second part of the url has text 
                return string.IsNullOrEmpty(propertyClassSecondaryUrlSlug)
                    ? string.Format("/{0}/results", PropertyClassUrlSlug(search.SearchRequest.PropertyClassIds.First()))
                    : string.Format("/{0}/{1}/results", PropertyClassUrlSlug(search.SearchRequest.PropertyClassIds.First()), propertyClassSecondaryUrlSlug);
            }

            var regionSlug = GetRegionSlug(search, ref locality);
            var propertyTypeIdentifier = GetPropertyTypeIdentifier(search);
            var hasSinglePropertyTypeIdentifier = search.SearchRequest.PropertyTypeIds.Count == 1;

            //I doubt this works correctly -- This is a method I created and is not the same as v3
            propertyClassSecondaryUrlSlug = PropertyClassSecondaryUrlSlug(search.SearchRequest.PropertyClassIds.FirstOrDefault(), search.SearchRequest.PropertyStatusIds.FirstOrDefault());

            var propertySlug = "";

            switch (search.SearchRequest.PropertyClassIds.First())
            {
                case (int)PropertyClassEnum.ResidentialForSale:
                case (int)PropertyClassEnum.ResidentialNewHomes:
                    if (search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.SaleAgreed))
                    {
                        propertySlug = hasSinglePropertyTypeIdentifier
                            ? "sale-agreed-" + propertyTypeIdentifier
                            : "sale-agreed-property";
                    }
                    else if (search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.Sold))
                    {
                        propertySlug = hasSinglePropertyTypeIdentifier
                            ? "sold-" + propertyTypeIdentifier
                            : "sold-property";
                    }
                    else
                    {
                        propertySlug = IsLandSearch(search) || hasSinglePropertyTypeIdentifier
                            ? propertyTypeIdentifier + "-for-sale"
                            : "property-for-sale";
                    }
                    break;
                case (int)PropertyClassEnum.LettingsToRent:
                    propertySlug += hasSinglePropertyTypeIdentifier
                        ? propertyTypeIdentifier
                        : "property";
                    propertySlug += "-to-rent";
                    break;
                case (int)PropertyClassEnum.LettingsToShare:
                    propertySlug += hasSinglePropertyTypeIdentifier
                        ? propertyTypeIdentifier + "-"
                        : string.Empty;
                    propertySlug += "shared-accommodation";
                    break;
                case (int)PropertyClassEnum.LettingsHolidayHomes:
                    propertySlug = hasSinglePropertyTypeIdentifier
                        ? propertyTypeIdentifier + "-to-rent"
                        : "property-to-rent";
                    break;
                case (int)PropertyClassEnum.Commercial:
                    if (search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.SaleAgreed) && search.SearchRequest.PropertyStatusIds.Count == 1)
                    {
                        propertySlug = hasSinglePropertyTypeIdentifier
                            ? "sale-agreed-" + propertyTypeIdentifier
                            : "sale-agreed";
                    }
                    else if (search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.Sold) && search.SearchRequest.PropertyStatusIds.Count == 1)
                    {
                        propertySlug = hasSinglePropertyTypeIdentifier
                            ? "sold-" + propertyTypeIdentifier
                            : "sold-property";
                    }
                    else if (search.SearchRequest.PropertyStatusIds.Any())
                    {
                        if (search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.ToLet) && search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.ForSale))
                        {
                            // ForSale and ToLet
                            propertySlug = hasSinglePropertyTypeIdentifier
                                ? propertyTypeIdentifier
                                : "commercial-property";

                        }
                        else if (!search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.ToLet))
                        {
                            //for sale ONLY may contain ForSale or ForSaleToLet
                            propertySlug = hasSinglePropertyTypeIdentifier
                                ? propertyTypeIdentifier + "-for-sale"
                                : "property-for-sale";
                        }
                        else if (!search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.ForSale))
                        {
                            //To Let ONLY may contain To Let or ForSaleToLet
                            propertySlug = hasSinglePropertyTypeIdentifier
                                ? propertyTypeIdentifier + "-to-rent"
                                : "property-to-rent";
                        }
                        else
                        {
                            propertySlug = hasSinglePropertyTypeIdentifier
                                ? propertyTypeIdentifier + "-for-sale-to-rent"
                                : "commercial-property";
                        }
                    }
                    else if (IsLandSearch(search) || hasSinglePropertyTypeIdentifier)
                    {
                        propertySlug = propertyTypeIdentifier;
                    }
                    else
                    {
                        propertySlug = "commercial-property";
                    }
                    break;
                case (int)PropertyClassEnum.OverseasForSale:
                    propertySlug = hasSinglePropertyTypeIdentifier
                            ? propertyTypeIdentifier + "-for-sale"
                            : "property-for-sale";
                    break;
                case (int)PropertyClassEnum.OverseasHolidayHomes:
                    propertySlug = hasSinglePropertyTypeIdentifier
                            ? propertyTypeIdentifier + "-to-rent"
                            : "property-to-rent";
                    break;
            }

            var localitySlug = (locality != null && !locality.IsTopLevelOverseasOrIreland && !locality.IsTopLevelRegion) ? "-in-" + locality.UrlSlug : "";

            if (!string.IsNullOrEmpty(propertyClassSecondaryUrlSlug) && !(IsHolidayHomes(search.SearchRequest.PropertyClassIds) && propertyClassSecondaryUrlSlug == "holiday-homes"))
            {
                regionSlug = string.Format("{0}/{1}", regionSlug, propertyClassSecondaryUrlSlug);
            }

            var propertyClassSlug = PropertyClassUrlSlug(search.SearchRequest.PropertyClassIds.First());

            //We have to add a hack to deal with land
            if (IsLandSearch(search))
            {
                propertyClassSlug = "land";
            }

            string url = string.IsNullOrEmpty(localitySlug) ?
                string.Format("/{0}/{1}/{2}", propertyClassSlug, regionSlug, propertySlug) :
                string.Format("/{0}/{1}/{2}{3}", propertyClassSlug, regionSlug, propertySlug, localitySlug);

            return url;
        }


        private string GetPropertyTypeIdentifier(BaseSearchProperties search)
        {

            if (search.SearchRequest.PropertyTypeIds.Count == 1)
            {
                var propertyType = _propertyTypeService.Get(new GetPropertyType
                {
                    ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    PropertyTypeId = search.SearchRequest.PropertyTypeIds.First()
                });
                return propertyType.UrlSlug;
            }

            if (IsLandSearch(search) && search.SearchRequest.PropertyTypeIds.Count == 4)
            {
                return "land";
            }
            return string.Empty;
        }

        private string GetRegionSlug(BaseSearchProperties search, ref LocalityService.Api.ServiceModel.Types.Locality locality)
        {
            switch (search.SearchRequest.LocalityIds.Count)
            {
                case 0:
                    if (search.SearchRequest.RegionId.HasValue) // assign locality to region
                    {
                        var regionLocality = _localityService.Get(new GetLocalityRequest
                        {
                            ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                            CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                            ChannelId = 1,
                            LocalityId = search.SearchRequest.RegionId.Value
                        });
                        return regionLocality.UrlSlug;
                    }

                    if (IsOverseas(search)) // get overseas locality
                    {
                        var overseasLocality = _localityService.GetOverseasLocality(new GetOverseasLocalityRequest
                        {
                            ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                            CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                            ChannelId = 1
                        });
                        return overseasLocality.UrlSlug;
                    }

                    //default to Ireland
                    var irelandLocality = _localityService.GetIrelandLocality(new GetIrelandLocalityRequest
                    {
                        ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                        CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                        ChannelId = 1
                    });
                    return irelandLocality.UrlSlug;
                case 1:
                    try
                    {
                        locality = _localityService.Get(new GetLocalityRequest
                        {
                            ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                            CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                            ChannelId = 1,
                            LocalityId = search.SearchRequest.LocalityIds.First()
                        });
                        return IsOverseas(search) ? locality.TopLevelRegion.UrlSlug : locality.Region.UrlSlug;
                    }
                    catch (Exception)
                    {
                        return string.Empty;
                    }
            }
            return string.Empty;
        }

        private string OverseasSecondaryUrlSlug(IEnumerable<int> propertyClassIds, string secondaryUrlSlug)
        {
            //-- The redirection for this is set in SeoUrlParser.AddPropertyClass
            //If we get LettingsHolidayHomes blank the second part of the url
            //If we get OverseasHolidayHomes we make the second section overseas-for-rent
            //If we get OverseasForSale we make the second section overseas-for-sale
            switch (propertyClassIds.First())
            {
                case (int)PropertyClassEnum.LettingsHolidayHomes:
                    return string.Empty;

                case (int)PropertyClassEnum.OverseasHolidayHomes:
                    return "overseas-for-rent";

                case (int)PropertyClassEnum.OverseasForSale:
                    return "overseas-for-sale";
            }
            return secondaryUrlSlug;
        }

        private string PropertyClassSecondaryUrlSlug(int propertyClassId, int? propertyStatusId = null)
        {
            //1	ResidentialForSale	residential	NULL
            //2	ResidentialNewHomes	residential	new-homes
            //3	LettingsToRent	lettings	NULL
            //4	LettingsToShare	lettings	share
            //5	LettingsHolidayHomes	lettings	holiday-homes
            //6	Commercial	commercial	NULL
            //9	OverseasForSale	overseas	NULL
            //10 OverseasHolidayHomes	overseas	holiday-homes
            switch (propertyClassId)
            {
                case (int)PropertyClassEnum.ResidentialForSale:
                    return string.Empty;
                case (int)PropertyClassEnum.ResidentialNewHomes:
                    return "new-homes";
                case (int)PropertyClassEnum.LettingsToRent:
                    return string.Empty;
                case (int)PropertyClassEnum.LettingsToShare:
                    return "share";
                case (int)PropertyClassEnum.LettingsHolidayHomes:
                    return "holiday-homes";
                case (int)PropertyClassEnum.Commercial:
                    return string.Empty;
                //    return propertyStatusId != null && propertyStatusId == (int)PropertyStatusEnum.ToLet
                //        ? "to-rent"
                //        : string.Empty;
                //case 7:
                //    return "overseas";
                //case 8:
                //    return "overseas";
                case (int)PropertyClassEnum.OverseasForSale:
                    return string.Empty;
                case (int)PropertyClassEnum.OverseasHolidayHomes:
                    return "holiday-homes";
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets the URL parameters.
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        private string GetUrlParameters(BaseSearchProperties search)
        {

            var urlParams = new StringBuilder();

            if (!IsSeoUrl(search))
            {
                urlParams.Append(FormatListParam("localities", search.SearchRequest.LocalityIds));
                if (search.SearchRequest.RegionId.HasValue)
                {
                    urlParams.Append(String.Format("&region={0}", search.SearchRequest.RegionId.Value));
                }
            }

            urlParams.Append(FormatListParam("agent", search.SearchRequest.GroupIds));

            //If it is a land search we have to deal with 4 property types so we check if its a land search and if we have 4 property types
            var appendTypes = search.SearchRequest.PropertyTypeIds.Count > 1 || !IsSeoUrl(search);
            if (IsLandSearch(search) && search.SearchRequest.PropertyTypeIds.Count == 4)
            {
                appendTypes = false;
            }
            if (appendTypes)
            {
                urlParams.Append(FormatListParam("type", search.SearchRequest.PropertyTypeIds));
            }

            if (!IsSeoPropertyStatus(search))
            {
                urlParams.Append(FormatListParam("status", search.SearchRequest.PropertyStatusIds));
            }

            if (search.Page > 1)
            {
                urlParams.Append(String.Format("&page={0}", search.Page));
            }

            if (search.SearchRequest.MinEnergyRating.HasValue)
            {
                urlParams.Append(String.Format("&minenergyrating={0}", search.SearchRequest.MinEnergyRating.Value));
            }

            if (search.SearchRequest.PreSixtyThree.HasValue && search.SearchRequest.PreSixtyThree == true)
            {
                urlParams.Append(String.Format("&presixtythreeonly={0}", true));
            }

            if (search.SearchRequest.MinPrice.HasValue)
            {
                urlParams.Append(string.Format("&minprice={0}", search.SearchRequest.MinPrice));
            }
            if (search.SearchRequest.MaxPrice.HasValue)
            {
                urlParams.Append(string.Format("&maxprice={0}", search.SearchRequest.MaxPrice));
            }

            if (search.SearchRequest.MinBeds.HasValue)
            {
                urlParams.Append(string.Format("&minbeds={0}", search.SearchRequest.MinBeds));
            }
            if (search.SearchRequest.MaxBeds.HasValue)
            {
                urlParams.Append(string.Format("&maxbeds={0}", search.SearchRequest.MaxBeds));
            }

            if (search.SearchRequest.MinBathrooms.HasValue)
            {
                urlParams.Append(string.Format("&minbathrooms={0}", search.SearchRequest.MinBathrooms));
            }

            if (search.SearchRequest.MinSize.HasValue)
                urlParams.Append(string.Format("&minsize={0}", search.SearchRequest.MinSize));
            if (search.SearchRequest.MaxSize.HasValue)
                urlParams.Append(string.Format("&maxsize={0}", search.SearchRequest.MaxSize));

            if (search.SearchRequest.MinLeaseTermInDays.HasValue && search.SearchRequest.MinLeaseTermInDays > 0)
            {
                urlParams.Append(string.Format("&minleasetermindays={0}", search.SearchRequest.MinLeaseTermInDays));
            }
            if (search.SearchRequest.MaxLeaseTermInDays.HasValue && search.SearchRequest.MinLeaseTermInDays > 0)
            {
                urlParams.Append(string.Format("&maxleasetermindays={0}", search.SearchRequest.MaxLeaseTermInDays));
            }

            if (!string.IsNullOrEmpty(search.SearchRequest.Query))
            {
                urlParams.Append(string.Format("&query={0}", HttpUtility.UrlEncode(search.SearchRequest.Query)));
            }

            if (search.SearchRequest.NegotiatorIds.Any())
            {
                urlParams.Append(FormatListParam("negotiator", search.SearchRequest.NegotiatorIds));
            }

            if (search.SearchRequest.SaleTypeIds.Any())
            {
                urlParams.Append(FormatListParam("saletypes", search.SearchRequest.SaleTypeIds));
            }

            if (search.SearchRequest.Tags.Any())
            {
                urlParams.Append(FormatListParam("tags", search.SearchRequest.Tags));
            }

            if (!string.IsNullOrEmpty(search.SearchRequest.SearchByDate))
            {
                urlParams.Append(string.Format("&datesearchtype={0}", HttpUtility.UrlEncode(search.SearchRequest.SearchByDate)));
            }

            if (search.SearchRequest.FromDate.HasValue)
            {
                urlParams.Append(string.Format("&fromdate={0}", HttpUtility.UrlEncode(search.SearchRequest.FromDate.Value.ToString("O"))));
            }

            if (search.SearchRequest.ToDate.HasValue)
            {
                urlParams.Append(string.Format("&todate={0}", HttpUtility.UrlEncode(search.SearchRequest.ToDate.Value.ToString("O"))));
            }


            return urlParams.ToString().TrimStart('&');
        }

        /// <summary>
        /// Formats the list parameter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name.</param>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        private string FormatListParam<T>(string name, List<T> list)
        {
            return list != null && list.Any() ? String.Format("&{0}={1}", name, list.Join("|")) : "";
        }

        private bool IsHolidayHomes(ICollection<int> propertyClassIds)
        {
            return propertyClassIds.Count == 1 &&
                      (propertyClassIds.Contains((int)PropertyClassEnum.LettingsHolidayHomes) ||
                       propertyClassIds.Contains((int)PropertyClassEnum.OverseasHolidayHomes) ||
                       propertyClassIds.Contains((int)PropertyClassEnum.OverseasForSale));
        }

        public bool IsSeoPropertyStatus(BaseSearchProperties search)
        {
            if (search.SearchRequest.PropertyStatusIds.Count == 1)
            {
                var propertyStatusId = search.SearchRequest.PropertyStatusIds.First();
                var propertyStatusIds = new List<int> { (int)PropertyStatusEnum.ForSale, (int)PropertyStatusEnum.ToLet, (int)PropertyStatusEnum.SaleAgreed, (int)PropertyStatusEnum.Sold };
                if (propertyStatusIds.Contains(propertyStatusId))
                {
                    return true;
                }
            }
            return search.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.ForSaleOrToLet);
        }

        public string RemoveHostname(string url)
        {
            var hostname = url.Match(@"https?://([A-z0-9\.:\-])+/").Single();
            return url.Remove(hostname);
        }

        public string ResetSort(string url)
        {
            if (url.Contains("sort="))
            {
                return url.Replace(url.Match("sort=[a-z]+").Single(), "direction=asc");
            }
            return url;
        }
        public string ResetPage(string url)
        {
            if (url.Contains("page="))
            {
                url = url.Replace(url.Match("page=[0-9]+").Single(), "page=1");
            }
            return url;
        }
        public string ClearUrl(string url)
        {
            return ResetPage(ResetSort(RemoveHostname(url)));
        }

        private void AddPropertyClass(BaseSearchProperties search, string section, string propertyClassSecondaryUrlSlug)
        {
            if (section != "holiday-homes" && section != "land")
            {
                var propertyClassId = GetPropertyClassByUrlSlugs(section, propertyClassSecondaryUrlSlug);
                if (propertyClassId.HasValue)
                {
                    search.SearchRequest.PropertyClassIds.Add(propertyClassId.Value);
                }
            }
            if (section == "land")
            {
                search.SearchRequest.PropertyClassIds.Add(1); //Resi
                search.SearchRequest.PropertyClassIds.Add(6); //Commercial
            }
            else
            {
                //-- Rules from SeoUrlGenerator.GetSearchCanonicalUrlPath
                //If we get LettingsHolidayHomes blank the second part of the url
                //If we get OverseasHolidayHomes we make the second section overseas-for-rent
                //If we get OverseasForSale we make the second section overseas-for-sale
                switch (propertyClassSecondaryUrlSlug)
                {
                    //-- We dont set the blank as it upsets the country use of the second slug
                    //case "":
                    //    search.PropertyClasses.Add(PropertyClassEnum.LettingsHolidayHomes);
                    //    break;
                    case "overseas-for-rent":
                        search.SearchRequest.PropertyClassIds.Add((int)PropertyClassEnum.OverseasHolidayHomes);
                        break;
                    case "overseas-for-sale":
                        search.SearchRequest.PropertyClassIds.Add((int)PropertyClassEnum.OverseasForSale);
                        break;
                }
            }
        }

        public int? GetPropertyClassByUrlSlugs(string mainUrlSlug, string secondaryUrlSlug)
        {
            var commercialPropertyTypes = new List<string>
            {
                "office",
                "industrial",
                "business",
                "investment",
                "pub-and-restaurant",
                "hotel-and-b-and-b",
                "retail",
                "farm-land",
                "development-land",
                "parking-space",
                "live-work-unit"
            };

            if (mainUrlSlug == "residential" && string.IsNullOrEmpty(secondaryUrlSlug))
            {
                return (int)PropertyClassEnum.ResidentialForSale;
            }
            if (secondaryUrlSlug == "new-homes")
            {
                return (int)PropertyClassEnum.ResidentialNewHomes;
            }
            if (mainUrlSlug == "rentals")
            {
                if (secondaryUrlSlug == "share")
                {
                    return (int)PropertyClassEnum.LettingsToShare;
                }
                return (int)PropertyClassEnum.LettingsToRent;
            }
            if (mainUrlSlug == "commercial")
            {
                return (int)PropertyClassEnum.Commercial;
            }
            if (mainUrlSlug == "onview" && secondaryUrlSlug.Contains("for-sale"))
            {
                return (int)PropertyClassEnum.ResidentialForSale;
            }
            if (mainUrlSlug == "onview" && secondaryUrlSlug.Contains("to-rent"))
            {
                return (int)PropertyClassEnum.LettingsToRent;
            }
            if (mainUrlSlug == "onview" && secondaryUrlSlug.Contains("commercial"))
            {
                return (int)PropertyClassEnum.Commercial;
            }
            if (mainUrlSlug == "onview" && commercialPropertyTypes.Contains(secondaryUrlSlug))
            {
                return (int)PropertyClassEnum.Commercial;
            }
            if (mainUrlSlug == "holiday-homes")
            {
                //case (int)PropertyClassEnum.LettingsHolidayHomes:
                //case (int)PropertyClassEnum.OverseasForSale:
                //case (int)PropertyClassEnum.OverseasHolidayHomes:
            }
            return null;
        }

        public Dictionary<string, string> GetReplacementValues(BaseSearchProperties request)
        {
            var values = new Dictionary<string, string>();

            var searchRequest = PrepareForSeoLinkGeneration(request);


            values = values.Merge(GetPropertyTypeValues(searchRequest));

            //sale type (for sale, sale agreed, to rent, to share)
            values["SaleType"] = GetSaleTypeDescriptor(searchRequest);

            if (searchRequest.SearchRequest.LocalityIds.Count == 1)
            {

                var locality = _localityService.Get(new GetLocalityRequest
                {
                    ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    ChannelId = 1,
                    LocalityId = searchRequest.SearchRequest.LocalityIds.First()
                });

                values["Locality"] = locality.DisplayName;
                values["LocalityAndRegion"] = locality.NameAndRegion;
                values["LocalitySelectionDescription"] = "in " + locality.NameAndRegion;
                values["AreaGuideLink"] = string.Format("<a href='{0}'>{1} property</a>", locality.AreaGuideUrl, locality.DisplayName);

                if (locality.IsIreland || locality.IsOverseas)
                {
                    values["LocalityHeading"] = "All";
                }
                else
                {
                    values["LocalityHeading"] = locality.DisplayName;
                }

                //Nearby search was removed as it is just not used in agent web sites for the moment
            }
            else
            {
                if (searchRequest.SearchRequest.LocalityIds.Count > 1)
                {
                    values["LocalityHeading"] = string.Format("{0} selected", searchRequest.SearchRequest.LocalityIds.Count);
                    if (searchRequest.SearchRequest.LocalityIds.Count <= 3)
                    {
                        var localityList = searchRequest.SearchRequest.LocalityIds.Select(m => GetLocalityDisplayName(searchRequest, m)).ToList().Join(", ", " or ", "");
                        values["LocalitySelectionDescription"] = string.Format("in {0}", localityList);
                    }
                    else
                    {
                        var localityList = searchRequest.SearchRequest.LocalityIds.Select(m => GetLocalityDisplayName(searchRequest, m)).OrderBy(m => m);
                        values["LocalitySelectionDescription"] = string.Format("in {0} (+{1} locations)", localityList.First(), (searchRequest.SearchRequest.LocalityIds.Count - 1));
                    }
                }
                else
                {
                    values["LocalityHeading"] = "All";

                    if (IsOverseas(searchRequest))
                    {
                        values["LocalitySelectionDescription"] = "Overseas";
                    }
                    else
                    {
                        values["LocalitySelectionDescription"] = "in Ireland";
                    }
                }
            }


            return values;
        }

        /// <summary>
        /// Gets the display name of the locality.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="localityId">The locality identifier.</param>
        /// <returns></returns>
        public string GetLocalityDisplayName(BaseSearchProperties request, int localityId)
        {
            var locality = _localityService.Get(new GetLocalityRequest
            {
                ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                ChannelId = 1,
                LocalityId = localityId
            });
            return locality.DisplayName;
        }

        /// <summary>
        /// Gets the property type values.
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        private Dictionary<string, string> GetPropertyTypeValues(BaseSearchProperties search)
        {
            var values = new Dictionary<string, string>();
            var clone = search.Clone();

            string propertyTypeSingular = "property";
            string propertyTypePlural = "properties";
            values["PropertyTypeForH1"] = propertyTypeSingular;

            if (search.SearchRequest.PropertyClassIds.Any())
            {
                if (search.SearchRequest.PropertyClassIds.First() == (int)PropertyClassEnum.ResidentialNewHomes)
                {
                    propertyTypeSingular = "new home";
                    propertyTypePlural = "new homes";
                    values["PropertyTypeForH1"] = propertyTypePlural;
                }
                else if (search.SearchRequest.PropertyClassIds.First() == (int)PropertyClassEnum.LettingsHolidayHomes)
                {
                    propertyTypeSingular = "holiday home";
                    propertyTypePlural = "holiday homes";
                    values["PropertyTypeForH1"] = propertyTypePlural;
                }
            }

            if (clone.SearchRequest.PropertyTypeIds.Count == 1)
            {
                var propertyType = _propertyTypeService.Get(new GetPropertyType
                {
                    ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                    PropertyTypeId = clone.SearchRequest.PropertyTypeIds.First()
                });

                propertyTypeSingular = propertyType.DisplayName.ToLower();
                propertyTypePlural = Inflector.MakePlural(propertyTypeSingular);
                values["PropertyTypeForH1"] = propertyTypePlural;
            }

            values["PropertyTypeSingular"] = propertyTypeSingular;
            values["PropertyTypePlural"] = propertyTypePlural;
            values["PropertyTypeSingularWithIndefiniteArticle"] = TextHelper.ApplyIndefiniteArticle(propertyTypeSingular);
            return values;
        }

        /// <summary>
        /// Gets the sale type descriptor.
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        private string GetSaleTypeDescriptor(BaseSearchProperties search)
        {
            switch (search.SearchRequest.PropertyClassIds.FirstOrDefault())
            {
                case (int)PropertyClassEnum.ResidentialForSale:
                case (int)PropertyClassEnum.ResidentialNewHomes:
                    switch (search.SearchRequest.PropertyStatusIds.FirstOrDefault())
                    {
                        case (int)PropertyStatusEnum.SaleAgreed:
                            return "sale agreed";
                        case (int)PropertyStatusEnum.Sold:
                            return "sold";
                        default:
                            return "for sale";
                    }
                case (int)PropertyClassEnum.LettingsToRent:
                case (int)PropertyClassEnum.LettingsHolidayHomes:
                    return "to rent";
                case (int)PropertyClassEnum.LettingsToShare:
                    return "to share";
                case (int)PropertyClassEnum.Commercial:

                    if (IsRentalSearch(search))
                    {
                        return "to rent";
                    }
                    return "for sale";
                case (int)PropertyClassEnum.OverseasForSale:
                    return "for sale";
                case (int)PropertyClassEnum.OverseasHolidayHomes:
                    return "to rent";
                default:
                    return "";
            }
        }

        protected string PropertyClassUrlSlug(int propertyClassId)
        {
            //1	ResidentialForSale	residential	NULL
            //2	ResidentialNewHomes	residential	new-homes
            //3	LettingsToRent	lettings	NULL
            //4	LettingsToShare	lettings	share
            //5	LettingsHolidayHomes	lettings	holiday-homes
            //6	Commercial	commercial	NULL
            //9	OverseasForSale	overseas	NULL
            //10 OverseasHolidayHomes	overseas	holiday-homes
            switch (propertyClassId)
            {
                case (int)PropertyClassEnum.ResidentialForSale:
                    return "residential";
                case (int)PropertyClassEnum.ResidentialNewHomes:
                    return "residential";
                case (int)PropertyClassEnum.LettingsToRent:
                    return "rentals"; //"lettings";
                case (int)PropertyClassEnum.LettingsToShare:
                    return "rentals"; //"lettings";
                case (int)PropertyClassEnum.LettingsHolidayHomes:
                    return "holiday-homes"; //"lettings"
                case 6:
                    return "commercial";
                //case 7:
                //    return "overseas";
                //case 8:
                //    return "overseas";
                case (int)PropertyClassEnum.OverseasForSale:
                    return "holiday-homes"; //"overseas"
                case (int)PropertyClassEnum.OverseasHolidayHomes:
                    return "holiday-homes"; //"overseas"
            }
            return string.Empty;
        }

        /// <summary>
        /// The preperation method for searches used to normalise the search so all base searches use the default information
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        protected BaseSearchProperties PrepareForSearch(BaseSearchProperties search)
        {
            var request = search.Clone();

            request.SearchRequest.LocalityIds = request.SearchRequest.LocalityIds ?? new List<int>();
            request.SearchRequest.PropertyTypeIds = request.SearchRequest.PropertyTypeIds ?? new List<int>();
            request.SearchRequest.PropertyClassIds = request.SearchRequest.PropertyClassIds ?? new List<int>();
            request.SearchRequest.PropertyStatusIds = request.SearchRequest.PropertyStatusIds ?? new List<int>();
            request.SearchRequest.GroupIds = request.SearchRequest.GroupIds ?? new List<int>();
            request.SearchRequest.SaleTypeIds = request.SearchRequest.SaleTypeIds ?? new List<int>();

            //PrepareForSearch
            if (request.SearchRequest.PropertyStatusIds.Count == 0)
            {
                foreach (var propertyClass in request.SearchRequest.PropertyClassIds)
                {
                    switch (propertyClass)
                    {
                        case (int)PropertyClassEnum.ResidentialForSale:
                        case (int)PropertyClassEnum.ResidentialNewHomes:
                        case (int)PropertyClassEnum.OverseasForSale:
                            request.SearchRequest.PropertyStatusIds.AddIfNew((int)PropertyStatusEnum.ForSale);
                            break;
                        case (int)PropertyClassEnum.OverseasHolidayHomes:
                        case (int)PropertyClassEnum.LettingsToRent:
                        case (int)PropertyClassEnum.LettingsToShare:
                        case (int)PropertyClassEnum.LettingsHolidayHomes:
                            request.SearchRequest.PropertyStatusIds.AddIfNew((int)PropertyStatusEnum.ToLet);
                            break;
                        case (int)PropertyClassEnum.Commercial:
                            request.SearchRequest.PropertyStatusIds.AddIfNew((int)PropertyStatusEnum.ForSale);
                            request.SearchRequest.PropertyStatusIds.AddIfNew((int)PropertyStatusEnum.ForSaleOrToLet);
                            break;
                    }
                }
            }
        
            //We make sure there are no 0's in the search criteria
            request.SearchRequest.LocalityIds = request.SearchRequest.LocalityIds.Where(m => m > 0).ToList();
            request.SearchRequest.PropertyTypeIds = request.SearchRequest.PropertyTypeIds.Where(m => m > 0).ToList();
            request.SearchRequest.PropertyClassIds = request.SearchRequest.PropertyClassIds.Where(m => m > 0).ToList();
            request.SearchRequest.PropertyStatusIds = request.SearchRequest.PropertyStatusIds.Where(m => m > 0).ToList();
            request.SearchRequest.GroupIds = request.SearchRequest.GroupIds.Where(m => m > 0).ToList();
            request.SearchRequest.SaleTypeIds = request.SearchRequest.SaleTypeIds.Where(m => m > 0).ToList();

            if (!request.SearchRequest.RegionId.HasValue && !request.SearchRequest.LocalityIds.Any())
            {
                request.SearchRequest.RegionId = IsOverseas(request) ? 2887 : 2168; //Overseas Or Ireland
            }

            return request;
        }

        /// <summary>
        /// Determines whether the specified search is overseas.
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        protected bool IsOverseas(BaseSearchProperties search)
        {
            return search.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.OverseasForSale) ||
                   search.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.OverseasHolidayHomes);
        }

        /// <summary>
        /// Determines whether [is seo URL] [the specified search].
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        protected bool IsSeoUrl(BaseSearchProperties search)
        {
            // 1) Had a region id
            // 2) Has less one locality id
            // 3) Has only one property class id, unless it is land and then it is 2
            //If it is a land search we have two property class ids so we ignore the property class id rule
            if (IsLandSearch(search))
            {
                return (search.SearchRequest.RegionId.HasValue && search.SearchRequest.LocalityIds.Count <= 1);
            }
            //If its not a land seaarch we check the 3 rules
            return (search.SearchRequest.RegionId.HasValue && search.SearchRequest.LocalityIds.Count <= 1) &&
                   search.SearchRequest.PropertyClassIds.Count == 1;
        }

        /// <summary>
        /// Determines whether the search request is a land search
        /// </summary>
        /// <param name="search">The search.</param>
        /// <returns></returns>
        protected bool IsLandSearch(BaseSearchProperties search)
        {
            var landPropertyTypeIds = new List<int> { 44, 14, 43, 13 };
            //If there are two property classes (1 & 6) and the property type id is in (44,14,43,13)
            if (search.SearchRequest.PropertyClassIds.Count == 2 && search.SearchRequest.PropertyClassIds.Contains(1) &&
                search.SearchRequest.PropertyClassIds.Contains(6))
            {
                //Does the search request only contain property types for land
                return search.SearchRequest.PropertyTypeIds.All(landPropertyTypeIds.Contains);
            }
            return false;
        }

        /// <summary>
        /// Determines whether [is rental search] [the specified search properties].
        /// </summary>
        /// <param name="searchProperties">The search properties.</param>
        /// <returns></returns>
        protected bool IsRentalSearch(BaseSearchProperties searchProperties)
        {
            if (searchProperties.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.LettingsToRent))
            {
                return true;
            }
            if (searchProperties.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.LettingsToShare))
            {
                return true;
            }
            if (searchProperties.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.LettingsHolidayHomes))
            {
                return true;
            }
            if (searchProperties.SearchRequest.PropertyClassIds.Contains((int)PropertyClassEnum.OverseasHolidayHomes))
            {
                return true;
            }
            if (searchProperties.SearchRequest.PropertyStatusIds.Contains((int)PropertyStatusEnum.ToLet))
            {
                return true;
            }
            return false;
        }


        private BaseSearchProperties PrepareForSeoLinkGeneration(BaseSearchProperties request)
        {
            AssignRegionIdIfNull(request);
            var searchRequest = PrepareForSearch(request);

            searchRequest.Page = 1;
            searchRequest.SortColumn = SortColumn.RefreshedDate;
            searchRequest.SortDirection = SortDirection.Desc;

            //clone.AmenityID = null; ?? -- Probaby not needed

            if (searchRequest.SearchRequest.GroupIds.Any())
            {
                searchRequest.SearchRequest.GroupIds.Clear();
            }

            searchRequest.SearchRequest.Query = string.Empty;

            //clone.IsHolidayHomes = IsHolidayHomes; ?? -- Probably not needed

            return searchRequest;
        }

        private void AssignRegionIdIfNull(BaseSearchProperties request)
        {
            if (request.SearchRequest.RegionId == null && !IsOverseas(request))
            {
                if (request.SearchRequest.LocalityIds.Count() == 1)
                {
                    request.SearchRequest.RegionId = _localityService.Get(new GetLocalityRequest()
                    {
                        ApiKey = ConfigSettings.AppConfig.ApiKey, //"21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                        CorrelationId = "21C74E36-3D9C-45AE-BB65-7C66EE17178E",
                        ChannelId = 1, // default channel id to MyHome.ie for Locality searches
                        LocalityId = request.SearchRequest.LocalityIds.FirstOrDefault()
                    }).MainParent.LocalityId;
                }
                else
                {
                    request.SearchRequest.RegionId = 2168; //Ireland
                }
            }
        }


        public string GenerateArticleUrl(Article article, List<int> categoryIds)
        {
            if (article == null)
                return "";

            var categories = _categoryService.GetByIds(categoryIds);

            var articleTitleSlug = article.Title.ToSlug();

            var articleCategoryString = "";

            foreach (var category in categories)
            {
                articleCategoryString += "/" + category.UrlSlug;
            }

            return "/advice"
                   + (!string.IsNullOrEmpty(articleCategoryString) ? articleCategoryString : "")
                   + "/" + articleTitleSlug
                   + "/" + article.Id + "/"
                   + (categoryIds != null && categoryIds.Any() ? string.Join("-", categoryIds) : "");
        }
    }
}
