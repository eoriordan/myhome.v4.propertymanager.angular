﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using MyHome.v4.Common.Extensions;
using MyHome.v4.PropertyManager.IService.IO;
using MyHome.v4.PropertyManager.Web.Helpers;

namespace MyHome.v4.PropertyManager.Service.Azure
{
    /// <summary>
    /// Azure file system provider.
    /// Note: Azure does not support hierarchal file systems. You can however name files 'content/images/image1.png'
    /// to mock a hierarchy. The location paramater in these methods is the azure container name. The file name can 
    /// then be set in a hierarchal way as above, or simply as one string. The container can only contain
    /// letters numbers or the dash character.
    /// http://msdn.microsoft.com/en-us/library/dd135715.aspx
    /// </summary>
    public class AzureFileSystemProvider : BaseFileSystemProvider, IFileSystemProvider
    {
        private readonly string _connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureFileSystemProvider"/> class.
        /// </summary>
        public AzureFileSystemProvider()
        {
            _connectionString = _connectionString ?? ConfigSettings.AppConfig.AzureStorageSan;
        }

        /// <summary>
        /// Gets the CloudStorageAccount using our Azure account settings.
        /// </summary>
        /// <param name="connection">The Azure Account settings.</param>
        /// <returns>A CloudStorageAccount, with which we can retrieve a client.</returns>
        protected CloudStorageAccount GetStorageAccount(string connection)
        {
            return CloudStorageAccount.Parse(connection);
        }

        /// <summary>
        /// Gets the base url for the current azure connection
        /// </summary>
        /// <returns>A string of the base url for azure</returns>
        public string GetBaseStorageUrl()
        {
            return GetStorageAccount(_connectionString).BlobEndpoint.ToString();
        }

        /// <summary>
        /// Gets the Azure Client
        /// </summary>
        /// <param name="storageAccount">The cloud storage account instanciated with our connection settings.</param>
        /// <returns> A CloudBlobClient with which we can retieve containers.</returns>
        protected CloudBlobClient GetClient(CloudStorageAccount storageAccount)
        {
            return storageAccount.CreateCloudBlobClient();
        }


        /// <summary>
        /// Connectes to Azure, authenticates, sets up a blob client and returns the container (directory) to work with.
        /// This will create the container if it does not exist.
        /// </summary>
        /// <param name="containerName">The name of the container to work with.</param>
        /// <returns>A CloudBlobContainer</returns>
        protected CloudBlobContainer GetContainer(string containerName)
        {
            // Retrieve storage account from connection string.
            var storageAccount = GetStorageAccount(_connectionString);

            // Create the blob client.
            var blobClient = GetClient(storageAccount);

            // Retrieve a reference to a container. 
            var container = blobClient.GetContainerReference(containerName);

            // Create the container if it doesn't already exist.
            //container.CreateIfNotExists();

            return container;
        }

        /// <summary>
        /// Gets a container and gets a reference to a file within that container
        /// </summary>
        /// <param name="fileName">The filename.</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns></returns>
        public CloudBlockBlob GetBlob(string fileName, string location)
        {
            //Create a container and return a reference to a file.
            var container = GetContainer(location);
            container.CreateIfNotExists();
            return container.GetBlockBlobReference(fileName);
        }


        /// <summary>
        /// Save a file into Azure.
        /// </summary>
        /// <param name="stream">The file stream</param>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <param name="skipChecks">if set to <c>true</c> [skip checks].</param>
        public string SaveFile(Stream stream, string fileName, string location, bool skipChecks = false)
        {
            //Generally if we are uploading to temp we want to skip the checks
            if (skipChecks)
            {
                if (!IsAllowedFileType(Path.GetExtension(fileName)))
                {
                    throw new InvalidOperationException("This filetype is not allowed.");
                }
                if (!IsWithinSizeLimit(stream))
                {
                    throw new InvalidOperationException("The file is too large to save to Azure.");
                }                
            }
            try
            {
                var localLocation = FixLocationPath(fileName, location);
                var localFileName = FixFilePath(fileName, location);

                //get the file
                var blob = GetBlob(localFileName, localLocation);
                blob.UploadFromStream(stream);
                return blob.Uri.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to save file to Azure.", ex);
            }
        }

        /// <summary>
        /// Save a file into Azure.
        /// </summary>
        /// <param name="stream">The file stream</param>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns>A true/false success bool</returns>
        public async Task<string> SaveFileAsync(Stream stream, string fileName, string location)
        {
            if (!IsAllowedFileType(Path.GetExtension(fileName)))
                throw new InvalidOperationException("This filetype is not allowed.");
            if (!IsWithinSizeLimit(stream))
                throw new InvalidOperationException("The file is too large to save to Azure.");
            try
            {
                //get the file
                var blob = GetBlob(fileName, location);
                using (stream)
                {
                    await blob.UploadFromStreamAsync(stream);
                    return blob.Uri.ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to save file to Azure.", ex);
            }
        }

        /// <summary>
        /// Save multiple files into Azure.
        /// </summary>
        /// <param name="streamsAndFileNames">A dictionary containing the streams and their corresponding file names</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns>A list of the files locations.</returns>
        public IEnumerable<string> SaveFiles(Dictionary<string, Stream> streamsAndFileNames, string location)
        {
            var fileLocations = new List<string>();
            foreach (var item in streamsAndFileNames)
            {
                try
                {
                    var item1 = item;
                    var f = SaveFile(item1.Value, item1.Key, location);
                    fileLocations.Add(f);
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to save file to Azure.", ex);
                }
            }
            return fileLocations;
        }

        /// <summary>
        /// Save multiple files into Azure.
        /// </summary>
        /// <param name="streamsAndFileNames">A dictionary containing the streams and their corresponding file names</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns>A list of the files locations.</returns>
        public async Task<IEnumerable<string>> SaveFilesAsync(Dictionary<string, Stream> streamsAndFileNames, string location)
        {
            var fileLocations = new List<string>();
            foreach (var item in streamsAndFileNames)
            {
                try
                {
                    var item1 = item;
                    var f = await SaveFileAsync(item1.Value, item1.Key, location);
                    fileLocations.Add(f);
                }
                catch (Exception ex)
                {
                    throw new Exception("Unable to save file to Azure.", ex);
                }
            }
            return fileLocations;
        }

        /// <summary>
        /// Delete a file from Azure
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns>A true/false success bool</returns>
        public bool DeleteFile(string fileName, string location)
        {
            try
            {
                var localLocation = FixLocationPath(fileName, location);
                var localFileName = FixFilePath(fileName, location);

                //get the file
                var blob = GetBlob(localFileName, localLocation);
                //Delete the file
                blob.Delete();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to delete file from Azure.", ex);
            }
        }

        /// <summary>
        /// Delete a file from Azure
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns>A true/false success bool</returns>
        public async Task<bool> DeleteFileAsync(string fileName, string location)
        {
            try
            {
                //get the file
                var blob = GetBlob(fileName, location);

                //Delete the file
                await blob.DeleteAsync();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to delete file from Azure.", ex);
            }
        }

        /// <summary>
        /// Delete a number of file from Azure
        /// </summary>
        /// <param name="pathsList">The list of paths where the files reside</param>
        /// <returns>A true/false success bool</returns>
        public bool DeleteFiles(Dictionary<string, string> pathsList)
        {
            try
            {
                foreach (var item in pathsList.Where(item => FileExists(item.Key, item.Value)))
                {
                    DeleteFile(item.Key, item.Value);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("There was an error whilst trying to delete one of the files from Azure.", ex);
            }
        }

        /// <summary>
        /// Delete a number of file from Azure
        /// </summary>
        /// <param name="pathsList">The list of paths where the files reside</param>
        /// <returns>A true/false success bool</returns>
        public async Task<bool> DeleteFilesAsync(Dictionary<string, string> pathsList)
        {
            try
            {
                foreach (var item in pathsList.Where(item => FileExists(item.Key, item.Value)))
                {
                    await DeleteFileAsync(item.Key, item.Value);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("There was an error whilst trying to delete one of the files from Azure.", ex);
            }
        }

        /// <summary>
        /// Get the full path/url of a file from Azure.
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns></returns>
        public string GetFilePath(string fileName, string location)
        {
            try
            {
                //get the file, return the URL
                return GetBlob(fileName, location).StorageUri.PrimaryUri.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Unable to get file path {0} in {1} from Azure.{2}", fileName, location, ex)); 
            }
        }

        /// <summary>
        /// Get the full path/url of a file from Azure.
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns></returns>
        public async Task<string> GetFilePathAsync(string fileName, string location)
        {
            try
            {
                //get the file, return the URL
                return await Task.Run(() => GetBlob(fileName, location).StorageUri.PrimaryUri.ToString());
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Unable to get file path {0} in {1} from Azure.{2}", fileName, location, ex)); 
            }
        }

        /// <summary>
        /// Download a file from Azure
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns>The file stream</returns>
        public Stream DownloadFile(string fileName, string location)
        {
            try
            {
                var localLocation = FixLocationPath(fileName, location);
                var localFileName = FixFilePath(fileName, location);

                //get the file
                var blob = GetBlob(localFileName, localLocation);

                //create a stream to hold it.
                var memoryStream = new MemoryStream();

                //downlaod to stream
                blob.DownloadToStream(memoryStream);

                memoryStream.Seek(0, SeekOrigin.Begin);

                return memoryStream;

            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Unable to download file {0} in {1} from Azure.{2}", fileName, location, ex));
            }
        }

        /// <summary>
        /// Download a file from Azure
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns>The file stream</returns>
        public async Task<Stream> DownloadFileAsync(string fileName, string location)
        {
            try
            {
                //get the file
                var blob = GetBlob(FixFilePath(fileName, location), FixLocationPath(fileName, location));

                //create a stream to hold it.
                var memoryStream = new MemoryStream();

                //downlaod to stream
                await blob.DownloadToStreamAsync(memoryStream);

                return memoryStream;

            }
            catch (Exception ex)
            {
                throw new Exception("Unable to download file from Azure.", ex);
            }
        }

        /// <summary>
        /// Download a number of files from Azure
        /// </summary>
        /// <param name="paths">A list of file paths</param>
        /// <returns>A list of the file streams.</returns>
        public IEnumerable<Stream> DownloadFiles(Dictionary<string, string> paths)
        {
            //list of streams to return
            var streams = new List<Stream>();

            //list of exceptions to return. This ensures the exception displays each file there is a problem with or can't find for easier debugging.
            var exceptions = new ConcurrentQueue<Exception>();

            foreach (var item in paths)
            {
                try
                {
                    var s = DownloadFile(item.Key, item.Value);
                    streams.Add(s);
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            }
            if (exceptions.Count > 0) throw new AggregateException(exceptions);
            return streams;
        }

        /// <summary>
        /// Download a number of files from Azure
        /// </summary>
        /// <param name="paths">A list of file paths</param>
        /// <returns>A list of the file streams.</returns>
        public async Task<List<Stream>> DownloadFilesAsync(Dictionary<string, string> paths)
        {
            //list of streams to return
            var streams = new List<Stream>();

            //list of exceptions to return. This ensures the exception displays each file there is a problem with or can't find for easier debugging.
            var exceptions = new ConcurrentQueue<Exception>();

            foreach (var item in paths)
            {
                try
                {
                    var s = await DownloadFileAsync(item.Key, item.Value);
                    streams.Add(s);
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            }
            if (exceptions.Count > 0) throw new AggregateException(exceptions);
            return streams;
        }

        /// <summary>
        /// Copies a file from one destination to another in Azure.
        /// </summary>
        /// <param name="file">The file stream to copy</param>
        /// <param name="fileName">The name of the file</param>
        /// <param name="destination">The destination for the file.</param>
        /// <returns>The new URL to the copied file</returns>
        public string CopyTo(Stream file, string fileName, string destination)
        {
            try
            {
                if (!DirectoryExists(destination))
                {
                    throw new Exception("This directory does not exist in Azure.");
                }
                return SaveFile(file, fileName, destination);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to copy file to new location in Azure.", ex);
            }
        }

        /// <summary>
        /// Copies a file from one destination to another in Azure.
        /// </summary>
        /// <param name="file">The file stream to copy</param>
        /// <param name="fileName">The name of the file</param>
        /// <param name="destination">The destination for the file.</param>
        /// <returns>The new URL to the copied file</returns>
        public async Task<string> CopyToAsync(Stream file, string fileName, string destination)
        {
            try
            {
                if (!DirectoryExists(destination))
                {
                    throw new Exception("This directory does not exist in Azure.");
                }
                return await SaveFileAsync(file, fileName, destination);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to copy file to new location in Azure.", ex);
            }
        }

        /// <summary>
        /// Copies a file from Azure to another provider
        /// </summary>
        /// <param name="file">The file to copy</param>
        /// <param name="fileName">The name of the file to copy.</param>
        /// <param name="destinationPath">The destination path in the other provider</param>
        /// <param name="provider">The destination provider</param>
        /// <returns>A true/false success bool</returns>
        public bool CopyTo(Stream file, string fileName,string destinationPath, IFileSystemProvider provider)
        {
            try
            {
                provider.CopyTo(file, fileName, destinationPath);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to copy file to " + provider.GetProviderName(), ex);
            }
        }

        /// <summary>
        /// Copies a file from Azure to another provider
        /// </summary>
        /// <param name="file">The file to copy</param>
        /// <param name="fileName">The name of the file to copy.</param>
        /// <param name="destinationPath">The destination path in the other provider</param>
        /// <param name="provider">The destination provider</param>
        /// <returns>A true/false success bool</returns>
        public bool CopyToAsync(Stream file, string fileName, string destinationPath, IFileSystemProvider provider)
        {
            try
            {
                provider.CopyTo(file, fileName, destinationPath);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to copy file to " + provider.GetProviderName(), ex);
            }
        }

        /// <summary>
        /// Checks whether a file exists in Azure
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="location">The location of the file. This is the container name.</param>
        /// <returns>A true/false exists bool</returns>
        public bool FileExists(string fileName, string location)
        {
            try
            {
                var localLocation = FixLocationPath(fileName, location);
                var localFileName = FixFilePath(fileName, location);

                //get the file
                var blob = GetBlob(localFileName, localLocation);

                return blob.Exists();

            }
            catch (Exception ex)
            {
                throw new Exception("Unable to check whether file exists in Azure.", ex);
            }
        }

        /// <summary>
        /// Check is a file exists at the specified location
        /// </summary>
        /// <param name="filePath">The file path to check for the existence of a file</param>
        /// <returns>Returns true if the file is found</returns>
        public bool FileExists(string filePath)
        {
            return FileExists(Path.GetFileName(filePath), Path.GetFullPath(filePath));
        }

        /// <summary>
        /// Creates a directory in Azure. NOTE@ Azure does not suppoer heirarchal file structure, so the path string is 
        /// the only parameter we use.
        /// </summary>
        /// <param name="path">The path to the new directory</param>
        /// <returns>The full path to the directory</returns>
        public string CreateDirectory(string path)
        {
            var localLocation = FixLocationPath(string.Empty, path);

            //The container method creates a container if one does not exist.
            var container = GetContainer(localLocation);

            return container.StorageUri.ToString();
        }

        /// <summary>
        /// Creates a directory in Azure. NOTE@ Azure does not suppoer heirarchal file structure, so the path string is 
        /// the only parameter we use.
        /// </summary>
        /// <param name="path">The path to the new directory</param>
        /// <returns>The full path to the directory</returns>
        public async Task<string> CreateDirectoryAsync(string path)
        {
            //The container method creates a container if one does not exist.
            var container = await Task.Run(() => GetContainer(path));

            return container.StorageUri.ToString();
        }

        /// <summary>
        /// Deletes a directory in Azure.
        /// </summary>
        /// <param name="path">The full path to the container.</param>
        /// <returns>A true/false success bool</returns>
        public bool DeleteDirectory(string path)
        {
            try
            {
                return GetContainer(path).DeleteIfExists();
            }
            catch (Exception ex)
            {
                throw new IOException("Unable to delete this directory in Azure", ex);
            }
        }


        /// <summary>
        /// Deletes a directory in Azure.
        /// </summary>
        /// <param name="path">The full path to the container.</param>
        /// <returns>A true/false success bool</returns>
        public async Task<bool> DeleteDirectoryAsync(string path)
        {
            try
            {
                return await GetContainer(path).DeleteIfExistsAsync();
            }
            catch (Exception ex)
            {
                throw new IOException("Unable to delete this directory in Azure", ex);
            }
        }


        /// <summary>
        /// Checks whether a directory exists in Azure.
        /// </summary>
        /// <param name="location">The name of a container.</param>
        /// <returns>A true/false exists bool</returns>
        public bool DirectoryExists(string location)
        {
            var container = GetContainer(FixLocationPath(string.Empty, location));
            return container.Exists();
        }

        /// <summary>
        /// Get the size of a file based on the file path
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="location">The file location.</param>
        /// <returns></returns>
        public long FileSize(string fileName, string location)
        {
            var blob = GetBlob(FixFilePath(fileName, location), FixLocationPath(fileName, location));
            blob.FetchAttributes();
            return blob.Properties.Length;
        }

        private string FixFilePath(string fileName, string location)
        {
            var completePath = Path.Combine(location, fileName).Replace("\\", "/");
            var components = completePath.Split('/').Where(m => !string.IsNullOrEmpty(m)).ToList();
            components.RemoveAt(0);
            return components.Join("/");
        }

        private string FixLocationPath(string fileName, string location)
        {
            var completePath = Path.Combine(location, fileName).Replace("\\", "/");
            var components = completePath.Split('/').Where(m=>!string.IsNullOrEmpty(m)).ToList();
            return components.FirstOrDefault();
        }

        /// <summary>
        /// Lists the URLs to all of the files within an Azure container.
        /// </summary>
        /// <param name="location">The container name from which we get a list of URLs for each file contained within.</param>
        /// <returns>A list of the file URLs</returns>
        public IEnumerable<string> ListDirectoryContents(string location)
        {
            var localLocation = FixLocationPath(string.Empty, location);
            var filePath = FixFilePath(string.Empty, location);
            if (!DirectoryExists(localLocation))
            {
                throw new Exception("This location does not exist in Azure");
            }
            try
            {
                var blobDirectory = GetContainer(localLocation).ListBlobs(filePath).FirstOrDefault() as CloudBlobDirectory;
                if (blobDirectory != null)
                {
                    var blobs = blobDirectory.ListBlobs().ToList();
                    return blobs.OfType<CloudBlockBlob>().Select(blob => Path.GetFileName(blob.Name)).ToList();
                }
                return new List<string>();
            }
            catch (Exception ex)
            {
                throw new Exception("There was a problem retrieving a list of files for this container from Azure", ex);
            }
        }

        /// <summary>
        /// Checks whether a blob exists on the cloud
        /// </summary>
        /// <param name="client">The client we use to connect to our storage</param>
        /// <param name="containerName">The name of the container in which we check for the file</param>
        /// <param name="fileName">The name of the file to check for.</param>
        /// <returns></returns>
        public bool BlobExistsOnCloud(CloudBlobClient client, string containerName, string fileName)
        {
            return client.GetContainerReference(containerName)
                         .GetBlockBlobReference(fileName)
                         .Exists();
        }

        /// <summary>
        /// Gets the name of this provider. Used in other providers when IFileSytemProvider is implemented
        /// </summary>
        /// <returns>The name of this provider</returns>
        public string GetProviderName()
        {
            return "Azure";
        }
    }
}
