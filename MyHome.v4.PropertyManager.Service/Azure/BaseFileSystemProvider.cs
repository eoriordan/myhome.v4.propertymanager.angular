﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MyHome.v4.PropertyManager.Service.Azure
{
    /// <summary>
    /// Common methods for implemented file system providers
    /// </summary>
    public class BaseFileSystemProvider
    {
        /// <summary>
        /// A dictionary of the allowed file extensions and their respective content types
        /// </summary>
        private readonly Dictionary<string, string> _allowedFileAndContentTypes = new Dictionary<string, string>
        {
            {".pdf", "application/pdf"},
            {".png", "image/png"},
            {".jpg", "image/jpeg"},
            {".jpeg", "image/jpeg"},
            {".tiff", "image/tiff"},
            {".doc", "application/msword"},
            {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
            {".ppt", "application/vnd.ms-powerpoint"},
            {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {".xls", "application/vnd.ms-excel"},
            {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            {".txt", "text/plain"},
            {".gif", "image/gif"}
        };

        //The max file size for a file upload - 20MB
        private const long MaximumFileSize = 20971520;

        /// <summary>
        /// Gets the file type by returning the extension of a file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetFileType(string path)
        {
            return Path.GetExtension(path.ToLower());
        }


        /// <summary>
        /// Gets the size in bytes of a file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public long GetFileSize(string path)
        {
            var fileInfo = new FileInfo(path);
            return fileInfo.Length;
        }

        /// <summary>
        /// Gets the content type of a file, checks the file is in the allowed file types first
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetFileContentType(string path)
        {
            if (IsAllowedFileType(path))
            {
                return _allowedFileAndContentTypes.Where(x => String.Equals(x.Key, GetFileType(path), StringComparison.CurrentCultureIgnoreCase)).Select(x => x.Value).First();
            }
            throw new InvalidOperationException(GetFileType(path) + " is not a known file type.");
        }

        /// <summary>
        /// Checks whether a file is allowed
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool IsAllowedFileType(string path)
        {
            return _allowedFileAndContentTypes.ContainsKey(GetFileType(path).ToLower());
        }

        /// <summary>
        /// Checks whether a file is within our allowed size limits
        /// </summary>
        /// <param name="fileStream">The stream of the file.</param>
        /// <returns></returns>
        public bool IsWithinSizeLimit(Stream fileStream)
        {
            return fileStream.Length < MaximumFileSize;
        }

        /// <summary>
        /// Gets a list of streams
        /// </summary>
        /// <param name="paths">A key value pair containing the file names(key) and paths(values) to files.</param>
        /// <returns>A list of Streams.</returns>
        public List<Stream> GetStreams(Dictionary<string, string> paths)
        {
            var streams = new List<Stream>();
            var memoryStream = new MemoryStream();
            foreach (var path in paths)
            {
                if (!File.Exists(Path.Combine(path.Key, path.Value)))
                {
                    throw new Exception("There is no file at that location");
                }
                try
                {
                    using (var fileStream = File.OpenRead(Path.Combine(path.Key, path.Value)))
                    {
                        fileStream.CopyTo(memoryStream);
                    }
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    streams.Add(memoryStream);
                    memoryStream = new MemoryStream();
                }
                catch (IOException ex)
                {
                    throw new Exception("There was a problem getting this files stream", ex);
                }
            }
            return streams;
        }
    }
}
