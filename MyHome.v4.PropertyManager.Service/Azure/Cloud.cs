﻿using System.Linq;

namespace MyHome.v4.PropertyManager.Service.Azure
{
    public static class Cloud
    {
        /// <summary>
        /// Combines the specified path parts of a cloud path together
        /// </summary>
        /// <param name="pathParts">The path parts.</param>
        /// <returns></returns>
        public static string Combine(params string[] pathParts)
        {
            var uri = string.Empty;
            if (pathParts != null && pathParts.Any())
            {
                var trims = new[] { '\\', '/' };
                uri = (pathParts[0] ?? string.Empty).TrimEnd(trims);
                for (int i = 1; i < pathParts.Count(); i++)
                {
                    uri = string.Format("{0}/{1}", uri.TrimEnd(trims), (pathParts[i] ?? string.Empty).TrimStart(trims));
                }
            }
            //Ensure all slashes are pointing in the right direction
            return uri.Replace('\\', '/');
        }
    }
}
