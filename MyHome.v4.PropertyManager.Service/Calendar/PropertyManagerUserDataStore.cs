﻿using System;
using System.Threading.Tasks;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.Users;
using Newtonsoft.Json;

namespace MyHome.v4.PropertyManager.Service.Calendar
{
    public class PropertyManagerUserDataStore : Google.Apis.Util.Store.IDataStore
    {
        /// <summary>
        /// The _user service
        /// </summary>
        private readonly IUserRepository _userRepository;
        /// <summary>
        /// The _user identifier
        /// </summary>
        private readonly int _userId;


        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyManagerUserDataStore"/> class.
        /// </summary>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="userId">The user identifier.</param>
        public PropertyManagerUserDataStore(IUserRepository userRepository, int userId)
        {
            _userRepository = userRepository;
            _userId = userId;
        }


        /// <summary>
        /// Stores the asynchronous.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public Task StoreAsync<T>(string key, T value)
        {
            var token = value as Google.Apis.Auth.OAuth2.Responses.TokenResponse;
            if (token != null && !String.IsNullOrEmpty(token.AccessToken))
            {
                var user = GetUser();

                user.UserConfiguration.GoogleAuthAccessToken = token.AccessToken;
                user.UserConfiguration.GoogleAuthTokenType = token.TokenType;
                user.UserConfiguration.GoogleAuthExpiresInSeconds = token.ExpiresInSeconds;
                user.UserConfiguration.GoogleAuthRefreshToken = token.RefreshToken;
                user.UserConfiguration.GoogleAuthScope = token.Scope;
                user.UserConfiguration.GoogleAuthIssued = token.Issued;

                _userRepository.Persist(user);

            }
            return TaskEx.Delay(0);
        }

        /// <summary>
        /// Deletes the asynchronous.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public Task DeleteAsync<T>(string key)
        {
            var user = GetUser();

            if (user.UserConfiguration.GoogleAuthEmail == key)
            {
                user.UserConfiguration.GoogleAuthAccessToken = string.Empty;
                user.UserConfiguration.GoogleAuthTokenType = string.Empty;
                user.UserConfiguration.GoogleAuthExpiresInSeconds = null;
                user.UserConfiguration.GoogleAuthRefreshToken = string.Empty;
                user.UserConfiguration.GoogleAuthScope = string.Empty;
                user.UserConfiguration.GoogleAuthIssued = DateTime.MinValue;

                _userRepository.Persist(user);
            }

            return TaskEx.Delay(0);
        }

        /// <summary>
        /// Gets the asynchronous.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public Task<T> GetAsync<T>(string key)
        {
            var tcs = new TaskCompletionSource<T>();
            var user = GetUser();

            if (typeof(T) == typeof(Google.Apis.Auth.OAuth2.Responses.TokenResponse))
            {
                var item = new Google.Apis.Auth.OAuth2.Responses.TokenResponse
                {
                    AccessToken = user.UserConfiguration.GoogleAuthAccessToken,
                    TokenType = user.UserConfiguration.GoogleAuthTokenType,
                    ExpiresInSeconds = user.UserConfiguration.GoogleAuthExpiresInSeconds,
                    Issued = user.UserConfiguration.GoogleAuthIssued,
                    RefreshToken = user.UserConfiguration.GoogleAuthRefreshToken,
                    Scope = user.UserConfiguration.GoogleAuthScope
                };
                tcs.SetResult(JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(item))); // Well this seems strange...
            }
            return tcs.Task;
        }

        /// <summary>
        /// Asynchronously clears all values in the data store.
        /// </summary>
        /// <returns></returns>
        public Task ClearAsync()
        {
            var user = GetUser();

            user.UserConfiguration.GoogleAuthAccessToken = string.Empty;
            user.UserConfiguration.GoogleAuthTokenType = string.Empty;
            user.UserConfiguration.GoogleAuthExpiresInSeconds = null;
            user.UserConfiguration.GoogleAuthRefreshToken = string.Empty;
            user.UserConfiguration.GoogleAuthScope = string.Empty;
            user.UserConfiguration.GoogleAuthIssued = DateTime.MinValue;

            _userRepository.Persist(user);

            return TaskEx.Delay(0);
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <returns></returns>
        private Schema.Users.User GetUser()
        {
            var user = _userRepository.GetById(_userId);
            if (user.UserConfiguration == null)
            {
                user.UserConfiguration = new UserConfiguration();
            }
            return user;
        }
    }
}
