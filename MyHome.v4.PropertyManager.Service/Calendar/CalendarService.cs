﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.IRepository.Calendar;
using MyHome.v4.PropertyManager.IRepository.Contacts;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.IService.Calendar;
using MyHome.v4.PropertyManager.Schema.Calendar;
using MyHome.v4.PropertyManager.Types.Calendar;
using MyHome.v4.PropertyManager.Types.Contacts;
using MyHome.v4.PropertyManager.Types.Groups;
using MyHome.v4.PropertyManager.Types.Property;
using ServiceStack;

namespace MyHome.v4.PropertyManager.Service.Calendar
{
    public class CalendarService : ICalendarService
    {
        private readonly ITaskRespository _taskRespository;
        private readonly IUserRepository _userRepository;
        private readonly IPropertyRepository _propertyRepository;
        private readonly IContactRepository _contactRepository;

        public CalendarService(
            IUserRepository userRepository,
            IPropertyRepository propertyRepository,
            IContactRepository contactRepository,
            ITaskRespository taskRepository)
        {
            _userRepository = userRepository;
            _taskRespository = taskRepository;
            _propertyRepository = propertyRepository;
            _contactRepository = contactRepository;
        }

        public CalendarItem GetCalendarItem(string externalEventId, Group group, Types.Users.User user)
        {
            var userName = GetUsername(user, group);
            var calendarService = GetCalendarService(user, group);

            if (userName != null && calendarService != null)
            {
                var calendarEvent = calendarService.Events.Get(userName, externalEventId).Execute();
                if (calendarEvent != null)
                {
                    var item = ToCalendarItem(calendarEvent, user);
                    var propertyId = GetExtendedProperty(calendarEvent, "myhome/1#propertyid");
                    var contactId = GetExtendedProperty(calendarEvent, "myhome/1#contactid");

                    if (contactId.HasValue)
                    {
                        var contact = _contactRepository.GetById(contactId.Value);
                        if (contact != null)
                            item.Contact = contact.ConvertTo<Types.Contacts.Contact>();
                    }
                    if (propertyId.HasValue)
                    {
                        var property = _propertyRepository.GetById(propertyId.Value);
                        if (property != null)
                            item.Property = property.ConvertTo<PropertySnippet>();
                    }

                    return item;
                }
            }

            return null;
        }

        public ICollection<CalendarItem> GetCalendarItems(Group group, ICollection<Types.Users.User> users, DateTime fromTime, DateTime toTime)
        {
            var items = new Collection<CalendarItem>();
            var contactIds = new HashSet<int>();
            var propertyIds = new HashSet<int>();
            var contactIdByItem = new Dictionary<string, int>();
            var propertyIdByItem = new Dictionary<string, int>();

            foreach (var user in users)
            {
                var userName = GetUsername(user, group);
                var calendarService = GetCalendarService(user, group);

                if (userName != null && calendarService != null)
                {
                    var events = GetEvents(calendarService, userName, fromTime, toTime);

                    foreach (var googleEvent in events)
                    {
                        items.Add(ToCalendarItem(googleEvent, user));

                        var propertyId = GetExtendedProperty(googleEvent, "myhome/1#propertyid");
                        var contactId = GetExtendedProperty(googleEvent, "myhome/1#contactid");

                        if (propertyId.HasValue)
                        {
                            propertyIds.Add(propertyId.Value);
                            propertyIdByItem.Add(googleEvent.Id, propertyId.Value);
                        }
                        if (contactId.HasValue)
                        {
                            contactIds.Add(contactId.Value);
                            contactIdByItem.Add(googleEvent.Id, contactId.Value);
                        }
                    }
                }
            }

            var contactsById = _contactRepository.Select(x => contactIds.Contains(x.ContactId)).ToDictionary(x => x.ContactId);
            var propertiesById = _propertyRepository.Select(x => propertyIds.Contains(x.PropertyId)).ToDictionary(x => x.PropertyId);

            foreach (var calendarItem in items)
            {
                if (contactIdByItem.ContainsKey(calendarItem.ExternalEventId))
                {
                    var contactId = contactIdByItem[calendarItem.ExternalEventId];
                    if (contactsById.ContainsKey(contactId))
                    {
                        calendarItem.Contact = contactsById[contactId].ConvertTo<Types.Contacts.Contact>();
                    }
                    
                }
                if (propertyIdByItem.ContainsKey(calendarItem.ExternalEventId))
                {
                    var propertyId = propertyIdByItem[calendarItem.ExternalEventId];
                    if (propertiesById.ContainsKey(propertyId))
                    {
                        calendarItem.Property = propertiesById[propertyId].ConvertTo<PropertySnippet>();
                    }

                }
            }

            return items;
        }

        public void DeleteEvent(Group group, Types.Users.User user, string externalEventId)
        {
            var userName = GetUsername(user, group);
            var calendarService = GetCalendarService(user, group);

            calendarService.Events.Delete(userName, externalEventId).Execute();
        }

        public void SaveEvent(Group group, Types.Users.User user, CalendarItem item)
        {
            var userName = GetUsername(user, group);
            var calendarService = GetCalendarService(user, group);

            var googleEvent = new Event
            {
                Id = item.ExternalEventId,
                Created = item.CreatedOn,
                Creator = new Event.CreatorData { DisplayName = item.CreatedBy },
                Start = new EventDateTime { DateTime = item.StartTime },
                Summary = item.Name,
                Description = item.Description,
                Location = item.Location
            };

            if (item.EndTime.HasValue)
            {
                googleEvent.End = new EventDateTime { DateTime = item.EndTime };
            }

            if (item.Reminder.HasValue)
            {
                googleEvent.Reminders = new Event.RemindersData
                {
                    Overrides = new List<EventReminder>
                    {
                        new EventReminder
                        {
                            Minutes = item.Reminder,
                            Method = "popup"
                        }
                    }
                };
            }

            googleEvent.ExtendedProperties = new Event.ExtendedPropertiesData
            {
                Shared = new Dictionary<string, string>
                {
                    { "myhome/1#tasktype", item.TaskType.GetValueOrDefault().ToString()}
                }
            };

            if (item.Property != null)
            {
                googleEvent.ExtendedProperties.Shared.Add("myhome/1#propertyid", item.Property.PropertyId.ToString());
            }
            if (item.Contact != null)
            {
                googleEvent.ExtendedProperties.Shared.Add("myhome/1#contactid", item.Contact.ContactId.ToString());
            }

            if (item.ExternalEventId == null)
            {
                calendarService.Events.Insert(googleEvent, userName).Execute();
            }
            else
            {
                calendarService.Events.Update(googleEvent, userName, googleEvent.Id).Execute();
            }
        }

        public PagedList<TaskItem> GetTasks(Group group, DateTime? minDuedate, DateTime? maxDueDate, int page, int pageSize)
        {
            Expression<Func<Task, bool>> query;

            if (minDuedate.HasValue && maxDueDate.HasValue)
                query = x => x.GroupId == group.GroupId &&
                             x.DueOn >= minDuedate &&
                             x.DueOn <= maxDueDate && 
                             x.CompletedOn == null;
            else if (minDuedate.HasValue)
                query = x => x.GroupId == group.GroupId && x.DueOn >= minDuedate && x.CompletedOn == null;
            else if (maxDueDate.HasValue)
                query = x => x.GroupId == group.GroupId && x.DueOn <= maxDueDate && x.CompletedOn == null;
            else
                query = x => x.GroupId == group.GroupId;

            return GetTasks(group, query, page, pageSize);
        }

        public PagedList<TaskItem> GetTasks(Group group, Types.Users.User user, DateTime? minDuedate, DateTime? maxDueDate, int page, int pageSize)
        {
            Expression<Func<Task, bool>> query;

            if (minDuedate.HasValue && maxDueDate.HasValue)
                query = x => x.AssignedToUserId == user.UserId &&
                             x.DueOn >= minDuedate &&
                             x.DueOn <= maxDueDate &&
                             x.CompletedOn == null;
            else if (minDuedate.HasValue)
                query = x => x.AssignedToUserId == user.UserId && x.DueOn >= minDuedate && x.CompletedOn == null;
            else if (maxDueDate.HasValue)
                query = x => x.AssignedToUserId == user.UserId && x.DueOn <= maxDueDate && x.CompletedOn == null;
            else
                query = x => x.AssignedToUserId == user.UserId;

            return GetTasks(group, query, page, pageSize);
        }

        public PagedList<TaskItem> GetCompletedTasks(Group group, int page, int pageSize)
        {
            Expression<Func<Task, bool>> query = x => x.GroupId == group.GroupId && x.CompletedOn != null;
            return GetTasks(group, query, page, pageSize);
        }

        public PagedList<TaskItem> GetCompletedTasks(Group group, Types.Users.User user, int page, int pageSize)
        {
            Expression<Func<Task, bool>> query = x => x.AssignedToUserId == user.UserId && x.CompletedOn != null;
            return GetTasks(group, query, page, pageSize);
        }

        public void UpdateTask(int taskId, int? completedByUserId, DateTime? completedOn)
        {
            var taskItem = _taskRespository.GetById(taskId);
            if (taskItem != null)
            {
                taskItem.CompletedOn = completedOn;
                taskItem.CompletedByUserId = completedByUserId;
                _taskRespository.Persist(taskItem);
            }
        }

        public void CreateTask(TaskItem task)
        {
            var dbTask = task.ConvertTo<Task>();

            if(task.Group != null)
                dbTask.GroupId = task.Group.GroupId;

            if(task.CreatedByUser != null)
                dbTask.CreatedByUserId = task.CreatedByUser.UserId;

            if(task.AssignedToUser != null)
                dbTask.AssignedToUserId = task.AssignedToUser.UserId;

            if(task.Property != null)
                dbTask.PropertyId = task.Property.PropertyId;

            if(task.Contact != null)
                dbTask.ContactId = task.Contact.ContactId;

            _taskRespository.Persist(dbTask);
        }

        private PagedList<TaskItem> GetTasks(Group group, Expression<Func<Task, bool>> query, int page, int pageSize)
        {
            ICollection<Task> dbTasks = _taskRespository.Select(query, page, pageSize).ToList();
            var resultsCount = _taskRespository.Count(query);

            var results = HydrateTasks(dbTasks, group);

            var pagedResults = new PagedList<TaskItem>
            {
                Page = page,
                PageSize = pageSize,
                ResultCount = (int)resultsCount
            };
            if (results != null)
            {
                pagedResults.AddRange(results.ToList());
            }

            return pagedResults;
        }

        private IEnumerable<TaskItem> HydrateTasks(ICollection<Task> dbTasks, Group group)
        {
            if(!dbTasks.Any())
                return Enumerable.Empty<TaskItem>();

            var userIds = dbTasks
                .Select(x => x.AssignedToUserId.GetValueOrDefault())
                .Distinct()
                .Union(dbTasks.Select(x => x.CreatedByUserId).Distinct())
                .Union(dbTasks.Select(x => x.CompletedByUserId.GetValueOrDefault()).Distinct());
            var usersById = _userRepository
                .Select(x => userIds.Contains(x.UserId))
                .Select(x => x.ConvertTo<Types.Users.User>())
                .ToDictionary(x => x.UserId);

            var contactIds = dbTasks.Select(x => x.ContactId.GetValueOrDefault()).Distinct();
            var contactsById = _contactRepository
                .Select(x => contactIds.Contains(x.ContactId))
                .Select(x => x.ConvertTo<Contact>())
                .ToDictionary(x => x.ContactId);

            var propertyIds = dbTasks.Select(x => x.PropertyId.GetValueOrDefault()).Distinct();
            var propertiesById = _propertyRepository
                .Select(x => propertyIds.Contains(x.PropertyId))
                .Select(x => x.ConvertTo<PropertySnippet>())
                .ToDictionary(x => x.PropertyId);

            var results = new Collection<TaskItem>();
            foreach (var dbTask in dbTasks)
            {
                var task = dbTask.ConvertTo<TaskItem>();

                task.Group = group;
                task.AssignedToUser = dbTask.AssignedToUserId.HasValue ? usersById[dbTask.AssignedToUserId.Value] : null;
                task.CreatedByUser = usersById[dbTask.CreatedByUserId];
                task.CompletedByUser = dbTask.CompletedByUserId.HasValue ? usersById[dbTask.CompletedByUserId.Value] : null;

                if (dbTask.ContactId.HasValue)
                    task.Contact = contactsById[dbTask.ContactId.Value];
                if (dbTask.PropertyId.HasValue)
                    task.Property = propertiesById[dbTask.PropertyId.Value];

                results.Add(task);
            }

            return results;
        }

        private static IEnumerable<Event> GetEvents(Google.Apis.Calendar.v3.CalendarService calendarService, string userName, DateTime fromTime, DateTime toTime)
        {
            try
            {
                var query = calendarService.Events.List(userName);
                query.SingleEvents = true;
                query.AlwaysIncludeEmail = true;
                query.TimeMin = fromTime;
                query.TimeMax = toTime;
                query.MaxResults = 100;

                var events = query.Execute();
                return events.Items;
            }
            catch (Exception)
            {
                return Enumerable.Empty<Event>();
            }
        }

        private static CalendarItem ToCalendarItem(Event googleEvent, Types.Users.User user)
        {
            return new CalendarItem
            {
                ExternalEventId = googleEvent.Id,
                UserId = user.UserId,
                UserName = user.DisplayName,
                CreatedOn = googleEvent.Created,
                CreatedBy = googleEvent.Creator == null ? string.Empty : googleEvent.Creator.DisplayName,
                StartTime = googleEvent.Start == null ? null : googleEvent.Start.DateTime,
                EndTime = googleEvent.End == null ? null : googleEvent.End.DateTime,
                IsRecuring = !String.IsNullOrEmpty(googleEvent.RecurringEventId),
                Name = googleEvent.Summary,
                Description = googleEvent.Description,
                Reminder = GetReminder(googleEvent),
                TaskType = GetExtendedProperty(googleEvent, "myhome/1#tasktype"),
                Location = googleEvent.Location
            };
        }

        private static int? GetExtendedProperty(Event googleEvent, string key)
        {
            if (googleEvent.ExtendedProperties == null || googleEvent.ExtendedProperties.Shared == null)
                return null;

            var properties = googleEvent.ExtendedProperties.Shared;
            if (properties.ContainsKey(key))
            {
                return int.Parse(googleEvent.ExtendedProperties.Shared[key]);
            }

            return null;
        }

        private static int? GetReminder(Event googleEvent)
        {
            if (googleEvent.Reminders == null || googleEvent.Reminders.Overrides == null)
                return null;

            var reminder = googleEvent.Reminders.Overrides.FirstOrDefault();
            if (reminder != null) return reminder.Minutes;

            return null;
        }

        /// <summary>
        /// Gets the username used in google calendars so we select the correct user
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        private static string GetUsername(Types.Users.User user, Group group)
        {
            if (user == null || group == null)
            {
                return string.Empty;
            }
            if (user.UserConfiguration.HasGoogleAuthEmail)
            {
                return user.UserConfiguration.GoogleAuthEmail;
            }
            return string.Format("{0}@{1}", user.Username.Split('@')[0], @group.GroupPrivateConfiguration.GoogleAppsDomain);
        }

        /// <summary>
        /// Gets the calendar service.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        private Google.Apis.Calendar.v3.CalendarService GetCalendarService(Types.Users.User user, Group group)
        {
            return new Google.Apis.Calendar.v3.CalendarService(new BaseClientService.Initializer
            {
                HttpClientInitializer = GetUserCredential(user, group),
                ApplicationName = "MyHome.ie Property Manager"
            });

        }

        /// <summary>
        /// Gets the user credential from the user credentials and creates a oAuth toaken so we can connect with the user
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="group">The group.</param>
        /// <returns></returns>
        private UserCredential GetUserCredential(Types.Users.User user, Group group)
        {
            var flow = GetUserAuthorizationCodeFlow(user.UserId);
            return new UserCredential(flow, GetUsername(user, group), new TokenResponse
            {
                AccessToken = user.UserConfiguration.GoogleAuthAccessToken,
                ExpiresInSeconds = user.UserConfiguration.GoogleAuthExpiresInSeconds,
                Issued = user.UserConfiguration.GoogleAuthIssued,
                RefreshToken = user.UserConfiguration.GoogleAuthRefreshToken,
                Scope = user.UserConfiguration.GoogleAuthScope,
                TokenType = user.UserConfiguration.GoogleAuthTokenType
            });
        }

        /// <summary>
        /// Gets the user authorization code flow.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public GoogleAuthorizationCodeFlow GetUserAuthorizationCodeFlow(int userId)
        {
            return GetAuthorizationCodeFlow(new PropertyManagerUserDataStore(_userRepository, userId));
        }

        /// <summary>
        /// Gets the authorization code flow.
        /// </summary>
        /// <param name="dataStore">The data store.</param>
        /// <returns></returns>
        private static GoogleAuthorizationCodeFlow GetAuthorizationCodeFlow(Google.Apis.Util.Store.IDataStore dataStore)
        {
            return new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer()
            {
                ClientSecrets = new ClientSecrets
                {
                    ClientId = "1092286964254-ppdfjjlr9h6dic4f9n9u9lf9fktv8qdg.apps.googleusercontent.com",
                    ClientSecret = "kMrww4JSnuAkG17luPetsz3E"
                },
                Scopes = new[]
                {
                    Google.Apis.Calendar.v3.CalendarService.Scope.Calendar,
                    Google.Apis.Drive.v2.DriveService.Scope.Drive
                    //We are not including contacts as we dont use them and at the time of coding
                    //there is no JSON api for contacts
                },
                DataStore = dataStore
            });
        }
    }
}
