﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyHome.v4.PropertyManager.IRepository
{
    public interface IBaseRepository<T> where T : class
    {
        /// <summary>
        /// Get the poco of type T from the persistent storage
        /// </summary>
        /// <param name="id">The uniqe id of the property to load from the persistent storage</param>
        /// <returns>Returns a poco of type T</returns>
        T GetById(int id);

        /// <summary>
        /// Get the poco of type T from the persistent storage
        /// </summary>
        /// <param name="id">The uniqe id of the property to load from the persistent storage</param>
        /// <returns>Returns a poco of type T</returns>
        Task<T> GetByIdAsync(int id);

        /// <summary>
        /// Get a list of poco's of type T from the persistent stroage based on there unique ids
        /// </summary>
        /// <param name="ids">The unique ids to load the items from there persistent storage</param>
        /// <returns>Returns a list of poco's of type T</returns>
        IEnumerable<T> GetByIds(List<int> ids);

        /// <summary>
        /// Get a list of poco's of type T from the persistent stroage based on there unique ids
        /// </summary>
        /// <param name="ids">The unique ids to load the items from there persistent storage</param>
        /// <returns>Returns a list of poco's of type T</returns>
        Task<IEnumerable<T>> GetByIdsAsync(List<int> ids);

        /// <summary>
        /// Get all poco's of type T from the persistent storage
        /// </summary>
        /// <returns>Returns all poco's of type T from the persistent storage</returns>
        IEnumerable<T> GetAll(int page = 1, int pageSize = 20);

        /// <summary>
        /// Get all poco's of type T from the persistent storage
        /// </summary>
        /// <returns>Returns all poco's of type T from the persistent storage</returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Persists the specified poco into the persistent storage and return the populated value
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        T Persist(T value);

        /// <summary>
        /// Persists of the specified pocos into the persistent storage
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        List<T> PersistMany(List<T> values);

        /// <summary>
        /// Deletes the object by its unique id using IsDeleted flag
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        bool Delete(int id);

        /// <summary>
        /// Deletes many objects by its unique id
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        bool DeleteMany(List<int> ids);

        /// <summary>
        /// Get a count from the persistent storage
        /// </summary>
        /// <returns>Returns a count from the persistent storage</returns>
        long Count(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Get all poco's of type T from the persistent storage
        /// </summary>
        /// <returns>Returns all poco's of type T from the persistent storage</returns>
        IEnumerable<T> Select(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Get all poco's of type T from the persistent storage
        /// </summary>
        /// <returns>Returns all poco's of type T from the persistent storage</returns>
        IEnumerable<T> Select(Expression<Func<T, bool>> expression, int page);

        /// <summary>
        /// Get all poco's of type T from the persistent storage
        /// </summary>
        /// <returns>Returns all poco's of type T from the persistent storage</returns>
        IEnumerable<T> Select(Expression<Func<T, bool>> expression, int page, int pageCount);

        /// <summary>
        /// Deletes the object by its unique id, deleted permanantly
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        bool HardDelete(int id);
    }
}
