﻿using MyHome.v4.PropertyManager.Schema.FeaturedProduct;

namespace MyHome.v4.PropertyManager.IRepository.FeaturedProduct
{
    public interface IFeaturedProductRepository : IBaseRepository<FeaturedSpot>
    {

    }
}
