﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema.Advice;

namespace MyHome.v4.PropertyManager.IRepository.Advice
{
    public interface IArticleRepository : IBaseRepository<Article>
    {
        List<Article> GetByCategory(int categoryId, int page, int pageSize);
        IEnumerable<Article> SearchArticles(string searchString);
    }
}
