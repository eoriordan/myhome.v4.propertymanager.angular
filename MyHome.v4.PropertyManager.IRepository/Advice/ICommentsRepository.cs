﻿using MyHome.v4.PropertyManager.Schema.Advice;

namespace MyHome.v4.PropertyManager.IRepository.Advice
{
    public interface ICommentsRepository : IBaseRepository<Comment>
    {
    }
}
