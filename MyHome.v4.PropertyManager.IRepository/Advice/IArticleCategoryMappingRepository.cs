﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema.Advice;

namespace MyHome.v4.PropertyManager.IRepository.Advice
{
    public interface IArticleCategoryMappingRepository : IBaseRepository<ArticleCategoryMapping>
    {
        List<ArticleCategoryMapping> GetByArticleId(int articleId);
    }
}
