﻿using MyHome.v4.PropertyManager.Schema.PropertyAccount;

namespace MyHome.v4.PropertyManager.IRepository.PropertyAccount
{
    public interface IPropertyAccountTransactionMethodRepository : IBaseRepository<PropertyAccountTransactionMethod>
    {
    }
}
