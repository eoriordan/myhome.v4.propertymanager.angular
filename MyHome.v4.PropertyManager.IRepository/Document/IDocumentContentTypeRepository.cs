﻿using MyHome.v4.PropertyManager.Schema.Document;

namespace MyHome.v4.PropertyManager.IRepository.Document
{
    public interface IDocumentContentTypeRepository : IBaseRepository<DocumentContentType>
    {
    }
}
