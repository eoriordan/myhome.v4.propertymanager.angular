﻿namespace MyHome.v4.PropertyManager.IRepository.Document
{
    public interface IDocumentRepository : IBaseRepository<Schema.Document.Document>
    {
    }
}
