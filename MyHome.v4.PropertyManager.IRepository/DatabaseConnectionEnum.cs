﻿namespace MyHome.v4.PropertyManager.IRepository
{
    /// <summary>
    /// The database connection enum specifying which database to connect to
    /// </summary>
    public enum DatabaseConnectionEnum
    {
        /// <summary>
        /// The core database
        /// </summary>
        Core,

        /// <summary>
        /// The property database
        /// </summary>
        Property,

        /// <summary>
        /// The core analytics database of the platform
        /// </summary>
        Analytics,

        /// <summary>
        /// The core statistics database of the platform
        /// </summary>
        Statistics,

        /// <summary>
        /// The core user database of the platform
        /// </summary>
        User,

        /// <summary>
        /// The core group database of the platform
        /// </summary>
        Group,

        /// <summary>
        /// The activity database of the platform
        /// </summary>
        Activity,

        /// <summary>
        /// The core document database of the platform
        /// </summary>
        Document,

        /// <summary>
        /// The core product database of the platform
        /// </summary>
        Product,

        /// <summary>
        /// The core featured spot database of the platform
        /// </summary>
        FeaturedSpot,

        /// <summary>
        /// The core contact database of the platform
        /// </summary>
        Contact,

        /// <summary>
        /// The core security database of the platform
        /// </summary>
        Security,

        /// <summary>
        /// The core advice database of the platform
        /// </summary>
        Advice
    }
}
