﻿namespace MyHome.v4.PropertyManager.IRepository.Activity
{
    public interface IActivityContactRepository : IBaseRepository<Schema.Activity.ActivityContact>
    {
    }
}
