﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema.Property;

namespace MyHome.v4.PropertyManager.IRepository.Property
{
    /// <summary>
    /// The property content repository to define the requirements for persistence of property content
    /// </summary>
    public interface IPropertyContentRepository : IBaseRepository<PropertyContent>
    {
        /// <summary>
        /// Gets the property content by property identifier.
        /// </summary>
        /// <param name="propertyId">The property identifier.</param>
        /// <returns></returns>
        List<PropertyContent> GetByPropertyId(int propertyId);

        /// <summary>
        /// Gets the property content by property ids.
        /// </summary>
        /// <param name="propertyIds">The property ids.</param>
        /// <returns></returns>
        List<PropertyContent> GetByPropertyIds(List<int> propertyIds);
    }
}
