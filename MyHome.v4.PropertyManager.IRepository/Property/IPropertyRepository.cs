﻿using System;
using System.Linq.Expressions;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.Schema;

namespace MyHome.v4.PropertyManager.IRepository.Property
{
    /// <summary>
    /// The interface to define the repository persistence methods for the database
    /// </summary>
    public interface IPropertyRepository :  IBaseRepository<Schema.Property.Property>
    {
        /// <summary>
        /// Get a single property from the database
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        Schema.Property.Property GetProperty(int propertyId);

        /// <summary>
        /// Searches for properties
        /// </summary>
        /// <param name="predicate">Search predicate</param>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortColumn">The sort column.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns>
        /// a list of properties
        /// </returns>
        PagedList<Schema.Property.Property> SearchProperties(Expression<Func<Schema.Property.Property, bool>> predicate, int page, int pageSize, SortColumn sortColumn, SortDirection sortDirection); 
    }
}
