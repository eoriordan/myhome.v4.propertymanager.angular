﻿using MyHome.v4.PropertyManager.Schema.Property;

namespace MyHome.v4.PropertyManager.IRepository.Property
{
    public interface IPropertyNoteRepository : IBaseRepository<PropertyNote>
    {
    }
}
