﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema.Contacts;

namespace MyHome.v4.PropertyManager.IRepository.Property
{
    public interface IPropertyContactRepository : IBaseRepository<PropertyContact>
    {
        List<PropertyContactWithContact> GetPropertyContactWithContacts(int propertyId);
    }
}
