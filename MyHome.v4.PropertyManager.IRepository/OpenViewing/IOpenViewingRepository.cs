﻿namespace MyHome.v4.PropertyManager.IRepository.OpenViewing
{
    public interface IOpenViewingRepository : IBaseRepository<Schema.OpenViewing.OpenViewing>
    {
    }
}
