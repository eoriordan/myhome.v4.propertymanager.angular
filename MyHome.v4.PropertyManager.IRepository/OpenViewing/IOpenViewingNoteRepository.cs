﻿using MyHome.v4.PropertyManager.Schema.OpenViewing;

namespace MyHome.v4.PropertyManager.IRepository.OpenViewing
{
    public interface IOpenViewingNoteRepository : IBaseRepository<OpenViewingNote>
    {
    }
}
