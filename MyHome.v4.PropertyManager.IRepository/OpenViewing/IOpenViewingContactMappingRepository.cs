﻿using MyHome.v4.PropertyManager.Schema.OpenViewing;

namespace MyHome.v4.PropertyManager.IRepository.OpenViewing
{
    public interface IOpenViewingContactMappingRepository : IBaseRepository<OpenViewingContactMapping>
    {
    }
}
