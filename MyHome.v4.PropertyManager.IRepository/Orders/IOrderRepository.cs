﻿using MyHome.v4.PropertyManager.Schema.Orders;

namespace MyHome.v4.PropertyManager.IRepository.Orders
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
    }
}
