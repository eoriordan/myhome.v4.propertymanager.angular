﻿using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Schema.Groups;

namespace MyHome.v4.PropertyManager.IRepository.Groups
{
    /// <summary>
    /// The interface for defining the persistence of group type to the persistent storage
    /// </summary>
    public interface IGroupTypeRepository : IBaseRepository<GroupType>
    {
    }
}
