﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Schema.Groups;

namespace MyHome.v4.PropertyManager.IRepository.Groups
{
    /// <summary>
    /// The interaface for defining the persistence of the group
    /// </summary>
    public interface IGroupRepository : IBaseRepository<Group>
    {
        /// <summary>
        /// Gets the by parent group identifier.
        /// </summary>
        /// <param name="parentGroupId">The parent group identifier.</param>
        /// <returns></returns>
        List<Group> GetByParentGroupId(int parentGroupId);
    }
}
