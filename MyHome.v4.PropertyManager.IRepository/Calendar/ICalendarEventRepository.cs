﻿namespace MyHome.v4.PropertyManager.IRepository.Calendar
{
    public interface ICalendarEventRepository : IBaseRepository<Schema.Calendar.CalendarEvent>
    {
    }
}
