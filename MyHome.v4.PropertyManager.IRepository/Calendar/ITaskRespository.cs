﻿namespace MyHome.v4.PropertyManager.IRepository.Calendar
{
    public interface ITaskRespository : IBaseRepository<Schema.Calendar.Task>
    {
    }
}
