﻿namespace MyHome.v4.PropertyManager.IRepository.Contacts
{
    public interface IContactRepository : IBaseRepository<Schema.Contacts.Contact>
    {
    }
}
