﻿namespace MyHome.v4.PropertyManager.IRepository.JobQueue
{
    public interface IJobQueueRepository : IBaseRepository<Schema.JobQueue.JobQueue>
    {
    }
}
