﻿namespace MyHome.v4.PropertyManager.IRepository.SavedSearch
{
    public interface ISavedSearchRepository : IBaseRepository<Schema.SavedSearch.SavedSearch>
    {
    }
}
