﻿namespace MyHome.v4.PropertyManager.IRepository.Auction
{
    public interface IAuctionRepository : IBaseRepository<Schema.Auction.Auction>
    {
    }
}
