﻿namespace MyHome.v4.PropertyManager.IRepository.Auction
{
    public interface IAuctionPropertyRepository : IBaseRepository<Schema.Auction.AuctionProperty>
    {
    }
}
