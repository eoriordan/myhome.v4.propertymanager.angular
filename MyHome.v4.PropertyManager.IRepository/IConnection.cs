﻿using System.Data;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.IRepository
{
    public interface IConnection
    {
        /// <summary>
        /// Get the database connection to the database server
        /// </summary>
        /// <param name="connection">The connection to get for the connection to the database</param>
        /// <returns>
        /// Return a connection to the server
        /// </returns>
        IDbConnection GetConnection(DatabaseConnectionEnum connection);

        /// <summary>
        /// Gets the dialect provider to enable us to write custom queries for the database
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        IOrmLiteDialectProvider GetDialectProvider(DatabaseConnectionEnum connection);


        /// <summary>
        /// Creates the base SQL expression which can then be passed to a select statement
        /// </summary>
        /// <typeparam name="T">The type to select from the DB</typeparam>
        /// <param name="connection">The connection.</param>
        /// <returns>
        /// Return a typed SQL expression which can be used to query the DB
        /// </returns>
        SqlExpression<T> CreateSqlExpression<T>(DatabaseConnectionEnum connection);

    }
}
