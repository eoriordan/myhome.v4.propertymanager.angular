﻿using System;
using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Schema.Users;

namespace MyHome.v4.PropertyManager.IRepository.Users
{
    /// <summary>
    /// The repository inrterface for dealing with the persistence of user sessions
    /// </summary>
    public interface IUserSessionRepository : IBaseRepository<UserSession>
    {
        /// <summary>
        /// Gets the session by session identifier.
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <returns></returns>
        UserSession GetBySessionId(Guid sessionId);

        /// <summary>
        /// Gets the by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        List<UserSession> GetByUserId(int userId);
    }
}
