﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Schema.Users;

namespace MyHome.v4.PropertyManager.IRepository.Users
{
    /// <summary>
    /// The interface for defining the persistence of users to the persistent storage
    /// </summary>
    public interface IUserRepository : IBaseRepository<User>
    {
        /// <summary>
        /// Gets the user by there username based on channel id
        /// </summary>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        User GetByUsername(int channelId, string username);

        /// <summary>
        /// Gets the users by their group identifier.
        /// </summary>
        /// <param name="groupId">The group identifier.</param>
        /// <returns></returns>
        List<User> GetByGroupId(int groupId);

        /// <summary>
        /// Gets the by group ids.
        /// </summary>
        /// <param name="groupIds">The group ids.</param>
        /// <returns></returns>
        List<User> GetByGroupIds(List<int> groupIds);

        /// <summary>
        /// Edits a user's configurable detais
        /// </summary>
        /// <param name="user">The group ids.</param>
        /// <returns></returns>
        User Edit(User user);

        /// <summary>
        /// Edits a user's configurable detais
        /// </summary>
        /// <returns></returns>
        User Login(string userName, string password);
    }
}
