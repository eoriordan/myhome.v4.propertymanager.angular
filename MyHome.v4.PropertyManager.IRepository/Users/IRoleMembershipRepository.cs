﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.Schema.Users;

namespace MyHome.v4.PropertyManager.IRepository.Users
{
    /// <summary>
    /// The interface for defining the persistence of role membership to the persistent storage
    /// </summary>
    public interface IRoleMembershipRepository : IBaseRepository<RoleMembership>
    {

        /// <summary>
        /// Gets the role memberships by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        List<RoleMembership> GetByUserId(int userId);

        /// <summary>
        /// Gets the role memberships by user ids.
        /// </summary>
        /// <param name="userIds">The user ids.</param>
        /// <returns></returns>
        List<RoleMembership> GetByUserIds(List<int> userIds);

    }
}
