﻿using MyHome.v4.PropertyManager.Schema.Users;

namespace MyHome.v4.PropertyManager.IRepository.Users
{
    public interface IRoleRepository : IBaseRepository<Role>
    {
    }
}
