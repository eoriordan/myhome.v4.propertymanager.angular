﻿namespace MyHome.v4.PropertyManager.IRepository.Product
{
    public interface IProductSubscriptionRepository : IBaseRepository<Schema.Product.ProductSubscription>
    {
    }
}
