﻿namespace MyHome.v4.PropertyManager.IRepository.Product
{
    public interface IProductRepository : IBaseRepository<Schema.Product.Product>
    {
    }
}
