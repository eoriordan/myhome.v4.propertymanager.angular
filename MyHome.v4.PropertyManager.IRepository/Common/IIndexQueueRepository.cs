﻿
namespace MyHome.v4.PropertyManager.IRepository.Common
{
    /// <summary>
    /// The interface to define the repository persistence methods for the database
    /// </summary>
    public interface IIndexQueueRepository : IBaseRepository<Schema.Common.IndexQueue>
    {
    }
}
