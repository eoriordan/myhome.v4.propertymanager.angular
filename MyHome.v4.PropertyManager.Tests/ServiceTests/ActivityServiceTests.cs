﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Moq;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.IRepository.Activity;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.Activity;
using MyHome.v4.PropertyManager.Schema.Contacts;
using MyHome.v4.PropertyManager.Schema.Property;
using MyHome.v4.PropertyManager.Service.Activity;
using MyHome.v4.PropertyManager.Types.Users;
using NUnit.Framework;

namespace MyHome.v4.PropertyManager.Tests.ServiceTests
{
    [TestFixture]
    public class ActivityServiceTests
    {
        /// <summary>
        /// Our system under test, in this case an instance of <see cref="ActivityService"/>.
        /// </summary>
        private ActivityService _sut;

        /*Mocks are set up in strict mode so any unexpected calls to methos/properties that are not mocked will result in test failure.*/
        private readonly Mock<IActivityRepository> _activityRepositoryMock = new Mock<IActivityRepository>(MockBehavior.Strict);
        private readonly Mock<IActivityContactRepository> _activityContactRepositoryMock = new Mock<IActivityContactRepository>(MockBehavior.Strict);
        private readonly Mock<IPropertyRepository> _propertyRepositoryMock = new Mock<IPropertyRepository>(MockBehavior.Strict);
        private readonly Mock<IContactService> _contactServiceMock = new Mock<IContactService>(MockBehavior.Strict);
        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>(MockBehavior.Strict);

        [SetUp]
        public void Init()
        {
            _sut = new ActivityService(
                _activityRepositoryMock.Object,
                _activityContactRepositoryMock.Object,
                _userServiceMock.Object,
                _contactServiceMock.Object,
                _propertyRepositoryMock.Object);
        }

        /// <summary>
        /// Verify that GetActivities will correctly build up the fully hydrated result.
        /// </summary>
        [Test]
        public void GetActivities_CorrectlyBuildsResult()
        {
            const int testContactId = 123;

            // Arange
            _activityRepositoryMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<Activity, bool>>>()))
                .Returns(new List<Activity>
                {
                    GetMockedActivity(1, testContactId),
                    GetMockedActivity(2, testContactId)
                });

            _activityContactRepositoryMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<ActivityContact, bool>>>()))
                .Returns(Enumerable.Empty<ActivityContact>());

            _contactServiceMock
                .SetupSequence(moq => moq.Select(It.IsAny<Expression<Func<Contact, bool>>>()))
                .Returns(new List<Types.Contacts.Contact>
                {
                    GetMockedContact(testContactId),
                    GetMockedContact(11),
                    GetMockedContact(12),
                    GetMockedContact(21),
                    GetMockedContact(22)
                });

            _userServiceMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<Schema.Users.User, bool>>>()))
                .Returns(new List<User>
                {
                    new User{ UserId = 1000 },
                    new User{ UserId = 2000 }
                });

            _propertyRepositoryMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<Property, bool>>>()))
                .Returns(new List<Property>
                {
                    GetMockedProperty(100),
                    GetMockedProperty(101),
                    GetMockedProperty(102),
                    GetMockedProperty(200),
                    GetMockedProperty(201),
                    GetMockedProperty(202),
                });

            // Act
            var result = _sut.GetContactActivity(testContactId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
            
            VerifyActivity(result, 1, testContactId);
            VerifyActivity(result, 2, testContactId);
        }

        private static void VerifyActivity(IEnumerable<Types.Activity.Activity> result, int activityId, int testContactId)
        {
            var activity = result.FirstOrDefault(x => x.ActivityId == activityId);
            Assert.IsNotNull(activity);
            Assert.IsNotNull(activity.Property);
            Assert.AreEqual(activityId * 100, activity.Property.PropertyId);
            Assert.IsNotNull(activity.User);
            Assert.AreEqual(activityId * 1000, activity.User.UserId);
            Assert.IsNotNull(activity.Contact);
            Assert.AreEqual(testContactId, activity.Contact.ContactId);
            Assert.AreEqual(ActivityTypeEnum.Note, activity.ActivityType);

            Assert.AreEqual(3, activity.Properties.Count());
            Assert.IsNotNull(activity.Properties.FirstOrDefault(x => x.PropertyId == activityId * 100));
            Assert.IsNotNull(activity.Properties.FirstOrDefault(x => x.PropertyId == activityId * 100 + 1));
            Assert.IsNotNull(activity.Properties.FirstOrDefault(x => x.PropertyId == activityId * 100 + 1));

            Assert.AreEqual(2, activity.Contacts.Count());
            Assert.IsNotNull(activity.Contacts.FirstOrDefault(x => x.ContactId == activityId * 10 + 1));
            Assert.IsNotNull(activity.Contacts.FirstOrDefault(x => x.ContactId == activityId * 10 + 2));
        }

        private static Types.Contacts.Contact GetMockedContact(int contactId)
        {
            return new Types.Contacts.Contact
            {
                ContactId = contactId,
                ContactDetails = new ContactDetails
                {
                    FirstName = "Test",
                    LastName = contactId.ToString()
                }
            };
        }

        private Property GetMockedProperty(int propertyId)
        {
            return new Property
            {
                PropertyId = propertyId,
                Address = "Property " + propertyId
            };
        }

        private static Activity GetMockedActivity(int activityId, int contactId)
        {
            return new Activity
            {
                ContactId = contactId,
                PropertyId = activityId * 100,
                CreatedOn = DateTime.Now,
                UserId = activityId * 1000,
                GroupId = 1,
                ActivityId = activityId,
                ActivityTypeId = 22,
                Title = "Test " + activityId,
                Summary = "Summary " + activityId,
                SubTitle = "Sub title " + activityId,
                Attachments = new ActivityAttachments
                {
                    Contacts = new List<ContactSnippet>
                    {
                        new ContactSnippet
                        {
                            ContactId = contactId
                        },
                        new ContactSnippet
                        {
                            ContactId = activityId * 10 + 1
                        },
                        new ContactSnippet
                        {
                            ContactId = activityId * 10 + 2
                        }
                    },
                    Properties = new List<PropertySnippet>
                    {
                        new PropertySnippet
                        {
                            PropertyId = activityId * 100
                        },
                        new PropertySnippet
                        {
                            PropertyId = activityId * 100 + 1
                        },
                        new PropertySnippet
                        {
                            PropertyId = activityId * 100 + 2
                        }
                    }
                }
            };
        }
    }
}
