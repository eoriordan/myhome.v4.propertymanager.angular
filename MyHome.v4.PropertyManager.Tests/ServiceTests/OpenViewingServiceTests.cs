﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Moq;
using MyHome.v4.PropertyManager.IRepository.Common;
using MyHome.v4.PropertyManager.IRepository.OpenViewing;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.Schema.OpenViewing;
using MyHome.v4.PropertyManager.Service.OpenViewing;
using NUnit.Framework;

namespace MyHome.v4.PropertyManager.Tests.ServiceTests
{
    [TestFixture]
    public class OpenViewingServiceTests
    {
        /// <summary>
        /// Our system under test, in this case an instance of <see cref="OpenViewingService"/>.
        /// </summary>
        private OpenViewingService _sut;

        /*Mocks are set up in strict mode so any unexpected calls to methos/properties that are not mocked will result in test failure.*/
        private readonly Mock<IPropertyRepository> _propertyRepositoryMock = new Mock<IPropertyRepository>(MockBehavior.Strict);
        private readonly Mock<IOpenViewingRepository> _openViewingRepositoryMock = new Mock<IOpenViewingRepository>(MockBehavior.Strict);
        private readonly Mock<IOpenViewingNoteRepository> _openViewingNoteRepositoryMock = new Mock<IOpenViewingNoteRepository>(MockBehavior.Strict);
        private readonly Mock<IOpenViewingContactMappingRepository> _openViewingContactMappingRepositoryMock = new Mock<IOpenViewingContactMappingRepository>(MockBehavior.Strict);
        private readonly Mock<IOpenViewingUserMappingRepository> _openViewingUserMappingRepositoryMock = new Mock<IOpenViewingUserMappingRepository>(MockBehavior.Strict);
        private readonly Mock<IIndexQueueRepository> _indexQueueRepositoryMock = new Mock<IIndexQueueRepository>(MockBehavior.Strict);
        private readonly Mock<IContactService> _contactServiceMock = new Mock<IContactService>(MockBehavior.Strict);

        [SetUp]
        public void Init()
        {
            _sut = new OpenViewingService(
                _propertyRepositoryMock.Object,
                _openViewingRepositoryMock.Object,
                _openViewingNoteRepositoryMock.Object,
                _openViewingContactMappingRepositoryMock.Object,
                _openViewingUserMappingRepositoryMock.Object,
                _indexQueueRepositoryMock.Object,
                _contactServiceMock.Object);
        }

        /// <summary>
        /// Verify that GetOpenViewings will correctly build up the fully hydrated result.
        /// </summary>
        [Test]
        public void GetOpenViewings_CorrectlyBuildResult()
        {
            // Arange
            _openViewingRepositoryMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<OpenViewing, bool>>>()))
                .Returns(new List<OpenViewing>
                {
                    GetMockedOpenViewing(100, 10),
                    GetMockedOpenViewing(100, 20)
                });

            _openViewingNoteRepositoryMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<OpenViewingNote, bool>>>()))
                .Returns(new List<OpenViewingNote>
                {
                    GetMockedOpenViewingNote(1, 10),
                    GetMockedOpenViewingNote(2, 10),
                    GetMockedOpenViewingNote(3, 20)
                });

            _openViewingContactMappingRepositoryMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<OpenViewingContactMapping, bool>>>()))
                .Returns(new List<OpenViewingContactMapping>
                {
                    new OpenViewingContactMapping{ContactId = 1, OpenViewingId = 10},
                    new OpenViewingContactMapping{ContactId = 2, OpenViewingId = 10}
                });

            _contactServiceMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<Schema.Contacts.Contact, bool>>>()))
                .Returns(new List<Types.Contacts.Contact>
                {
                    new Types.Contacts.Contact { ContactId = 1 },
                    new Types.Contacts.Contact { ContactId = 2 }
                });

            // Act
            var result = _sut.GetOpenViewings(1);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());

            var firstOpenViewing = result.FirstOrDefault(x => x.OpenViewingId == 10);
            Assert.IsNotNull(firstOpenViewing);
            Assert.AreEqual(2, firstOpenViewing.Notes.Count);
            Assert.IsNotNull(firstOpenViewing.Notes.FirstOrDefault(x => x.Id == 1));
            Assert.IsNotNull(firstOpenViewing.Notes.FirstOrDefault(x => x.Id == 2));
            Assert.AreEqual(2, firstOpenViewing.Contacts.Count);
            Assert.IsNotNull(firstOpenViewing.Contacts.FirstOrDefault(x => x.ContactId == 1));
            Assert.IsNotNull(firstOpenViewing.Contacts.FirstOrDefault(x => x.ContactId == 2));

            var secondOpenViewing = result.FirstOrDefault(x => x.OpenViewingId == 20);
            Assert.IsNotNull(secondOpenViewing);
            Assert.AreEqual(1, secondOpenViewing.Notes.Count);
            Assert.IsNotNull(secondOpenViewing.Notes.FirstOrDefault(x => x.Id == 3));
            Assert.AreEqual(0, secondOpenViewing.Contacts.Count);

            _openViewingRepositoryMock.Verify(moq => moq.Select(It.IsAny<Expression<Func<OpenViewing, bool>>>()), Times.Once);
            _openViewingNoteRepositoryMock.Verify(moq => moq.Select(It.IsAny<Expression<Func<OpenViewingNote, bool>>>()), Times.Once);
            _openViewingContactMappingRepositoryMock.Verify(moq => moq.Select(It.IsAny<Expression<Func<OpenViewingContactMapping, bool>>>()), Times.Once);
            _contactServiceMock.Verify(moq => moq.Select(It.IsAny<Expression<Func<Schema.Contacts.Contact, bool>>>()), Times.Once);
        }

        private static OpenViewing GetMockedOpenViewing(int propertyId, int openviewingId)
        {
            return new OpenViewing
            {
                PropertyId = propertyId,
                OpenViewingId = openviewingId,
                CreatedOn = DateTime.Now,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddHours(1),
                CreatedByUserId = 123,
                IsPublic = true,
                GroupId = 456,
                ModifiedOn = DateTime.Now,
                ModifiedByUserId = 789
            };
        }

        private static OpenViewingNote GetMockedOpenViewingNote(int noteId, int openviewingId)
        {
            return new OpenViewingNote
            {
                OpenViewingId = openviewingId,
                UserId = 123,
                NoteText = String.Format("Note {0}", noteId),
                DateCreated = DateTime.Now,
                Id = noteId
            };
        }
    }
}
