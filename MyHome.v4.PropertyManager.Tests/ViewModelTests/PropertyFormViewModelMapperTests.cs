﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Moq;
using MyHome.v4.Common.Schema;
using MyHome.v4.Common.Schema.Enumerations;
using MyHome.v4.PropertyManager.IService.Auction;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.IService.Property;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.Groups;
using MyHome.v4.PropertyManager.Types.Auction;
using MyHome.v4.PropertyManager.Types.Property;
using MyHome.v4.PropertyManager.Types.Users;
using MyHome.v4.PropertyManager.WebUI.Areas.Property.Mappers;
using MyHome.v4.ServiceClient.Locality;
using MyHome.v4.ServiceClient.Locality.Schema;
using NUnit.Framework;
using Group = MyHome.v4.PropertyManager.Types.Groups.Group;
using GroupPrivateConfiguration = MyHome.v4.PropertyManager.Types.Groups.GroupPrivateConfiguration;

namespace MyHome.v4.PropertyManager.Tests.ViewModelTests
{
    [TestFixture]
    public class PropertyFormViewModelMapperTests
    {
        private const int PropertyId = 1010;

        /// <summary>
        /// Our system under test, in this case an instance of <see cref="PropertyFormViewModelMapper"/>.
        /// </summary>
        private PropertyFormViewModelMapper _sut;

        private Group _group;

        /*Mocks are set up in strict mode so any unexpected calls to methos/properties that are not mocked will result in test failure.*/
        private readonly Mock<IPropertyService> _propertyServiceMock = new Mock<IPropertyService>(MockBehavior.Strict);
        private readonly Mock<IAuctionService> _auctionServiceMock = new Mock<IAuctionService>(MockBehavior.Strict);
        private readonly Mock<IContactService> _contactServiceMock = new Mock<IContactService>(MockBehavior.Strict);
        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>(MockBehavior.Strict);
        private readonly Mock<ILocalityService> _localityServiceMock = new Mock<ILocalityService>(MockBehavior.Strict);

        [SetUp]
        public void Init()
        {
            _sut = new PropertyFormViewModelMapper(
                _contactServiceMock.Object,
                _propertyServiceMock.Object,
                _auctionServiceMock.Object,
                _localityServiceMock.Object);

            _group = new Group
            {
                GroupId = 1,
                GroupPrivateConfiguration = new GroupPrivateConfiguration
                {
                    PublicTagGroups = new TagGroups(),
                    PrivateTagGroups = new TagGroups()
                }
            };

            _group.GroupPrivateConfiguration.PublicTagGroups.Add(new TagGroup
            {
                Name = "public",
                Tags = new List<Tag>{ new Tag("public_1"), new Tag("public_2") }
            });

            _group.GroupPrivateConfiguration.PrivateTagGroups.Add(new TagGroup
            {
                Name = "private",
                Tags = new List<Tag> { new Tag("private_1"), new Tag("private_2") }
            });

            _propertyServiceMock.Setup(moq => moq.GetSaleTypes(It.IsAny<PropertyClassEnum>()))
                .Returns(Enumerable.Empty<SaleType>());

            _auctionServiceMock.Setup(moq => moq.GetPropertyAuctionId(It.IsAny<int>())).Returns(PropertyId);
            _auctionServiceMock.Setup(moq => moq.GetById(It.IsAny<int>())).Returns(new Auction
            {
                AuctionId = 1
            });

            _auctionServiceMock.Setup(moq => moq.Select(It.IsAny<Expression<Func<Schema.Auction.Auction, bool>>>()))
                .Returns(Enumerable.Empty<Auction>());

            _userServiceMock.Setup(moq => moq.GetById(It.IsAny<int>())).Returns(new User
            {
                UserId = 1
            });

            _userServiceMock.Setup(moq => moq.Select(It.IsAny<Expression<Func<Schema.Users.User, bool>>>()))
                .Returns(Enumerable.Empty<User>());

            _contactServiceMock
                .Setup(moq => moq.Select(It.IsAny<Expression<Func<Schema.Contacts.Contact, bool>>>()))
                .Returns(new List<Types.Contacts.Contact>
                {
                    new Types.Contacts.Contact { ContactId = 1, TagList = "<Developer>", ContactDetails = new ContactDetails{ FirstName = "contact 1" }},
                    new Types.Contacts.Contact { ContactId = 2, TagList = "<Solicitor>", ContactDetails = new ContactDetails{ FirstName = "contact 2" } }
                });

            _localityServiceMock.Setup(moq => moq.Get(It.IsAny<GetLocalityRequest>()))
                .Returns(new LocalityService.Api.ServiceModel.Types.Locality());
        }

        [Test]
        public void ToViewModel_NewProperty_ShouldProduceEmptyVm()
        {
            var vm = _sut.ToViewModel(PropertyClassEnum.ResidentialForSale, PropertyStatusEnum.Appraisal, false, _group);

            Assert.IsNotNull(vm);
        }
    }
}
