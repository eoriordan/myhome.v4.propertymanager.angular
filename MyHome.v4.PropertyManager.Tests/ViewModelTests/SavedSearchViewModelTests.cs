﻿using MyHome.v4.PropertyManager.Schema.SavedSearch;
using MyHome.v4.PropertyManager.Web.Helpers;
using MyHome.v4.PropertyManager.WebUI.Models.Contacts;
using NUnit.Framework;
using SavedSearch = MyHome.v4.PropertyManager.Types.SavedSearch.SavedSearch;

namespace MyHome.v4.PropertyManager.Tests.ViewModelTests
{
    [TestFixture]
    public class SavedSearchViewModelTests
    {
        [Test]
        public void NotificationFrequency_ShouldDefault()
        {
            var savedSearch = new SavedSearch
            {
                NotificationSettings = new NotificationSetting
                {
                    Frequency = "NaN"
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual(SearchFormHelpers.SavedSearchFrequency[0], vm.NotificationFrequency);
        }

        [Test]
        public void EmptySearchProperties_NoPriceBedsOrSize()
        {
            var savedSearch = new SavedSearch();

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.IsNull(vm.Beds);
            Assert.IsNull(vm.Price);
            Assert.IsNull(vm.Size);

            savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest()
            };

            vm = new SavedSearchViewModel(savedSearch);
            Assert.IsNull(vm.Beds);
            Assert.IsNull(vm.Price);
            Assert.IsNull(vm.Size);
        }

        [Test]
        public void Beds_NoMin()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MaxBeds = 5
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("< 5 beds", vm.Beds);
        }

        [Test]
        public void Beds_NoMax()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MinBeds = 5
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("5 + beds", vm.Beds);
        }

        [Test]
        public void Beds_MinAndMax()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MinBeds = 1,
                    MaxBeds = 5
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("1-5 beds", vm.Beds);
        }

        [Test]
        public void Size_NoMin()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MaxSize = 150
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("< 150 m² (1,614 ft²)", vm.Size);
        }

        [Test]
        public void Size_NoMax()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MinSize = 100
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("100 m² (1,076 ft²) +", vm.Size);
        }

        [Test]
        public void Size_MinAndMax()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MinSize = 100,
                    MaxSize = 150
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("100 m² (1,076 ft²)-150 m² (1,614 ft²)", vm.Size);
        }

        [Test]
        [SetCulture("en-IE")]
        public void Price_NoMin()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MaxPrice = 100000
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("< €100,000", vm.Price);
        }

        [Test]
        [SetCulture("en-IE")]
        public void Price_NoMax()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MinPrice = 50000
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("€50,000 +", vm.Price);
        }

        [Test]
        [SetCulture("en-IE")]
        public void Price_MinAndMax()
        {
            var savedSearch = new SavedSearch
            {
                SearchPropertiesRequest = new SearchPropertiesRequest
                {
                    MinPrice = 50000,
                    MaxPrice = 100000
                }
            };

            var vm = new SavedSearchViewModel(savedSearch);
            Assert.AreEqual("€50,000-€100,000", vm.Price);
        }
    }
}
