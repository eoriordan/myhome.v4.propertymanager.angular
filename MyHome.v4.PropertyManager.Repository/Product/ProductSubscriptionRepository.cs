﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Product;

namespace MyHome.v4.PropertyManager.Repository.Product
{
    public class ProductSubscriptionRepository : BaseRepository<Schema.Product.ProductSubscription>, IProductSubscriptionRepository
    {
        public ProductSubscriptionRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
