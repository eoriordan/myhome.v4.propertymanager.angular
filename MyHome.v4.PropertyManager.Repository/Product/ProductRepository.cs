﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Product;

namespace MyHome.v4.PropertyManager.Repository.Product
{
    public class ProductRepository : BaseRepository<Schema.Product.Product>, IProductRepository
    {
        public ProductRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
