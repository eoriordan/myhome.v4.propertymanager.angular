﻿using System;
using System.Collections.Generic;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Groups;
using MyHome.v4.PropertyManager.Schema.Groups;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.Repository.Groups
{
    /// <summary>
    /// The repository for the persistence of groups
    /// </summary>
    public class GroupRepository : BaseRepository<Group>, IGroupRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public GroupRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Group)
        {
        }

        /// <summary>
        /// Gets the by parent group identifier.
        /// </summary>
        /// <param name="parentGroupId">The parent group identifier.</param>
        /// <returns></returns>
        public List<Group> GetByParentGroupId(int parentGroupId)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<Group>(q => q.ParentGroupId == parentGroupId);
            }
        }

        /// <summary>
        /// Persists the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public override Group Persist(Group value)
        {
            UpdateBeforePersist(value);
            return base.Persist(value);
        }

        public override List<Group> PersistMany(List<Group> values)
        {
            foreach (var group in values)
            {
                UpdateBeforePersist(group);
            }
            return base.PersistMany(values);
        }

        /// <summary>
        /// Updates the before persist.
        /// </summary>
        /// <param name="value">The value.</param>
        private void UpdateBeforePersist(Group value)
        {
            if (value.GroupId == 0)
            {
                value.CreatedOn = DateTime.Now;
                if (string.IsNullOrWhiteSpace(value.Name))
                {
                    value.Name = string.Empty;
                }
            }
        }
    }
}
