﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Groups;
using MyHome.v4.PropertyManager.Schema.Groups;

namespace MyHome.v4.PropertyManager.Repository.Groups
{
    /// <summary>
    /// The repository for the persistence of group types to the persistent storage
    /// </summary>
    public class GroupTypeRepository : BaseRepository<GroupType>, IGroupTypeRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupTypeRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public GroupTypeRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Group)
        {
        }
    }
}
