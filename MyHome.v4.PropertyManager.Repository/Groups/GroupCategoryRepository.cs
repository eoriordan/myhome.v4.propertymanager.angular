﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Groups;
using MyHome.v4.PropertyManager.Schema.Groups;

namespace MyHome.v4.PropertyManager.Repository.Groups
{
    /// <summary>
    /// The repository for the persistence of group categories to the database
    /// </summary>
    public class GroupCategoryRepository : BaseRepository<GroupCategory>, IGroupCategoryRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GroupCategoryRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public GroupCategoryRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Group)
        {
        }
    }
}
