﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Activity;

namespace MyHome.v4.PropertyManager.Repository.Activity
{
    public class ActivityContactRepository : BaseRepository<Schema.Activity.ActivityContact>, IActivityContactRepository
    {
        public ActivityContactRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
