﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Activity;

namespace MyHome.v4.PropertyManager.Repository.Activity
{
    public class ActivityRepository : BaseRepository<Schema.Activity.Activity>, IActivityRepository
    {
        public ActivityRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
