﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.Schema.Property;

namespace MyHome.v4.PropertyManager.Repository.Property
{
    public class PropertyOfferRepository : BaseRepository<PropertyOffer>, IPropertyOfferRepository
    {
        public PropertyOfferRepository(IConnection connection)
            : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
