﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.Schema.Property;

namespace MyHome.v4.PropertyManager.Repository.Property
{
    public class PropertyNoteRepository : BaseRepository<PropertyNote>, IPropertyNoteRepository
    {
        public PropertyNoteRepository(IConnection connection)
            : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
