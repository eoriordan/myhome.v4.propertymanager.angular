﻿using System.Collections.Generic;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.Schema.Contacts;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.Repository.Property
{
    public class PropertyContactRepository : BaseRepository<PropertyContact>, IPropertyContactRepository
    {
        public PropertyContactRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Contact)
        {
        }

        public List<PropertyContactWithContact> GetPropertyContactWithContacts(int propertyId)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();

                var propertyContactsWithContact = 
                    cn.Select<PropertyContactWithContact>(string.Format(@"SELECT * FROM PropertyContact
                                      INNER JOIN Contact ON PropertyContact.ContactID = Contact.ContactID
                                      WHERE PropertyID = {0}", propertyId));

                if (propertyContactsWithContact.Any())
                    return propertyContactsWithContact;
                return new List<PropertyContactWithContact>();
            }
        }
    }
}
