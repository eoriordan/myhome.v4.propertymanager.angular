﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.Schema.Property;

namespace MyHome.v4.PropertyManager.Repository.Property
{
    public class SaleTypeRepository : BaseRepository<SaleType>, ISaleTypeRepository
    {
        public SaleTypeRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Contact)
        {
        }
    }
}
