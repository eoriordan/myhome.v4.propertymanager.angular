﻿using System;
using System.Linq.Expressions;
using MyHome.v4.Common.Schema;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.Schema;
using ServiceStack.OrmLite;
using SortDirection = MyHome.v4.PropertyManager.Schema.SortDirection;

namespace MyHome.v4.PropertyManager.Repository.Property
{
    /// <summary>
    /// The repository to deal with the persistence of properties in the myhome database
    /// </summary>
    public class PropertyRepository : BaseRepository<Schema.Property.Property>, IPropertyRepository
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public PropertyRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Core)
        {
        }

        /// <summary>
        /// Get a single propety from the database
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public Schema.Property.Property GetProperty(int propertyId)
        {
            return GetById(propertyId);
        }

        /// <summary>
        /// Searches for properties using values from search poco parameter
        /// </summary>
        /// <param name="predicate">Search predciate</param>
        /// <param name="page">The page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sortColumn">The sort column.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns>
        /// A list of properties.
        /// </returns>
        public PagedList<Schema.Property.Property> SearchProperties(Expression<Func<Schema.Property.Property, bool>> predicate, int page, int pageSize, SortColumn sortColumn, SortDirection sortDirection)
        {
            var pagedResponse = new PagedList<Schema.Property.Property>();
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                
                var skip = page > 1 ? (page - 1)*pageSize : 0;
                var rows = pageSize;

                //Create a sql expression so we can create the request
                var sqlExpression = Connection.CreateSqlExpression<Schema.Property.Property>(DatabaseConnection);

                //Add the where clause
                sqlExpression = sqlExpression.Where(predicate);

                //Limit the expression
                sqlExpression = sqlExpression.Limit(skip, rows);

                switch (sortColumn)
                {
                    //TODO: This is not the full implementation, we need to take the logic from the front end search not the PM
                    case SortColumn.AddedDate:
                        sqlExpression = sortDirection == SortDirection.Asc ? sqlExpression.OrderBy(m => m.CreatedOnDate) : sqlExpression.OrderByDescending(m => m.CreatedOnDate);
                        break;
                    case SortColumn.ModifiedDate:
                        sqlExpression = sortDirection == SortDirection.Asc ? sqlExpression.OrderBy(m => m.ModifiedOnDate) : sqlExpression.OrderByDescending(m => m.ModifiedOnDate);
                        break;
                    case SortColumn.RefreshedDate:
                        sqlExpression = sortDirection == SortDirection.Asc ? sqlExpression.OrderBy(m => m.RefreshedOn) : sqlExpression.OrderByDescending(m => m.RefreshedOn);
                        break;
                    case SortColumn.Address:
                        sqlExpression = sortDirection == SortDirection.Asc ? sqlExpression.OrderBy(m => m.DisplayAddress) : sqlExpression.OrderByDescending(m => m.DisplayAddress);
                        break;
                    case SortColumn.Price:
                        sqlExpression = sortDirection == SortDirection.Asc ? sqlExpression.OrderBy(m => m.MinSortPrice).ThenBy(m=>m.MinRentalPrice) : sqlExpression.OrderByDescending(m => m.MinSortPrice).ThenByDescending(m => m.MinRentalPrice);
                        break;
                }

                var items = cn.Select(sqlExpression);
                
                var totalCount = cn.Count(predicate);

                pagedResponse.AddRange(items);
                pagedResponse.Page = page;
                pagedResponse.PageSize = pageSize;
                pagedResponse.ResultCount = (int)totalCount;
            }
            return pagedResponse;
        }
    }
}
