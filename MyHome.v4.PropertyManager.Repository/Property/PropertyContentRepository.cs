﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Schema.Property;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.Repository.Property
{
    /// <summary>
    /// The concrete implementation of the property content repository to connect to SQL Server
    /// </summary>
    public class PropertyContentRepository : BaseRepository<PropertyContent>, IPropertyContentRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyContentRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public PropertyContentRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Core)
        {
        }


        /// <summary>
        /// Gets the property content by property identifier.
        /// </summary>
        /// <param name="propertyId">The property identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<PropertyContent> GetByPropertyId(int propertyId)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<PropertyContent>(x => x.PropertyId == propertyId);
            }  
        }

        /// <summary>
        /// Gets the property content by property ids.
        /// </summary>
        /// <param name="propertyIds">The property ids.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<PropertyContent> GetByPropertyIds(List<int> propertyIds)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<PropertyContent>(x => Sql.In(x.PropertyId, propertyIds));
            }  
        }
    }
}
