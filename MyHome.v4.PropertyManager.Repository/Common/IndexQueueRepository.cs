﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Common;

namespace MyHome.v4.PropertyManager.Repository.Common
{
    public class IndexQueueRepository : BaseRepository<Schema.Common.IndexQueue>, IIndexQueueRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IndexQueueRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public IndexQueueRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Core)
        {
        }
    }
}
