﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Orders;
using MyHome.v4.PropertyManager.Schema.Orders;

namespace MyHome.v4.PropertyManager.Repository.Orders
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.FeaturedSpot)
        {
        }
    }
}
