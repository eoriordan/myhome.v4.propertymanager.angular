﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MyHome.v4.PropertyManager.IRepository;

namespace MyHome.v4.PropertyManager.Repository
{
    public abstract class BaseAbstractRepository<T> : IBaseRepository<T> where T : class
    {
        /// <summary>
        /// Gets or sets the connection.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        protected IConnection Connection { get; set; }

        /// <summary>
        /// Gets or sets the database connection.
        /// </summary>
        /// <value>
        /// The database connection.
        /// </value>
        protected DatabaseConnectionEnum DatabaseConnection { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseAbstractRepository{T}" /> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="databaseConnection">The database connection.</param>
        protected BaseAbstractRepository(IConnection connection, DatabaseConnectionEnum databaseConnection)
        {
            Connection = connection;
            DatabaseConnection = databaseConnection;
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public abstract T GetById(int id);


        /// <summary>
        /// Gets the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public abstract Task<T> GetByIdAsync(int id);


        /// <summary>
        /// Gets the by ids.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        public abstract IEnumerable<T> GetByIds(List<int> ids);


        /// <summary>
        /// Gets the by ids asynchronous.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        public abstract Task<IEnumerable<T>> GetByIdsAsync(List<int> ids);


        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerable<T> GetAll(int page = 1, int pageSize = 20);


        /// <summary>
        /// Gets all asynchronous.
        /// </summary>
        /// <returns></returns>
        public abstract Task<IEnumerable<T>> GetAllAsync();


        /// <summary>
        /// Persists the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public abstract T Persist(T value);


        /// <summary>
        /// Persists the many.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public abstract List<T> PersistMany(List<T> values);


        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public abstract bool Delete(int id);


        /// <summary>
        /// Deletes the many.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        public abstract bool DeleteMany(List<int> ids);

        /// <summary>
        /// Get a count from the persistent storage
        /// </summary>
        /// <returns>Returns a count from the persistent storage</returns>
        public abstract long Count(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public abstract IEnumerable<T> Select(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public abstract IEnumerable<T> Select(Expression<Func<T, bool>> expression, int page);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="page"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        public abstract IEnumerable<T> Select(Expression<Func<T, bool>> expression, int page, int pageCount);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract bool HardDelete(int id);
    }
}
