﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.OpenViewing;
using MyHome.v4.PropertyManager.Schema.OpenViewing;

namespace MyHome.v4.PropertyManager.Repository.OpenViewing
{
    public class OpenViewingNoteRepository : BaseRepository<OpenViewingNote>, IOpenViewingNoteRepository
    {
        public OpenViewingNoteRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
