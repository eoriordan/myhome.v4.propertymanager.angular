﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.OpenViewing;
using MyHome.v4.PropertyManager.Schema.OpenViewing;

namespace MyHome.v4.PropertyManager.Repository.OpenViewing
{
    public class OpenViewingContactMappingRepository : BaseRepository<OpenViewingContactMapping>, IOpenViewingContactMappingRepository
    {
        public OpenViewingContactMappingRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
