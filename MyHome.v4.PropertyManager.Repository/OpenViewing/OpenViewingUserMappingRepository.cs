﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.OpenViewing;
using MyHome.v4.PropertyManager.Schema.OpenViewing;

namespace MyHome.v4.PropertyManager.Repository.OpenViewing
{
    public class OpenViewingUserMappingRepository : BaseRepository<OpenViewingUserMapping>, IOpenViewingUserMappingRepository
    {
        public OpenViewingUserMappingRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
