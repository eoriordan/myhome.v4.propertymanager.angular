﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.OpenViewing;

namespace MyHome.v4.PropertyManager.Repository.OpenViewing
{
    public class OpenViewingRepository : BaseRepository<Schema.OpenViewing.OpenViewing>, IOpenViewingRepository
    {
        public OpenViewingRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
