﻿using System.Collections.Generic;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Advice;
using MyHome.v4.PropertyManager.Schema.Advice;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.Repository.Advice
{
    public class ArticleRepository : BaseRepository<Article>, IArticleRepository
    {
        public ArticleRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Advice)
        {
        }

        public List<Article> GetByCategory(int categoryId, int page, int pageSize)
        {
            return Select(x => x.CategoryId == categoryId, page, pageSize).ToList();
        }

        public IEnumerable<Article> SearchArticles(string searchString)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<Article>(artic => artic.SearchAbleContent.Contains(searchString));
            }
        }
    }
}
