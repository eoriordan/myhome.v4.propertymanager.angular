﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Advice;
using MyHome.v4.PropertyManager.Schema.Advice;

namespace MyHome.v4.PropertyManager.Repository.Advice
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Advice)
        {
        }
    }
}
