﻿using System.Collections.Generic;
using System.Linq;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Advice;
using MyHome.v4.PropertyManager.Schema.Advice;

namespace MyHome.v4.PropertyManager.Repository.Advice
{
    public class ArticleCategoryMappingRepository : BaseRepository<ArticleCategoryMapping>, IArticleCategoryMappingRepository
    {
        public ArticleCategoryMappingRepository(IConnection connection)
            : base(connection, DatabaseConnectionEnum.Advice)
        {
        }

        public List<ArticleCategoryMapping> GetByArticleId(int articleId)
        {
            return Select(x => x.ArticleId == articleId).ToList();
        }
    }
}
