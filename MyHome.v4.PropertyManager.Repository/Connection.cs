﻿using System.Data;
using MyHome.v4.PropertyManager.IRepository;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using ServiceStack.Serialization;

namespace MyHome.v4.PropertyManager.Repository
{

    public class Connection : IConnection
    {

        public Connection()
        {
            SqliteDialect.Provider.StringSerializer = new JsonDataContractSerializer();
            SqlServerDialect.Provider.StringSerializer = new JsonDataContractSerializer();
        }

        public IDbConnection GetConnection(DatabaseConnectionEnum connection)
        {
            return new OrmLiteConnectionFactory(System.Configuration.ConfigurationManager.AppSettings[connection.ToString()], SqlServerDialect.Provider).CreateDbConnection();
        }

        /// <summary>
        /// Gets the dialect provider.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public IOrmLiteDialectProvider GetDialectProvider(DatabaseConnectionEnum connection)
        {
            return SqlServerDialect.Provider;
        }

        /// <summary>
        /// Creates the SQL expression.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public SqlExpression<T> CreateSqlExpression<T>(DatabaseConnectionEnum connection)
        {
            var sqlExpression = new SqlServerExpression<T>(GetDialectProvider(connection)) as SqlExpression<T>;
            return sqlExpression;
        }
    }
}
