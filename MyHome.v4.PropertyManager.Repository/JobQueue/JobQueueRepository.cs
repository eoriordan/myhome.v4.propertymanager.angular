﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.JobQueue;

namespace MyHome.v4.PropertyManager.Repository.JobQueue
{
    public class JobQueueRepository : BaseRepository<Schema.JobQueue.JobQueue>, IJobQueueRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobQueueRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public JobQueueRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Group)
        {
        }
    }
}
