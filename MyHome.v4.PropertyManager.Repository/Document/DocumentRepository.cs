﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Document;

namespace MyHome.v4.PropertyManager.Repository.Document
{
    public class DocumentRepository : BaseRepository<Schema.Document.Document>, IDocumentRepository
    {
        public DocumentRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
