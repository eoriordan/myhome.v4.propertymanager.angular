﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Document;
using MyHome.v4.PropertyManager.Schema.Document;

namespace MyHome.v4.PropertyManager.Repository.Document
{
    public class DocumentContentTypeRepository : BaseRepository<DocumentContentType>, IDocumentContentTypeRepository
    {
        public DocumentContentTypeRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
