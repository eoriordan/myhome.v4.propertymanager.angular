﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Contacts;
using MyHome.v4.PropertyManager.Schema.Contacts;

namespace MyHome.v4.PropertyManager.Repository.Contacts
{
    public class ContactRepository : BaseRepository<Contact>, IContactRepository
    {
        public ContactRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Contact)
        {
        }
    }
}
