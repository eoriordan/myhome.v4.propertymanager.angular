﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.PropertyAccount;
using MyHome.v4.PropertyManager.Schema.PropertyAccount;

namespace MyHome.v4.PropertyManager.Repository.PropertyAccount
{
    public class PropertyAccountTransactionTypeRepository : BaseRepository<PropertyAccountTransactionType>, IPropertyAccountTransactionTypeRepository
    {
        public PropertyAccountTransactionTypeRepository(IConnection connection, DatabaseConnectionEnum connectionEnum) : base(connection, connectionEnum)
        {
        }
    }
}
