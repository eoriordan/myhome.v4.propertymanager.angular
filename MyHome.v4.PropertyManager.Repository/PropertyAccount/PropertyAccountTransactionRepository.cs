﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.PropertyAccount;
using MyHome.v4.PropertyManager.Schema.PropertyAccount;

namespace MyHome.v4.PropertyManager.Repository.PropertyAccount
{
    public class PropertyAccountTransactionRepository : BaseRepository<PropertyAccountTransaction>, IPropertyAccountTransactionRepository
    {
        public PropertyAccountTransactionRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Core)
        {
        }
    }
}
