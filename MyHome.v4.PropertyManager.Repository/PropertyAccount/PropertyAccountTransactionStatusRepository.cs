﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.PropertyAccount;
using MyHome.v4.PropertyManager.Schema.PropertyAccount;

namespace MyHome.v4.PropertyManager.Repository.PropertyAccount
{
    public class PropertyAccountTransactionStatusRepository : BaseRepository<PropertyAccountTransactionStatus>, IPropertyAccountTransactionStatusRepository
    {
        public PropertyAccountTransactionStatusRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Core)
        {
        }
    }
}
