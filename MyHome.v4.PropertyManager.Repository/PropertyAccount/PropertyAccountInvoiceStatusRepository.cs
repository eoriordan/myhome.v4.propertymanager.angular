﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.PropertyAccount;
using MyHome.v4.PropertyManager.Schema.PropertyAccount;

namespace MyHome.v4.PropertyManager.Repository.PropertyAccount
{
    public class PropertyAccountInvoiceStatusRepository : BaseRepository<PropertyAccountInvoiceStatus>, IPropertyAccountInvoiceStatusRepository
    {
        public PropertyAccountInvoiceStatusRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Core)
        {
        }
    }
}
