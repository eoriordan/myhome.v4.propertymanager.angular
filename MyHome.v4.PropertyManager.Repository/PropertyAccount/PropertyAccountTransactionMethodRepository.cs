﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.PropertyAccount;
using MyHome.v4.PropertyManager.Schema.PropertyAccount;

namespace MyHome.v4.PropertyManager.Repository.PropertyAccount
{
    public class PropertyAccountTransactionMethodRepository : BaseRepository<PropertyAccountTransactionMethod>, IPropertyAccountTransactionMethodRepository
    {
        public PropertyAccountTransactionMethodRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Core)
        {
        }
    }
}
