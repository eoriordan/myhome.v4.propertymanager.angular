﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.Schema.Users;

namespace MyHome.v4.PropertyManager.Repository.Users
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.User)
        {
        }
    }
}
