﻿using System.Collections.Generic;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.Schema.Users;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.Repository.Users
{
    /// <summary>
    /// The repository for the persistence of role memberships to the persistent storage
    /// </summary>
    public class RoleMembershipRepository : BaseRepository<RoleMembership>, IRoleMembershipRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoleMembershipRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public RoleMembershipRepository(IConnection connection) : base(connection , DatabaseConnectionEnum.Core)
        {
        }

        /// <summary>
        /// Gets the role memberships by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<RoleMembership> GetByUserId(int userId)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<RoleMembership>(q=>q.UserId == userId);
            }
        }

        /// <summary>
        /// Gets the role memberships by user ids.
        /// </summary>
        /// <param name="userIds">The user ids.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<RoleMembership> GetByUserIds(List<int> userIds)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<RoleMembership>(q => Sql.In(q.UserId, userIds));
            }
        }
    }
}
