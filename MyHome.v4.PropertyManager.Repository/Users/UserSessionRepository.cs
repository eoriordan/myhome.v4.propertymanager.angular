﻿using System;
using System.Collections.Generic;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Schema.Users;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.Repository.Users
{
    /// <summary>
    /// The repository to deal with the persistence of user sessions
    /// </summary>
    public class UserSessionRepository : BaseRepository<UserSession>, IUserSessionRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserSessionRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public UserSessionRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Core)
        {
        }

        /// <summary>
        /// Persists the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public override UserSession Persist(UserSession value)
        {
            UpdateBeforePersist(value);
            return base.Persist(value);
        }

        /// <summary>
        /// Persists the many.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public override List<UserSession> PersistMany(List<UserSession> values)
        {
            foreach (var userSession in values)
            {
                UpdateBeforePersist(userSession);
            }
            return base.PersistMany(values);
        }

        /// <summary>
        /// Gets the session by session identifier.
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public UserSession GetBySessionId(Guid sessionId)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Single<UserSession>(m => m.SessionId == sessionId);
            }
        }

        /// <summary>
        /// Gets the by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public List<UserSession> GetByUserId(int userId)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<UserSession>(m => m.UserId == userId);
            }
        }


        /// <summary>
        /// Updates the before persist.
        /// </summary>
        /// <param name="value">The value.</param>
        private void UpdateBeforePersist(UserSession value)
        {
            if (value.UserSessionId == 0)
            {
                value.CreatedOn = DateTime.Now;
                value.LastActivityOn = DateTime.Now;
            }
        }

    }
}
