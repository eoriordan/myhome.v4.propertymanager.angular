﻿using System;
using System.Collections.Generic;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.Schema;
using MyHome.v4.PropertyManager.Schema.Users;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.Repository.Users
{
    /// <summary>
    /// The repository for the persistence of users to the persistent storage
    /// </summary>
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        public UserRepository(IConnection connection)
            : base(connection, DatabaseConnectionEnum.Core)
        {
        }


        /// <summary>
        /// Gets the user by there username.
        /// </summary>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public User GetByUsername(int channelId, string username)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Single<User>(q => q.Username == username);
            }
        }

        /// <summary>
        /// Gets the users by their group identifier.
        /// </summary>
        /// <param name="groupId">The group identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<User> GetByGroupId(int groupId)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<User>(q => q.GroupId == groupId);
            }
        }

        /// <summary>
        /// Gets the users by their group identifier.
        /// </summary>
        /// <param name="groupIds">The group identifiers.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public List<User> GetByGroupIds(List<int> groupIds)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<User>(q => Sql.In(q.GroupId, groupIds));
            }
        }

        /// <summary>
        /// Persists the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public override User Persist(User value)
        {
            UpdateBeforePersist(value);
            return base.Persist(value);
        }

        /// <summary>
        /// Persists the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public User Edit(User value)
        {
            UpdateBeforePersist(value);

            //We only allow a user to edit some details on their account
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                cn.UpdateOnly(new User { ContactDetails = value.ContactDetails, UserConfiguration = value.UserConfiguration},
                    p => new { p.ContactDetails, p.UserConfiguration },
                    p => p.UserId == value.UserId);

                return GetById(value.UserId);
            }
        }

        /// <summary>
        /// Persists the many.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public override List<User> PersistMany(List<User> values)
        {
            foreach (var value in values)
            {
                UpdateBeforePersist(value);
            }
            return base.PersistMany(values);
        }

        /// <summary>
        /// Update the user before persist to ensure they have the correct fields set
        /// </summary>
        /// <param name="value"></param>
        public void UpdateBeforePersist(User value)
        {
            if (value.UserId == 0)
            {
                value.CreatedOn = DateTime.Now;
                value.ModifiedOn = DateTime.Now;
            }
            else
            {
                value.ModifiedOn = DateTime.Now;
            }
        }

        public User Login(string userName, string password)
        {
            throw new NotImplementedException();
        }
    }
}
