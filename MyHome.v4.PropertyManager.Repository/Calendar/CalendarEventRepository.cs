﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Calendar;

namespace MyHome.v4.PropertyManager.Repository.Calendar
{
    public class CalendarEventRepository : BaseRepository<Schema.Calendar.CalendarEvent>, ICalendarEventRepository
    {
        public CalendarEventRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Contact)
        {
        }
    }
}
