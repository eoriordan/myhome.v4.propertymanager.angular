﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Calendar;

namespace MyHome.v4.PropertyManager.Repository.Calendar
{
    public class TaskRepository : BaseRepository<Schema.Calendar.Task>, ITaskRespository
    {
        public TaskRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Contact)
        {
        }
    }
}
