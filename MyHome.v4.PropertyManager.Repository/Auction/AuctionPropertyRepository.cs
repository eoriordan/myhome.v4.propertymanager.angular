﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Auction;

namespace MyHome.v4.PropertyManager.Repository.Auction
{
    public class AuctionPropertyRepository : BaseRepository<Schema.Auction.AuctionProperty>, IAuctionPropertyRepository
    {
        public AuctionPropertyRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Contact)
        {
        }
    }
}
