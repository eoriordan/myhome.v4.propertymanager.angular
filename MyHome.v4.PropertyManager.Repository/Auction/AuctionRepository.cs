﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Auction;

namespace MyHome.v4.PropertyManager.Repository.Auction
{
    public class AuctionRepository : BaseRepository<Schema.Auction.Auction>, IAuctionRepository
    {
        public AuctionRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Contact)
        {
        }
    }
}
