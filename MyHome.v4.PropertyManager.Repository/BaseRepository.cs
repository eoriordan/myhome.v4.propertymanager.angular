﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MyHome.v4.PropertyManager.IRepository;
using ServiceStack.OrmLite;

namespace MyHome.v4.PropertyManager.Repository
{
    /// <summary>
    /// The base repository inherited by all other repositories to provide based functionaliy
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseRepository<T> : BaseAbstractRepository<T> where T : class
    {
        /// <summary>
        /// The connection to use
        /// </summary>
        private readonly DatabaseConnectionEnum _connectionEnum;

        /// <summary>
        /// nitializes a new instance of the <see cref="BaseRepository{T}"/> class.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="connectionEnum"></param>
        public BaseRepository(IConnection connection, DatabaseConnectionEnum connectionEnum)
            : base(connection, connectionEnum)
        {
            Connection = connection;
            _connectionEnum = connectionEnum;
        }


        /// <summary>
        /// Get the poco of type T from the persistent storage
        /// </summary>
        /// <param name="id">The uniqe id of the property to load from the persistent storage</param>
        /// <returns>Returns a poco of type T</returns>
        public override T GetById(int id)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.SingleById<T>(id);
            }
        }

        /// <summary>
        /// Gets the by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Task<T> GetByIdAsync(int id)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.SingleByIdAsync<T>(id);
            }
        }


        /// <summary>
        /// Get a list of poco's of type T from the persistent storage based on there unique ids
        /// </summary>
        /// <param name="ids">The unique ids to load the items from there persistent storage</param>
        /// <returns>Returns a list of poco's of type T</returns>
        public override IEnumerable<T> GetByIds(List<int> ids)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.SelectByIds<T>(ids);
            }
        }

        /// <summary>
        /// Gets the by ids asynchronous.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override async Task<IEnumerable<T>> GetByIdsAsync(List<int> ids)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return await cn.SelectByIdsAsync<T>(ids);
            }
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<T> GetAll(int page = 1, int pageSize = 20)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.Select<T>();
            }
        }

        /// <summary>
        /// Gets all asynchronous.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override async Task<IEnumerable<T>> GetAllAsync()
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return await cn.SelectAsync<T>();
            }
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool Delete(int id)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.DeleteById<T>(id) == 1;
            }
        }

        /// <summary>
        /// Deletes the many.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool DeleteMany(List<int> ids)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                //We assume a success if all of the ids were deleted so the counts were the same
                return cn.DeleteByIds<T>(ids) == ids.Count;
            }
        }

        /// <summary>
        /// Get a count from the persistent storage
        /// </summary>
        /// <returns>Returns a count from the persistent storage</returns>
        public override long Count(Expression<Func<T, bool>> expression)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();

                var sqlExpression = Connection.CreateSqlExpression<T>(_connectionEnum);
                sqlExpression = sqlExpression.Where(expression);
                return cn.Count(sqlExpression);
            }
        }

        /// <summary>
        /// Persists the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override T Persist(T value)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                cn.Save(value);
                return value;
            }
        }

        /// <summary>
        /// Persists the many.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override List<T> PersistMany(List<T> values)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                cn.SaveAll(values);
                return values;
            }
        }

        public override IEnumerable<T> Select(Expression<Func<T, bool>> expression)
        {
            return Select(expression, 1, 5000);
        }

        public override IEnumerable<T> Select(Expression<Func<T, bool>> expression, int page)
        {
            return Select(expression, page, 5000);
        }

        /// <summary>
        /// Performs select statement
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="page"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override IEnumerable<T> Select(Expression<Func<T, bool>> expression, int page, int pageCount)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();

                var skip = page > 1 ? (page - 1) * pageCount : 0;
                var rows = pageCount;

                var sqlExpression = Connection.CreateSqlExpression<T>(_connectionEnum);
                sqlExpression = sqlExpression.Where(expression);
                sqlExpression = sqlExpression.Limit(skip, rows);
                return cn.Select(sqlExpression);
            }
        }

        /// <summary>
        /// Completely deleted record from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override bool HardDelete(int id)
        {
            using (var cn = Connection.GetConnection(DatabaseConnection))
            {
                cn.Open();
                return cn.DeleteById<T>(id) == 1;
            }
        }
    }
}
