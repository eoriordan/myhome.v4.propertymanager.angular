﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.FeaturedProduct;
using MyHome.v4.PropertyManager.Schema.FeaturedProduct;

namespace MyHome.v4.PropertyManager.Repository.FeaturedProduct
{
    public class FeaturedProductRepository : BaseRepository<FeaturedSpot>, IFeaturedProductRepository
    {
        public FeaturedProductRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.FeaturedSpot)
        {
        }
    }
}
