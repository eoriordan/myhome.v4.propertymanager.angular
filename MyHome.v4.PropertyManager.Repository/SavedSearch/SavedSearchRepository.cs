﻿using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.SavedSearch;

namespace MyHome.v4.PropertyManager.Repository.SavedSearch
{
    public class SavedSearchRepository : BaseRepository<Schema.SavedSearch.SavedSearch>, ISavedSearchRepository
    {
        public SavedSearchRepository(IConnection connection) : base(connection, DatabaseConnectionEnum.Property)
        {
        }
    }
}
