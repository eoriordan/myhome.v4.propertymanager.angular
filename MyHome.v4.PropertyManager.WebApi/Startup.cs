﻿using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(MyHome.v4.PropertyManager.WebApi.Startup))]
namespace MyHome.v4.PropertyManager.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            var config = new HttpConfiguration();
            ConfigureDI(app);
            ConfigureOAuth(app);
            
            WebApiConfig.Register(config);
            app.UseWebApi(config);
        }
    }
}