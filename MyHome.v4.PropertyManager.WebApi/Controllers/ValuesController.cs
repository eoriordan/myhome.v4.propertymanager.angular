﻿using System.Collections.Generic;
using System.Web.Http;

namespace MyHome.v4.PropertyManager.WebApi.Controllers
{
    [Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            var user = User as System.Security.Claims.ClaimsPrincipal;
            var claims = user.Claims;
            return new string[] { "value1", "value2" };
        }
    }
}