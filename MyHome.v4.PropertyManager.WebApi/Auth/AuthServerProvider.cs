﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Schema.SavedSearch;
using MyHome.v4.PropertyManager.Types.Users;
using MyHome.v4.PropertyManager.WebUI;

namespace MyHome.v4.PropertyManager.WebApi.Auth
{
    public class AuthServerProvider : OAuthAuthorizationServerProvider
    {
        private const string UserRoles = "userRoles";

        private readonly IUserService _userService;
        private readonly IHashingService _hashingService;

        public AuthServerProvider()
        {
            _userService = DependencyInjection.Instance.Container.GetInstance<IUserService>();
            _hashingService = DependencyInjection.Instance.Container.GetInstance<IHashingService>(); ;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult(true);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var user = GetUser(context.UserName, context.Password);
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return Task.FromResult(true);
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.DisplayName));
            identity.AddClaim(new Claim(UserRoles, user.RolesAsString));

            if (user.Roles != null)
            {
                foreach (var userRole in user.Roles)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, userRole.ToString()));
                }
            }

            context.Validated(identity);
            context.Request.Context.Authentication.SignIn(identity);
            return Task.FromResult(true);
        }

        public override Task TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            var claims = context.Identity.Claims;
            foreach (var claim in claims)
            {
                if (claim.Type == UserRoles)
                {
                    context.AdditionalResponseParameters.Add(UserRoles, claim.Value);
                }
                if (claim.Type == ClaimTypes.Name)
                {
                    context.AdditionalResponseParameters.Add("userName", claim.Value);
                }
            }
            
            return base.TokenEndpointResponse(context);
        }

        private User GetUser(string userName, string password)
        {
            var potentialUser = _userService.GetByUsername(userName);
            if (potentialUser != null && potentialUser.RowStatusId != RowStatusEnum.Banned)
            {
                if (potentialUser.HashedPassword == this._hashingService.HashPassword(password, potentialUser.PasswordSalt))
                {
                    _userService.LoggedIn(potentialUser.UserId);
                    return potentialUser;
                }
            }

            return null;
        }
    }
}