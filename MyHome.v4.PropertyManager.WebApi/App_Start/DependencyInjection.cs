﻿using System;
using MyHome.v4.Common.Caching;
using MyHome.v4.PropertyManager.IRepository;
using MyHome.v4.PropertyManager.IRepository.Activity;
using MyHome.v4.PropertyManager.IRepository.Advice;
using MyHome.v4.PropertyManager.IRepository.Auction;
using MyHome.v4.PropertyManager.IRepository.Calendar;
using MyHome.v4.PropertyManager.IRepository.Common;
using MyHome.v4.PropertyManager.IRepository.Contacts;
using MyHome.v4.PropertyManager.IRepository.Document;
using MyHome.v4.PropertyManager.IRepository.FeaturedProduct;
using MyHome.v4.PropertyManager.IRepository.Groups;
using MyHome.v4.PropertyManager.IRepository.JobQueue;
using MyHome.v4.PropertyManager.IRepository.OpenViewing;
using MyHome.v4.PropertyManager.IRepository.Orders;
using MyHome.v4.PropertyManager.IRepository.Product;
using MyHome.v4.PropertyManager.IRepository.Property;
using MyHome.v4.PropertyManager.IRepository.SavedSearch;
using MyHome.v4.PropertyManager.IRepository.Users;
using MyHome.v4.PropertyManager.IService.Activity;
using MyHome.v4.PropertyManager.IService.Advice;
using MyHome.v4.PropertyManager.IService.Auction;
using MyHome.v4.PropertyManager.IService.Calendar;
using MyHome.v4.PropertyManager.IService.Contacts;
using MyHome.v4.PropertyManager.IService.Document;
using MyHome.v4.PropertyManager.IService.FeaturedProduct;
using MyHome.v4.PropertyManager.IService.Groups;
using MyHome.v4.PropertyManager.IService.IO;
using MyHome.v4.PropertyManager.IService.Objects;
using MyHome.v4.PropertyManager.IService.OpenViewing;
using MyHome.v4.PropertyManager.IService.Property;
using MyHome.v4.PropertyManager.IService.SavedSearch;
using MyHome.v4.PropertyManager.IService.SessionProvider;
using MyHome.v4.PropertyManager.IService.Url;
using MyHome.v4.PropertyManager.IService.Users;
using MyHome.v4.PropertyManager.Repository;
using MyHome.v4.PropertyManager.Repository.Activity;
using MyHome.v4.PropertyManager.Repository.Advice;
using MyHome.v4.PropertyManager.Repository.Auction;
using MyHome.v4.PropertyManager.Repository.Calendar;
using MyHome.v4.PropertyManager.Repository.Common;
using MyHome.v4.PropertyManager.Repository.Contacts;
using MyHome.v4.PropertyManager.Repository.Document;
using MyHome.v4.PropertyManager.Repository.FeaturedProduct;
using MyHome.v4.PropertyManager.Repository.Groups;
using MyHome.v4.PropertyManager.Repository.JobQueue;
using MyHome.v4.PropertyManager.Repository.OpenViewing;
using MyHome.v4.PropertyManager.Repository.Orders;
using MyHome.v4.PropertyManager.Repository.Product;
using MyHome.v4.PropertyManager.Repository.Property;
using MyHome.v4.PropertyManager.Repository.SavedSearch;
using MyHome.v4.PropertyManager.Repository.Users;
using MyHome.v4.PropertyManager.Service.Activity;
using MyHome.v4.PropertyManager.Service.Advice;
using MyHome.v4.PropertyManager.Service.Auction;
using MyHome.v4.PropertyManager.Service.Azure;
using MyHome.v4.PropertyManager.Service.Calendar;
using MyHome.v4.PropertyManager.Service.Contacts;
using MyHome.v4.PropertyManager.Service.Document;
using MyHome.v4.PropertyManager.Service.FeaturedProduct;
using MyHome.v4.PropertyManager.Service.Groups;
using MyHome.v4.PropertyManager.Service.IO;
using MyHome.v4.PropertyManager.Service.Objects;
using MyHome.v4.PropertyManager.Service.OpenViewing;
using MyHome.v4.PropertyManager.Service.Property;
using MyHome.v4.PropertyManager.Service.SavedSearch;
using MyHome.v4.PropertyManager.Service.SessionProvider;
using MyHome.v4.PropertyManager.Service.Url;
using MyHome.v4.PropertyManager.Service.User;
using MyHome.v4.PropertyManager.Web.Helpers;
using MyHome.v4.ServiceClient.Locality;
using MyHome.v4.ServiceClient.PropertyType;
using SimpleInjector;
using SimpleInjector.Integration.Web;

namespace MyHome.v4.PropertyManager.WebUI
{
    public class DependencyInjection
    {
        private static volatile DependencyInjection _instance;
        private static readonly object SyncRoot = new Object();

        private DependencyInjection()
        {
            Container = BuildContainer();
        }

        public Container Container { get; set; }

        public static DependencyInjection Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = new DependencyInjection();
                    }
                }

                return _instance;
            }
        }

        public Container BuildContainer()
        {
            var container = new Container();

            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            //register connection to database
            container.Register<IConnection, Connection>(Lifestyle.Singleton);

            //register data level
            container.Register<IUserRepository, UserRepository>(Lifestyle.Singleton);
            container.Register<IRoleRepository, RoleRepository>(Lifestyle.Singleton);
            container.Register<IRoleMembershipRepository, RoleMembershipRepository>(Lifestyle.Singleton);
            container.Register<IGroupRepository, GroupRepository>(Lifestyle.Singleton);
            container.Register<IPropertyRepository, PropertyRepository>(Lifestyle.Singleton);
            container.Register<IPropertyContentRepository, PropertyContentRepository>(Lifestyle.Singleton);
            container.Register<IPropertyOfferRepository, PropertyOfferRepository>(Lifestyle.Singleton);
            container.Register<IArticleRepository, ArticleRepository>(Lifestyle.Singleton);
            container.Register<ICategoryRepository, CategoryRepository>(Lifestyle.Singleton);
            container.Register<IArticleCategoryMappingRepository, ArticleCategoryMappingRepository>(Lifestyle.Singleton);
            container.Register<IFeaturedProductRepository, FeaturedProductRepository>(Lifestyle.Singleton);
            container.Register<IContactRepository, ContactRepository>(Lifestyle.Singleton);
            container.Register<IPropertyContactRepository, PropertyContactRepository>(Lifestyle.Singleton);
            container.Register<IIndexQueueRepository, IndexQueueRepository>(Lifestyle.Singleton);
            container.Register<IOrderRepository, OrderRepository>(Lifestyle.Singleton);
            container.Register<IOpenViewingRepository, OpenViewingRepository>(Lifestyle.Singleton);
            container.Register<IOpenViewingNoteRepository, OpenViewingNoteRepository>(Lifestyle.Singleton);
            container.Register<IOpenViewingContactMappingRepository, OpenViewingContactMappingRepository>(Lifestyle.Singleton);
            container.Register<IOpenViewingUserMappingRepository, OpenViewingUserMappingRepository>(Lifestyle.Singleton);
            container.Register<IActivityRepository, ActivityRepository>(Lifestyle.Singleton);
            container.Register<IActivityContactRepository, ActivityContactRepository>(Lifestyle.Singleton);
            container.Register<IDocumentRepository, DocumentRepository>(Lifestyle.Singleton);
            container.Register<IDocumentContentTypeRepository, DocumentContentTypeRepository>(Lifestyle.Singleton);
            container.Register<ISavedSearchRepository, SavedSearchRepository>(Lifestyle.Singleton);
            container.Register<IProductSubscriptionRepository, ProductSubscriptionRepository>(Lifestyle.Singleton);
            container.Register<IProductRepository, ProductRepository>(Lifestyle.Singleton);
            container.Register<ISaleTypeRepository, SaleTypeRepository>(Lifestyle.Singleton);
            container.Register<IAuctionRepository, AuctionRepository>(Lifestyle.Singleton);
            container.Register<IAuctionPropertyRepository, AuctionPropertyRepository>(Lifestyle.Singleton);
            container.Register<IJobQueueRepository, JobQueueRepository>(Lifestyle.Singleton);
            container.Register<ICalendarEventRepository, CalendarEventRepository>(Lifestyle.Singleton);
            container.Register<ITaskRespository, TaskRepository>(Lifestyle.Singleton);

            //register service level
            container.Register<ILocalityService, CachedLocalityService>(Lifestyle.Singleton);
            container.Register<IPropertyTypeService, CachedPropertyTypeService>(Lifestyle.Singleton);
            container.Register<IUserService, UserService>(Lifestyle.Singleton);
            container.Register<IRoleService, RoleService>(Lifestyle.Singleton);
            container.Register<IRoleMemberShipService, RoleMemberShipService>(Lifestyle.Singleton);
            container.Register<IGroupService, GroupService>(Lifestyle.Singleton);
            container.Register<IPropertyService, PropertyService>(Lifestyle.Singleton);
            container.Register<IArticleService, ArticleService>(Lifestyle.Singleton);
            container.Register<ICategoryService, CategoryService>(Lifestyle.Singleton);
            container.Register<IArticleCategoryMappingService, ArticleCategoryMappingService>(Lifestyle.Singleton);
            container.Register<IUrlService, UrlService>(Lifestyle.Singleton);
            container.Register<IObjectService, ObjectService>(Lifestyle.Singleton);
            container.Register<IContactService, ContactsService>(Lifestyle.Singleton);
            container.Register<IPropertyContactsService, PropertyContactService>(Lifestyle.Singleton);
            container.Register<IFeaturedProductService, FeaturedProductService>(Lifestyle.Singleton);
            container.Register<IOpenViewingService, OpenViewingService>(Lifestyle.Singleton);
            container.Register<IContactsSearchService, ContactsSearchService>(Lifestyle.Singleton);
            container.Register<IActivityService, ActivityService>(Lifestyle.Singleton);
            container.Register<IDocumentService, DocumentService>(Lifestyle.Singleton);
            container.Register<ISavedSearchService, SavedSearchService>(Lifestyle.Singleton);
            container.Register<IAuctionService, AuctionService>(Lifestyle.Singleton);
            container.Register<IPropertyMediaService, PropertyMediaService>();
            container.Register<ICalendarService, CalendarService>(Lifestyle.Singleton);
            container.Register<IHashingService, HashingService>(Lifestyle.Singleton);
            container.Register<IPropertyOfferService, PropertyOfferService>(Lifestyle.Singleton);

            //register redis cache provider and configuration
            container.Register<IMyHomeConfigurationManager>(() => new MyHomeConfigurationManager(ConfigSettings.AppConfig.Platform), Lifestyle.Singleton);
            container.Register<ICacheClient, MyHomeRedisCacheClient>(Lifestyle.Singleton);

            //register providers
            container.Register<ISessionProvider, SessionProvider>();
            container.Register<IFileSystemProvider, AzureFileSystemProvider>();
            container.Register<IImageUtils, ImageUtils>();

            //container.Register<IPropertyFormViewModelMapper, PropertyFormViewModelMapper>(Lifestyle.Transient);

            //Auth
            //container.Register<IUserStore<ApplicationUser, int>, UserStore>(Lifestyle.Scoped);
            //container.Register<IUserManager, UserManager>(Lifestyle.Scoped);
            //container.Register(() => container.IsVerifying()
            //    ? new OwinContext(new Dictionary<string, object>()).Authentication
            //    : HttpContext.Current.GetOwinContext().Authentication, Lifestyle.Scoped);

            return container;
        }

    }
}